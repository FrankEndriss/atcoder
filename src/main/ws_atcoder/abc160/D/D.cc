/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int INF = 1e9;

void solve() {
	cini(n);
	cini(x);
	cini(y);
	x--;
	y--;

	vvi adj(n);
	for (int i = 0; i < n - 1; i++) {
		adj[i].push_back(i + 1);
		adj[i + 1].push_back(i);
	}
	if (abs(x - y) > 1) {
		adj[x].push_back(y);
		adj[y].push_back(x);
	}

	vi dist(n);

	vi f(n);
	for (int i = 0; i < n; i++) {
		dist.assign(n, INF);
		dist[i] = 0;
		queue<int> q;
		q.push(i);

		while (q.size()) {
			int node = q.front();
			q.pop();
			for (int chl : adj[node]) {
				if (dist[node] + 1 < dist[chl]) {
					dist[chl] = dist[node] + 1;
					q.push(chl);
				}
			}
		}

		for (int j = i + 1; j < n; j++)
			f[dist[j]]++;
	}

	for (int i = 1; i <= n - 1; i++) {
		cout << f[i] << endl;
	}

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

