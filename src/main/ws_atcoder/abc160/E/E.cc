/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(cnt0);	// red apples
	cini(cnt1);	// green apples

	cini(a);
	cini(b);
	cini(c);

	ll sum = 0;
	for (int i = 0; i < cnt0; i++) {
		cini(aux);
		sum += aux;
	}
	cinai(w0, a - cnt0);

	for (int i = 0; i < cnt1; i++) {
		cini(aux);
		sum += aux;
	}
	cinai(w1, b - cnt1);

	cinai(w2, c);

	/* Now we can choose to eat
	 * c apples from w2, or
	 * c-1 apples from w2 + 1 apple from w0/w1, or
	 * c-2 apples from w2 + 2 apple from w0/w1, or
	 * ...
	 * Apples from w0/w1 go like
	 */

	vll prew0(w0.size() + 1);
	for (int i = 1; i <= w0.size(); i++)
		prew0[i] = prew0[i - 1] + w0[i - 1];

	vll prew1(w1.size() + 1);
	for (int i = 1; i <= w1.size(); i++)
		prew1[i] = prew1[i - 1] + w1[i - 1];

	vll dpab(w0.size() + w1.size() + 1);	// dpab[i]=max worth of eating i apples from a and b

	dpab[0] = 0;
	dpab[1] = max(prew0[1], prew1[1]);
	//dpab[2] = max(prew0[2], max(prew1[2], prew0[1] + prew1[1]));

	for (int i = 1; i < dpab.size(); i++) {
		ll ma = 0;
		for (int j = 0; j <= i && j <= w0.size(); j++) {	// to slow, n^2
			// j== cnt red; i-j==cnt green
			if (i - j <= w1.size()) {
				ma = max(ma, prew0[j] + prew1[i - j]);
			}
		}
		dpab[i] = ma;
	}

	ll ans = sum;
	ll prew2 = 0;
	for (int i = 0; i <= c; i++) {	// eat i of colorless apples, paint others optimally
		if (c - i < dpab.size()) {
			log(ans);
			ans = max(ans, sum + dpab[c - i] + prew2);
		}

		prew2 += w2[i];
	}

	cout << ans << endl;

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

