/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cinll(n);
	cinll(m);
	cinai(a, n);
	sort(a.begin(), a.end(), greater<int>());
	vll pre(n);
	pre[0] = a[0];
	for (int i = 1; i < n; i++)
		pre[i] = pre[i - 1] + a[i];

	/* j == jth diagonal */
	ll ans = 0;
	for (int i = 0; (i < n + n) && m; i++) {
		int idx = i;
		if (idx >= n)
			idx = n - (idx - n + 1);
		//cout << "m=" << m << " i=" << i << " idx=" << idx << endl;

		if (m >= idx + 1) {
			ll dval = pre[idx] * 2;
			//cout << "dval=" << dval << endl;
			ans += dval;
			m -= (idx + 1);
		} else { // use only the biggest m values
			vi vals;
			for (int j = 0; j <= idx; j++) {
				vals.push_back(a[j] + a[idx - j]);
			}
			sort(vals.begin(), vals.end(), greater<int>());
			for (int j = 0; j < m; j++)
				ans += vals[j];
			m = 0;
		}
	}
	cout << ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

