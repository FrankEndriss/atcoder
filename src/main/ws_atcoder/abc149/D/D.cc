/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n);
	cini(k);
	vi rsp(3);
	cin >> rsp[0] >> rsp[1] >> rsp[2];
	vi vt(n);
	cins(t);
	for (char c : t) {
		if (c == 'r')
			vt.push_back(0);
		else if (c == 's')
			vt.push_back(1);
		else if (c == 'p')
			vt.push_back(2);
		else
			assert(false);
	}

	vvi dp(n, vi(3));	// dp[i][j] max score after round i when having played j in that round

	vi dpP(n);	// dpP[i]=point got in round i
	vi dpH(n);	// dpH[i]=hand we got point throug

#define R 0
#define S 1
#define P 2

	for (int i = 0; i < n; i++) {
		if (t[i] == 'r')
			dpH[i] = P;
		else if (t[i] == 's')
			dpH[i] = R;
		else if (t[i] == 'p')
			dpH[i] = S;
		else
			assert(false);
		dpP[i] = rsp[dpH[i]];

		if (i >= k && dpH[i - k] == dpH[i]) {
			dpP[i] = 0;
			dpH[i] = -1;
		}
	}
	ll ans = 0;
	for (int i = 0; i < n; i++)
		ans += dpP[i];
	cout << ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

