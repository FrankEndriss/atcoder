/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n);
	vs ans(n, string(n, '.'));
	function<void(int, int)> p3 = [&](int i, int j) {
		ans[i][j] = 'a';
		ans[i][j + 1] = 'a';
		ans[i + 1][j + 2] = 'b';
		ans[i + 2][j + 2] = 'b';
	};
	function<void(int, int)> p4 = [&](int i, int j) {
		ans[i][j] = 'a';
		ans[i][j + 1] = 'a';
		ans[i + 1][j] = 'b';
		ans[i + 1][j + 1] = 'b';
		ans[i + 2][j + 2] = 'a';
		ans[i + 2][j + 3] = 'a';
		ans[i + 3][j + 2] = 'b';
		ans[i + 3][j + 3] = 'b';

		ans[i + 2][j] = 'c';
		ans[i + 3][j] = 'c';
		ans[i + 2][j + 1] = 'd';
		ans[i + 3][j + 1] = 'd';
		ans[i + 0][j + 2] = 'c';
		ans[i + 1][j + 2] = 'c';
		ans[i + 0][j + 3] = 'd';
		ans[i + 1][j + 3] = 'd';

	};

	bool ok = false;
	if (n % 3 == 0) {
		ok = true;
		for (int i = 0; i < n; i += 3)
			for (int j = 0; j < n; j += 3)
				p3(i, j);
	} else if (n % 4 == 0) {
		ok = true;
		for (int i = 0; i < n; i += 4)
			for (int j = 0; j < n; j += 4)
				p4(i, j);
	}

	if (ok)
		for (string s : ans)
			cout << s << endl;
	else
		cout << -1 << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

