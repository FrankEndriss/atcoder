/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n);
	cini(m);
	cini(v);
	cini(p);
	cinai(a, n);
	sort(a.begin(), a.end(), greater<int>());

	vi evs(n);		// extra votes space
	vll evssum(n);
	for (int i = n - 1; i >= p - 1; i--) {
		evs[i] = a[p - 1] - a[i];
		if (i == n - 1)
			evssum[i] = evs[i];
		else
			evssum[i] = evssum[i + 1] + evs[i];
	}

	int ans = p;

	for (int i = p; i < n; i++) {
		int diff = a[p - 1] - a[i];
		if (diff > m) // cannot be in top pth at all
			break;

		/* Number of better concurrent probs getting a vote, m times.
		 * num votes - ( number of lessw probs + number of top probs + oneSelfVote) */
		int extraVoted = max(0, v - ((p - 1) + (n - i - 1) + 1));

		int myScore = a[i] + m;		//  score prob a[i] will get
		int points = extraVoted * m;

		for (int j = i - 1; j >= p - 1 && points > 0; j--) {
			assert(myScore >= a[j]);
			points -= (min(myScore - a[j], m));
		}
		if (points <= 0)
			ans++;
	}

	cout << ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

