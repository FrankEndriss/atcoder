/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int solve1(string a, string b, string c) {

	int ans = a.size() + b.size() + c.size();

	for (int i = 0; i <= a.size(); i++) {
		/* check if a and b match here. Note at i=a.size() there is allways a match. */
		bool match = true;
		for (int j = 0; match && i + j < a.size() && j < b.size(); j++) {
			if (a[i] != '?' && b[j] != '?' && a[i] != b[j])
				match = false;
		}

		if (match) {
			string ab = a;
			for (int j = 0; j < b.size(); j++) {
				if (i + j >= ab.size())
					ab += b[j];
				else if (b[j] != '?')
					ab[i + j] = b[j];
			}
			log(a, b, i, ab);

			for (int i1 = 0; i1 <= ab.size(); i1++) {
				/* check if ab and c match at i1 */
				match = true;
				for (int j = 0; match && i1 + j < ab.size() && j < c.size(); j++) {
					if (ab[i1] != '?' && c[j] != '?' && ab[i1] != c[j])
						match = false;
				}
				if (match) {
					ans = min(ans, max((int) ab.size(), (int) (i1 + c.size())));
				}
			}
		}
	}
	return ans;
}

void solve() {
	cins(a);
	cins(b);
	cins(c);

	int ans = min(solve1(a, b, c), solve1(a, c, b));
	cout << ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

