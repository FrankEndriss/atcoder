/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* first we need to know all subseqs (begin and end) with
 * sum(l,r)==s.
 * Then we cal calc number of pairs (idxL,idxR) including these.
 * for a given seq of x[L]..x[R] there are L posibilities for the left bound, and n-R for the right,
 * hence L*(n-R) pairs.
 * How to find all subseqs (begin and end) with sum==s?
 * Two pointer.
 * bahh, that does not work. With twopointer we find only subarrays, not subsequences.
 * need to analyze other codes...
 */
void solve() {
	cini(n);
	cini(s);
	cinai(a, n);

	set<pii> seqs;
	int L = 0;
	int R = 0;
	int sum = 0;
	while (L < n) {
		while (R < n && sum < s) {
			sum += a[R];
			R++;
		}

		if (sum == s) {
			seqs.insert( { L, R });
		}

		while (L < n && sum > s) {
			sum -= a[L];
			L++;
		}

		if (sum == s) {
			seqs.insert( { L, R });
			sum -= a[L];
			L++;
		}

		if (sum < s && R >= n)
			break;
	}

	int ans = 0;
	for (pii p : seqs) {
		//cout << "L=" << p.first << " R=" << p.second << endl;
		ans += (p.first + 1) * (n - p.second);
		ans %= 998244353;
	}
	cout << ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

