/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* We find optimal cuts for recht including 0,0.
 * Let r,c be right,lower corner of that cuts.
 *
 * Then, for all these r,c, we find optimal cuts
 * for rects with left upper= (r+1,0), and (0, c+1)
 * and so on... until all cutted.
 *
 * It is simpler.
 * Since H is bound to max 10, we can try all possible
 * cuts, and greedy find the W cuts for every one.
 */
void solve() {
	cini(H);
	cini(W);
	cini(K);log(H, W, K);

	vector<vector<char>> a(H, vector<char>(W));
	for (int i = 0; i < H; i++)
		for (int j = 0; j < W; j++) {
			cin >> a[i][j];
			a[i][j] -= '0';
		}

	log("did read a");

	int ans = 1e9;
	for (int mask = 0; mask < (1 << (H - 1)); mask++) {
		log(mask);

		int lans = 0;
		vvi blocks(1);
		for (int idx = 0; idx < H; idx++) { // if ith bit is set cut after row i
			blocks.back().push_back(idx);
			if (mask & (1 << idx)) {
				blocks.push_back(vi());
				lans++;
			}
		} log("created blocks");

		vi cnt(blocks.size());
		for (int i = 0; i < W; i++) {
			vi cntcol(blocks.size());
			for (int j = 0; j < blocks.size(); j++) {
				for (int k : blocks[j])
					cntcol[j] += a[k][i];
			}
			for (int j = 0; j < blocks.size(); j++) {
				if (cntcol[j] > K) {
					lans = 1e9;
					break;
				}

				if (cntcol[j] + cnt[j] > K) {
					lans++;
					cnt = cntcol;
					break;
				} else
					cnt[j] += cntcol[j];
			}
		}
		ans = min(ans, lans);
	}
	cout << ans << endl;

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

