/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* number of zeros in 10^^mul */
ll zeros(int mul) {
	assert(mul >= 0 && mul < 20);

	if (mul == 0)
		return 0;
	else if (mul == 1)
		return 1;
	else {
		ll ret = mul + 9 * zeros(mul - 1);
//		cout << "mul=" << mul << " ret=" << ret << endl;
		return ret;
	}
}

void solve() {
	cinll(n);
	if (n % 2 == 0) {	// 2*4*6*8*10...
		int mul = 1;
		ll num = 10;
		while (num < n) {
			num *= 10;
			mul++;
		}

		if (num > n) {
			num /= 10;
			mul--;
		}

		ll ans = 0;
		while (n) {
			ans += zeros(mul) * (n / num);
			n -= (num * (n / num));
			num /= 10;
			mul--;
			//cout << "n=" << n << " ans=" << ans << endl;
		}

		cout << ans << endl;
	} else { // 3 * 5 * 7...
		cout << 0 << endl;
	}
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

