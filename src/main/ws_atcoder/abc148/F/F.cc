/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n);
	cini(pray);
	pray--;
	cini(hunter);
	hunter--;
	vvi tree(n);
	for (int i = 0; i < n - 1; i++) {
		cini(u);
		u--;
		cini(v);
		v--;
		tree[u].push_back(v);
		tree[v].push_back(u);
	}
	/* find subtree where pray is, and pathlen to pray.
	 * count deepest path in that subtree.
	 * Dont ignore parity.
	 **/

	function<int(int, int)> dfs1 = [&](int node, int parent) {
		if (node == pray)
			return 1;

		for (int chl : tree[node]) {
			if (chl != parent) {
				int level = dfs1(chl, node);
				if (level > 0)
					return level + 1;
			}
		}
		return -1;
	};

	function<int(int, int)> dfs2 = [&](int node, int parent) {
		int ans = 0;
		for (int chl : tree[node])
			if (chl != parent)
				ans = max(ans, dfs2(chl, node));

		return ans + 1;
	};

	int ans = -1;
	for (int chl : tree[hunter]) {
		int level = dfs1(chl, hunter);
		if (level > 0) {
			ans = dfs2(chl, hunter);
			ans--;
			break;
		}
	}

	cout << ans << endl;

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

