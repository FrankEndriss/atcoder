/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

template<typename T>
void floyd_warshal(vector<vector<T>> &d, int n) {
	for (int k = 0; k < n; ++k) {
		for (int i = 0; i < n; ++i) {
			for (int j = 0; j < n; ++j) {
				d[i][j] = min(d[i][j], d[i][k] + d[k][j]);
			}
		}
	}
}

#define idx(p) (p.first * m + p.second)

const int INF = 1e9;
void solve() {
	cini(n);
	cini(m);
	vs s(n);
	for (int i = 0; i < n; i++)
		cin >> s[i];

	vvi dm(n * m, vi(n * m, INF));

	const vector<pii> offs = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			if (s[i][j] == '#')
				continue;
			pii p = { i, j };
			dm[idx(p)][idx(p)] = 0;

			for (pii off : offs) {
				pii np = { i + off.first, j + off.second };
				if (np.first < 0 || np.first >= n || np.second < 0 || np.second >= m)
					continue;
				if (s[np.first][np.second] == '#')
					continue;

				dm[idx(p)][idx(np)] = 1;
			}
		}
	}

	floyd_warshal(dm, n * m);

	int ans = 0;
	for (int i = 0; i < n * m; i++) {
		for (int j = 0; j < n * m; j++) {
			if (dm[i][j] < INF)
				ans = max(ans, dm[i][j]);
		}
	}
	cout << ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

