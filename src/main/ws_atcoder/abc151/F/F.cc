/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n);
	vector<pii> p;
	for (int i = 0; i < n; i++) {
		cin >> p[i].first >> p[i].second;
	};

	/* point/dist with min(maxDist) to the three points.
	 * in a tri with an angel >=90degree it is the center
	 * between the two farthest away points.
	 * AB=farthest away points.
	 * For less than 90 it is somewhere on the vertical to
	 * AB throug the midpoint of AB.
	 * */
	function<double(int, int, int)> r = [&](int i1, int i2, int i3) {
		double ans = 1e9;
		;
		double px = (p[i1].first + p[i2].first + p[i3].first) / 3.0;
		double py = (p[i1].second + p[i2].second + p[i3].second) / 3.0;

		return ans;
	};

	double ans = 1e9;
	for (int i1 = 0; i1 < n; i1++) {
		for (int i2 = i1 + 1; i2 < n; i2++) {
			for (int i3 = i2 + 1; i3 < n; i3++) {
				ans = min(ans, r(i1, i2, i3));
			}
		}
	}
}
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
solve();
}

