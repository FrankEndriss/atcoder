/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MOD = 1e9 + 7;

int mul(const int v1, const int v2, int mod = MOD) {
	return (int) ((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod = MOD) {
	int res = 1;
	while (p != 0) {
		if (p & 1)
			res = mul(res, a, mod);
		p >>= 1;
		a = mul(a, a, mod);
	}
	return res;
}

int pl(int v1, int v2, int mod = MOD) {
	int res = v1 + v2;

	if (res < 0)
		res += mod;

	else if (res >= mod)
		res -= mod;

	return res;
}

int inv(const int x, const int mod = MOD) {
	return toPower(x, mod - 2);
}

int fraction(const int zaehler, const int nenner) {
	return mul(zaehler, inv(nenner));
}

const int N = 100005;
vector<int> fac(N);

void initFac(int mod = MOD) {
	fac[1] = 1;
	for (int i = 2; i < N; i++) {
		fac[i] = mul(fac[i - 1], i, mod);
	}
}

/** see https://cp-algorithms.com/combinatorics/binomial-coefficients.html */
int nCr(int n, int k) {
	if (k == 0 || k == n)
		return 1;
	if (k == 1 || k == n - 1)
		return n;
	if (k > n / 2)
		return nCr(n, n - k);

	return mul(mul(fac[n], inv(fac[k])), inv(fac[n - k]));
}

void solve() {
	cini(n);
	cini(k);
	cinai(a, n);
	sort(a.begin(), a.end());

	if (k == 1) {
		cout << "0" << endl;
		return;
	} else if (k == n) {
		cout << a.back() - a[0] << endl;
		return;
	}

	/* There are nCr(n,k) different sets of size k.
	 * nCr(n-1,k-1) ways with a[0] in it.
	 *  nCr(k,k) times a[k-1] is biggest
	 *  nCr(k+1,k) times a[k] is biggest
	 *  nCr(k+2,k) times a[k+1] is biggest
	 *  ...
	 *
	 * nCr(n-2,k-1) ways with a[0] not in it but a[1] in it.
	 * nCr(n-3,k-1) ways with a[0] and a[1] not in it but a[2] in it.
	 * ...
	 */

	int ans = 0;

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	initFac();
	solve();
}

