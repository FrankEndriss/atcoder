/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

template<typename T>
class fenwick {
public:
	vector<T> fenw;
	int n;

	fenwick(int _n) :
			n(_n) {
		fenw.resize(n);
	}

	void modify(int x, T v) {
		while (x < n) {
			fenw[x] += v;
			x |= (x + 1);
		}
	}

	/* get sum of range (0,x), including x */
	T get(int x) {
		T v { };
		while (x >= 0) {
			v += fenw[x];
			x = (x & (x + 1)) - 1;
		}
		return v;
	}
};

void solve() {
	cini(n);
	cini(d);
	cinll(a);
	vector<pii> xh(n);
	for (int i = 0; i < n; i++) {
		cin >> xh[i].first >> xh[i].second;
	}
	sort(xh.begin(), xh.end());

	fenwick<ll> fen(n + 1);	// fen.get(x) == demage at x so far

	/* remove one monster after the other
	 * allways bombing at xh[i].first+d+1 until current moster is dead.
	 */
	ll ans = 0;
	int r = 0;
	for (int i = 0; i < n; i++) {
		ll demage = fen.get(i);
		//cout << "i=" << i << " demage=" << demage << endl;
		if (xh[i].second > demage) {	// monster not dead so far
			int bombs = (xh[i].second - demage + a - 1) / a;
			ans += bombs;
			//cout << "bombs=" << bombs << endl;
			fen.modify(i, bombs * a);
			// find last affected index
			r = max(r, i);
			while (r < n && xh[i].first + 2 * d >= xh[r].first)
				r++;

			//cout << "i=" << i << " r=" << r << endl;

			fen.modify(r, -bombs * a);
		}
	}
	cout << ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

