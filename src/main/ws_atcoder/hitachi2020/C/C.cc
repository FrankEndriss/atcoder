/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* Greedy...
 * Paint tree in two alternating colors.
 * Use color with less nodes, assign all i%3==1 values
 * to the nodes, andi%3==0.
 * Assign remaining numbers to other color nodes.
 *
 */
void solve() {
	cini(n);
	vvi adj(n);
	for (int i = 0; i < n - 1; i++) {
		cini(u);
		cini(v);
		u--;
		v--;
		adj[u].push_back(v);
		adj[v].push_back(u);
	}

	if (n == 2) {
		cout << "1 2" << endl;
		return;
	}

	vb col(n);
	vi colcnt(2);
	colcnt[false]++;

	function<void(int, int)> dfs = [&](int node, int parent) {
		if (parent >= 0) {
			col[node] = !col[parent];
			colcnt[col[node]]++;
		}

		for (int c : adj[node])
			if (c != parent)
				dfs(c, node);
	};
	dfs(0, -1);

	bool val = true;
	if (colcnt[false] < colcnt[true])
		val = false;

	vi ans(n);
	vi idx = { 3, 1, 2 };
	if (colcnt[val] <= (n / 3)) {
		for (int i = 0; i < n; i++) {
			if (col[i] == val) {
				ans[i] = idx[0];
				idx[0] += 3;
			}
		}
	} else {
		for (int i = 0; i < n; i++) {
			if (col[i] == val) {
				if (idx[1] <= n) {
					ans[i] = idx[1];
					idx[1] += 3;
				} else {
					ans[i] = idx[0];
					idx[0] += 3;
				}
			}
		}
	}

	for (int i = 0; i < n; i++) {
		if (ans[i] == 0) {
			for (int j = 0; j < 3; j++) {
				if (idx[j] <= n) {
					ans[i] = idx[j];
					idx[j] += 3;
					break;
				}
			}
		}
	}

	for (int i = 0; i < n; i++)
		cout << ans[i] << " ";
	cout << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

