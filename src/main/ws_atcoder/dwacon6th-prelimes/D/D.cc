/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n);
	vi a(n);
	for (int i = 0; i < n; i++) {
		cin >> a[i];
	}

	vi ans;
	vb vis(n);
	int vidx = 0;

	function<bool(void)> possible = [&]() {
		if (vidx == n)
			return true;

		/*
		 if (ans.size() > 0)
		 cout << "possible, ans.size()=" << ans.size() << " ans.back()=" << ans.back() << endl;
		 else
		 cout << "possible, ans.size()=" << ans.size() << endl;
		 */

		if (ans.size() == 0) {
			for (int start = 1; start <= n; start++) {
				vis[start - 1] = true;
				if (start == 1)
					vidx = 1;
				else
					vidx = 0;

				ans.push_back(start);
				if (possible()) {
					copy(ans.begin(), ans.end(), ostream_iterator<int>(cout, " "));
					return true;
				} else {
					ans.pop_back();
					vis[start - 1] = false;
				}
			}
			cout << "-1" << endl;
			return false;
		} else {
			int prev = 0;
			int ovidx = vidx;
			for (int i = vidx; i < n; i++) {
				while (i < n && vis[i])
					i++;
				if (i == n)
					break;
				prev = i + 1;

				if (prev != a[ans.back() - 1]) {
					ans.push_back(prev);
					vis[prev - 1] = true;
					if (prev - 1 == vidx)
						vidx++;

					if (possible())
						return true;
					else {
						ans.pop_back();
						vis[prev] = false;
						vidx = ovidx;
					}
				}
			}
		}

		return false;
	};

	possible();
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

