/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MOD = 1e9 + 7;
int mul(const int v1, const int v2, int mod = MOD) {
	return (int) ((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod = MOD) {
	int res = 1;
	while (p != 0) {
		if (p & 1)
			res = mul(res, a, mod);
		p >>= 1;
		a = mul(a, a, mod);
	}
	return res;
}

int pl(int v1, int v2, int mod = MOD) {
	int res = v1 + v2;

	if (res < 0)
		res += mod;

	else if (res >= mod)
		res -= mod;

	return res;
}

int inv(const int x, const int mod = MOD) {
	return toPower(x, mod - 2);
}

const int N = 1000000;
vector<int> fac(N);

void initFac(int mod = MOD) {
	fac[1] = 1;
	for (int i = 2; i < N; i++) {
		fac[i] = mul(fac[i - 1], i, mod);
	}
}

int nCr(int n, int k) {
	if (k > n - k)
		k = n - k;

	int ans = 1;
	for (int i = 0; i < k; i++) {
		ans = mul(ans, n - i);
		ans = mul(ans, inv(i + 1));
	}
	return ans;
}

void solve() {
	cinll(n);
	cinll(a);
	cinll(b);

	/* Property: sum(nCr(n,k), 0, n) == 2^n
	 * ans== 2^n - nCr(n,a) - nCr(n,b)
	 * How to calc nCr(n,a) for n up to 1e9?
	 * Note that we can divide by multiplication with inverse.
	 */
	int ans = toPower(2, n);
	ans = pl(ans, -1);
	ans = pl(ans, -nCr(n, a));
	ans = pl(ans, -nCr(n, b));

	cout << ans << endl;
}

int main() {
	cin.tie(nullptr);
	initFac();
	std::ios::sync_with_stdio(false);
	solve();
}

