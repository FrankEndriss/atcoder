/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We got a set of int [L,R]
 * But we need to remove all substrings of other elements.
 * Consider R has x digits, then obviously we can use
 * any x digit number.
 * Also we can use x-1 digit numbers if there is no such
 * x digit number.
 * So ans is max of that both.
 *
 * Except L comes into play.
 * Cases:
 * 1. all (x-1)-digit numbers are in range, ans=max x-digit numbers or x-1 digit numbers.
 *   L*10<=x
 *   -> ans=max(R-x, x*9/10)
 * 2. all number from L to R conrtibute
 *   L>=x
 *   -> ans=R-L
 * 3. The x-digit numbers contribute, also the x-1 digit numbers, but at most x*9/10 at all.
 *   L*10>=x
 *   -> ans=...
 *  Consider
 *  456 1221
 *  so we go 222 4-digit numbers 1000-1221,
 *  but also 3-digit numbers 456-999
 *
 * ...after half an hour not solving this prob it feels kind of boring... :/
 * Whats the edgecase here, or did I get the text wrong?
 *
 * example 3 18
 * Why is ans=10, not ans=9, where comes the 1 from?
 * example 30 180
 * Numbers are 100-180: 81
 * 81-99: 19, sum=100
 *
 *
 * ***
 * Try other algo.
 * Iterate x, foreach x find how much x-digit numbers can we use,
 * either because xxx>=L && xxx<x
 * or because x+xxx<R
 * Note that here xxx can start with leading 0.
 *
 * Same, should work, but 41/42 WA :/
 * ******
 * Actually we can allways choose some y biggest numbers.
 * So, which is the biggest number we cannot choose, because
 * it exists as a postfix of higher used number?
 *
 * Fucking binary search it.
 */
void solve() {
    cini(L);
    cini(R);

    int l=L-1;    /* numbers we cannot choose */
    int r=R+1;    /* numbers we can choose */
    while(l+1<r) {
        int mid=(l+r)/2;
        int x=1;
        while(x<mid)
            x*=10;
        if(x+mid>R)
            r=mid;
        else
            l=mid;
    }
    //cerr<<"L="<<L<<" R="<<R<<" l="<<l<<" r="<<r<<endl;
    int ans=R-r+1;
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
