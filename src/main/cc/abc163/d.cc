/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* we choose at least k numbers from (0,N), how
 * many sums possible ?
 * k<=n
 * nCr(n,k) + nCr(n,k+1) + ... nCr(n,n) sets,
 * but _not_ every set has distinct sum.
 * Sets of diff size have diff sums.
 * So, how much sets in one size have same sum?
 *
 * Example, k=2, n=4
 * 1+2=3
 * 1+3=4
 * 1+4=5
 * 2+3=5
 * 2+4=6
 * 3+4=7
 * We can build all sums between smallest and biggest.
 * Smallest is 1+2+...+k
 * Biggest is n+n-1+n-2+n-(k-1)
 * ...
 */
void solve() {
    cini(n);
    cini(k);

    const int MOD=1e9+7;
    int ans=0;
    for(int i=k; i<=n+1; i++) { /* i=number of choosen numbers */
        int mi=((i-1)*i)/2; /* min sum of choosen numbers: 0+1+2.. */
        int ma=n*i - ((i-1)*i)/2; /* max sum of choosen numbers: n-0+n-1+n-2... */
        ans+=ma-mi+1;
        ans%=MOD;
    }
    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

