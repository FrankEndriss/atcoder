/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* We want to move most A[i] children
 * the longest distance.
 *
 * Max flow network, Edmonds-Karp?
 * source to all pos, INF capa
 * pos to all other pos, conrib is capa
 * pos to sink...no.
 */
void solve() {
    cini(n);
    vector<pii> a(n);
    for(int i=0; i<n; i++) {
        cin>>a[i].first;
        a[i].second=i;
    }
    int l=0;
    int r=n-1;

    sort(a.begin(), a.end(), greater<pii>());
    int ans=0;
    for(int i=0; i<n; i++) {
/* if same a[i].first is there more than once we need to 
 * reorder all those elements by contribution.*/
        if(i+1<n && a[i]==a[i+1]) {
            int j=i+1;
            while(j+1<n && a[j]==a[j+1])
                j++;
            sort(a.begin()+i, a.begin()+j+1, [&](pii a1, pii a2) {
                int c1=0;
                if(abs(a1.second-l)>abs(a1.second-r)) {
                    c1=a1.first*abs(a1.second-l);
                } else {
                    c1=a1.first*abs(a1.second-r);
                }
                int c2=0;
                if(abs(a2.second-l)>abs(a2.second-r)) {
                    c2=a2.first*abs(a2.second-l);
                } else {
                    c2=a2.first*abs(a2.second-r);
                }
                return c2<c1;
            });
        }

        if(abs(a[i].second-l)>abs(a[i].second-r)) {
            ans+=a[i].first*abs(a[i].second-l);
            l++;
        } else {
            ans+=a[i].first*abs(a[i].second-r);
            r--;
        }
    }
    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

