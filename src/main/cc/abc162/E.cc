/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

const int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while (res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

int sumdivs(int n) {
    int ans=1;
    for(int i=2; i*i<=n; i++) {
        if(n%i==0) {
            ans+=i;
            if(i*i!=n)
                ans+=n/i;
        }
    }
    return ans;
}

/* seqs of len n
 * with element choosen from 1..k
 * so k^n seqs
 *
 * find seqs where gcd!=1
 * gcd==2 ->
 * 2, x1*2, x2*2,...: Let y2 number of multiples of 2 in 1..k
 * ans+=(y2^n)*2
 *
 * How to subtract y6 from/and y3,y2 ?
 * The y6 seqs are included in the y1 seqs, the y2 seqs and in the y3 seqs.
 * yX seqs are included in all seqs of divisors of X.
 * ans+=(y6^n)*(6-sumDivs)
 */
void solve() {
    cini(n);
    cini(k);

    int ans=toPower(k, n);
    for(int i=2; i<=k; i++) {
        int di=sumdivs(i);
        int cnt=k/i;    /* number of multiples of i in 1..k */
        int seqs=toPower(cnt,n);    /* num seqs with gcd(..)==i */
        ans=pl(ans, mul(seqs, pl(i, -di)));
    }
    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

