/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

void solve() {
    cini(n);
    cins(s);

    vector<char> cth={ 'R', 'G', 'B' };
    vector<int> rgb(256);
    rgb['R']=0;
    rgb['G']=1;
    rgb['B']=2;

    vvi pre(3, vi(n+1));  /* pre[i][j] == number of R=0 G=1 B=2 in postfix starting at j */

    for(int i=n-1; i>=0; i--) {
        for(int j=0; j<3; j++) {
            pre[j][i]=pre[j][i+1];
        }
        pre[rgb[s[i]]][i]++;
    }


    int ans=0;
    for(int i=0; i<n; i++) {
        for(int j=i+1; j<n; j++) {
            if(s[i]==s[j])
                continue;
            int th=0;
            if((s[i]=='R' && s[j]=='G') || (s[i]=='G' && s[j]=='R'))
                th=2;
            else if((s[i]=='R' && s[j]=='B') || (s[i]=='B' && s[j]=='R'))
                th=1;
            else
                th=0;

            ans+=pre[th][j+1];
            if(j+(j-i)<n && s[j+(j-i)]==cth[th])
                ans--;
        }
    }
    cout<<ans<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

