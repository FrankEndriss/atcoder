
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int P2=1000;

/* flip 1/0 at pos */
void flip(int pos) {
    cout<<"< "<<pos<<" "<<P2<<" "<<pos<<endl;
    // if pos<1 then pos=1 else pos=0
}

/*
 * Create powers of 2 somewhere.
 *
 * Check bits from highest to lowest of a[0]
 * if set
 * double a[1] the number of bit times to a[x]
 * add a[x] to a[2]
 *
 * How to check bits?
 * ->if number is smaller bit is not set
 *
 * How to create a 1 somewhere? A and B could be 0. In that case we cannot create a 1 anywhere.
 * However, if we assume one of A and B is zero, result must be zero too.
 * So assume both are non zero.
 *
 * We would first need to separate all bits of a[0] into single registers.
 *
 */
void solve() {
    cout<<"< "<<P2<<" 0 "<<P2<<endl;   /* set a[P2]=1 by assume A and B are non zero. */
    for(int i=0; i<32; i++) 
        cout<<"+ "<<P2+i<<" "<<P2+i<<" "<<P2+i+1<<endl;     /* a[P2+1]=a[P2]*2 */

    const int R1=2000;  /* Register R1 */
    for(int bit=0; bit<31; bit++)  {
        cout<<"< 0 
        R1=
    }

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
