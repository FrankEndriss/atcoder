
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * We cannot make longer from shorter.
 * We cannot make one from another of same length;
 * So we can sort by length and find
 * for each i how many j exist
 *
 * *************
 * Sort string by reverse strng, then find length
 * of all consecutive pairs.
 * then for each string find number of subsequent strings
 * with common postfix of len of string.
 *
 * Find all with postfix of min len of stringlen-1.
 * Then use all where last char of string occours somewhere
 * later in the string.
 * We would need for all string int[26] of "last occurence of" to check.
 * But we cannot check each string, since that would run O(n^2)
 *
 * ... idk :/
 */
void solve() {
    cini(n);
    cinas(s,n);

    sort(all(s), [&](string &s1, string &s2) {
        return s1.size()<s2.size();
    });

    vi id(n);
    iota(all(id), 0);
    int prev=0;
    for(int i=0; i<n; i++) {
        const size_t sz = s[i].size();

        /* compare postfixes of size sz */
        auto cmp=[&](int i1, int i2) {
            for(size_t j=0; j<sz; j++) {
                char c1=s[i1][s[i1].size()-sz+j];
                char c2=s[i2][s[i1].size()-sz+j];
                if(c1!=c2)
                    return c1<c2;
            }
            return false;
        };

        if(s[i].size()>prev) {
            iota(all(id),0);
            sort(id.begin()+i, id.end(), cmp);
        }

        int cnt=upper_bound(s.begin()+i+1, s.end(), i, cmp)-
                lower_bound(s.begin()+i+1, s.end(), i, cmp);
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
