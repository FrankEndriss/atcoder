
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* simple fraction implementation based on long long */
struct fract {
    ll f, s;

    fract(ll counter, ll denominator) :
        f(counter), s(denominator) {
        norm();
    }

    bool operator<(const fract &rhs) {
        return f * rhs.s < s * rhs.f;
    }

    fract& operator+=(const fract &rhs) {
        ll g = __gcd(this->s, rhs.s);
        ll lcm = this->s * (rhs.s / g);
        this->f = this->f * (lcm / this->s) + rhs.f * (lcm / rhs.s);
        this->s = lcm;
        norm();
        return *this;
    }

    friend fract operator+(fract lhs, const fract &rhs) {
        lhs += rhs;
        return lhs;
    }

    friend fract operator-(fract lhs, const fract &rhs) {
        lhs += fract(-rhs.f, rhs.s);
        return lhs;
    }

    fract& operator*=(const fract &rhs) {
        this->f *= rhs.f;
        this->s *= rhs.s;
        norm();
        return *this;
    }

    friend fract operator*(fract lhs, const fract &rhs) {
        lhs *= rhs;
        return lhs;
    }

    friend fract operator/(fract lhs, const fract &rhs) {
        return lhs * fract(rhs.s, rhs.f);
    }

    void norm() {
        int sign = ((this->f < 0) + (this->s < 0)) % 2;
        sign = -sign;
        if (sign == 0)
            sign = 1;
        ll g = __gcd(abs(this->f), abs(this->s));
        if (g != 0) {
            this->f = (abs(this->f) * sign) / g;
            this->s = abs(this->s) / g;
        }
    }
};

/* parse double as big integers,
 * store as fract.
 * Store fraction part as fraction,
 * and whole number as whole number.
 *
 * Then check the pairs.
 * We can pair each number other number if
 * the sum of fraction parts multiplies to int.
 *
 * Then calc 
 * result = fract1*fract2/1e9 + wh1*fract2 + wh2*fract1
 * if(result%1e9==0)
 *      isInt==true;
 *
 * Basically we need to find pairs of numbers
 * where gcd() is bg 1e9
 */
void solve() {
    cini(n);
    string line;
    getline(cin,line);


    vector<fract> fr;

    for(int i=0; i<n; i++) {
        getline(cin,line);
        auto pidx=line.find('.');
        if(pidx==string::npos) {
            int num;
            istringstream iss(line);
            iss>>num;
            fr.emplace_back(num,1);
        } else { 
            string fi=line.substr(0,pidx);
            string se=line.substr(pidx+1,line.size()-pidx);
            cerr<<"fi="<<fi<<" se="<<se<<endl;

            istringstream issfi(fi);
            int num; issfi>>num;
            int ten=1;
            int num2=0;
            for(char c : se) {
                num2*=10;
                num2+=c-'0';
                ten*=10;
            }

            num*=ten;
            num+=num2;
            fr.emplace_back(num, ten);
        }
    }

    for(int i=0; i<n; i++) 
        cerr<<fr[i].f<<" / "<<fr[i].s<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
