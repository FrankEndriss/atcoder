/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* 
 * The black cell cannot be choosen arbitrary.
 * max(pos) of the black cell is the number of 
 * previously existing rows/cols.
 *
 * dp?
 * There are at most 6000 add operations.
 * For each one the resulting grid has
 * (a, a+i) * (b,b+i) cells
 *
 * There are c-a vert ops, and d-b horz ops.
 *
 * Example
 * a=3
 * b=4
 * vops=2
 * hops=2
 * vvhh -> 4 * 4 * 5 * 5
 * vhvh -> 4 * 4 * 5 * 5
 * vhhv -> 4 * 4 * 4 * 6
 * hvvh -> 3 * ....
 * hvhv
 * hhvv
 *
 * View from other end.
 * If last move was choose vert there are d posi
 * to choose the last cell.
 * If last move was choose horz, there are c posi
 * to choose the last cell, which one pos
 * overlapping by both cases.
 *
 * So, number of posis in last two moves are...
 */
const int MOD=998244353;
void solve() {
    cini(a);
    cini(b);
    cini(c);
    cini(d);
    
    vvi dp(c+1, vi(d+1, 1));
    for(int i=a+1; i<=c; i++) {
        for(int j=b+1; j<=d; j++) {
//...
        }
    }
    
    int ans=1;
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
