
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * What is complexity of brute force?
 * We take one of 100 oranges, Taka gets it.
 * We take one of 99 oranges, Aoki get it.
 * We take one of 98 oranges, ...one of them gets it
 * O(99!)
 *
 * Consider d=cnt[T]-cnt[A]
 * After each move d<=max(w[i]);
 *
 * Before last move, permutation contributes if d=w[last]
 * So, we need to find foreach last element the number of permutations
 * producing d as last diff.
 *
 * Note that the sum of all w[i] <= N*N ==10000
 *
 * Consider a permutation with p[l] as last element.
 * We can knapsack if there is a split of the set of w[] to get that d,
 * but how can we check if it is constructable within the rules of the game,
 * and how can we see in how much ways it is?
 *
 * WLOG sort w[].
 * Consider first choosen element w[i1], d+=w[i1], and second
 * w[i2], d-=w[i2].
 * There are upper_bound(w[i1]) elements as second element that make the third
 * element belong to taka, and n-upper_bound(w[i1])-2 that make it belong to aki.
 * And then?
 *
 * ***
 */
using mint=modint998244353;
void solve() {
    cini(n);
    cinai(w,n);
}

signed main() {
    solve();
}
