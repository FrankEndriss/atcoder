/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * dp[i]=min dist to reach vertex i
 */
const int INF=1e18;
void solve() {
    cini(n);
    cini(m);
    cini(k);

    vi dp(n, INF);
    dp[0]=0;

    vvi edg(m, vi(3));
    for(int i=0; i<m; i++) {
        cini(a); a--;
        cini(b); b--;
        cini(c);
        edg[i]={a,b,c};
    }
    for(int i=0; i<k; i++) {
        cini(e);
        e--;

        //int dpa=min(dp[edg[e][0]], dp[edg[e][1]]+edg[e][2]);
        int dpb=min(dp[edg[e][1]], dp[edg[e][0]]+edg[e][2]);
        //dp[edg[e][0]]=dpa;
        dp[edg[e][1]]=dpb;
    }

    if(dp[n-1]==INF) {
        cout<<-1<<endl;
    } else 
        cout<<dp[n-1]<<endl;

}

signed main() {
       solve();
}
