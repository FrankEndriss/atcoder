/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Simple knapsack.
 *
 * Something wrong, WA :/
 */
const int N=1e5+7;
void solve() {
    cini(n);
    cini(s);

    int mi=0;
    vi d(n);
    vvb vis(N, vb(n));
    vb ini(n);
    for(int i=0; i<n; i++) {
        cini(a);
        cini(b);
        mi+=a;
        d[i]=b-a;
    }

    vb ans(N);
    ans[mi]=true;
    vis[mi]=ini;

    for(int i=0; i<n; i++) {
        if(d[i]>0) {
            for(int j=N; j>=0; j--) {
                if(j-d[i]>=0 && ans[j-d[i]]) {
                    ans[j]=true;
                    vis[j]=vis[j-d[i]];
                    vis[j][i]=true;
                }
            }
        } else if(d[i]<0) {
            for(int j=0; j<=N; j++) {
                if(j-d[i]<=s && ans[j-d[i]]) {
                    ans[j]=true;
                    vis[j]=vis[j-d[i]];
                    vis[j][i]=true;
                }
            }
        }
    }

    if(ans[s]) {
        cout<<"Yes"<<endl;
        for(int i=0; i<n; i++)
            if(vis[s][i])
                cout<<"H";
            else
                cout<<"T";
    } else {
        cout<<"No"<<endl;
    }

}

signed main() {
    solve();
}
