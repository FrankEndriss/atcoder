/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
void solve() {
    cini(n);
    vvi a(n, vi(n));
    for(int i=0; i<n; i++) 
        for(int j=0; j<n; j++) 
            cin>>a[i][j];

    vector<vector<map<int,int>>> dp(n, vector<map<int,int>>(n));
    dp[0][0][a[0][0]]=1;
    for(int j=1; j<n; j++) {
        for(auto ent : dp[0][j-1])
            dp[0][j][ent.first^a[0][j]]+=ent.second;
    }
    for(int i=1; i<n; i++) {
        for(auto ent : dp[i-1][0])
            dp[i][0][ent.first^a[i][0]]+=ent.second;

        for(int j=1; j<n; j++) {
            for(auto ent : dp[i-1][j])
                dp[i][j][ent.first^a[i][j]]+=ent.second;
            for(auto ent : dp[i][j-1])
                dp[i][j][ent.first^a[i][j]]+=ent.second;
        }
    }
    cout<<dp[n-1][n-1][0]<<endl;


}

signed main() {
       solve();
}
