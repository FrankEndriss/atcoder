/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Fucking hard to read statement text. sic.
 * Greedy sell the latest volumes.
 *
 * WA for unknown reason, sic :/
 */
void solve() {
    cini(n);
    deque<int> a;
    for(int i=0; i<n; i++) {
        cini(aux);
        a.push_back(aux);
    }
    sort(all(a));

    int ans=0;
    for(int i=1; i<=n && a.size(); i++) {
        if(a[0]==i) {
            ans=i;
            a.pop_front();
        } else if(a.size()>=2) {
                ans=i;
                a.pop_back();
                a.pop_back();
        } else 
            break;
    }
    cout<<ans<<endl;

}

signed main() {
       solve();
}
