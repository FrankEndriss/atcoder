/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, d;
    cin>>n>>d;
    vvi x(n, vi(d));
    fori(n)
        for(int j=0; j<d; j++)
            cin>>x[i][j];

    int ans=0;
    for(int i=0; i<n; i++)  {
        for(int j=i+1; j<n; j++) {
            ll a=0;
            for(int k=0; k<d; k++) {
                int t=x[i][k]-x[j][k];
                a+=t*t;
            }
            ll sq=(ll)sqrt(a);
            if(sq*sq==a)
                ans++;
        }
    }

    cout<<ans<<endl;

}

