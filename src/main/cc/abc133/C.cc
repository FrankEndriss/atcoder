/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    ll l, r;
    cin>>l>>r;
    while(l>=2019)
        l-=2019;
    while(r-l>2019)
        r-=2019;

    ll ans=2019;
    for(ll i=l; i<r; i++)
        for(ll j=i+1; j<=r; j++)
            if(ans>(i*j)%2019)
                ans=(i*j)%2019;

    cout<<(int)ans<<endl;

}

