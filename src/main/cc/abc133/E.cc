/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, k;
    cin>>n>>k;
    vvi tree(n+1);
    fori(n-1) {
        int u, v;
        cin>>u>>v;
        tree[u].push_back(v);
        tree[v].push_back(u);
    }

    const int MOD=1e9+7;
    const int ROOT=1;

    ll ans=1;
    // d=number of used colors in parent and sisters
    function<void(int, int, int)> dfs=[&](int node, int p, int d) {
 // note: id d>=k then no way to color the tree with only k colors
        ans=ans*(k-d);
        ans%=MOD;
        int i=1;
        if(node!=ROOT)
            i++;
        for(int c : tree[node])
            if(c!=p)
                dfs(c, node, i++);
    };

    dfs(ROOT, 0, 0);

/*
    for(int i=k-1; i>=2; i--) {
        ans*=i;
        ans%=MOD;
    }
*/

    cout<<ans<<endl;
}

