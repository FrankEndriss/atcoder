/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We must move the 1 to the front, and that is allways possible.
 * Also, among all ways to do this, we want to choose that way that
 * moves the 2 in second position, if possible.
 * If not possible the 3...and so on.
 *
 * Consider the numbers we can move to the front:
 * It is the one that is allready in front, or one of the 2*k
 * rightmost numbers (RN).
 * So find the smallest among those numbers (mi), moving that to 
 * the front is the last move.
 * Consider the number of moves:
 *  we can 'remove' with each action 1 or 2 numbers right of mi, so 
 *  we want to find the smallest possible number we can move as
 *  prelast action. That number is, maybe, the second smallest number
 *  among RN.
 *  ...somehow recursive
 */
void solve() {

}

signed main() {
       solve();
}
