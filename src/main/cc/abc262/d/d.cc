/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using mint=modint998244353;

const int MAXN = 100;
mint C[MAXN + 1][MAXN + 1];

const bool init_nCr=[]() {
    C[0][0] = 1;
    for (int n = 1; n <= MAXN; ++n) {
        C[n][0] = C[n][n] = 1;
        for (int k = 1; k < n; ++k)
            C[n][k] = C[n - 1][k - 1] + C[n - 1][k];
    }
    return true;
}();

mint nCr(int n, int k) {
    return C[n][k];
}


/**
 * We want to choose i numbers so that sum%i==0
 * Group the numbers by a[j]%i, then
 * combine the groups, ...somehow :/
 * Knapsack?
 *
 * dp[k][i][j]=ways to choose i of first k numbers with sum%i==j
 */
void solve() {
    cini(n);
    cinai(a,n);

    cerr<<"n="<<n<<endl;
    //vector<vector<vector<mint>>> dp(n+1, vector<vector<mint>>(n+1, vector<mint>(n+1)));

    mint ans=0;
    for(int i=1; i<=n; i++) {   /* we choose i numbers at all */
        /*
            dp[k][j][l]=number of ways to choose
                among first k numbers j numbers, with sum%i==l
        */
        vector<vector<vector<mint>>> dp(n+1, vector<vector<mint>>(n+1, vector<mint>(n+1)));

        for(int k=1; k<=n; k++) {   /* prefix length */
            dp[k][0][0]=1;
            for(int j=0; j+1<=k; j++) {
                for(int l=0; l<i; l++) 
                    dp[k][j+1][(l+a[k-1])%i]+=dp[k][j][l];
            }
        }

        ans+=dp[n][i][0];
    }

    cout<<ans.val()<<endl;
}

signed main() {
       solve();
}
