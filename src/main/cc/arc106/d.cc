
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * all pairs (a[i]+a[j])^x
 * for all x
 * That contributes
 * a[i]^(x-0) * a[j]^0
 * a[i]^(x-1) * a[j]^1
 * a[i]^(x-2) * a[j]^2
 * ...
 * a[i]^(x-x) * a[j]^x
 *
 * assume a[k]
 * let sum(a[i..j]) the sum of segment i,j in a[]
 *
 * for x=1 it contributs
 * a[k]^(1-0) * sum(a[k+1..n]^0)
 * -> n*(n+1)/2
 * x=2
 * a[k]^(2-0) * (a[k+1]^0 + a[k+2]^0+...+a[n]^0)
 * x=3
 * a[k]^(3-0) * (a[k+1]^0 + a[k+2]^0+...+a[n]^0)
 *
 */
constexpr int MOD=998244353;
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    vvi p(k+1, vi(n,1));
    vvi pp(k+1, vi(n));
    for(int i=0; i<=k; i++) 
        for(int j=0; j<n; j++)  {
            if(i>0)
                p[i][j]=(p[i-1][j]*a[j])%MOD;
            if(j==0)
                pp[i][j]=p[i][j];
            else
                pp[i][j]=(p[i][j]+pp[i][j-1])%MOD;
        }
    
    for(int i=1; i<=k; i++) {
        int ans=0;
        for(int j=0; j<n; j++)  {
            int pre=pp[i][n-1]-pp[i][j];
            ans+=(p[i][j]*pre)%MOD;
            ans%=MOD;
        }
        cout<<ans<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
