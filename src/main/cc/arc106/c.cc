
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * Both print min 1 and max N
 * Value of second cannot be bigger than first since
 * first is optimal.
 * So M<0 is not possible.
 *
 * if M==0 choose all non overlapping like
 * (1,2),(3,4),...,(n*2-1,n*2)
 *
 * if M==1 make first overlap the next two, like
 * (1,6),(2,3),(4,5),(7,8),...
 *
 * if M==N-1 ?
 * -> Not possible
 */
void solve() {
    cini(n);
    cini(m);

    if(n==1 && m==0) {
        cout<<"1 2"<<endl;
        return;
    }

    if(m<0 || m>=n-1) {
        cout<<-1<<endl;
        return;
    }

    set<int> s;
    for(int i=2; i<=n*2; i++) 
        s.insert(i);

    vi l(n);
    vi r(n);
    l[0]=1;
    if(m>0) {
        r[0]=4+m*2;
        s.erase(4+m*2);
    } else {
        r[0]=2;
        s.erase(2);
    }
    for(int i=1; i<n; i++)  {
        auto it=s.begin();
        l[i]=*it;
        s.erase(it);
        it=s.begin();
        r[i]=*it;
        s.erase(it);
    }
    for(int i=0; i<n; i++) 
        cout<<l[i]<<" "<<r[i]<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
