
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using mint=modint1000000007;

/* matrix multiplication */
vector<vector<mint>> mulM(vector<vector<mint>> &m1, vector<vector<mint>> &m2) {
    assert(m1.size()==m1[0].size() && m2.size()==m2[0].size() && m1.size()==m2.size());
    const int sz=m1.size();
    vector<vector<mint>> ans(sz, vector<mint>(sz));
    for(int i=0; i<sz; i++) {
        for(int j=0; j<sz; j++) {
            for(int k=0; k<sz; k++) {
                ans[i][j]+= m1[i][k] * m2[k][j];
            }
        }
    }
    return ans;
}

/* matrix pow */
vector<vector<mint>> toPower(vector<vector<mint>> a, int p) {
    const int sz=a.size();
    vector<vector<mint>> res(sz, vector<mint>(sz));
    for(int i=0; i<sz; i++)
        res[i][i]=1;

    while (p != 0) {
        if (p & 1)
            res = mulM(a, res);
        p >>= 1;
        a = mulM(a, a);
    }
    return res;
}

/* This is somehow matrix multiplication...
 * I cant recall the details.
 * Maybe actually simple raise the adj matrix to the Kth power,
 * then take the sum of all cells.
 */
void solve() {
    cini(n);
    cini(k);

    vector<vector<mint>> adj(n, vector<mint>(n));
    for(int i=0; i<n; i++) 
        for(int j=0; j<n; j++) {
            cini(aux);
            adj[i][j]=aux;
        }

    adj=toPower(adj, k);
    mint ans=0;
    for(int i=0; i<n; i++) 
        for(int j=0; j<n; j++) 
            ans+=adj[i][j];

    cout<<ans.val()<<endl;
}

signed main() {
    solve();
}
