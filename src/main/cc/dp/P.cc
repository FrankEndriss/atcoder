
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* define a dfs as returning number of ways painting the subtree
 * if parent is black or white.
 */
using mint=modint1000000007;
void solve() {
    cini(n);
    vvi adj(n);

    for(int i=0; i+1<n; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    vector<vector<mint>> dp(2, vector<mint>(n));
    vvb vis(2, vb(n));

    /* 0=white, 1=black */
    function<mint(int,int,int)> dfs=[&](int v, int p, int c) {
        if(vis[c][v])
            return dp[c][v];

        mint answ=1;
        mint ansb=0;
        if(c==0)
            ansb=1;

        for(int chl : adj[v]) {
            if(chl==p)
                continue;
            answ*=dfs(chl,v,0);
            if(c==0)
                ansb*=dfs(chl,v,1);
        }
        vis[c][v]=true;
        return dp[c][v]=answ+ansb;
    };

    cout<<dfs(0, -1, 0).val()<<endl;

}

signed main() {
    solve();
}
