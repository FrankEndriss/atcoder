/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    string s, t;
    cin>>s>>t;

//cout<<s<<endl;
//cout<<t<<endl;
    vector<vector<int>> dp(s.size()+1, vector<int>(t.size()+1));

    for(uint i=0; i<=s.size(); i++) {
        for(uint j=0; j<=t.size(); j++) {
            if(i==0 || j==0)
                dp[i][j]=0;
            else if(s[i-1]==t[j-1])
                dp[i][j]=dp[i-1][j-1]+1;
            else
                dp[i][j]=max(dp[i][j-1], dp[i-1][j]);
        }
    }
    int len=dp[s.size()][t.size()];
//cout<<"len="<<len<<endl;

    /* recover */
    string ans;
    int n=(int)s.size();
    int m=(int)t.size();
    //while(ans.size()<len) {
    while(n>0 && m>0) {
        if(s[n-1]==t[m-1]) {
//cout<<"n="<<n<<" m="<<m<<endl;
            ans=s[n-1]+ans;
            n--;
            m--;
        } else if(dp[n-1][m]>dp[n][m-1]) {
           n--;
        } else {
           m--;
        }
    }

    cout<<ans<<endl;

}

