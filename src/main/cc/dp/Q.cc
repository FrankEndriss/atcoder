
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/segtree>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* adjust as needed */
using S=ll; /* type of values */

S st_op(S a, S b) {
    return max(a,b);
}

S st_e() {
    return 0LL;
}

using stree=segtree<S, st_op, st_e>;

/* 
 * same algo as LIS
 * We maintain a list of hights, and the max beauty we can get at that hight.
 * Process flowers left to right.
 * Foreach flower we need to find the max beauty smaller than the current
 * hight. Then max beauty for the current hight is that max plus the
 * current beauty.
 * Seems simplest to maintain the beauties of hights in a segtree.
 */
const int N=2e5+7;
void solve() {
    cini(n);
    cinai(h,n);
    cinai(a,n);

    stree seg(N);

    for(int i=0; i<n; i++) {
        int ma=seg.prod(0,h[i]);
        seg.set(h[i], a[i]+ma);
        //cerr<<"i="<<i<<" h[i]="<<h[i]<<" ma="<<ma<<" a[i]="<<a[i]<<endl;
    }

    cout<<seg.prod(0,N)<<endl;
}

signed main() {
    solve();
}
