
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * Adding a digit creates 10 new numbers.
 * We are interested in how much numbers result in what remainder by D,
 * since the final answer is the number of numbers with remainder eq 0.
 * But, we need to be careful to count only the numbers up to K.
 *
 * Lets iterate the digits starting and least significant
 * while maintaining two DPs:
 * dp1[i]=number of numbers with remainder i with current number of digits
 * dp2[i]=number of numbers with remainder i but less or eq current K
 */
using mint=modint1000000007;
void solve() {
    cins(k);
    reverse(all(k));
    cini(d);

    vector<mint> dp1(d);
    vector<mint> dp2(d);
    dp1[0]=1;
    dp2[0]=1;

    for(size_t i=0; i<k.size(); i++) {
        vector<mint> dp10(d);
        vector<mint> dp20(d);

        for(int dd=0; dd<=9; dd++)
            for(int j=0; j<d; j++)
                dp10[(j+dd)%d]+=dp1[j];

        const int kk=k[i]-'0';
        for(int dd=0; dd<kk; dd++)
            for(int j=0; j<d; j++) 
                dp20[(j+dd)%d]+=dp1[j];

        for(int j=0; j<d; j++) 
            dp20[(j+kk)%d]+=dp2[j];

        dp10.swap(dp1);
        dp20.swap(dp2);
    }

    dp2[0]-=1;
    cout<<dp2[0].val()<<endl;


}

signed main() {
    solve();
}
