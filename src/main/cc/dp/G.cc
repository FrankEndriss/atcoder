/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
    int n, m;
    cin>>n>>m;
    vector<vector<int>> tree(n+1);
    fori(m) {
        int x, y;
        cin>>x>>y;
        tree[x].push_back(y);
    }

    vector<int> len(n+1);
    function<void(int)> dfs=[&](int v) {
        if(len[v]>0)
            return len[v];

        int ret=1;
        for(auto c : tree[v]) {
            dfs(c);
            ret=max(ret, 1+len[c]);
        }
        len[v]=ret;
    };

    int ans=0; /* number of vertex */
    for(int i=1; i<=n; i++)  {
        dfs(i);
        ans=max(ans, len[i]);
    }

    cout<<ans-1<<endl; /* number of edges */
    return 0;
}

