
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Since N=21 this should be solveable with a bitmask dp.
 * But 41 bits would be to much.
 * But we can use 
 * dp[i][mask]=number of ways when first i men are used and mask women are used.
 * That way it is O(N*(1<<N))
 */
using mint=modint1000000007;
void solve() {
    cini(n);
    const int N=1<<n;

    vvi a(n, vi(n));
    for(int i=0; i<n; i++) 
        for(int j=0; j<n; j++)
            cin>>a[i][j];

    vector<vector<mint>> dp(n+1, vector<mint>(N));
    vvb vis(n+1, vb(N));

    vis[n][N-1]=true;
    dp[n][N-1]=1;

    function<mint(int,int)> go=[&](int i, int mask) {
        //cerr<<"go, i="<<i<<" mask="<<bitset<20>(mask)<<endl;
        if(vis[i][mask])
            return dp[i][mask];

        mint ans=0;
        for(int j=0; j<n; j++) {
            if((mask&(1<<j))==0 && a[i][j])
                ans+=go(i+1,mask|(1<<j));
        }
        vis[i][mask]=true;
        return dp[i][mask]=ans;
    };

    cout<<go(0, 0).val()<<endl;
}

signed main() {
    solve();
}
