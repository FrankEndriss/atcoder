/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)


int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
int n;
    cin>>n;
    vector<vector<int>> abc(n, vector<int>(3));
    fori(n)
        cin>>abc[i][0]>>abc[i][1]>>abc[i][2];

    vector<vector<int>> dp(n, vector<int>(3)); // dp[i][j][k]=max joy on day i when choosen aktivity j that day, and notsame/same the day before

    fori(2) {
        dp[0][0]=abc[0][0];
        dp[0][1]=abc[0][1];
        dp[0][2]=abc[0][2];
    }

    forn3(i, 1, n)
        forn3(j, 0, 3)
           dp[i][j]=max(dp[i-1][(j+1)%3], dp[i-1][(j+2)%3]) + abc[i][j];

    int ans=0;
    fori(3)
        ans=max(ans, dp[n-1][i]);

    cout<<ans<<endl;
}

