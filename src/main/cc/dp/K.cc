/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, k;
    cin>>n>>k;
    vector<int> a(n);
    fori(n)
        cin>>a[i];

    int amin=*min_element(a.begin(), a.end());
    //int amax=*max_element(a.begin(), a.end());
    vector<bool> win(k+1);
    vector<bool> vis(k+1, false);

//cout<<"n="<<n<<" k="<<k<<" amin="<<amin<<endl;
    for(int i=0; i<amin; i++) {
        win[i]=false;
        vis[i]=true;
    }

    function<bool(int)> isWin=[&](int nextK) {
        assert(nextK>=0);

//cout<<"isWin("<<nextK<<")"<<endl;
        if(vis[nextK])
            return win[nextK];

        // if at least one position is reachable which is not win, nextK is win.
        bool lwin=false;
        for(int i=0; i<n; i++) {
            if(nextK>=a[i] && !isWin(nextK-a[i])) {
                lwin=true;
                break;
            }
        }
        vis[nextK]=true;
//cout<<"win["<<nextK<<"]="<<lwin<<endl;
        return win[nextK]=lwin;
    };

    for(int i=amin; i<k; i++) 
        isWin(i);

    int ans=isWin(k);
    cout<<(ans?"First":"Second")<<endl;

/* k stones.
 * 0 to min(a[i])-1 is loosing pos.
 * So, min(a[i]) to min(a[i])-1+max(a[i]) is winning pos. 
 * So, if loosing pos is reacheable its winning pos, if not then its loosing pos...
 */


}

