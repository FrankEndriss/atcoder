
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Recursive definition and implementation
 * forech range l,r let f(l,r,p)=result if it is p turn with current q be l,r.
 * if p=1 then 
 *  ans=max(a[l]+f(l+1,r), a[r]+f(l,r-1))
 * else
 *  ans=min(a[l]+f(l+1,r), a[r]+f(l,r-1))
 *
 * Note that if called recursive, each l,r is called only once, not for 
 * both players, since the distance r-l determines which players 
 * turn it is.
 */
void solve() {
    cini(n);
    cinai(a,n);

    vvi dp(n, vi(n));
    vvb vis(n, vb(n));

    function<int(int,int,int)> go=[&](int l, int r, const int p) {
        //cerr<<"l="<<l<<" r="<<r<<" p="<<p<<endl;
        if(l==r) {
            if(p)
                return a[l];
            else
                return -a[l];
        }

        if(vis[l][r])
            return dp[l][r];

        vis[l][r]=true;
        if(p) {
            return dp[l][r]=max(a[l]+go(l+1, r, !p), a[r]+go(l, r-1, !p));
        } else {
            return dp[l][r]=min(-a[l]+go(l+1, r, !p), -a[r]+go(l, r-1, !p));
        }
    };

    int ans=go(0,n-1, 1);
    cout<<ans<<endl;
}

signed main() {
    solve();
}
