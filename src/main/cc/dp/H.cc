/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int h, w;
    cin>>h>>w;

    vector<vector<bool>> grid(h, vector<bool>(w));
    fori(h) {
        string s;
        cin>>s;
        forn3(j, 0, w)
            grid[i][j]=(s[j]=='#');     /* walls */
    }

    
    vector<vector<int>> ans(h, vector<int>(w));
    const int MOD=1000000007;

    bool wall=false;
    fori(h) {
        if(grid[i][0])
            wall=true;
        ans[i][0]=wall?0:1;
    }
    wall=false;
    fori(w) {
        if(grid[0][i])
            wall=true;
        ans[0][i]=wall?0:1;
    }

    forn3(i, 1, h)
        forn3(j, 1, w) {
            if(grid[i][j])
                ans[i][j]=0;
            else
                ans[i][j]=(ans[i-1][j]+ans[i][j-1]) % MOD;
        }

    cout<<ans[h-1][w-1]<<endl;
}

