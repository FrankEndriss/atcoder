
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vector<int> h(n);
    vector<int> c(n);
    fori(n)
        cin>>h[i];

    c[0]=0;
    c[1]=abs(h[1]-h[0]);
    forn3(i, 2, n)
        c[i]=min(c[i-1]+abs(h[i-1]-h[i]), c[i-2]+abs(h[i-2]-h[i]));

    cout<<c[n-1]<<endl;
        

}

