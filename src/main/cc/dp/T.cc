
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* How to build the dp state here?
 * Trivial would be
 * dp[i][mask]=number of permutations with last digit i and mask digits used
 * Then iterate for N positions...
 *
 * Is there allways a permutation satisfing the string s?
 * Yes, induction. We can use as first digit
 * the smallest or biggest number in p, then solve the remaining part of s.
 * But that does not really help in finding the number of perms :/
 * Actually we are interested in the perms with _not_ the smallest or
 * biggest number in first position.
 * Consider maintaining the last used p, and the number of remainging
 * p bigger and smaller the last used p.
 * That would lead to a dp:
 * dp[i][b][s]=number of perms where last used digits was ith of the remaining
 *   p and there are b bigger and s smaller.
 * So, since we know how much numbers are remaining, we do not need b and s:
 * dp[i]=number of perms where last used digit is ith of the remaining digits.
 */
using mint=modint1000000007;
void solve() {
    cini(n);
    cins(s);

    vector<mint> dp(n);
    for(int i=0; i<n; i++) {
        if((s[0]=='<' && i<n-1) || (s[0]=='>' && i>0))
            dp[i]=1;

    }
    for(int pos=0; pos<s.size(); pos++) {
        vector<mint> dp0(n);
        // TODO
    }
}

signed main() {
    solve();
}
