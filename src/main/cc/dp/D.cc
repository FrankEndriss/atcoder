/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

const int N=101;
const int MAXN=100001;
int n, W;


int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cin>>n>>W;
    vector<int> wt(n), val(n);

    fori(n)
        cin>>wt[i]>>val[i];

    vector<vector<ll>> dp(n+1, vector<ll>(W+1, 0));

    for(int i=0; i<=n; i++) {
        for(int j=0; j<=W; j++) {
            if(i==0 || j==0)
                dp[i][j]=0;
            else if(wt[i-1]<=j)
                dp[i][j]=max(val[i-1] + dp[i-1][j-wt[i-1]], dp[i-1][j]);
            else    
                dp[i][j]=dp[i-1][j];
        }
    }

    ll ans=dp[n][W];
    cout<<ans<<endl;
}

