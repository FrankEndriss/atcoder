
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* So we have segments, initally of len=1, and combine
 * adjacent segments to the cost of the sum of the elements of the
 * two combined segments.
 *
 * So, let dp[i][j] be cost to combine all parts from i to j into
 * one segement, then
 * dp[i][j]=min(k=i..j-1; dp[i][k]+dp[k+1][j]) + sum(i,j)
 */
const int INF=1e18;
void solve() {
    cini(n);
    cinai(a,n);

    vi pre(n+1);
    vvi dp(n, vi(n, INF));
    for(int i=0; i<n; i++) {
        dp[i][i]=0;
        pre[i+1]=pre[i]+a[i];
    }

    for(int len=2; len<=n; len++) {
        for(int l=0; l+len-1<n; l++) {
            const int r=l+len-1;
            /* calc dp[i][r] */
            for(int mid=l; mid<r; mid++) {
                dp[l][r]=min(dp[l][r], 
                        dp[l][mid]+dp[mid+1][r] + pre[r+1]-pre[l]);
            }
        }
    }

    cout<<dp[0][n-1]<<endl;
}

signed main() {
    solve();
}
