
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vvd= vector<vd>;
using vvvd= vector<vvd>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * We put the problem other way round.
 * That is, we start with all empty plates, and find the 
 * number of expected roles to get a configuration.
 * So, dp is
 * dp[i][j][k]=expected number of roles until we get i ones, j twos and k threes
 *
 * see https://www.youtube.com/watch?v=vQP2yrD58JU
 *
 * For some reason still WA :/
 */
void solve() {
    cini(n);
    vi cnt(4);
    for(int i=0; i<n; i++) {
        cini(aux);
        cnt[aux]++;
    }

    vvvd dp(n+2, vvd(n+2, vd(n+2, 0.0)));

    for(int k=0; k<=cnt[3]; k++) 
        for(int j=0; j<=cnt[2]+cnt[3]-k; j++) 
            for(int i=0; i<=cnt[1]+cnt[2]+cnt[3]-j-k; i++) {
                if(i+j+k==0)
                    continue;

                const ld ijk=i+j+k;

                dp[i][j][k]=n/ijk;

                if(i>0)
                    dp[i][j][k]+=dp[i-1][j][k]*i/ijk;
                if(j>0)
                    dp[i][j][k]+=dp[i+1][j-1][k]*j/ijk;
                if(k>0)
                    dp[i][k][k]+=dp[i][j+1][k-1]*k/ijk;
            }

    cout<<dp[cnt[1]][cnt[2]][cnt[3]]<<endl;

}

signed main() {
    solve();
}
