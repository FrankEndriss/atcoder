

/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, k;
    cin>>n>>k;
    vector<int> h(n);
    fori(n)
        cin>>h[i];

    vector<int> c(n, 1000000001);
    c[0]=0;
    fori(n) {
        for(int j=1; j<=k; j++) {
            if(i+j<n) 
                c[i+j]=min(c[i+j], c[i]+abs(h[i]-h[i+j]));
            }
    }

    cout<<c[n-1]<<endl;
        

}

