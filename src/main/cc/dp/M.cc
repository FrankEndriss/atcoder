
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * Ignoring the constraints we have
 * dp[i][j]=number of ways to distribute j candies to first i childs
 *
 * Unfortunatly the simple implementation is O(N*K^2)
 * We need to optimize this to O(N*K)
 *
 * since we can give up to a[i] candies to the current child,
 * the number of ways is 
 * dp0[i]=sum(dp[max(0,i-a[i])]..dp[i])
 *
 * So we can solve this using prefix sums.
 */
using mint=modint1000000007;
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    vector<mint> dp(k+1, mint(0));
    vector<mint> pre(k+2, mint(0));
    for(int i=0; i<=k; i++) {
        if(i<=a[0])
            dp[i]=1;
        pre[i+1]=pre[i]+dp[i];
    }

    for(int i=1; i<n; i++) {
        vector<mint> dp0(k+1, mint(0));
        vector<mint> pre0(k+2, mint(0));

        for(int j=0; j<=k; j++) {
            int mi=max(0LL, j-a[i]);
            dp0[j]=pre[j+1]-pre[mi];
            pre0[j+1]=pre0[j]+dp0[j];
        }

        dp.swap(dp0);
        pre.swap(pre0);
    }

    cout<<dp[k].val()<<endl;
}

signed main() {
    solve();
}
