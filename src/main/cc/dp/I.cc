/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vector<double> p(n);
    fori(n)
        cin>>p[i];

    vector<vector<double>> dp(n+1, vector<double>(n+1)); /* dp[i][j]= prop of having i heads after j tosses */

    dp[0][0]=1;

    for(int i=1; i<=n; i++) {
        dp[0][i]=dp[0][i-1]*(1-p[i-1]);
        //cout<<dp[0][i]<<"  |  ";
    }
    //cout<<endl;

    for(int i=1; i<=n; i++) {
        for(int j=1; j<=n; j++) {
            dp[i][j]=dp[i-1][j-1]*p[j-1] + dp[i][j-1]*(1-p[j-1]);
            //cout<<dp[i][j]<<"  |  ";
        }
        //cout<<endl;
    }

    double ans=0;
    for(int i=(n+2)/2; i<=n; i++)
        ans+=dp[i][n];

    cout<<setprecision(10)<<ans<<endl;
}

