/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using mint=modint998244353;

const int N=2e5+7;
vector<mint> fac(N);
const bool initFac=[]() {
    fac[1]=1;
    for(int i=2; i<N; i++) {
        fac[i]=fac[i-1]*i;
    }
    return true;
}();

/** see https://cp-algorithms.com/combinatorics/binomial-coefficients.html */
mint nCr(int n, int k) {
    if(k==0 || k==n)
        return mint(1);
    if(k==1 || k==n-1)
        return mint(n);
    if(k>n/2)
        return nCr(n, n-k);

    return fac[n] * fac[k].inv() * fac[n-k].inv();
}

/**
 * Note that each set is a bitset<2^N>
 *
 * ans=sum of
 * -number of sets of even size
 * -number of sets with odd size with xorsum !=0
 *
 * Is another defintion possible?
 * Because that looks like it should be transformed somehow. What
 * is the opposite definition?
 * ans= number of all sets minus
 * -number of sets with odd size and xorsum==0
 *
 * How to find number of sets with xorsum==0 for a given size?
 * For xorsum==0 the number of each bit set must be even...
 * looks like inclusion/exclusion :/
 * But, nevertheless, bits are independent of each other. And, actually
 * all bits are same, so we have to calc only for one bit then toPower(n)
 *
 * So, how to find number of sets with xorsum==0 with one bit.
 * -> Just build the sum on nCr(0,n)+nCr(2,n)+nCr(4,n)+...+nCr(n,n)
 *
 * so, its O(n^2) and WA :/
 */
void solve() {
    cini(n);

    // this is not how 2^(2^n) is calculated :/
    mint ans=mint(2).pow(mint(2).pow(n));  /* all sets */

    mint onebit=mint(1);
    for(int i=1; i<=n; i+=2) {   /* set size odd */
        for(int j=0; j<=i; j+=2) {  /* times the bit is set */
            onebit+=nCr(i,j);
        }
    }
    ans=ans-(onebit.pow(n));
    cout<<ans.val()<<endl;
}

signed main() {
       solve();
}
