
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We could dfs all combinations of the rules...
 * but that way TLE.
 *
 * Can we check contraints for the numbers by components?
 * Each rule bounds a[x[i]] to a[y[i]] in 3 possible ways.
 * So if we choose some x[i] and check all 3 rules for it,
 * all other rules determine.
 * So we can choose the min sum of the non-empty answers.
 * ***
 * We want to apply the smaller rule to each element.
 * But that not possible if an x[i]==1 or y[i]==1 is given.
 * So in that case we want to apply the eq rule.
 * If this also does not work, we want to apply the bg rule.
 * ...but how, in less than dfs all rules?
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);

    vector<tuple<int,int,int>> adj(n);
    vi x(k);
    vi y(k);
    for(int i=0; i<k; i++) {
        cini(p); p--;
        cin>>x[i];
        cini(q); q--;
        cin>>y[i];
        adj[p].emplace_back(u,x,y);
        adj[u].emplace_back(p,y,x);
    }

    vb vis(n);
    for(int i=0; i<n; i++) {
        if(vis[i])
            continue;


    }
}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
