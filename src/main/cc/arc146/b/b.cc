/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * M operations, then choose K numbers.
 *
 * Go from highest to lowest bit.
 * Check foreach number how much operations we need to
 * set that bit.
 * If possible to create k of these bits with remaining m ops then do it.
 * Note that with all numbers that we increased to get the bit,
 * all other bits are not set afterwards.
 * But, nevertheless, we needed min number of operations.
 *
 * So I guess its allways best to choose the biggest numbers.
 * ... It seems to not be. So, what else is it?
 *
 * If more than k numbers all have the bit set, then we need to
 * consider all of them for the next round.
 * Since the next bit could be unsettable, and then it is
 * arbitrary which of the numbers from the first round we want to use.
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);

    cinai(a,n);

    sort(all(a), greater<int>());
    int ans=0;

    for(int b=30; b>=0; b--) {
        const int val=(1LL<<b);

        int mm=0;
        for(int i=0; i<k; i++) {
            if((a[i]&val)==0) {
                mm+=(val-a[i]);
            }
        }

        if(mm<=m) { /* bit can be created, so do it */
            if(mm>0) {
                while(a.size()>k)
                    a.pop_back();
            } else {
                while((a.back()&val)==0)
                    a.pop_back();
            }


            ans+=val;
            m-=mm;

            for(size_t i=0; i<a.size(); i++)
                if(a[i]&val)
                    a[i]^=val;
                else
                    a[i]=0;
        } else { /* just remove the bit, it is unused */
            for(size_t i=0; i<a.size(); i++)
                if(a[i]&val)
                    a[i]^=val;
        }

        sort(all(a), greater<int>());
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}
