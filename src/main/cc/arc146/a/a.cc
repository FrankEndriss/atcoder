/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * The three biggest should allways work.
 *
 * Then we need to sort them by digits.
 *
 * So make tuples:
 * <byDigit,numDigits,num>
 */
void solve() {
    cini(n);
    cinai(a,n);

    sort(all(a), greater<int>());

    vvi dig(3);
    vi cnt(3);
    for(int i=0; i<3; i++) {
        int aa=a[i];
        while(aa) {
            dig[i].push_back(aa%10);
            aa/=10;
        }
        reverse(all(dig[i]));
        cnt[i]=(int)dig[i].size();
    }

    int madig=(int)max(dig[0].size(), max(dig[1].size(), dig[2].size()));
    for(int i=0; i<3; i++) 
        while(dig[i].size()<madig)
            dig[i].push_back(0);

    vector<tuple<vi,int,int>> data={ 
        { dig[0], cnt[0], a[0]},
        { dig[1], cnt[1], a[1]},
        { dig[2], cnt[2], a[2]},
    };

    sort(all(data));
    reverse(all(data));

    auto [aa,c,a0]=data[0];
    int ans=a0;
    auto [aa1,c1,a1]=data[1];
    for(int i=0; i<c1; i++) 
        ans*=10;
    ans+=a1;
    auto [aa2,c2,a2]=data[2];
    for(int i=0; i<c2; i++) 
        ans*=10;
    ans+=a2;

    cout<<ans<<endl;

}

signed main() {
       solve();
}
