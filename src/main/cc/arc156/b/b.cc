
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


using mint=modint998244353;

const int NN=1e6;
vector<mint> fac(NN);

const bool initFac=[]() {
    fac[1]=1;
    for(int i=2; i<NN; i++) {
        fac[i]=fac[i-1]*i;
    }
    return true;
}();

/** see https://cp-algorithms.com/combinatorics/binomial-coefficients.html */
mint nCr(int n, int k) {
    cerr<<"nCr, n="<<n<<" k="<<k<<endl;
    if(k==0 || k==n)
        return mint(1);
    if(k==1 || k==n-1)
        return mint(n);
    if(k>n/2)
        return nCr(n, n-k);

    return fac[n]*fac[k].inv()*fac[n - k].inv();
}

/**
 * Consider allways choosing 0 elements, or not the included zero, so we get another 0.
 * so we wrote K times 0.
 * If there is initial no 0 on the board, on first round we allways write 0.
 *
 * Let M=MEX of numbers available in current round.
 * So in each round we can choose any number between 0..M,
 * and if choosen M, then M++
 *
 * So, it is stars'n'bars, where some of the numbers have to be chooses
 * at least once to make higher numbers available.
 * Consider J to be the max number choosen, so for all numbers Y<J that are
 * not in initial set, we have to choose Y at least once.
 * So iterate J and add up numbers.
 */
const int N=2e5+7;
void solve() {
    cini(n);
    cini(k);

    cerr<<"n="<<n<<" k="<<k<<endl;
    vb a(N+k+1);

    for(int i=0; i<n; i++) {
        cini(aux);
        cerr<<"aux="<<aux<<endl;
        a[aux]=true;
    }

    int cnt=0;
    mint ans=0;
    for(int i=0; cnt<=k && i<N+k+1; i++) {

        //cerr<<"i="<<i<<" cnt="<<cnt<<endl;

        if(cnt>k)
            break;

        /* The highest number we did choose was i,
         * and we did choose all lower numbers not in initial set
         * at least once.
         * let that number be extra.
         *
         * now we can basically choose k-extra times between i+1 numbers,
         * so it is i+1 bars with k-extra stars.
         **/

        int extra=cnt+1;

        ans+=nCr((k-extra)+i+1,i+1);

        if(!a[i])
            cnt++;
    }

    cout<<ans.val()<<endl;

}

signed main() {
        solve();
}
