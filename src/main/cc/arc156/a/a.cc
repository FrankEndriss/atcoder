
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Go greedy, find the right one of a pair next to next.
 * So, kind of two pointer.
 *
 * ...but WA. So I am missing something?
 *
 * Consider "1111"
 * -> "0101"
 * -> "0000"
 *
 * How can this be wrong? On each move, we cut one '1' kind of "free",
 * until all are free.
 *
 * --> We can also flip '0' positions, so
 * "1100"
 * "0101"
 * "0000"
 *
 * So there are some edgecases with no solution, and some 
 * with cnt/2+1 instead of cnt/2
 *
 * What about "110"
 * -> -1
 *
 * ... but mildley not intersting...still 2 WA, some more edgecases :/
 * ****
 * Lets look at it otherwise:
 * Parity of number of '1' does not change, so if cnt%2!=0 ans=-1
 * Else, if it is more than 2 '1's, we can cut with each flip one free,
 * so ans=cnt/2
 * Else it is 2 '1's, and if n==2 then ans=-1,
 * else if(n==3 && s[1]=='1') then ans=-1
 * else if(n==2) then ans=-1
 * else if both '1's next to next ans=2
 * else ans=1
 * ...but still WA :/
 *
 * What about "0110"?
 */
void solve() {
    cini(n);
    cins(s);

    int cnt=0;
    for(int i=0; i<n; i++) 
        cnt+=s[i]=='1';

    if(cnt%2 !=0) {
        cout<<-1<<endl;
        return;
    }

    if(cnt>2) {
        cout<<cnt/2<<endl;
        return;
    }

    assert(cnt<=2);
    // cnt==2
    
    if(n==3 && s[1]=='1') {
        cout<<-1<<endl;
        return;
    }

    if(s=="0110") {
        cout<<3<<endl;
        return;
    }

    /* check if next to next */
    for(int i=1; i<n; i++) {
        if(s[i]=='1' && s[i-1]=='1') {
            cout<<2<<endl;
            return;
        }
    }

    cout<<cnt/2<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
