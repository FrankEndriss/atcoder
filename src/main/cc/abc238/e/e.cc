/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

#include <atcoder/lazysegtree>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using S=int; /* type of values */
using F=int; /* type of upates */

S st_op(S a, S b) {
    return a+b;
}

/* This is the neutral element */
const S INF=1e9;
S st_e() {
    return 0;
}

/* This applies an update to some value. */
S st_mapping(F f, S x) {
    return min(f,x);
}

/* This combines two updates.  */
F st_composition(F f, F g) {
    return min(f,g);
}

/* This is the neutral update. 
 * It is similar to st_e() in some sense.  */
F st_id() {
    return 1;
}

using stree=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;

/**
 * If we got two overlapping segments, and know the sum of
 * the intersection, we can construct all parts.
 * If a segment is of size 1, we know that.
 *
 * So iterate all segments i, sorted by r, and try to match
 * intersections, in descending order of lr[i].second
 *
 * Matching is, that we search the interval with biggest l[j]<=r[i]
 * ***
 * Consider pairs of overlapping intervals.
 * If we know the sum of the intersecting part, we also know
 * the sum of the two nonoverlapping parts.
 *
 * So if we want to know sum(0,n), we need to find the sum of the intersection
 * of some sum(i,j), where we know sum(0,j) and and sum(i,n)
 *
 * Try recursive impl.
 */

const int iINF=1e9;
void solve() {
	cini(n);
	cini(q);

	vi data(n,1);
	stree seg(data);

	set<pii> lr;
	set<pii> rl;
	for(int i=0; i<q; i++) {
		cini(l); l--;
		cini(r); r--;
		lr.emplace(l,r+1);
		rl.emplace(r+1,l);
	}

	/* return true if sum(l,r) is known */
	function<bool(int,int)> go=[&](int l, int r) {
		auto it=lr.count({l,r});
		if(it==1)
			return true;
	};

}

signed main() {
    solve();
}
