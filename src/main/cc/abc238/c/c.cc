/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * let d(x)=10^(numDigits(x)-1)
 * f(x)=x-d(x)
 * So, first digit--
 *
 * Thats so clumsy annoying math problem... sic.
 * *****
 * There are 9 nums with 1 digit
 * -> 1+2+...+9
 * There are 90 nums with 2 digits
 * -> 1+2+...+90
 * There are 900 nums with 3 digits
 * -> 1+2+...+900
 * ...
*/
using mint = modint998244353;

void solve() {
	cini(n);

	int d=1;
	int nn=n;
	mint ans=0;
	mint nine=9;
	while(nn) {
		if(nn>=10)
			ans+=nine*nine;

		nine*=10;
		d*=10;
		nn/=10;
	}

	ans+=nine*(n-d);
	cout<<ans.val()<<endl;
}

signed main() {
    solve();
}
