/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * It must be that
 * s>=2*a, since each bit of a must be in x and y
 * Then we need to add the bits of s-2*a to x xor y.
 */
void solve() {
	cini(a);
	cini(s);

	if(s<2*a) {
		cout<<"No"<<endl;
		return;
	}

	int x=2*a;
	s-=x;

	/*
	cerr<<"a="<<bitset<61>(a)<<endl;
	cerr<<"s="<<bitset<61>(s)<<endl;
	cerr<<"s="<<bitset<61>(x)<<endl;
	*/

	if((s&a)==0)
		cout<<"Yes"<<endl;
	else
		cout<<"No"<<endl;
}

signed main() {
	cini(t);
	while(t--)
    		solve();
}
