/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/** TODO:
 * Observation:
 * We can immitate the  edges by having edges from i to i-1 with cost 0,
 * and edges from l[i] to r[i] with cost c[i].
 * Then simple dijkstra.
 */
int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(n);
    cini(m);
    vi l(m), r(m), c(m);
    for(int i=0; i<m; i++) {
        cin>>l[i]>>r[i]>>c[i];
    }

    vi id(m);
    iota(id.begin(), id.end(), 0);
    sort(id.begin(), id.end(), [&](int i1, int i2) {
        if(l[i1]==l[i2])
            return r[i1]<r[i2];
        else
            return l[i1]<l[i2];
    });

    const int INF=1e9;

    vi dp(n+1, INF);
    dp[0]=dp[1]=0;

    for(int i=0; i<m; i++) {
        for(int j=l[id[i]]; j<=r[id[i]]; j++) {
            dp[j]=min(dp[j], dp[l[id[i]]]+c[id[i]]);
        }
        prevR=r[id[i]];
    }
#ifdef DEBUG
    cout<<"dp=";
    for(auto i : dp)
        cout<<i<<" ";
    cout<<endl;
#endif
    if(dp[n]==INF)
        cout<<-1<<endl;
    else
        cout<<dp[n]<<endl;
}

