/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cinai(a,n);
    cinai(b,n);

    vector<pii> a1(n);
    vi b1(n);
    set<pii> sa;
    for(int i=0; i<n; i++) {
        a1[i].first=a[i];
        a1[i].second=i;
        b1[i]=b[i];
        sa.insert({a[i],i});
    }
    sort(a1.begin(), a1.end());
    sort(b1.begin(), b1.end());

    for(int i=0; i<n; i++) {
        if(b1[i]<a1[i].first) {
            cout<<"No"<<endl;
            return 0;
        }
    }

/** We need to check if last two unswapped elements break
 * the order, ie if the number of needed swaps is n-1 or less.
 * So find the case where n-1 swaps are needed.
 * How?
 * This is, if we swap starting left, we end up with the last
 * two elements in wrong order. This is, if we need to swap 
 * every element, and while this the last one gets moved to 
 * the pre-last position (or the pre-last to the last pos).
 * Lets simply simulate that.
 */

    const int INF=1e9;
    for(int i=0; i<n-2; i++) {
        if(a[i]<=b[i])  { // no swap needed
            cout<<"Yes"<<endl;
            return 0;
        }

/* find biggest a[j] which is a[j]<=b[i] and j>i */
        auto it=sa.upper_bound(make_pair(b[i], INF));
#ifdef DEBUG
cout<<"i="<<i<<" b[i]="<<b[i]<<" it->first="<<it->first<<" it->second="<<it->second<<endl;
#endif
        it--;

        while(it->second<=i)
            it--;

        swap(a[i], a[it->second]);
        sa.erase(it);
    }

    if(a[n-2]<=b[n-2] && a[n-1]<=b[n-1])
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;
    

}

