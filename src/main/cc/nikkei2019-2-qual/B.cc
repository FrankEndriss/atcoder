/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MOD=998244353;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    vi d(n);
    vi f(n);
    int maxD=-1;
    for(int i=0; i<n; i++) {
        cin>>d[i];
        f[d[i]]++;
        maxD=max(maxD, d[i]);
        if(i>0 && d[i]==0) {
            cout<<0<<endl;
            return 0;
        }
    }

    if(d[0]!=0) {
        cout<<0<<endl;
        return 0;
    }

    ll ans=1;
    for(int i=2; i<=maxD; i++) {
        for(int j=0; j<f[i]; j++) {
            ans*=f[i-1];
            ans%=MOD;
        }
    }
    cout<<ans<<endl;
}

