
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"




/**
 * majority == "more than the half"
 *
 * Just check all substrings, how to count?
 * ...no
 *
 * Observe that there is no need to check substring of len>4,
 * because if there would be one with a majority, we could cut it
 * in the middle, * and one of its halfes would break the majority.
 * example "aabca" -> "aab", "ca"
 *
 * So, how to check all strings of len 3?
 *
 * Let dp[i][j] be number of posibilities with
 * these 2 letters in last 2 positions.
 */
using mint=modint998244353;

void solve() {
    cini(n);
    cins(s);

    const int N=26;

    vector<vector<mint>> dp(N, vector<mint>(N));
    set<int> si;
    if(s[0]=='?') {
        for(int i=0; i<N; i++)
            si.insert(i);
    } else
        si.insert(s[0]-'a');

    set<int> sj;
    if(s[1]=='?') {
        for(int i=0; i<N; i++)
            sj.insert(i);
    } else
        sj.insert(s[1]-'a');

    for(int i : si)
        for(int j : sj)
            if(i!=j)
                dp[i][j]=1;

    for(int pos=2; pos<n; pos++) {
        const char c =s[pos];
        vector<vector<mint>> dp0(N, vector<mint>(N));
        for(int i=0; i<N; i++)
            for(int j=0; j<N; j++) {
                if(i==j)
                    continue;

                if(c!='?') {
                    const int cc=c-'a';   /* 'a'==1 'z'==26 */
                    if(cc!=i && cc!=j)
                        dp0[j][cc]+=dp[i][j];
                } else {
                    for(int b=0; b<N; b++)
                        if(b!=i && b!=j)
                            dp0[j][b]+=dp[i][j];
                }
            }
        dp.swap(dp0);
    }


    mint ans=0;
    for(int i=0; i<N; i++)
        for(int j=0; j<N; j++)
            ans+=dp[i][j];

    cout<<ans.val()<<endl;
}

signed main() {
    solve();
}
