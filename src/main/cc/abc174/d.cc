/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*
 * Binary string W/R
 * Finally string must be RRRWWW
 *
 * We need to remove all W left of the rightmost R,
 * or all R rigth of the leftmost W
 * dp
 * We can swap all pairs of wrong placed chars,
 * and need to flip the remaining.
 */
void solve() {
    cini(n);
    cins(s);
    vi cntR(n+1);
    vi cntW(n+1);

    for(int i=1; i<=n; i++)  {
        cntR[i]=cntR[i-1];
        cntW[i]=cntW[i-1];
        if(s[i-1]=='R')
            cntR[i]++;
        else
            cntW[i]++;
    }

    int ans=1e9;
    for(int i=0; i<=n; i++) {
        ans=min(ans, max(cntW[i], cntR[n]-cntR[i]));
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
