/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using mint=modint998244353;
const int N=2e5+7;

vector<mint> fac(N);

const bool initFac=[]() {
    fac[0]=1;
    fac[1]=1;
    for(int i=2; i<N; i++) {
        fac[i]=fac[i-1]*i;
    }
    return true;
}();

/** see https://cp-algorithms.com/combinatorics/binomial-coefficients.html */
mint nCr(int n, int k) {
    if(k==0 || k==n)
        return mint(1);
    if(k==1 || k==n-1)
        return mint(n);
    if(k>n/2)
        return nCr(n, n-k);

    return fac[n]*fac[k].inv()*fac[n - k].inv();
}

/**
 * What to do with 0 elements?
 * Once we remove them, they simply go away.
 * Otherwise we can put together all other elements,
 * independent of the value.
 *
 * A seq of len-i without 0:
 * (i-1) * (i-2) * ... * 1
 * -> fac[i-1];
 *
 * Let i=elements!=0, and j=freq(0)
 * ans=fac[i]*nCr(n, j)
 */
void solve() {
    cini(n);

    int cnt0=0;
    int cnt=0;
    for(int i=0; i<n; i++) {
        cini(aux);
        if(aux==0) {
            cnt0++;
        } else
            cnt++;
    }

    mint ans=fac[cnt-1]; //*nCr(n,cnt0);
    cout<<ans.val()<<endl;
}

signed main() {
    solve();
}
