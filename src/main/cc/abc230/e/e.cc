/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Iterate i=1..sqrt(n)
 *
 * N/i contributes
 * N/(i-1) - N/i numbers contribute i
 *
 * n=3: 3+1+1=5
 * n=4: 4+2+1+1=8
 * n=5: 5+2+1+1+1=10
 * n=6: 6+3+2+1+1+1=14
 * n=9: 9+4+3+2+1+1+1+1+1=23
 * n=10 10+5+3+2+2+1+1+1+1+1=27
 *
 * I dont get it :/
 */
void solve() {
    cini(n);

    if(n==1) {
        cout<<1<<endl;
        return;
    } else if(n==2) {
        cout<<3<<endl;
        return;
    } else if(n==3) {
        cout<<5<<endl;
        return;
    }

    vi num;
    int ans=0;
    for(int i=1; i*i<=n; i++) {
        int d=n/i;
        ans+=d;
        num.push_back(d);
    }

    const int nn=num.size();
    for(int i=1; i<nn; i++) {
        ans+=(num[i-1]-num[i])*(n/num[i-1]);
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}
