
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Check all pairs of towns, and the gcd'ed spells, count them.
 */
void solve() {
    cini(n);
    vi x(n);
    vi y(n);
    for(int i=0; i<n; i++) {
        cin>>x[i]>>y[i];
    }

    set<pii> s;
    for(int i=0; i<n; i++) {
        for(int j=0; j<n; j++) {
            if(i==j)
                continue;

            int dx=x[i]-x[j];
            int dy=y[i]-y[j];
            if(dx!=0 && dy!=0) {
                int g=gcd(abs(dx),abs(dy));
                dx/=g;
                dy/=g;
                s.emplace(dx,dy);
                //cerr<<"dx="<<dx<<" dy="<<dy<<endl;
            } else if(dx==0) {
                s.emplace(0,dy/abs(dy));
            } else if(dy==0) {
                s.emplace(dx/abs(dx),0);
            } else
                assert(false);
        }
    }
    cout<<s.size()<<endl;
}

signed main() {
    solve();
}
