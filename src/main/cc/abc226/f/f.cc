
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int MOD=998244353;
const int N=100;


int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

int fraction(const int nom, const int denom) {
    return mul(nom, inv(denom));
}



vector<int> fac(N);

const bool initFac=[](const int mod) {
    fac[1]=1;
    for(int i=2; i<N; i++) {
        fac[i]=mul(fac[i-1], i, mod);
    }
    return true;
}(MOD);

/** see https://cp-algorithms.com/combinatorics/binomial-coefficients.html */
int nCr(int n, int k) {
    if(k==0 || k==n)
        return 1;
    if(k==1 || k==n-1)
        return n;
    if(k>n/2)
        return nCr(n, n-k);

    return mul(mul(fac[n], inv(fac[k])), inv(fac[n - k]));
}
/**
 * The score of a perm is the len of the longest cycle in it.
 * Let c(i) be the number of such perms with maxlen=i
 * So we need to count them:
 * c(n)=1
 * c(n-1)=n=2*nCr(n,1)*fac[1]
 * c(n-2)=2*nCr(n,2)*fac[2]
 * ...
 * 
 * if n<=n2 we need to consider the perms where the other perms
 * create a longer cycle.
 *
 * c(n/2)=nCr(n, n/2) - 
 *    for( j: n/2+1..n-n/2)     // all longer cycle lens
 *      nCr(n-n/2, j)
 *
 * *** 
 * What about directions? Simply double each perm?
 * Also note that we need to pow the score.
 *
 * ...to complecated :/
 */
using mint=modint998244353;
void solve() {
    cini(n);
    cini(k);

    fac[0]=1;
    mint ans=1; /* the zero scream perm */

    for(int len=n; len>=2; len--) {
        mint lans=nCr(n, len);
        lans*=2;
        lans*=fac[n-len];
        for(int j=len+1; j<=n-len; j++) {
            mint lans2;
            lans2=nCr(n-len, j);
            lans2*=2;
            lans2*=fac[n-len-j];
            lans-=lans2;
        }

        mint mlen=len;
        lans=lans*mlen.pow(k);
        ans+=lans;
    }

    cout<<ans.val()<<endl;

}

signed main() {
    solve();
}
