
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We can assume that each a5 is carried by an b5,
 * else not possible.
 * So there are only 4 weigths, but still 5 peron kinds.
 *
 * Also all b[0] carry a[0] or nothing.
 * So there are only 4 person kinds, 2..5
 */
void solve() {
    vi a(6);
    vi b(6);
    for(int i=1; i<6; i++) 
        cin>>a[i];
    for(int i=1; i<6; i++) 
        cin>>b[i];

    /* check the heavy 5 packages, can be carried by b[5] only */
    if(a[5]>b[5]) {
        cout<<"No"<<endl;
        return;
    }

    b[5]-=a[5];
    a[5]=0;

    /* check b[1] people, they can carry only a[1] packages */
    int mi=min(a[1],b[1]);
    a[1]-=mi;

    /* check b[2] people, it is allways optimal to give them all b[2] 
     * packages, then remaining a[1] packages.
     */
    mi=min(a[2],b[2]);
    a[2]=max(0LL, a[2]-mi);
    b[2]-=mi;
    a[1]=max(0LL, a[1]-b[2]*2);

    /* check b[3] people, give them 
     * a[3]
     * a[2]+a[1], 
     * 3*a[1].
     */
    mi=min(a[3],b[3]);
    a[3]-=mi;
    b[3]-=mi;
    mi=min(b[3], a[2]);
    b[3]-=mi;
    a[2]-=mi;
    a[1]=max(0LL, a[1]-mi);

    mi=min(b[3], (a[1]+2)/3);
    a[1]=max(0LL, a[1]-mi*3);

    /* check b[4] people.
     * a[4]
     * a[3]+a[1]
     * 2*a[2]
     * a[2]+2*a[1]
     * 4*a[1]
     */
    mi=min(a[4],b[4]);
    a[4]-=mi;
    b[4]-=mi;
    mi=min(b[4], a[3]);
    b[4]-=mi;
    a[3]-=mi;
    a[1]=max(0LL, a[1]-mi);

    mi=min(b[4], (a[2]+1)/2);
    b[4]-=mi;
    a[2]=max(0LL, a[2]-mi*2);

    // a2+a1+a1
    mi=min(b[4], a[2]);
    b[4]-=mi;
    a[2]-=mi;
    a[1]=max(0LL, a[1]-mi*2);

    // 4*a[1]
    a[1]=max(0LL, a[1]-b[4]*4);

    /* check b[5] people
     * a[4]+a[1]
     * a[3]+a[2]
     * a[3]+2*a[1]
     * 2*a[2]+a[1]
     * a[2]+3*a[1]
     * 5*a[1]
     */

    mi=min(b[5], a[4]);
    b[5]-=mi;
    a[4]-=mi;
    a[1]=max(0LL, a[1]-mi);

    // a[3]+a[2]
    mi=min(b[5], a[3]);
    b[5]-=mi;
    a[3]-=mi;
    a[2]=max(0LL, a[2]-mi);

    ...TODO

    mi=min(b[5], (a[2]+1)/2);
    b[5]-=mi;
    a[2]=max(0LL, mi*2);
    a[1]=max(0LL, a[1]-mi);

    mi=min(b[5], a[2]);
    b[5]-=mi;
    a[2]-=mi;
    a[1]=max(0LL, a[1]-mi*3);

    a[1]=max(0LL, b[5]*5);


    bool ok=true;
    for(int i=1; i<5; i++) 
        if(a[i])
            ok=false;

    if(ok)
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
