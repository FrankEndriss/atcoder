
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * A tree, topoligical order.
 *
 * Then do a bfs from vertex n, each reacheable counts.
 */
void solve() {
    cini(n);
    vvi adj(n); /* vertex */
    vi cost(n);
    for(int i=0; i<n; i++) {
        cin>>cost[i];
        cini(k);
        for(int j=0; j<k; j++) {
            cini(v);
            adj[i].push_back(v-1);
        }
    }

    queue<int> q;
    q.push(n-1);
    int ans=cost[n-1];
    vb vis(n);
    vis[n-1]=true;
    while(q.size()) {
        int v=q.front();
        q.pop();
        for(int chl : adj[v]) {
            if(!vis[chl]) {
                ans+=cost[chl];
                vis[chl]=true;
                q.push(chl);
            }
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}
