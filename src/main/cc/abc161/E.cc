/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* First, find earliest workdays possible.
 * For each one check if it is possible to work enough days if not
 * worked on that day.
 * Speedup use memoization.
 * ...no.
 *
 * Greedy.
 * find earliest possible workdays.
 * from last to first, try move that workday to a later day.
 * if not possible that day is bound, else not.
 *
 */
void solve() {
    cini(n);
    cini(k);
    cini(c);
    cins(s);

    vi fdays;

    /* find earliest possible workdays */
    for(int i=0; fdays.size()<k && i<n; i++) {
        if(s[i]=='o') {
            fdays.push_back(i);
            i+=c;
        }
    }
    assert(fdays.size()==k);

/*
cout<<"fdays: ";
for(int i : fdays)
    cout<<i<<" ";
cout<<endl;
*/

    vi ans;

    int latest=n-1; // latest date next workday can be placed
    for(int i=fdays.size()-1; i>=0; i--) {
        bool canMove=false;
        for(int j=latest; !canMove && j>fdays[i]; j--) {
            if(s[j]=='o') {
                canMove=true;
                latest=j-c-1;
            }
        }

        if(!canMove) {
            ans.push_back(fdays[i]);
            latest=fdays[i]-c-1;
        }
    }
    sort(ans.begin(), ans.end());
    for(int i : ans)
        cout<<i+1<<endl;
    


}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

