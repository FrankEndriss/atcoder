

/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }


typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* increment lunlun number */
void inc(vi &a) {
    bool all9=true;
    for(int i=0; i<a.size(); i++) {
        if(a[i]!=9)
            all9=false;
    }
    if(all9) {
        for(int i=0; i<a.size(); i++)
            a[i]=0;
        a.push_back(1);
        return;
    }

    for(int i=0; i+1<a.size(); i++) {
        if(a[i]<a[i+1]+1 && a[i]<9) {
            a[i]++;
            for(int j=i-1; j>=0; j--)
                a[j]=max(0LL, a[j+1]-1);
            return;
        }
    }
    a.back()++;
    for(int i=0; i+1<a.size(); i++) {
        a[a.size()-2-i]=max(0LL, a[a.size()-1-i]-1);
    }
}
    

/* every digit can have at most 3 values */
void solve() {
    cini(n);
    vi a;
    a.push_back(1);
    for(int i=1; i<n; i++)  {
        inc(a);
/*
for(int i=0; i<a.size(); i++)
    cout<<a[a.size()-1-i];
cout<<endl;
*/
    }

    while(a.size()) {
        cout<<a.back();
        a.pop_back();
    }
    cout<<endl;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
        solve();
}

