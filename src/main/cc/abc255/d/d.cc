/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Prefix sums
 */
void solve() {
    cini(n);
    cini(q);
    cinai(a,n);
    sort(all(a));
    vi pre(n+1);
    for(int i=0; i<n; i++) 
        pre[i+1]=pre[i]+a[i];

    for(int i=0; i<q; i++) {
        cini(x);
        auto it=lower_bound(all(a), x);
        int cnt=distance(a.begin(), it);

        int ans=0;
        if(it!=a.begin()) {
            ans+=abs(x*cnt-pre[cnt]);
        }
        if(it!=a.end()) {
            int sum=pre[n]-pre[cnt];
            cnt=n-cnt;
            ans+=abs(x*cnt-sum);
        }
        cout<<ans<<endl;
    }
}

signed main() {
        solve();
}
