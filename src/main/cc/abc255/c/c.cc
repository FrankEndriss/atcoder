/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * A, A+D, A+2*D,...,A+N*D
 *
 * Consider non-negative d, or mirror all numbers.
 * There are 3 cases:
 * x<a, x>a+n*d, or x in between them.
 *
 *
 * But does not work for some cases.
 * Edgecases?
 * Overflow?
 *
 * idk :/
 */
void solve() {
    using lll=__int128;
    cini(ix);
    cini(ia);
    cini(id);
    cini(nn);

    lll x=ix;
    lll a=ia;
    lll d=id;
    lll n=nn;

    if(d<0) {
        x=-x;
        a=-a;
        d=-d;
    }

    if(x<=a) {
        cout<<(int)(a-x)<<endl;
    } else if(x>=a+(n-1)*d) {
        cout<<(int)(x-(a+(n-1)*d))<<endl;
    } else {
        lll diff=(x-a)%d;
        cout<<min((int)diff, (int)(d-diff))<<endl;
    }
}

signed main() {
        solve();
}
