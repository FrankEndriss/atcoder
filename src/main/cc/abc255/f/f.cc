/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider p[0], its root.
 * If i[0]==p[0], then left child of p[0]==null
 * else p[0].left==p[1]
 * if i[0]==p[1], then left child of p[1]==null
 * else p[1].left=p[2]
 * ...
 * then, how?
 *
 * Consider steps t.
 * Consider iterating i[]
 * Obviously every i[] must be seen before in p.
 *
 * So, consider pt[] and it[], the times where
 * the elements are choosen.
 *
 */
void solve() {
    cini(n);
    cinai(p,n);
    cinai(k,n);

    for(int i=0; i<n; i++) {
        p[i]--;
        k[i]--;
    }

    vi pt(n);
    vi kt(n);
    int pidx=0;
    int kidx=0;

    vvi adj(n, vi(2, -1));
    vi pp(n, -1);

    vb vis(n);
    int t=0;
    int cur=p[0];
    while(true) {
        while(!vis[k[kidx]]) {
            pt[p[pidx]]=t++;
            vis[p[pidx]]=true;
            pidx++;
        }
    }
}

signed main() {
        solve();
}
