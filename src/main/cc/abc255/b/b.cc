/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Max distance from ligth person to not light person?
 * Incredible bad problem statement.
 *
 * ....
 * No.
 * Of all unlight persons, find each min dist to light person.
 * From these min find the max.
 */
void solve() {
    cini(n);
    cini(k);

    cinai(a,k);
    vb l(n);
    for(int i=0; i<k; i++)
        l[a[i]-1]=true;

    vi x(n);
    vi y(n);
    for(int i=0; i<n; i++) 
        cin>>x[i]>>y[i];

    double ans=0;
    for(int i=0; i<n; i++) {
        if(l[i])
            continue;

        double lans=1e18;
        for(int j=0; j<n; j++) {
            if(!l[j]) 
                continue;

            lans=min(lans, hypot(x[i]-x[j], y[i]-y[j]));
        }

        ans=max(ans, lans);
    }

    cout<<ans<<endl;

}

signed main() {
        solve();
}
