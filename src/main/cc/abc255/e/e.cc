/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * All a[i] are determined by a[0]
 * By a[0]+=1 We decrement also all other a[i] on odd positions,
 * and increment all other a[i] on even positions.
 *
 * Note very small, M=10
 * So check each position for inc/decrements that fits the lucky
 * numbers.
 * Choose the best inc/decrement.
 */
void solve() {
    cini(n);
    cini(m);

    cinai(s,n-1);
    cinai(x,m);

    vi a(n);
    a[0]=0;
    for(int i=1; i<n; i++)
        a[i]=s[i-1]-a[i-1];

    map<int,int> f;
    for(int i=0; i<n; i+=2) {
        for(int j=0; j<m; j++) 
            f[a[i]-x[j]]++;
    }
    for(int i=1; i<n; i+=2) {
        for(int j=0; j<m; j++) 
            f[x[j]-a[i]]++;
    }

    int ans=0;
    for(auto ent : f)
        ans=max(ans, ent.second);

    cout<<ans<<endl;
}

signed main() {
        solve();
}
