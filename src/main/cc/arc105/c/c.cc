
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


template <typename E>
class SegmentTree {
  private:
    vector<E> tree;
    E neutral;
    function<E (E, E)> cumul;

    int treeDataOffset;
    const int ROOT = 1;

    inline int leftChild(const int treeIdx) {
        return  treeIdx * 2;
    }
    inline int rightChild(const int treeIdx) {
        return treeIdx * 2 + 1;
    }
    inline int parent(const int treeIdx) {
        return treeIdx / 2;
    }
    inline bool isOdd(const int idx) {
        return (idx & 0x1);
    }

  public:
    /** @param beg, end The initial data.
     * @param pNeutral the Element neutral to cumul, ie 0 for sum, 1 for product etc
     * @param pCumul The cumulative function to create the "sum" of two nodes.
    **/
    template<typename Iterator>
    SegmentTree(Iterator beg, Iterator end, E pNeutral, function<E (E, E)> pCumul) {
        neutral=pNeutral;
        cumul=pCumul;
        treeDataOffset=end-beg;
        tree=vector<E>(treeDataOffset*2, pNeutral);

        int i=treeDataOffset;
        for (auto it=beg; it!=end; it++)
            tree[i++] = *it;

        for (int j=treeDataOffset - 1; j>=1; j--)
            tree[j] = cumul(tree[leftChild(j)], tree[rightChild(j)]);
    }

    /** Updates all elements in interval(idxL, idxR].
    * iE add 42 to all elements from 4 to inclusive 7:
    * update(4, 8, [](int i1) { return i1+42; });
    */
    void updateRange(int pIdxL, int pIdxR, function<E (E)> apply) {
        pIdxL+=treeDataOffset;
        pIdxR+=treeDataOffset;
        for(int i=pIdxL; i<pIdxR; i++)
            tree[i]=apply(tree[i]);

        while (pIdxL != ROOT) {
            pIdxL=parent(pIdxL);
            pIdxR=max(pIdxL, parent(pIdxR-1));
            for(int i=pIdxL; i<=pIdxR; i++)
                tree[i]=cumul(tree[leftChild(i)], tree[rightChild(i)]);
        }
    }

    /** Updates the data at dataIdx to value returned by apply. */
    void updateApply(int dataIdx, function<E(E)> apply) {
        updateSet(dataIdx, apply(get(dataIdx)));
    }

    /** Updates the data at dataIdx by setting it to value. */
    void updateSet(int dataIdx, E value) {
        int treeIdx = treeDataOffset + dataIdx;
        tree[treeIdx] = value;

        while (treeIdx != ROOT) {
            treeIdx = parent(treeIdx);
            tree[treeIdx] = cumul(tree[leftChild(treeIdx)], tree[rightChild(treeIdx)]);
        }
    }

    /** @return value at single position, O(1) */
    E get(int idx) {
        return tree[idx+treeDataOffset];
    }

    /** @return the cumul(idxL, idxR], iE idxL inclusive, idxR exclusive. */
    E get(int pIdxL, int pIdxR) {
        int idxL = pIdxL + treeDataOffset;
        int idxR = pIdxR + treeDataOffset;
        E cum = neutral;
        while (idxL < idxR) {
            if (isOdd(idxL)) { // left is odd
                cum = cumul(cum, tree[idxL]);
                idxL++;
            }
            if (isOdd(idxR)) {
                idxR--;
                cum = cumul(cum, tree[idxR]);
            }
            idxL = parent(idxL);
            idxR = parent(idxR);
        }
        return cum;
    }

};

/* can we brute force this?
 * 8!==40320
 * 7!==5040
 *
 * Note that we can reverse the final order of camels.
 * Anyway we should start (or end) with the heaviest camel.
 * So there are only 7! possible camel orderings.
 *
 * if any single camel is heavier than the lightest bridge then -1
 *
 * **********************************
 * dp[usedCamels][j]= min possible dist between first camel and last camel 
 *   if usedCamels and last of them is j.
 *
 * -> This does not work, since we still have to consider the prelast camel
 *  because there could be 3 on a bridge... sic :/
 *  ***********************************
 *
 * What about the brute-force for a given order?
 * We would need to consider all subgroups and their weight.
 * The smallest colapsing bridge of that weight determines
 * the min dist between first camel of group and last camel of group.
 * With this we get some min distances, which determines the min
 * sum of distances.
 *
 * How much subgroups are there?
 * One of size 8, nCr(8,1) of size 1, nCr(8,2) of size 2...
 *
 * nCr(8,4)==70
 * so ~300 subgroupbs
 *
 * With segment tree we can find min bridge in O(logN)
 * should fit!
 * seg[i]= length of bridge at position i
 * We need to query the max length of a bridge with weight
 * less than some value.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);

    cinai(w,n);
    sort(all(w), greater<int>());   /* w[0]=heaviest camel */

    vector<pii> vl(m);
    for(int i=0; i<m; i++) 
        cin>>vl[i].second>>vl[i].first;

    sort(all(vl), greater<pii>()); 

    vi l(m);
    for(int i=0; i<m; i++) 
        l[i]=vl[i].second;
    SegmentTree<int> seg(all(l), INF, [](int i1, int i2) { return max(i1, i2); });

    vi camels(n);
    iota(all(camels), 0LL); /* camel 0 will go first anyway */

    /* return min dist for current permutation and gives sizes */
    function<int(vi,int)> go=[&](vi gsizes, int used) {
        int ans=INF;
        if(used==n-1) {
            vvi d(n, vi(n));    /* d[i][j]=min dist between i and j */
            int idx=0;
            for(int g=0; g<gsizes.size(); g++) {
                int wgroup=0;
                for(int i=0; i<gsizes[g]; i++)
                    wgroup+=w[camels[idx+i]];
                        
                auto it=upper_bound(all(vl), make_pair(wgroup, -INF), greater<pii>());
                int tidx=it-vl.begin();
                int minlen=seg.query(tidx, m);
                d[camels[idx]][camels[idx+gsizes[g]]]=minlen;
            }

            vi lans(n);
            for(int i=1; i<n; i++) {
                //...
            }

        } else {
            for(int i=gsizes.back(); i>=1; i--) {
                if(used+i<=n-1) {
                    gsizes.push_back(i);
                    ans=min(ans, go(gsizes, used+i));
                    gsizes.pop_back();
                }
            }
        }

    };

    int ans=INF;
    do {
        for(int i=8; i>=1; i--) {
            vi gsizes(1,i);
            ans=min(ans, go(gsizes, i));
        }
    }while(next_permutation(camels.begin()+1, camels.end());

    cout<<ans<<endl;
}

signed main() {
    for(int i=1; i<8; i++) {
        cout<<"i="<<i<<" nCr(8,i)="<<nCr(8,i)<<endl;
    }
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
