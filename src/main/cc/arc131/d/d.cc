/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * It is allways optimal to use as much arrows as possible.
 * Go from left to right.
 * Given that the leftmost arrow hits somewhere to get
 * points, it is allways optimal to hit the leftmost possible
 * location in that area.
 * Assume it is allways optimal to us one on the negative end.
 *
 * So there are not more than N possible starting points.
 * How to find the score foreach one in less than O(N)?
 *
 * Its a dp.
 * Foreach area consider to 
 * -put first arrow in leftmost position, and that much arrows into area
 *  that the leftmost arrow of next area is _not_ at its begin.
 * -put at most arrows into that area so that the leftmost arrow
 *  of the next area _is_ at its begin.
 *
 * Result after each area is a list of score,offs
 * with non increasing score,
 * and increasing offs.
 * Does this work out, why?
 * ***
 * After like 1 hour:
 * Note that n is the max number of arrows :/
 */
void solve() {
    cini(n);
    cini(m);
    cini(d);
    cinai(r,m+1);
    cinai(s,m);

    map<int,int> dp;    /* offs,score */
    dp[0]=0;
    for(int i=m; i>0; i--) {
        map<int,int> dp0;
        dp0[0]=0;   /* consider no previous arrow */
        int sz=r[i]-r[i-1]; /* size of current segment */
        if(i==1)
            sz*=2;

        for(auto [offs,score] : dp) {
            if(offs>=sz) {
                dp0[offs-sz]=max(dp0[offs-sz], score);
                continue;
            }

            int cnt0=max(0LL, sz-offs)/d;
            dp0[0]=max(dp0[0],score+cnt0*s[i-1]);

            int cnt1=cnt0+1;
            int offs2=offs+cnt1*d-sz;
            assert(offs2<d);
            dp0[offs2]=max(dp0[offs2], score+cnt1*s[i-1]);
            /* consider to optimize dp0 since else it is O(N*M) */
        }
        dp.swap(dp0);
    }

    for(int i=2; i<=m; i++) {
        map<int,int> dp0;
        dp0[0]=0;
        int sz=r[i]-r[i-1]; /* size of current segment */

        for(auto [offs,score] : dp) {
            if(offs>=sz) {
                dp0[offs-sz]=max(dp0[offs-sz], score);
                continue;
            }

            int cnt0=max(0LL, sz-offs)/d;
            dp0[0]=max(dp0[0],score+cnt0*s[i-1]);

            int cnt1=cnt0+1;
            int offs2=offs+cnt1*d-sz;
            assert(offs2<d);
            dp0[offs2]=max(dp0[offs2], score+cnt1*s[i-1]);
            /* consider to optimize dp0 since else it is O(N*M) */
        }

        dp.swap(dp0);
    }

    int ans=0;
    for(auto ent : dp)
        ans=max(ans, ent.second);

    cout<<ans<<endl;
}

signed main() {
    solve();
}
