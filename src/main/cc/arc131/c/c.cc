/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider some subset of a[] to have remaining xorsum==0.
 * So, for odd sized subsets player 1 can win, 
 * for even sized player 2.
 * There could be a lot such subsets.
 *
 * Check if first number ends the game.
 * Then ???
 *
 * Actually we change the sum^a[i] by choosing that a[i].
 * So current player wants to choose that a[i] that makes the sum 0.
 * If not possible, he wants to choose an a[i] for that no exists any
 * a[j] so that sum^a[i]^a[j]==0
 *
 * How to find a good a[i]?
 * ***
 * The remaining cookies have xorsum==0 if the eaten cookies
 * have the initial xorsum.
 *
 * So both player want to make the eaten sum eq the initial sum.
 * ***
 * Consider this to be a graph, where the value of a path
 * is the xor of the vertex labels.
 * But note that the graph is huge, since all vertex are interconnected.
 * ***
 * Let p[0] be the a[i] choosen in first move.
 * How to find if one (maybey the first) player can force to win?
 * If first can, then because first move wins the game,
 * or on second move there are only pairs available
 * that together eq inisum^p[0]
 * How to check that?
 * ***
 * Why cookies are distinct?
 * Allways last move wins?
 */
void solve() {
    cini(n);

    vs ans={ "Lose", "Win" };
    cout<<ans[n%2]<<endl;
    return;

    int sum=0;
    set<int> a;
    for(int i=0; i<n; i++) {
        cini(aux);
        sum^=aux;
        a.insert(aux);
    }
    const int inisum=sum;
    sum=0;

    for(int i=0; i<n; i++) {
        auto it=a.find(inisum^sum);
        if(it!=a.end()) {
            cout<<ans[i%2]<<endl;
            return;
        }

        int done=false;
        for(it=a.begin(); it!=a.end(); it++) {
            int val=sum^(*it);
            auto it2=a.find(inisum^val);
            if(it2==a.end()) {
                a.erase(it);
                sum=val;
                done=true;
                break;
            }
        }
        if(!done) {
            cout<<ans[(i+1)%2]<<endl;
            return;
        }
    }

    assert(false);
    //cout<<ans[n%2]<<endl;
}

signed main() {
    solve();
}
