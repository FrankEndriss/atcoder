/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We can allways give a square the fith color since there are 
 * only 4 sourroundings.
 */
void solve() {
    cini(h);
    cini(w);

    cinas(a,h);

    const vi dx={ -1, 1, 0, 0 };
    const vi dy={ 0, 0, -1, 1 };

    for(int i=0; i<h; i++)
        for(int j=0; j<w; j++)  {
            if(a[i][j]!='.')
                continue;

            vb vis(5);
            for(int k=0; k<4; k++) {
                int ii=i+dx[k];
                int jj=j+dy[k];
                if(ii>=0 && ii<h && jj>=0 && jj<w && a[ii][jj]!='.') {
                    vis[a[ii][jj]-'1']=true;
                }
            }

            for(int k=0; k<5; k++) 
                if(!vis[k]) {
                    a[i][j]='1'+k;
                    break;
                }
        }

    for(int i=0; i<h; i++)
        cout<<a[i]<<endl;
}

signed main() {
    solve();
}
