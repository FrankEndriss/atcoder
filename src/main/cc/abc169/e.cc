/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Median can be integer or .5
 * We need to find smallest possible med
 * and biggest possible. All between should
 * be possible.
 *
 * If n is odd .5 not possible.
 * There are segments for x...
 *
 * Evt:
 *
 *
 */
void solve() {
    cini(n);
    vector<pii> evt;
    for(int i=0; i<n; i++) {
        cini(a);
        cini(b);
        evt.push_back({a,-1});
        evt.push_back({b, 1});
    }

    sort(evt.begin(), evt.end());

// assume n==odd

    int ans=0;
    int cnt=0;  // open intervals
    int L=0;
    for(int i=0; i<evt.size(); i++) {
        if(evt[i].second<0) {
            cnt++;
            if(cnt==(n+1)/2)
                L=evt[i].first;
        } else {
            cnt--;
            if(cnt==(n+1/2)-1) {
                if(n&1) {
                    ans+=evt[i].first-L+1;
                } else {
                    int lcnt=evt[i].first-L+1;
                    if(lcnt>1)
                        lcnt=lcnt+lcnt-1;
                    ans+=cnt;
                }
            }
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
