
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* First we need to find the number of cakes per sum.
 * This is somehow a dp.
 * Let dp2[i]=number of two criteria with sum i
 * for 1: 0
 * for even i: i-1
 * for odd  i: i-1
 * dp2[4]={1,3}, {3,1}, {2,2} -> 3
 * dp2[5]={1,4}, {2,3}...and reverse, -> 4
 *
 * Let dp[i]=num of three crit with sum i
 * dp[3]=1
 * dp[4]=3*sum(...
 * This does now work out, since if the first crit differs from the two
 * others, there are 3 permutations, else there are two or one.
 * So we need to find that number otherwise :/
 * How does this "triple nCr" work?
 *
 * If we would iterate the triples with sum=x, then we would
 * for(int i=1; i<x; i++)
 * for(int j=1; i+j<x; j++)
 *   ans.push_back({i,j,x-(i+j)});
 *
 * dp3[i]=sum(dp2[j]) for j=0..i-1
 *
 * Then find the sum of the kth cake.
 */
void solve() {
    cini(n);
    cini(k);


    // TODO

}

signed main() {
    solve();
}
