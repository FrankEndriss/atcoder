
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to find some subseq sum which is eq some other subseq sum.
 * Sum is build mod 200.
 *
 * there are 2^200 sum, so we cannot bruteforce.
 * But we can take all numbers modulo 200, then there are much less possible sums.
 *
 * If there are two equal elements we simply choose them.
 * Else find a pair of numbers beeing eq another single element.
 * else...
 *
 * *** 
 * Can we find for some given n the subseqs resulting in that n?
 * -> yes, knapsack
 *
 * We collect foreach sum the list of idx leading to that sum.
 * Once we find a non empty list, we got two different lists, 
 * and output them.
 *
 * *****
 * After an hour and some failed submissions
 * still implementation errors... so demotivating :/
 *
 * ***
 * Ok, key observation is, that any 201 different subsets include
 * at least one pair of equals sums, caused by pigeon.
 * So we simply construct 201 such seqs (by using all combis of first 8 elements)
 * and output the twice found one.
 */
void solve() {
    cini(n);
    cinai(a,n);

    const int cnt=min(8LL,n);
    const int nn=1<<cnt;

    vvi ans(2);

    vi s(200);
    for(int i=1; i<nn; i++) {
        int sum=0;
        for(int j=0; j<cnt; j++) 
            if(i&(1<<j))
                sum+=a[j];

        sum%=200;
        if(s[sum]!=0) {
            for(int j=0; j<cnt; j++) {
                if(i&(1<<j))
                    ans[0].push_back(j+1);
                if(s[sum]&(1<<j))
                    ans[1].push_back(j+1);
            }

            cout<<"Yes"<<endl;
            cout<<ans[0].size()<<" ";
            for(int j : ans[0])
                cout<<j<<" ";
            cout<<endl;
            cout<<ans[1].size()<<" ";
            for(int j : ans[1])
                cout<<j<<" ";
            cout<<endl;
            return;
        } else
            s[sum]=i;
    }

    cout<<"No"<<endl;
}

signed main() {
    solve();
}
