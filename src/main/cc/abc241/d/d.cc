/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
void solve() {
    cini(q);
    multiset<int> v;

    for(int i=0; i<q; i++) {
        cini(t);
        if(t==1) {
            cini(a);
            v.insert(a);
        } else if(t==2) {
            cini(x);
            cini(k);

            auto it=v.upper_bound(x);
            if(it==v.begin()) {
                cout<<-1<<endl;
                continue;
            }
            it--;

            bool done=false;
            for(int j=1; !done && j<k; j++) {
                if(it!=v.begin())
                    it--;
                else {
                    cout<<-1<<endl;
                    done=true;
                }
            }
            if(!done) {
                if(it==v.end())
                        cout<<-1<<endl;
                else
                    cout<<*it<<endl;
            }

        } else if(t==3) {
            cini(x);
            cini(k);
            auto it=v.lower_bound(x);
            bool done=false;
            for(int j=1; !done && j<k; j++) {
                if(it!=v.end())
                    it++;
                else {
                    cout<<-1<<endl;
                    done=true;
                }
            }
            if(!done) {
                if(it==v.end())
                        cout<<-1<<endl;
                else
                    cout<<*it<<endl;
            }
        }
    }
}

signed main() {
    solve();
}
