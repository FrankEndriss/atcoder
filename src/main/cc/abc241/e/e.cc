/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

//#define endl "\n"


/**
 * This somehow produces a circle.
 *
 * ...But the circle is long, it is defined by 
 * sum%n and position, which is N*N
 * :/
 * How does max(a[i])=1e6 comes into play?
 *
 * Nah, it is a simple circle on max n positions, each a[i] moves the position
 * by a[i]%n.
 *
 * ...Another nah, its not a circle.
 * It is a prefix, then a circle :/
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    vb vis(n);
    vis[0]=true;
    vi b;

    int sum=0;
    b.push_back(0);
    int cstart=-1;
    for(int i=0; ; i++) {
        sum+=a[sum];
        sum%=n;

        if(vis[sum])  {
            cstart=sum;
            break;
        }

        vis[sum]=true;
        b.push_back(sum);
    }

    //cerr<<"after 0, cstart="<<cstart<<endl;
    //cerr<<"b[]=";
    //for(int i : b)
    //    cerr<<i<<" ";
    //cerr<<endl;

    /* prefix */
    int ans=0;
    int idx=0;
    for(; idx<k && b[idx]!=cstart; idx++) {
        ans+=a[b[idx]];
    }
    //cerr<<"after 1"<<endl;
    
    assert(k>=idx);
    if(k==idx) {
        cout<<ans<<endl;
        return;
    }
    k-=idx;

    vb vis2(n);
    vi c;
    c.push_back(cstart);
    vis2[cstart]=true;
    for(int i=0; ; i++) {
        cstart+=a[cstart];
        cstart%=n;
        if(vis2[cstart])
            break;

        vis2[cstart]=true;
        c.push_back(cstart);
    }
    //cerr<<"after 2"<<endl;

    sum=0;
    for(size_t i=0; i<c.size(); i++) 
        sum+=a[c[i]];

    //cerr<<"after 3"<<endl;

    ans+=sum*(k/c.size());
    for(size_t i=0; i<(k%c.size()); i++)
        ans+=a[c[i]];

    //cerr<<"after 4"<<endl;
    cout<<ans<<endl;
}

signed main() {
    solve();
}
