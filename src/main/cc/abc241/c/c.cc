/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Use prefix sums to count the number of white cells in 6 consecutive cells.
 *
 * ...still some off by one :/
 * Missunderstood the problem?
 *
 * ..."one of its diagonals". Thanks a lot.
 */
void solve() {
    cini(n);
    cinas(s,n);

    // do idiotic brute force instead of prefix sums
    for(int i=0; i<n; i++) {
        for(int j=0; j<n; j++) {
            vi cnt(4);
            for(int k=0; k<6; k++) {
                if(j+5<n)
                    cnt[0]+=(s[i][j+k]=='#');
                if(i+5<n)
                    cnt[1]+=(s[i+k][j]=='#');
                if(j+5<n && i+5<n)
                    cnt[2]+=(s[i+k][j+k]=='#');
                if(j-5>=0 && i+5<n)
                    cnt[3]+=(s[i+k][j-k]=='#');
            }
            if(cnt[0]>=4 || cnt[1]>=4 || cnt[2]>=4 || cnt[3]>=4) {
                cout<<"Yes"<<endl;
                return;
            }
        }
    }
    cout<<"No"<<endl;
}

signed main() {
    solve();
}
