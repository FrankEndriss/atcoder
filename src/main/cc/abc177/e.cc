
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

set<int> fact(int i) {
    set<int> ans;
    for(int j=2; j*j<=i; j++) {
        while(i%j==0) {
            ans.insert(j);
            i/=j;
        }
    }
    if(i!=1)
        ans.insert(i);
    return ans;
}

void solve() {
    cini(n);
    cinai(a,n);

    int g=a[0];
    int pairwise=true;
    set<int> allf;
    for(int i=0; i<n; i++) {
        if(i>0)
            g=gcd(g,a[i]);

        if(pairwise) {
            set<int> f=fact(a[i]);
            for(int j : f) {
                if(allf.count(j)>0)
                    pairwise=false;
                allf.insert(j);
            }
        }
    }
    if(pairwise)
        cout<<"pairwise coprime"<<endl;
    else if(g==1)
        cout<<"setwise coprime"<<endl;
    else
        cout<<"not coprime"<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
