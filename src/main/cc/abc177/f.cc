
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * How to move?
 *
 * We can arrive at any position in target row.
 * Check if no position is possible for ans=-1 
 * with max posx.
 *
 * How to find rightmost possible startpoint?
 *
 * We can index all rows starting from bottom.
 *
 * We need to maintain a segment tree and range updates.
 *
 * In that segtree we maintain min value and store
 * for each position by how much  we must move right
 * if started from that position.
 * Then on each ab we update the range (a,b) in the segtree.
 * The value after update is max(oldval, (b+1)-pos)
 *
 * How to lazy update?
 * -> to complecated
 *
 */
const int INF=1e9;
void solve() {
    cini(h);
    cini(w);

    SegmentTree<int> seg(w+1, INF);
    seg.plus=[](int i1, int i2) {
        return min(i1,i2);
    };

    int posx=1;
    int start=1e9;
    set<pii> ba;    // { b,a }
    for(int i=0; i<h; i++) {
        cini(a);
        cini(b);
        start=min(minA,a);
        if(posx>=a)
            posx=max(posx,b+1);

        if(posx>w)
            cout<<-1<<endl;
        else
            cout<<posx+i-minA+1<<endl;
    }

    vi maxStart(h, w);
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
