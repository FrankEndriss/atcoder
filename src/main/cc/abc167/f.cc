/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s)
        c = c == ','?  ' ': c;
    stringstream ss;
    ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) {
    cerr << endl;
}
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0)
        cerr << ", ";
    stringstream ss;
    ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

const int INF=1e9;

struct tri {
    int b;
    int mi;
};

bool operator<(const tri &t1, const tri &t2) {
    if(t1.b==t2.b)
        return t2.mi<t1.mi;

    return t2.b<t1.b;
}

/* we need to order the triples in a way that
 * sum of b is >=0 for all prefixes
 * sum of all b is zero
 * sum of b + current min is >=0
 *
 * Greedy
 * So, if we choose one after the other, we can choose
 * at current position all with t.mi>=-currentBal
 * From those we choose the one with max(bal).
 * Do we really need the max(bal)?
 * ...Seems, yes.
 * How to impl that?
 */
void solve() {
    cini(n);
    vector<tri> vt;

    int sum=0;

    for(int i=0; i<n; i++) {
        cins(s);
        int bal=0;
        int mi=0;
        for(char c : s) {
            if(c=='(')
                bal++;
            else
                bal--;
            mi=min(mi,bal);
        }

        tri t;
        t.b=bal;
        t.mi=mi;
        vt.push_back(t);
        sum+=t.b;
    }

    if(sum!=0) {
        cout<<"No"<<endl;
        return;
    }

    sort(vt.begin(), vt.end(), [&](tri &t1, tri &t2) {
        return t2.mi<t1.mi;
    });

    set<tri> usable;
    int idx=0;
    for(int i=0; i<n; i++) {
        if(vt[i].mi==0) {
            usable.insert(vt[i]);
            idx++;
        } else 
            break;
    }

    /* check if sorting works */
    int bal=0;
    for(int i=0; i<n; i++) {
        auto it=usable.begin();
        if(it==usable.end()) {
            cout<<"No"<<endl;
            return;
        }
        tri cur=*it;

        if(bal<0 || cur.mi+bal<0) {
            cout<<"No"<<endl;
            return;
        }
        bal+=cur.b;
        usable.erase(it);

        while(idx<n && vt[idx].mi>-bal) {
            usable.insert(vt[idx]);
            idx++;
        }
    }

    cout<<"Yes"<<endl;
    return;
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

