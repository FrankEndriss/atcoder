
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int N=50;
const vi di= { -1, 1, 0, 0};
const vi dj= { 0, 0, -1, 1};
const vector<char> dc= { 'U', 'D', 'L', 'R' };

vvb vis0(N, vb(N)); /* direct vis */
vvb vis1(N, vb(N)); /* also vis */
vvi t(N, vi(N));
vvi p(N, vi(N));

inline void alsoset(int i, int j) {
    for(int k=0; k<4; k++) {
        const int ii=i+di[k];
        const int jj=j+dj[k];
        if(ii>=0 && ii<N && jj>=0 && jj<N && t[i][j]==t[ii][jj]) {
            vis1[ii][jj]=vis0[i][j];
            break;
        }
    }
};

const int T=2e7;
int cnt=0;
ll ans=0;      /* best answer so far */
string apath;   /* path of best answer */

ll sum=0;      /* sum in current path */
string path;    /* current path */


void go(int i, int j) {
    if(cnt++>T)
        return;

    //assert(!vis0[i][j] && !vis1[i][j]);
    vis0[i][j]=true;
    alsoset(i,j);
    sum+=p[i][j];
    if(sum>ans) {
        ans=sum;
        apath=path;
    }

    using t3=tuple<int,int,int>;
    vector<t3> next;
    for(int d=0; d<4; d++) {
        const int ii=i+di[d];
        const int jj=j+dj[d];
        if(ii>=0 && ii<N && jj>=0 && jj<N && !vis0[ii][jj] && !vis1[ii][jj]) {
            next.emplace_back(ii,jj,d);
        }
    }

    /* Now sort the next steps by some criteria.
     * 1. Use a simple heuristic where we want to stay on
     * maximize the distance between the middle of the map and the
     * borders.
     */
    sort(all(next), [](t3 &p1, t3 &p2) {
        auto [p1x,p1y,p1d]=p1;
        auto [p2x,p2y,p2d]=p2;

        /* dist from center quad */
        const int dc1=abs(N/2-p1x)*abs(N/2-p1x)+abs(N/2-p1y)*abs(N/2-p1y);
        const int dc2=abs(N/2-p2x)*abs(N/2-p2x)+abs(N/2-p2y)*abs(N/2-p2y);

        /* dist from border quad */
        const int x1=min(p1x, N-p1x);
        const int y1=min(p1y, N-p1y);
        const int d1=min(dc1, min(x1,y1)*min(x1,y1));

        const int x2=min(p2x, N-p2x);
        const int y2=min(p2y, N-p2y);
        const int d2=min(dc2, min(x2,y2)*min(x2,y2));

        /* maximize both distances */
        return d1 > d2;
    });

    for(auto [iii,jjj,ddd] : next) {
        path+=dc[ddd];
        go(iii,jjj);
        path.pop_back();
    }

    vis0[i][j]=false;
    alsoset(i,j);
    sum-=p[i][j];
};

/* There is a more or less simple brute force,
 * first implement that.
 */
void solve() {
    cini(si);
    cini(sj);

    for(int i=0; i<N; i++)
        for(int j=0; j<N; j++)
            cin>>t[i][j];

    for(int i=0; i<N; i++)
        for(int j=0; j<N; j++)
            cin>>p[i][j];

    go(si,sj);

    cerr<<ans<<endl;
    cout<<apath<<endl;

}

signed main() {
    solve();
}
