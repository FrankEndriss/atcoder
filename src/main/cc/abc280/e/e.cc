/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using mint=modint998244353;

/**
 * e= 1 + f(n-2)*p + f(n-1)*(1-p)
 */
void solve() {

    cini(n);
    cini(lp);
    mint p=mint(lp)*mint(100).inv();

    vector<mint> dp(n+1);
    vb vis(n+1);
    function<mint(int)> f=[&](int nn) {
        if(nn<=0)
            return mint(0);

        if(vis[nn])
            return dp[nn];

        vis[nn]=true;
        dp[nn]= mint(1)+f(nn-2)*p + f(nn-1)*(mint(1)-p);

        return dp[nn];
    };

    mint ans=f(n);

    cout<<ans.val()<<endl;
    
    
}

signed main() {
//    cini(t);
//    while(t--)
        solve();
}
