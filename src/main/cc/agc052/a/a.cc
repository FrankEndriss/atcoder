
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

#define endl "\n"

/* Consider n=1, first example 
 * in
 * 01 -> 0101
 * 01 -> 0101
 * 10 -> 1010
 *
 * n=2
 * 1010 -> 10101010
 * 0011 -> 00110011
 * 1100 -> 11001100
 *      11001
 *
 * can there be some dp construction?
 * -> no. It must be some greedy.
 *
 * Consider the next symbol to use in ans.
 * It increases the index in all the three strings by some
 * amount...
 *
 * Consider using n '0's, then n '1's.
 * This allways works, but result is one char to short.
 * How can we construct the last char?
 */
void solve() {
    cini(n);
    cinas(s,3);
    string ans;
    for(int i=0; i<n; i++) 
        ans+='1';
    for(int i=0; i<n; i++) 
        ans+='0';
    ans+='1';

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
