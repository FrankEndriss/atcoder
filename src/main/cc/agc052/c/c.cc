
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


const int MOD=998244353;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

/*
 * dp would be
 * dp[i][j]=number of possible arrays with i elements if prefix sum==j
 *
 * Problem is, that j is much to big for that.
 *
 * At first position we can use p-1 different elements.
 * At each consecutive position we can use p-2 different elements,
 * (because there are p-1, and on of those would make the sum div by p).
 *
 * What about that reordering?
 * What arrays are not good?
 * -> The sum of all elements must not be multiple of p.
 * (p-1)^n - (p-1)^(n-1)
 * What about arrys of one and the same element?
 * Since there are no divisors of p (prime) that should not be a problem.
 *
 * If all elements are 1 and N>=P
 * then the array 111...111 is not good.
 *
 * How much are there with sum%p==0?
 * All with len n-1 and sum!=0.
 * And there are len (p-1)^(n-2)
 *
 * len=1, p-1 with sum!=0
 * len=2, (p-1) with sum==0 and (p-1)*(p-2) with sum !=0.
 * len=3, 
 */
void solve() {
    cini(n);
    cini(p);

    vvi dp(2, vi(n+1)); /* dp[i][j]= sumParity==i at position j */
    dp[0][1]=0;
    dp[1][1]=p-1;
    for(int i=2; i<=n; i++) {
        dp[0][i]=dp[1][i-1];
        dp[1][i]=mul(dp[0][i-1], p-2);
    }
    int ans=pl(dp[1][n], -dp[0][n]);

    if(n>=p)
        ans=pl(ans, -(p-1));    /* all digits same */

    cout<<ans<<endl;
}

signed main() {
    solve();
}
