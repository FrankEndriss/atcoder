
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * consider cnt[i] be the number of formulars possible on prefix up to i.
 * Consider post[i] be the sum of all possible last terms of formular s up to i.
 *
 * Add a digit to s0 makes:
 * ans=2*ans+9*post[i-1]+cnt[i-1]*d
 *
 * cnt[i+1]=cnt[i]*2
 * post[i+1]=post[i]*10 + post[i]*s[i]*2
 *
 * *****
 * For some reason that does not work :/
 */
using mint=modint998244353;
void solve() {
    cins(s);

    vector<mint> cnt(s.size()+7);
    vector<mint> post(s.size()+7);

    cnt[0]=1;
    post[0]=s[0]-'0';
    mint ans=post[0];

    for(size_t i=1; i<s.size(); i++) {
        const int d=s[i]-'0';
        cnt[i]=cnt[i-1]*2;
        ans=ans*2+post[i-1]*9+cnt[i]*d;
        post[i]=post[i-1]*10 + cnt[i]*d;
        //cerr<<"i="<<i<<" s[i]="<<s[i]<<" ans="<<ans.val()<<" cnt[i]="<<cnt[i].val()<<" post[i]="<<post[i].val()<<endl;
    }

    cout<<ans.val()<<endl;
}

signed main() {
    solve();
}
