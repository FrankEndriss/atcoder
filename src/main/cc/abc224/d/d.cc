
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * There are 9!=3e5 possible configurations.
 * So do a bfs ans see if the initial one can be reached.
 */
void solve() {
    cini(m);
    vvi adj(9);
    for(int i=0; i<m; i++) {
        cini(u);
        cini(v);
        adj[u-1].push_back(v-1);
        adj[v-1].push_back(u-1);
    }

    vi a(9,-1);
    for(int i=0; i<8; i++) {
        cini(p);
        a[p-1]=i;
    }

    map<vi,int> dp;
    dp[a]=0;

    queue<vi> q;
    q.push(a);

    while(q.size()) {
        a=q.front();
        const int val=dp[a];
        q.pop();
        int idx=-1;
        for(int i=0; i<9; i++) 
            if(a[i]==-1) {
                idx=i;
                break;
            }

        assert(a[idx]==-1);

        for(int p1 : adj[idx]) {
            swap(a[idx],a[p1]);
            auto it=dp.find(a);
            if(it==dp.end()) {
                dp[a]=val+1;
                q.push(a);
            }
            swap(a[idx],a[p1]);
        }
    }

    vi end={ 0,1,2,3,4,5,6,7,-1};
    auto it=dp.find(end);
    if(it==dp.end())
        cout<<-1<<endl;
    else
        cout<<dp[end]<<endl;

}

signed main() {
    solve();
}
