
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider the cells ordered by a[i][j] non ascending.
 * Whenever we can move one of the pieces, it can be moved
 * one step more than that of the target cell.
 *
 * So just dp in that order.
 *
 * How to implement?
 * Foreach cell we need to knwo the max value of same row and same col.
 * So maintain the max of that.
 *
 * ***
 * But note that a[i][j] is _not_ distinct!
 */
const int INF=1e9+7;
void solve() {
    cini(h);
    cini(w);
    cini(n);
    using t3=tuple<int,int,int,int>;
    vector<t3> m(n);
    for(int i=0; i<n; i++) {
        cini(r); r--;
        cini(c); c--;
        cini(a);
        m[i]={ a, r, c, i };
    }
    sort(all(m), greater<t3>());

    vi dpr(h,-1);    /* dpr[i]=max val in row i */
    vi dpc(w,-1);
    vi ans(n);
    int preva=INF;
    vector<pii> ddpr; /* r,val */
    vector<pii> ddpc;
    for(int i=0; i<n; i++) {
        auto [a,r,c,ii]=m[i];

        if(a!=preva) {
            for(pii j : ddpr)
                dpr[j.first]=max(dpr[j.first],j.second);
            for(pii j : ddpc)
                dpc[j.first]=max(dpc[j.first],j.second);

            ddpr.clear();
            ddpc.clear();
        }
        preva=a;

        const int val=max(dpr[r]+1, dpc[c]+1);
        ddpr.emplace_back(r,val);
        ddpc.emplace_back(c,val);

        ans[ii]=val;
    }

    for(int i=0; i<n; i++) 
        cout<<ans[i]<<endl;

}

signed main() {
    solve();
}
