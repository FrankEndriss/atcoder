/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note N<=8
 * Brute force
 */
void solve() {
    cini(n);
    cini(m);

    vvi adj1(n);
    for(int i=0; i<m; i++) {
        cini(a); a--;
        cini(b); b--;
        adj1[a].push_back(b);
        adj1[b].push_back(a);
    }

    for(int i=0; i<n; i++)
        sort(all(adj1[i]));

    vvi adj2(n);
    for(int i=0; i<m; i++) {
        cini(a); a--;
        cini(b); b--;
        adj2[a].push_back(b);
        adj2[b].push_back(a);
    }

    vi p(n);
    iota(all(p), 0);
    do {
        vvi adj3(n);
        for(int i=0; i<n; i++) 
            adj3[p[i]]=adj2[i];

        for(int i=0; i<n; i++) {
            for(size_t k=0; k<adj3[i].size(); k++) 
                adj3[i][k]=p[adj3[i][k]];
            sort(all(adj3[i]));
        }
        if(adj1==adj3) {
            cout<<"Yes"<<endl;
            return;
        }
    }while(next_permutation(all(p)));

    cout<<"No"<<endl;
}

signed main() {
    solve();
}
