/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using mint=modint998244353;

const int N=2e6+7;
vector<mint> fac(N);

const bool initFac=[]() {
    fac[1]=1;
    for(int i=2; i<N; i++) {
        fac[i]=fac[i-1]*i;
    }
    return true;
}();

/** see https://cp-algorithms.com/combinatorics/binomial-coefficients.html */
mint nCr(int n, int k) {
    if(k==0 || k==n)
        return 1;
    if(k==1 || k==n-1)
        return n;
    if(k>n/2)
        return nCr(n, n-k);

    return fac[n]*fac[k].inv()*fac[n-k].inv();
}

/**
 * let m=manhatten distance p1..p2
 * if k==m then...
 * like pascals triangle.
 * And each two steps more available can be done before/after any other step.
 * How to calc the binomial coefficient fast enough?
 * -> use nCr
 */
void solve() {
    cini(h);
    cini(w);
    cini(k);
    cini(x1);
    cini(y1);
    cini(x2);
    cini(y2);

    const int dx=abs(x1-x2);
    const int dy=abs(y1-y2);

    if(dx+dy>k || (k-dx-dy)%2 || dx+dy==0) {
            cout<<0<<endl;
            return;
    }

    mint ans=nCr(max(dx,dy), min(dx,dy));

    /* before each of the dx+dy steps, we can do one or more
     * of the (k-dx-dy)/2 free steps.
     * But not all of them all of the time.
     * The steps come in pairs of opposite direction, and foreach
     * pair we need to do the first of the pair first.
     * Also the first of these pairs can go in 4 directions.
     * But also consider that that might be wrong, since the grid 
     * is not endless...
     * To complecated.
     * TODO
     */
    cout<<ans.val()<<endl;
}

signed main() {
    solve();
}
