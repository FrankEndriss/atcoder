/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We cannot brute force all 18! permutations.
 * Some greedy?
 * Lets iterate all pairs of adjacent elements.
 * Whenever a swap would make the result better do that swap.
 *
 * Note that the order of operations does not matter.
 * So first do the swaps, then the inc/dec.
 * ...but does not work :/
 */
void solve() {
    cini(n);
    cini(x);
    cini(y);
    cinai(a,n);
    cinai(b,n);

    bool done=false;
    int ans=0;
    while(!done) {
        done=true;
        for(int i=0; i+1<n; i++) {
            if(abs(a[i]-b[i])*x+abs(a[i+1]-b[i+1])*x > abs(a[i]-b[i+1])*x+abs(a[i+1]-b[i])*x+y) {
                ans+=y;
                swap(a[i],a[i+1]);
                done=false;
            }
        }
    }
    for(int i=0; i<n; i++)
        ans+=abs(a[i]-b[i])*x;

    cout<<ans<<endl;

}

signed main() {
    solve();
}
