/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<pii> vpii;
typedef vector<vpii> vvpii;
typedef vector<vi> vvi;
typedef vector<vll> vvll;
typedef vector<vll> vvll;
typedef vector<vpii> vvll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, m;
    cin>>n>>m;
    vvpii graph(n);
    vvpii ans(n);
const int BLACK=-1;
const int RED=1;
const int UKN=0;

    fori(m) {
        int a, b;
        cin>>a>>b;
        a--;    
        b--;
        graph[a].push_back({b, UKN});
        graph[b].push_back({a, UKN});
    }
/* all leafs have to have no outgoing edges. */
    queue<int> tocheck;
    fori(n) {
        if(graph[i].size()==1) {
            tocheck.push(i);
        }
    }

    

}

