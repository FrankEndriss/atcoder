/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<vi> vvi;
typedef vector<vll> vvll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vi a(n);
    map<int, int> f;
    fori(n)  {
        cin>>a[i];
        f[a[i]]++;
    }

    sort(a.begin(), a.end());
    if(a[0]==0 && f.size()==1) {    // all zero
        cout<<"Yes"<<endl;
        return 0;
    }

    bool ans=(n%3==0) && (f.size()==3);
    vector<int> vals;
    for(auto e : f) {
        ans=ans && e.second==n/3;
        vals.push_back(e.first);
    }

    if(vals.size()==2 && vals[0]==0 && f[vals[0]]==n/3 && f[vals[1]]==2*n/3) { // two same and one zero
        cout<<"Yes"<<endl;
        return 0;
    }


    ans=ans && ((vals[0]^vals[1])==vals[2]);
    ans=ans && ((vals[1]^vals[2])==vals[0]);
    ans=ans && ((vals[2]^vals[0])==vals[1]);

    if(ans)
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;
    
}

