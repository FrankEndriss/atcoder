/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<vi> vvi;
typedef vector<vll> vvll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vi a(n);
    fori(n)
        cin>>a[i];

    vii dp(n, vi(n));

typedef tuple<ll, ll, ll, ll> t3;
/* find max value for subseq starting at i of len j */
    function<t3(int, int)> calc=[&](int i, int j) {
        if(j==3)
            return a[i]+2*a[i+1]+a[i+2];

...
        
    }
    

}

