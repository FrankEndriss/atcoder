/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * clear stage i for x times need
 * a[0..i]+b[0..i]+b[i]*(x-1)
 *
 * But we want X stages at all, so the ith stage only x-i times.
 *
 * WA...overflow?
 */
const int INF=4e18;
void solve() {
    cini(n);
    cini(x);
    int ans=INF;
    int sum=0;
    for(int i=0; i<min(n,x+1); i++) {
        cini(a);
        cini(b);
        sum+=a;
        sum+=b;

        int lans=sum+(x-1-i)*b;
        //cerr<<"i="<<i<<" lans="<<lans<<endl;
        ans=min(ans, lans);
    }
    cout<<ans<<endl;
}

signed main() {
        solve();
}
