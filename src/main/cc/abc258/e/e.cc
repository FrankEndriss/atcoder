/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * The contents of each box is determined by the position of the 
 * first potato.
 * So we do find the number of potatos in each box by binary search 
 * for all positions.
 *
 * Then create binary lifting table to quickly jump in that list.
 *
 * Note: We need to consider being x>sum(w[]), in that case we need to 
 * make x%=sum(w[]);
 */
void solve() {
    cini(n);
    cini(q);
    cini(x);
    cinai(w,n);

    int sumw=0;
    for(int i=0; i<n; i++)  {
        w.push_back(w[i]);
        sumw+=w[i];
    }
    int xx=x/sumw;
    x%=sumw;

    vi pre(w.size()+1);
    for(size_t i=0; i<w.size(); i++)
        pre[i+1]=pre[i]+w[i];

    const int M=45;
    vvi b(M, vi(n));
    vi s(n);    /* s[i]=size of box with first potato w[i] */
    for(int i=0; i<n; i++) {
        /* binsearch the position where sum(w[i..l])<x && sum(w[i..r])>=x */
        auto it=lower_bound(pre.begin(), pre.end(), pre[i]+x);
        assert(it!=pre.end());
        int r=distance(pre.begin(), it);
        b[0][i]=r%n;
        s[i]=r-i;
        //cerr<<"i="<<i<<" dp[i]="<<dp[i]<<endl;
    }

    for(int i=1; i<M; i++)
        for(int j=0; j<n; j++) 
            b[i][j]=b[i-1][b[i-1][j]];

    for(int i=0; i<q; i++) {
        cini(k);
        k--;
        int v=0;
        for(int j=0; j<M; j++)
            if(k&(1LL<<j)) 
                v=b[j][v];

        int ans=s[v];
        cout<<ans+xx*n<<endl;
    }

}

signed main() {
        solve();
}
