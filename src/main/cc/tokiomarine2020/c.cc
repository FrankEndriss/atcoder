
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* how that?
 * We have n segments, so at every position there is a number
 * of intersecting segments.
 * Doing one step is sweepline, then replace a[] by b[]
 * But we cannot do this k times :/
 * ...
 * After some short time all are on, then we can stop.
 *
 * Note: there is a technique called accumulated sums
 * which works better than sweepline in this case.
 * It is basically prefix sums instead of evt. The advantage
 * is that there is no need to sort the array.
 * see https://img.atcoder.jp/tokiomarine2020/editorial.pdf
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    bool fini=false;
    for(int i=0; !fini && i<k; i++) {
        vector<pii> evt;
        for(int j=0; j<n; j++) {
            evt.emplace_back(j-a[j],  1);
            evt.emplace_back(j+a[j]+1, -1);
        }
        sort(all(evt));

        vi b(n);
        int cnt=0;
        int eidx=0;

        fini=true;
        for(int j=0; j<n; j++) {
            while(eidx<evt.size() && evt[eidx].first<=j) {
                cnt+=evt[eidx].second;
                eidx++;
            }
            b[j]=cnt;
            if(b[j]!=a[j])
                fini=false;
        
        }
        a.swap(b);
    }
    for(int ans : a)
        cout<<ans<<" ";
    cout<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
