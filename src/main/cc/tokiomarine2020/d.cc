/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Bintree build like segment tree.
 * We need to knapsack on the items in path to root.
 *
 * It is at most 17 items per query.
 * So brute force...no.
 *
 * How to reuse previous queries?
 * For each level the number of dp entries doubles.
 *
 * We can reuse queries by sorting queries by v.
 * Then do dfs and answer per vertex.
 */
void solve() {
    cini(n);
    vi val(n+1);
    vi wei(n+1);
    for(int i=0; i<n; i++)
        cin>>val[i]>>wei[i];

    const int N=1e5+1;
    vector<vector<pii>> quer(N); /* query by v */

    cini(q);
    for(int i=0; i<q; i++) {
        cini(vv);
        cini(l);
        quer[vv].emplace_back(l,i);
    }

    stack<vi> memo;
    vi zero(N);
    memo.push(zero);
    int memotop=0;

    function<void(int)> query=[&](int v) {
        if(memotop==v)
            return;

        assert(memotop<v);

        if(v/2>0) 
            query(v/2);

        assert(memotop==v/2);

        vi cur(N);
        for(int i=0; i<N; i++) 
            if(i+wei[v]<N)
                cur[i+wei[v]]=max(memo.top()[i]+val[v], memo.top()[i+wei[v]]);
            else
                cur[i]=memo.top()[i];

        memo.push(cur);
        memotop=v;
    };

    function<void(int)> dfs=[&](int v) {
        if(v*2<=n) {
            dfs(v*2);
        }

        if(v*2+1<=n) {
            dfs(v*2+1);
        }

        if(quer[v].size()>0) {
            query(v);
        }

        for(size_t i=0; i<quer[v].size(); i++)
            quer[v][i].first=memo.top()[quer[v][i].first];

        memo.pop();
        memotop=v/2;
    };

    dfs(1);
cerr<<"memo.size()="<<memo.size()<<endl;
    assert(memo.size()==1);

    vector<pii> ans;
    for(size_t i=0; i<quer.size(); i++) 
        for(size_t j=0; j<quer[i].size(); j++) 
            ans.emplace_back(quer[i][j].second, quer[i][j].first);

    sort(all(ans));
    for(int i=0; i<ans.size(); i++)
        cout<<ans[i].second<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
