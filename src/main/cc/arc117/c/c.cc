
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Brute force if fairly clear. How can we optimze?
 * Consider a pattern of size x.
 * It produces the same result everywhere it is contained.
 *
 * Consider calculating the result for all patterns of size 12 (531441)
 * Then we can jump 12 rows at once.
 * ...But still got 4e5*4e5/12 operations :/
 * ***
 * Put it upside down:
 * If last is B, the the 2nd row was BB or RW or WR
 * ***
 * If colors are switchable we got only 4 patterns:
 * 000 : 0
 * 010 : 2
 * 001 : 1
 * 012 : 1
 * ...idk :/
 */
void solve() {
    cini(n);
    cins(s);
}

signed main() {
    solve();
}
