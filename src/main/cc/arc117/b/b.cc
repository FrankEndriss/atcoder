
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


const int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

/* Sort the buildings by hight.
 * Note that buildings of same hight allways stay same hight.
 * Let x be the number of distinct heigths.
 * Then we can decrease x different postfix of the buildings.
 * If we decrease one with diff==1 to the next smaller, then
 * they get merged into one hight.
 *
 * We can decrease x different postfixes for a[0] times.
 * We can decrease x-1 different postfixes for a[1]-a[0] times.
 * ...
 * How would dfs work?
 * current state -> decrease one of the x postfixes, foreach recalc x
 * So, ans is the product of (a lot of) numbers of postfixes.
 * How to maintain that?
 * If it is only one hight, then that h is ans.
 * If it is two hights (h0 and h1), then
 * We can decrease both for h0 times, and after each such op
 * we can decrease h1 for h1-h0 times, resulting in two equal
 * buildings.
 * So, for two buildings 
 * ans= (h0+1) * (h1-h0+1)
 * So, for three buildings (1,2,4):
 *
 * 1,2,4
 *  1,2,3
 *   1,2,2
 *    1,1,1
 *    0,1,1
 *   1,1,2
 *    1,1,1
 *    0,0,1
 *   0,1,2
 *    0,1,1
 *    0,0,1
 *  1,1,3
 *   1,1,2
 *    1,1,1
 *    0,0,1
 *   0,0,2
 *    0,0,1
 *  0,1,3
 *   0,1,2
 *    0,1,1
 *    0,0,1
 *   0,0,2
 *    0,0,1
 *    0,0,0
 *
 * So for last digit
 *
 */
void solve() {
    cini(n);
    set<int> s;
    for(int i=0; i<n; i++) {
        cini(aux);
        s.insert(aux);
    }

    vi a;
    for(int i : s)
        a.push_back(i);

    int ans=1;
    int m0=1;
    int h0=0;
    for(int h1 : s) {
        ans=mul(ans, mul(m0, h1-h0+1));
        h0=h1;
        m0=h1-h0+1;
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}
