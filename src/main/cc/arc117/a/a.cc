
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Use the bigger number with 1,2,3...m=max(a,b),
 * sum=m*(m+1)/2
 * Construct a seq with min(a,b) elements and that sum.
 */
void solve() {
    cini(a);
    cini(b);
    vi ans;

    for(int i=1; i<=max(a,b); i++) 
        ans.push_back(i);

    for(int i=1; i<min(a,b); i++)
        ans.push_back(-i);

    int ma=max(a,b)*(max(a,b)+1)/2;
    int mi=(min(a,b)-1)*min(a,b)/2;
    ans.push_back(-(ma-mi));

    if(a<b) {
        for(int i=0; i<a+b; i++)
            ans[i]=-ans[i];
    }
    for(int i : ans)
        cout<<i<<" ";
    cout<<endl;
}

signed main() {
    solve();
}
