
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vector<int> x(n);
    vector<int> y(n);

    fori(n)
        cin>>x[i]>>y[i];

    map<pair<int, int>, int> f;

    for(int i=0; i<n; i++)
        for(int j=0; j<n; j++)
            if(i!=j)
                f[{x[j]-x[i], y[j]-y[i]}]++;

    int mx=0;
    for(auto p=f.begin(); p!=f.end(); p++)
        mx=max(mx, p->second);

    int ans=n-mx;
    cout<<ans<<endl;
    

}

