/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

ll ex[2][3];

/* returns max possible acorn after double exchange */
ll maxEx(ll n, int eIdx1, int eIdx2) {
/** TODO this is the knapsack problem, greedy does not work. */


    ll ans=n;
    vector<int> p({ 0, 1, 2 });    
    do {
        vector<ll> metal(3);    /* amount of metal p[i] after first half change */
        vector<ll> nn(3);       /* rest of acorn after change of metal p[i] */

        if(ex[eIdx1][p[0]]<ex[eIdx2][p[0]])
            metal[p[0]]=n/ex[eIdx1][p[0]];
        nn[p[0]]=n-metal[p[0]]*ex[eIdx1][p[0]];

        if(ex[eIdx1][p[1]]<ex[eIdx2][p[1]])
            metal[p[1]]=nn[p[0]]/ex[eIdx1][p[1]];
        nn[p[1]]=nn[p[0]]-metal[p[1]]*ex[eIdx1][p[1]];

        if(ex[eIdx1][p[2]]<ex[eIdx2][p[2]])
            metal[p[2]]=nn[p[1]]/ex[eIdx1][p[2]];
        nn[p[2]]=nn[p[1]]-metal[p[2]]*ex[eIdx1][p[2]];

/* metal[i] are the metals we have, nn[p[2]] the remaing acorns.
 * Now second trade. */
        ll mx=nn[p[2]];
        for(int i=0; i<3; i++)
            mx+=metal[p[i]]*ex[eIdx2][p[i]];

//cout<<"mx="<<mx<<endl;
        ans=max(ans, mx);

    }while(next_permutation(p.begin(), p.end()));

    return ans;
}


int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    ll n;
    cin>>n>> ex[0][0]>> ex[0][1]>> ex[0][2]>> ex[1][0]>> ex[1][1]>> ex[1][2];

    ll ans=maxEx(n, 0, 1);
    ans=maxEx(ans, 1, 0);

    cout<<ans<<endl;
    
}

