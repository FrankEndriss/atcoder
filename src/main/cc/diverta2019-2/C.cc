/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vector<int> a(n);
    fori(n)
        cin>>a[i];

    sort(a.begin(), a.end());
    
    vector<pair<ll, ll>> ans;
    ll sum=0;
    if(a[0]>=0) { // no negative
        sum=a[0];
        for(int i=1;i<n-1; i++) {
            ans.push_back({ sum, a[i] });
            sum-=a[i];
        }
        ans.push_back({ a[n-1], sum });
        sum=a[n-1]-sum;
    } else if(a[n-1]<=0) { // no positive
        sum=a[n-1];
        for(int i=0; i<n-1; i++) {
            ans.push_back({ sum, a[i] });
            sum-=a[i];
        }
    } else { // mixed
/* use one neg, subtract all pos except one.
 * use that one pos, subtract all other neg.
 */
        int firstPos=-1;    // position of first positive
        for(int i=0; i<n; i++)
            if(a[i]>=0) {
                firstPos=i;
                break;
            }

        sum=a[0];
        for(int i=firstPos; i<n-1; i++) {
            ans.push_back({ sum, a[i] });
            sum-=a[i];
        }
        ans.push_back({ a[n-1], sum });
        sum=a[n-1]-sum;

        for(int i=1; i<firstPos; i++) {
            ans.push_back({ sum, a[i] });
            sum-=a[i];
        }
    }

    cout<<sum<<endl;
    for(auto p : ans)
        cout<<p.first<<" "<<p.second<<endl;
}

