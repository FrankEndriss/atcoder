/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to find the sums at the positions
 * just before each first negative number in b[c].
 *
 * How to find the negative subsequences in b[]?
 *
 * Note that abs(x[i]) is very small, <=4
 * But that seems only to not overflow.
 *
 * Consider the values b[j] at positions y[0], y[1],...
 * From these find the positions where b[k],b[k+1] changes sign.
 * Then calc the values for a[k],a[k+1]
 * Then ans=max(a[k])
 */
void solve() {
    cini(n);
    cini(m);
    vi x(n);
    vi y(n);

    vvi preB; /* pos,sum,val,cnt */
    preB.push_back({0,0,0,0});
    for(int i=0; i<n; i++) {
        vi cur(4);
        cur[0]=preB.back()[0]+preB.back()[3];
        cur[1]=preB.back()[1]+preB.back()[2]*preB.back()[3];

        cin>>cur[2]>>cur[3];

        if((preB.back()[1]>0 && cur[1]<=0)) {
            int cnt=abs(preB.back()[1]/preB.back()[2]);
            if(cnt>0) {
                vi cur0(4);
                cur0[0]=preB.back()[0]+cnt;
                cur0[1]=preB.back()[1]+cnt*preB.back()[2];
                cur0[2]=preB.back()[2];
                cur0[3]=cnt;
                preB.push_back(cur0);

                cur[3]-=cnt;
            }
        }
        preB.push_back(cur);
    }
    int ans=preB.back()[1];
    int endsum=preB.back()[1]+preB.back()[2]*preB.back()[3];

    if(preB.back()[1]>0 && endsum<=0) {
            int cnt=abs(preB.back()[1]/preB.back()[2]);
            if(cnt>0) {
                vi cur0(4);
                cur0[0]=preB.back()[0]+cnt;
                cur0[1]=preB.back()[1]+cnt*preB.back()[2];
                cur0[2]=preB.back()[2];
                cur0[3]=cnt;

                preB.back()[3]-=cnt;
                auto it=preB.end();
                it--;
                preB.insert(it,cur0);
            }
    }


    int sum=0;

    for(size_t i=0; i<preB.size(); i++) {
        const int cnt=preB[i][3];
        sum+=preB[i][1]*cnt;
        sum+=preB[i][2]*(cnt*(cnt+1))/2;
        ans=max(ans, sum);
    }

    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
