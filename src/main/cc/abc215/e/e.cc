
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We want to know how much subseqs are there
 * where each symbol is there in a block only.
 * 
 * So we need to iterate the string, and foreach position
 * find the number of ways up to position with
 * mask used letters, and last letter k.
 *
 * Then for not using the current letter, dp[i]=dp[i-1]
 *
 * And using the current letter
 * we can use all prefix where last letter is same as current letter
 *
 * dp[i][mask][kk]+=dp[i-1][mask][kk]
 * or current letter is not used.
 */
using mint=modint998244353;
void solve() {
    cini(n);
    cins(s);

    const int b=(1<<10);

    /* dp[i][mask][k]= number of ways to choose up to idx=i 
     * with mask used 
     * and last used=k 
     * Note k==0 -> none used so far
     **/
    vector<vector<vector<mint>>> dp(n+1, vector<vector<mint>>(b, vector<mint>(11)));

    dp[0][0][0]=1;
    for(int i=0; i<n; i++) {
        const int mm=1<<(s[i]-'A');
        const int kk=s[i]-'A'+1;

        dp[i+1]=dp[i]; /* not choose the current one */

        for(int mask=0; mask<b; mask++) {
            if(mask&mm) {
                dp[i+1][mask][kk]+=dp[i][mask][kk];
                for(int k=0; k<=10; k++) {  /* choose the current one */
                    if(k!=kk) 
                        dp[i+1][mask][kk]+=dp[i][mask^mm][k];
                }
            }
        }
    }

    mint ans=0;
    for(int i=0; i<b; i++) 
        for(int k=1; k<=10; k++)
            ans+=dp[n][i][k];

    cout<<ans.val()<<endl;
}

signed main() {
    solve();
}
