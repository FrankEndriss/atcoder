/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    if (res < 0)
        res += mod;

    else if(res>=mod)
        res-=mod;

    return res;
}

void solve() {
    cini(n);
    vll a(n);
    vi b(60);   // b[i]== times bit i is set
    for(int i=0; i<n; i++) {
        cin>>a[i];
        for(int j=0; j<60; j++) {
            if(a[i]&(1LL<<j))
                b[j]++;
        }
    }
#ifdef DEBUG
cout<<"bitcnt:20=";
for(int i=0; i<20; i++) 
    cout<<b[i]<<" ";
cout<<endl;
#endif

    vi bmuls(60);   
    bmuls[0]=1;
    for(int i=1; i<60; i++)
        bmuls[i]=toPower(2,i);

    ll ans=0;
    for(int i=0; i<n; i++) {
        for(int j=0; j<60; j++) {
            int bsum;
            if(a[i]&(1LL<<j)) {
                b[j]--;
                bsum=(n-i-1)-b[j];
            } else {
                bsum=b[j];
            }
            if(j>0) {
                bsum=mul(bsum, bmuls[j]);
            }
            ans=pl(ans,bsum);

        }
    }
    cout<<ans<<endl;

}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
        solve();
}

