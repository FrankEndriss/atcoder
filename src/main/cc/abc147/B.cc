/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

ll atol(string &s) {
    istringstream iss(s);
    ll i;
    iss>>i;
    return i;
}

void atoli(string &s, vll &a) {
    istringstream iss(s);
    int i=0;
    while(iss.good())
        cin>>a[i++];
}

void solve() {
    cins(s);
    int sz=(int)s.size();
    int l,r;
    if(sz&1) {
        l=r=((sz+1)/2)-1;
    } else {
        l=(sz/2)-1;
        r=l+1;
    }

    int ans=0;
    for(; l>=0; l--, r++) {
        if(s[l]!=s[r])
            ans++;
    }
    cout<<ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
        solve();
}

