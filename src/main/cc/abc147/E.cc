/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<pii> vpii;
typedef vector<vpii> vvpii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<vb> vvb;
typedef vector<vvb> vvvb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;


void solve() {
    cini(H);
    cini(W);
    vvi a(H, vi(W));
    vvi b(H, vi(W));
    vvi c(H, vi(W));
    for(int i=0; i<H; i++)
        for(int j=0; j<W; j++) 
            cin>>a[i][j];
    for(int i=0; i<H; i++)
        for(int j=0; j<W; j++)  {
            cin>>b[i][j];
        }

    const int OFFS=80*160+1;

    vvvb dp(H, vvb(W, vb(OFFS*2)));
    dp[0][0][a[0][0]-b[0][0]+OFFS]=true;
    dp[0][0][b[0][0]-a[0][0]+OFFS]=true;

    for(int i=0; i<H; i++) {
        for(int j=0; j<W; j++) {
            for(int k=0; k<OFFS*2; k++) {
                if(dp[i][j][k]) {
                    if(i+1<H) {
                        int val=a[i+1][j]-b[i+1][j];
                        dp[i+1][j][k+val]=true;
                        dp[i+1][j][k-val]=true;
                    }
                    if(j+1<W) {
                        int val=a[i][j+1]-b[i][j+1];
                        dp[i][j+1][k+val]=true;
                        dp[i][j+1][k-val]=true;
                    }
                }
            }
        }
    }

    int ans=1e9;
    for(int i=0; i<OFFS*2; i++)
        if(dp[H-1][W-1][i])
            ans=min(ans, abs(i-OFFS));

    cout<<ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
        solve();
}

