/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

ll atol(string &s) {
    istringstream iss(s);
    ll i;
    iss>>i;
    return i;
}

void atoli(string &s, vll &a) {
    istringstream iss(s);
    int i=0;
    while(iss.good())
        cin>>a[i++];
}

int cntBits(int i) {
    int ans=0;
    while(i) {
        ans+=(i&1);
        i>>=1;
    }
    return ans;
}

void solve() {
    cini(n);
    vvi a(n, vi(n, 2));

    for(int i=0; i<n; i++) {
        cini(A);
        for(int j=0; j<A; j++) {
            cini(x);
            x--;
            cini(y);
            a[i][x]=y;
        }
    }

// a[i][j]== person i says person j is honest==1, unkind==0, unknown==2

    int ans=0;
    for(int i=0; i<(1<<n); i++) {  // the truth

        // check all statements of all persons
        // if all is ok then i is possible truth
        bool ok=true;
        for(int j=0; j<n && ok; j++) {    
            if(i&(1<<j)) {  // if jth bit is set j is honest, 
                // so all j says must be true
                for(int k=0; k<n && ok; k++) {
                    if(a[j][k]!=2) {    // j says something about k
                        if(a[j][k]!=((i>>k)&1))
                            ok=false;
                    }
                }
            }
        }
        if(ok)
            ans=max(ans,cntBits(i));
    }
    cout<<ans<<endl;
        
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
        solve();
}

