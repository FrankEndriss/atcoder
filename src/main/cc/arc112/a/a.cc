
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * consider each C
 *
 * foreach possible a there are a number of b
 * such that the result is in range [a,b]
 *
 * How to find the closed formular?
 * for minA, 
 * minB=
 * maxB=b
 */
void solve() {
    cini(a);
    cini(b);
    if(a>b)
        swap(a,b);

    //cerr<<"a="<<a<<" b="<<b<<endl;
    int minA=a+a;
    int maxA=b;
    //cerr<<"minA="<<minA<<" maxA="<<maxA<<endl;
    int cnt=max(0LL, (maxA-minA+1));
    cout<<cnt*(cnt+1)/2<<endl;;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
