
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Note: if player can take the coin he must take it.
 * Else if there exists a child with coin player must move to a child,
 * but player can choose which one child.
 * Else must move to parent.
 *
 * Consider a subtree of hight 2, so one vertex with n child/leaf vertex.
 * The one taking the coin on the parent vertex also gets the 
 * coin from the first child.
 * So with odd number of childs, he gets 2 more, on even 1 more.
 *
 * How can we model the number of won coins in a subtree?
 * Let the first player of a subtree be the one who takes the root coin of it.
 * So, the second player of a subtree has the disadvantage.
 * What is the parity of number of moves in a subtree?
 * Each node is visited once, and once the coin is taken.
 * Then it is visited once per child on the way back from that child.
 * moves=numVertex+numEdges*2
 * And since numEdges==numVertex-1, it is
 * moves=numVertex*3-2
 *
 * Since the player who not takes the first coin chooses the next subtree.
 * If he chooses one with even number of moves he has to choose the next, too.
 * And he will get less coins, twice.
 * So, it should be more profitable to choose one with odd number of moves.
 *
 * ...So we have some recursive math jerk off formular somehow telling
 * which child is the best to move the piece to.
 * Thanks a lot and goodby.
 */
void solve() {
}

signed main() {
    solve();
}
