
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Again we need to find some closed formular :/
 * We want to use mul-1 as often as possible because it is cheaper.
 * if B is positive, we can 
 * p1 to make it negative, then
 * p2 to make it abs 1 bigger, then
 * p1 to make it positive...
 *
 * We use p1 once, then p2 to change absolute value.
 * Then p1 twice, and p2 once, repeat.
 *
 * If we start with positive value this works forever,
 * else it works B times. So if C > 2*B we need to 
 * add on time p1 in beginning.
 *
 * So, we got several cases:
 * C==1: once p1, ans=2 or 1 if B==0
 * C==2: once p2, ans=2
 * C==3: once p1, once p2, ans=3
 * C>3:
 *   B==0 start with p2, then p1
 *     then as long as possible p1, p2, p1
 *
 *   B<0 && 
 *   B>0
 * ... somehow :/
 *
 */
void solve() {

}

signed main() {
    solve();
}
