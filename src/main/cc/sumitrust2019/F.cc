/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cinll(t1);
    cinll(t2);
    cinll(a1);
    cinll(a2);
    cinll(b1);
    cinll(b2);

    if(a1*t1+a2*t2 == b1*t1+b2*t2) {
        cout<<"infinity"<<endl;
        return;
    }
    if(a1==b1) {
        cout<<0<<endl;
        return;
    }

    int ans=0;
    bool aFront=false;
    bool aSame=true;
    ll pa=0;
    ll pb=0;
    while(true) {
//cout<<"pa="<<pa<<" pb="<<pb<<endl;
        bool chg=false;
        pa+=a1*t1;
        pb+=b1*t1;

        if(pa>pb) {
            if(!aSame && !aFront) {
                ans++;
                chg=true;
            }
            aFront=true;
            aSame=false;
        }

        if(pb>pa) {
            if(!aSame && aFront) {
                ans++;
                chg=true;
            }
            aFront=false;
            aSame=false;
        }

        if(pa==pb) {
            ans++;
            aSame=true;
            chg=true;
        }

        pa+=a2*t2;
        pb+=b2*t2;
        if(pa>pb) {
            if(!aSame && !aFront) {
                ans++;
                chg=true;
            }
            aFront=true;
            aSame=false;
        }
        if(pb>pa) {
            if(!aSame && aFront) {
                ans++;
                chg=true;
            }
            aFront=false;
            aSame=false;
        }

        if(pa==pb) {
            ans++;
            aSame=true;
            chg=true;
        }

        ll mi=min(pa,pb);
        pa-=mi;
        pb-=mi;


        if(!chg)
            break;
    }
    cout<<ans<<endl;

}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

