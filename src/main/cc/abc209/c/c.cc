
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* At each position we can use c[i] integers.
 * But not the previous one.
 *
 * consider c[]=3, 5, 7, 6
 * ans[0]=1+1+1=3;
 * ans[1]=2+2+2+3+3=10
 * ans[2]=8+8+8+7+7+10+10=58
 * ans[3]=50+50+50+51+51+48=...
 *
 * How to do that in less than O(n^2)?
 */
using mint=modint1000000007;
void solve() {
    cini(n);
    cinai(a,n);

    int ans=a[0];
    for(int i=1; i<n; i++) {

    }
}

signed main() {
    solve();
}
