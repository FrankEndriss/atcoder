
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* we got a directed graph with circles and need to find 
 * the parity of possible pathlengths.
 * Each player has a preferred pathlen parity, and if he can
 * choose a path he will choose the one with that parity.
 *
 * How to choose the right path?
 * We need to know foreach vertex the possible pathlenghts :/
 * Would be simpler without loops.
 *
 * There are vertex without outgoing edges, lets call them 
 * terms. Each player wants to choose a vertex at even dist
 * to a term, since these are winning positions.
 * But, on the way to the term, there must not be any choice
 * for the opponent to another term, of his parity.
 *
 * Lets find the terms, and from the terms the winning
 * positions. But somehow consider that the loosing
 * position between the winning positions must not be
 * winning positions of other terms.
 * Because such loosing/winning position kind of kills the
 * first term.
 * How to implement that?
 */
void solve() {
}

signed main() {
    solve();
}
