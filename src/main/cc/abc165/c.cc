/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* find the seq of len N with max elem M
 * where the diff off a[b]-a[c] is maximized
 * Let sort the quads by b and assign in order.
 * How to not ignore d?
 *
 * simply sort by d and place what is possible?
 *
 * can we find xor pairs of quads?
 * if two overlap in a way we cannot assign values to
 * a they are exclusive. And then?
 *
 * knapsack
 *
 * can we brute force all possible ans-arrays?
 * ..no, since that are up to 10^10
 */
struct quad {
    int a,b,c,d;
}
void solve() {
    cini(n);
    cini(m);
    cini(q);

    vector<quad> vq(q);
    for(int i=0; i<n; i++) {
        cin>>vq[i].a>>vq[i].b>>vq[i].c>>vq[i].d;
        vq[i].a--;
        vq[i].b--;
    }
    sort(vq.begin(), vq.end(), [&](quad &q1, quad &q2) {
        //if(q1.d!=q2.d)
        //    return q2.d<q1.d;   // biggest first
        //else 
        if(q1.a!=q2.a)
            return q1.a<q2.a;
        else
            return q1.b<q2.b;
    });

/* try to set the q. if not possible.. then what? */
    vi a(n,1);
    for(int i=0; i<q; i++) {
        if(a[vq[i].b]==-1) {
            a[vq[i].b]=
        }
    }
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

