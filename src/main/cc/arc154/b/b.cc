
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/string>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Check freq of all symbols, must be same.
 *
 * Then iterate until end, count wrap arrounds...
 * Not so simple.
 *
 * Consider T from right to left.
 * Also, we have to make S from right to left.
 *
 * So, the longest postfix of S that is a prefix of T
 * does not need to be processed, all other chars have to.
 *
 * This is somehow prefix arrays or some string algorithm...
 * ... looks like Z algorithm with t+"$"+s
 *
 * ***
 * No, its much more complected:
 * Consider the first notmoved char in S, all left of it where moved,
 * and we whare ok to insert them anywhere.
 * So, all chars notmoved in S must be a subseq of T.
 * So we need to find the longest postfix of S that is a subseq of T.
 * Find len by going from right to left.
 * ...but that also not right, since we cannot move the chars anywhere...
 * No fun for me, to complecated :/
 */
void solve() {
    cini(n);
    cins(s);
    cins(t);

    vi fs(256);
    vi ft(256);
    for(int i=0; i<n; i++) {
        fs[s[i]]++;
        ft[t[i]]++;
    }
    if(fs!=ft) {
        cout<<-1<<endl;
        return;
    }

    int tidx=n-1;
    int cnt=0;
    for(int i=n-1; i>=0; i--) {
        if(s[i]==t[tidx]) {
            cnt++;
            tidx--;
        }
    }
    cout<<n-cnt<<endl;

}

signed main() {
        solve();
}
