
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider most significant digit differing in both
 * numbers.
 * We move all other  bigger digits to the lower one,
 * and all lower to the bigger one.
 *
 * Then, how to output the result of multiplication?
 * -> Consider the multiplication of A by 1,
 *  then multiply by the first digit.
 *  Then by 10, and the next digit... and so on.
 */
using mint=modint998244353;
void solve() {
    cini(n);
    cins(a);
    cins(b);

    string a0;
    string b0;

    int l=0;
    for(int i=0; i<n; i++) {
        if(l==0) {
            a0.push_back(a[i]);
            b0.push_back(b[i]);
            if(a[i]<b[i])
                l=-1;
            else if(a[i]>b[i])
                l=1;
        } else if(l>0) {    /* a0 smaller */
            a0.push_back(max(a[i], b[i]));
            b0.push_back(min(a[i], b[i]));
        } else {
            b0.push_back(max(a[i], b[i]));
            a0.push_back(min(a[i], b[i]));
        }
    }

    mint num=0;
    for(int i=0; i<n; i++) {
        num*=10;
        num+=(a0[i]-'0');
    }

    mint ans=0;
    for(int i=n-1; i>=0; i--) {
        ans+=num*(b0[i]-'0');
        num*=10;
    }

    cout<<ans.val()<<endl;
}

signed main() {
        solve();
}
