
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * How?
 * Obviously we have to create the longest possible subseqs.
 * So, one starts with the first letter.
 * Create this.
 * What second, last letter?
 * How big is K?
 * Just try to create the biggest possible.
 * ...
 * I will end in off-by-one hell :/
 * And it seems to notwork for some reason.
 */
void solve() {
    cini(n);
    n*=3;
    cins(s);
    assert(s.size()==n);


    vi ans(n);
    for(int j=0; j<6; j++) {
        vvi preL(3, vi(n+1));
        for(int i=0; i<n; i++) {
            for(int k=0; k<3; k++)
                preL[k][i+1]=preL[k][i];

            int jj=s[i]-'A';
            if(ans[i]==0)
                preL[jj][i+1]=preL[jj][i]+1;
        }

        int ma=0;
        vi p= {0,1,2};
        vi maperm=p;
        do {
            int cnt=0;
            for(int l=0, r=n-1; l<r;) {
                while(l<r && (s[l]-'A'!=p[0] || ans[l]!=0))
                    l++;

                while(l<r && (s[r]-'A'!=p[2] || ans[r]!=0))
                    r--;

                if(l>=r)
                    break;

                cnt++;

                /* is this correct prefix sum? */
                int lma=min(cnt, preL[p[1]][r]-preL[p[1]][l+1]);
                if(lma>ma) {
                    ma=lma;
                    maperm=p;
                }
                l++;
                r--;
            }
        } while(next_permutation(all(p)));

        vi cnt(3);
        for(int i=0; i<n; i++) {
            if(cnt[0]<ma) {
                if(ans[i]==0 && s[i]=='A'+maperm[0]) {
                    cnt[0]++;
                    ans[i]=j+1;
                }
            } else if(cnt[1]<ma) {
                if(ans[i]==0 && s[i]=='A'+maperm[1]) {
                    cnt[1]++;
                    ans[i]=j+1;
                }
            } 
        }
        
        for(int i=n-1; i>=0; i--) {
            if(cnt[2]<ma) {
                if(ans[i]==0 && s[i]=='A'+maperm[2]) {
                    cnt[2]++;
                    ans[i]=j+1;
                }
            }
        }
        /*
        assert(cnt[0]==cnt[1]);
        assert(cnt[1]==cnt[2]);
        assert(cnt[2]==ma);
        */
    }

    for(int i=0; i<n; i++)
        cout<<ans[i];
    cout<<endl;
}

signed main() {
    solve();
}
