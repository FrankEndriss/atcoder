/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/** stolen from tourist, Problem D, https://codeforces.com/contest/1208/standings/page/1 */
template <typename T>
struct fenwick {
    vector<T> fenw;
    int n;

    fenwick(int _n) : n(_n) {
        fenw.resize(n);
    }

    void update(int x, T v) {
        while (x < n) {
            fenw[x] += v;
            x |= (x + 1);
        }
    }

    /* get sum of range (0,x), including x */
    T query(int x) {
        T v{};
        while (x >= 0) {
            v += fenw[x];
            x = (x & (x + 1)) - 1;
        }
        return v;
    }
};

template <typename T>
struct fenwick2d {
    vector<fenwick<T>> fenw2d;
    int n, m;

    fenwick2d(int x, int y) : n(x), m(y) {
        fenw2d.resize(n, fenwick<int>(m));
    }

    void update(int x, int y, T v) {
        while (x < n) {
            fenw2d[x].update(y, v);
            x |= (x + 1);
        }
    }

    T query(int x, int y) {   // range 0..x/y, including x/y
        x=min(x, n-1);
        y=min(y, m-1);
        T v{};
        while (x >= 0) {
            v += fenw2d[x].query(y);
            x = (x & (x + 1)) - 1;
        }
        return v;
    }
};

/**
 * Note that we can make 2nd stamp smaller if it is bigger than the first.
 * Second chooses the subgrid of first with max sum.
 * First chooses the subgrid so that sum of it minus second sum is max.
 *
 * So find the max sum of second foreach position, put that sums into 
 * 2D fenwick.
 *
 * Then findsum of 1st for all positions, and in the rect of possible 
 * positions of second the max val of second.
 *
 * ans=max of that differences.
 *
 * ...but max with fenwick does not work :/
 * How to find the max in 2d subrect?
 *
 * Note that we allways need to max of a fixed size subrect, so create that
 * with col and row max of that size.
 *
 * Note that it is super error prone implementation :/
 * Still something wrong :/
 */
void solve() {
    cini(h);
    cini(w);

    cini(h1);
    cini(w1);
    cini(h2);
    cini(w2);

    h2=min(h2,h1);
    w2=min(w2,w1);

    cerr<<"begin"<<endl;
    vvi pre(h+1,vi(w+1));
    for(int i=0; i<h; i++) 
        for(int j=0; j<w; j++) {
            cini(aux);
            pre[i+1][j+1]=aux+pre[i][j+1]+pre[i+1][j]-pre[i][j];
        }
    cerr<<"did pre"<<endl;

    /* first create dp2[i][j]=sum of rect of size h2,w2 beginning at i,j */
    vvi dp2(h, vi(w));
    for(int i=0; i+h2-1<h; i++) 
        for(int j=0; j+w2-1<w; j++) {
            dp2[i][j]=pre[i+h2][j+w2]
                -pre[i][j+w2]
                -pre[i+h2][j]
                +pre[i][j];
        }
    cerr<<"did dp2"<<endl;

    /* same for h1,w1 */
    vvi dp1(h, vi(w));
    for(int i=0; i+h1-1<h; i++) 
        for(int j=0; j+w1-1<w; j++) {
            dp1[i][j]=pre[i+h1][j+w1]
                -pre[i][j+w1]
                -pre[i+h1][j]
                +pre[i][j];
        }
    cerr<<"did dp1"<<endl;

    /* now we need a dp3[i][j]=max value in rect of size h1-h2+1,w1-w2+1 starting at i,j
     * For this create column max of last h1-h2+1 values,
     * the from them the row max of last w1-w2+1 col values.
     **/
    vvi dpC(h, vi(w));  /** dpC[i][j]=max val of h1-h2+1 column values starting at dp2[i][j] */
    int vals=h1-h2+1;
    for(int j=0; j<w; j++) {
        multiset<int> q;
        for(int i=0; i+vals-1<h; i++) {
            q.insert(dp2[i][j]);
            if(q.size()>vals) {
                auto it=q.find(dp2[i-vals][j]);
                q.erase(it);
            }
            if(i+1>=vals)
                dpC[i+1-vals][j]=*q.rbegin();
        }
    }
    cerr<<"did dpC"<<endl;

    vvi dpR(h, vi(w));
    vals=w1-w2+1;
    for(int i=0; i<h; i++) {
        multiset<int> q;
        for(int j=0; j+vals-1<w; j++) {
            q.insert(dpC[i][j]);
            if(q.size()>vals) {
                auto it=q.find(dpC[i][j-vals]);
                q.erase(it);
            }
            if(j+1>=vals)
                dpR[i][j+1-vals]=*q.rbegin();
        }
    }
    cerr<<"did dpR"<<endl;

    int ans=0;
    for(int i=0; i+h1-1<h; i++) {
        for(int j=0; j+w1-1<w; j++) {
            ans=max(ans, dp1[i][j]-dpR[i][j]);
        }
    }
    cout<<ans<<endl;


}

signed main() {
    solve();
}
