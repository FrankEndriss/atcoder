/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
void solve() {
    cini(n);
    cini(k);

    vvi p(n, vi(3));
    vi a(n);
    for(int i=0; i<n; i++)
        for(int j=0; j<3; j++)  {
            cin>>p[i][j];
            a[i]+=p[i][j];
        }

    sort(all(a), greater<int>());
    int score=a[k-1];   /* must reach better score */
    for(int i=0; i<n; i++) {
        int sum=p[i][0]+p[i][1]+p[i][2]+300;
        if(sum>=score)
            cout<<"Yes"<<endl;
        else
            cout<<"No"<<endl;
    }
}

signed main() {
    solve();
}
