/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Foreach query of type 1 we need to find 
 * the next position right of xi that is !=-1.
 * Just maintain that positions in a set, and binary search?
 */
const int N=1<<20;
void solve() {
    set<int> s;
    for(int i=0; i<N; i++) 
        s.insert(i);

    cini(q);
    vi a(N, -1);
    for(int i=0; i<q; i++) {
        cini(t);
        cini(xx);
        int x=xx;
        x--;
        x%=N;
        if(t==1) {
            auto it=s.lower_bound(x);
            if(it==s.end())
                it=s.begin();
            a[*it]=xx;
            s.erase(it);
        } else {
            cout<<a[x]<<endl;
        }
    }
}

signed main() {
    solve();
}
