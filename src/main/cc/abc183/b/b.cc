
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * let 
 * sk be slope of line from S to X.
 * gk be slope of line from X to G.
 * sk=-1/gk
 * x=Sx + sy * -1/gk
 * x=Gx - gy * gk
 *
 *
 * d=Gx-Sx.
 * ds=d*Sy/(Sy+Gy)
 * dg=d*Gy/(Sy+Gy)
 * ds+dg=d
 */
void solve() {
    cini(sx);
    cini(sy);
    cini(gx);
    cini(gy);

    double d=gx-sx;
    double ds=d*sy/(sy+gy);
    double dg=d*gy/(sy+gy);
    cout<<sx+ds<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
