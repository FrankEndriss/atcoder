
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int MOD=1e9+7;

void norm(int &i) {
    while(i<0)
        i+=MOD;
    i%=MOD;
}

/* note that we can move only down/right, so there
 * is no loop possible.
 *
 * This most likely witll timeout.
 * How to optimize?
 *
 * First col and first row
 *
 * Build BITs for all rows and cols.
 * Index forach position how much fields to the left
 * contribute to that field.
 * Then build the dp in direction right/down maintaining
 * the updates as range updates in segtrees.
 * -> this does not work since we cannot separate
 *  rows and columns.
 *
 *  We need to build prefix sums on rows, cols and diagonals!
 */
void solve() {
    cini(h);
    cini(w);
    cinas(s,h);

    vvi dp(h, vi(w));
    vvi pretop(h, vi(w));
    vvi prelef(h, vi(w));
    vvi predia(h, vi(w)); 

    dp[0][0]=1;
    pretop[0][0]=1;
    prelef[0][0]=1;
    predia[0][0]=1;

    for(int i=0; i<h; i++) {
        for(int j=0; j<w; j++) {
            if(i==0 && j==0)
                continue;

            if(s[i][j]=='#')
                continue;

            if(i>0)
                dp[i][j]+=pretop[i-1][j];
            if(j>0)
                dp[i][j]+=prelef[i][j-1];
            if(i>0 && j>0)
                dp[i][j]+=predia[i-1][j-1];

            norm(dp[i][j]);

            pretop[i][j]=dp[i][j];
            if(i>0) {
                pretop[i][j]+=pretop[i-1][j];
                norm(pretop[i][j]);
            }

            prelef[i][j]=dp[i][j];
            if(j>0) {
                prelef[i][j]+=prelef[i][j-1];
                norm(prelef[i][j]);
            }

            predia[i][j]=dp[i][j];
            if(i>0 && j>0) {
                predia[i][j]+=predia[i-1][j-1];
                norm(predia[i][j]);
            }
        }
    }

    cout<<dp[h-1][w-1]<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
