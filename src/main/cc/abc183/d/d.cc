
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to check if there exists a minute
 * where more water is needed than supplied
 * by the heater. Sweep line.
 */
void solve() {
    cini(n);
    cini(w);

    vector<pii> evt;
    for(int i=0; i<n; i++) {
        cini(s);
        cini(t);
        cini(p);
        evt.emplace_back(s, p);
        evt.emplace_back(t, -p);
    }
    sort(all(evt));

    int sum=0;
    int prev=-1;
    string ans="Yes";
    for(int i=0; i<2*n; i++) {
        if(evt[i].first!=prev && sum>w) {
            ans="No";
            break;
        }

        sum+=evt[i].second;
        prev=evt[i].first;
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
