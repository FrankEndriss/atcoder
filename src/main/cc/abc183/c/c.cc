/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Since N==8 there are not that much paths.
 */
void solve() {
    cini(n);
    cini(k);
    vvi t(n, vi(n));
    for(int i=0; i<n; i++) 
        for(int j=0; j<n; j++) 
            cin>>t[i][j];

    vi p(n-1);
    iota(all(p), 0);

    int ans=0;
    do {
        int len=0;
        int prev=0;
        for(int next : p) {
            int idx=next+1;
            len+=t[prev][idx];
            prev=idx;
        }
        len+=t[prev][0];
        if(len==k)
            ans++;

    }while(next_permutation(all(p)));

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
