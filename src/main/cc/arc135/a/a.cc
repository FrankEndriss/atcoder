/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * An even number creates the even number x/2
 * An odd number creates two numbers, x/2 and x/2+1
 * So just simulate, it stops fairly fast.
 */
using mint=modint998244353;
void solve() {
    cini(x);

    map<int,mint> memo;

    function<mint(int)> go=[&](int i) {
        if(i<=4)
            return mint(i);

        auto it=memo.find(i);
        if(it!=memo.end())
            return it->second;

        mint ans=go(i/2)*go((i+1)/2);
        return memo[i]=ans;
    };

    mint ans=go(x);
    cout<<ans.val()<<endl;
}

signed main() {
    solve();
}
