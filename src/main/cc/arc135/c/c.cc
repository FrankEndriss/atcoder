/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that we cannot use any X, but only those contained in a[].
 * Doing an operation twice with same X does noting, so 
 * we search foreach a[i] in intial a[], use it for xor or not.
 *
 * Consider highest bit in a[].
 * If there is more set bits than unset bits we want to 
 * have an even number of usages, else odd. If same then dont care.
 *
 * Note that there are some special rules:
 * If some bit is set in all a[i], then unsetting it once
 * will cause it to go away persistent :/
 *
 * Consider also the number of operations.
 * If we do overall an even number of ops, the first op will be neutralized by the
 * other operations. Also all other ops on even positions.
 *
 * So, if we do odd number of operations, only the even positions contribute,
 * else only the odd ones.
 *
 * So, consider doing the the odd position operations allways on the same
 * elements as the operation before (which is then 0), so it does nothing anyway.
 * ***
 * No. Consider again the second operation.
 * It negates the first, or it does nothing.
 * If it negates the first only the second applies...
 * That is, we can do effectively only one operation, whatever we do.
 * So, find the a[i] that produces the best result.
 *
 * That is then based on bit counting.
 * Consider the MSB. If there are more numbers with having that bit set,
 * we do not want to unset it. So, it mainly halves the number of
 * possible candidates.
 * Then go on with the second bit and so on.
 */
void solve() {
    cini(n);
    cinai(a,n);

    vi cnt(30);
    for(int i=0; i<n; i++)
        for(int j=0; j<30; j++) 
            cnt[j]+=(a[i]>>j)&1LL;

    int ans=accumulate(all(a), 0LL);

    for(int i=0; i<n; i++) {
        int lans=0;
        for(int j=0; j<30; j++) {
            if(a[i]&(1<<j))
                lans+=(1<<j)*(n-cnt[j]);
            else
                lans+=(1<<j)*cnt[j];
        }

        ans=max(ans, lans);
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}
