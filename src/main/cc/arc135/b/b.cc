/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * s[0]=a[0]+a[1]+a[2]
 * s[1]=a[1]+a[2]+a[3]
 *
 * Note s[n-1]=a[n-1]+a[n]+a[n+1]
 * And no other s[i] depend on a[n+1]
 * So we can choose a[n] to any value we like...somehow.
 *
 ****
 * What happend if we increment a[0]?
 * ->a[1] (or a[2]) gets decremented
 *  So, if a[1] gets decremented, a[2] (or a[3]) gets incremented
 *  ...
 *  ****
 *  However, obviously something missing :/
 *  idk
 */
void solve() {
    cini(n);
    cinai(s,n);

    if(n==1) {
        cout<<"Yes"<<endl;
        cout<<s[0]<<" 0 0"<<endl;
        return;
    }

    vi a(n+2);
    for(int i=n-1; i>=0; i--)
        a[i]=s[i]-a[i+1]-a[i+2];

    // now most likely some a[i]<0
    // We can adjust some a[i]+=y by
    // a[i+1]-=y
    // a[i+3]+=y
    // a[i+4]-=y
    // ...
    vi inc(3);
    for(int i=0; i<n; i++) {
        a[i]+=inc[i%3];
        if(a[i]<0) {
            inc[i%3]-=a[i];
            inc[(i+1)%3]+=a[i];
            a[i]=0;
        }
    }
    a[n]+=inc[n%3];
    a[n+1]+=inc[(n+1)%3];

    bool ok=true;
    if(a[n]<0 || a[n+1]<0)
        ok=false;

    for(int i=0; i<n; i++) {
        if(a[i]<0 || s[i]!=a[i]+a[i+1]+a[i+2])
            ok=false;
    }

    if(!ok)
        cout<<"No"<<endl;
    else {
        cout<<"Yes"<<endl;
        for(int i=0; i<n+2; i++)
            cout<<a[i]<<" ";
        cout<<endl;
    }

}

signed main() {
    solve();
}
