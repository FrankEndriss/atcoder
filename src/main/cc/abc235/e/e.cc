/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* LCA based on euler path with segment tree and tin/tout. */
struct LCA {
    vector<int> height, euler, first, segtree;
    vector<bool> visited;
    int n;
    int time;
    vector<int> tin;
    vector<int> tout;

    LCA(vector<vector<int>> &adj, int root = 0) {
        n = (int)adj.size();
        height.resize(n);
        first.resize(n);
        euler.reserve(n * 2);
        visited.assign(n, false);
        tin.resize(n);
        tout.resize(n);
        time=1;
        dfs(adj, root);
        int m = (int)euler.size();
        segtree.resize(m * 4);
        build(1, 0, m - 1);
    }

    void dfs(vector<vector<int>> &adj, int node, int h = 0) {
        tin[node]=time++;
        visited[node] = true;
        height[node] = h;
        first[node] = (int)euler.size();
        euler.push_back(node);
        for (auto to : adj[node]) {
            if (!visited[to]) {
                dfs(adj, to, h + 1);
                euler.push_back(node);
            }
        }
        tout[node]=time++;
    }

    void build(int node, int b, int e) {
        if (b == e) {
            segtree[node] = euler[b];
        } else {
            int mid = (b + e) / 2;
            build(node << 1, b, mid);
            build(node << 1 | 1, mid + 1, e);
            int l = segtree[node << 1], r = segtree[node << 1 | 1];
            segtree[node] = (height[l] < height[r]) ? l : r;
        }
    }

    int query(int node, int b, int e, int L, int R) {
        if (b > R || e < L)
            return -1;
        if (b >= L && e <= R)
            return segtree[node];
        int mid = (b + e) >> 1;

        int left = query(node << 1, b, mid, L, R);
        int right = query(node << 1 | 1, mid + 1, e, L, R);
        if (left == -1) return right;
        if (right == -1) return left;
        return height[left] < height[right] ? left : right;
    }

    /* @return true if parent is ancestor of node in O(1) */
    bool isAncestor(int node, int parent) {
        return tin[node]>tin[parent] && tin[node]<tout[parent];
    }

    /* @return the lca of u and v */
    int lca(int u, int v) {
        int left = first[u], right = first[v];
        if (left > right)
            swap(left, right);
        return query(1, 0, (int)euler.size() - 1, left, right);
    }
};

/**
 * Consider the MST of G.
 * If an edge e[i] is part of that MST, 
 * then and edge enew would be also part of ...
 * ****
 * While creating the MST the both vertex u and v where
 * connected by adding some vertex. If that vertex
 * has less w[i] than enew, then enew is not in the MST.
 * How to implement?
 * How can we see if the edges with less weigth than enew 
 * connect u and v or not?
 * -> Well, if on the path from u to v there is an edge with
 *    more weight than enew, enew will change the MST.
 * So, on each query find max weight on path from u to v. How?
 * -> Somehow lowest ancestor.
 * Root somewhere, then... ???
 *
 * To much algorythms.
 * ...
 * ******
 * Ok, we can do some kind of binary lifting.
 * Then, if we got lca(u,v) we can find the max
 * weight in that table.
 */
void solve() {
	cini(n);
	cini(m);
	cini(q);

	vector<tuple<int,int,int>> edg;
	for(int i=0; i<m; i++) {
		cini(u); u--;
		cini(v); v--;
		cini(w);
		adj[u].push_back(v);
		adj[v].push_back(u);

		edg.emplace_back(w,u,v);
	}
	sort(all(edg));

	dsu d(n);

	vvi adj(n);
	for(int i=0; i<m; i++) {
		auto [w,u,v]=edg[i];
		if(!d.same(u,v)) {
			adj[u].push_back(v);
			adj[v].push_back(u);
			d.merge(u,v);
		}
	}

	LCA lca(adj);

	vvi dp(n, vi(20));
	for(int i=0; i<n; i++) 
		dp[i][0]=0;

	vi pp(n);
	function<void(int,int)> dfs=[&](int v, int p) {
		pp[v]=p;
		for(int chl : adj[v])
			if(chl!=p)
				dfs(chl, v);
	};

	for(int j=1; j<20; j++) {
		for(int i=0; i<n; i++) {
			dp[i][j]=max(dp[i][j-1]
					...we also need binary lifting of the parents...
					...to tired.
		}
	}

	for(int i=0; i<q; i++) {
	}
}

signed main() {
    solve();
}
