/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider a+b+c=N
 * Then there is onle one way, since each garden needs one seed.
 * For a+b+c<N there is no way.
 * Let y=a+b+c-N, ie the "free to place" seedlings.
 * If we put firstly b,c into the gardens, then we can put an a
 * in each b,c garden, in 2^min(a,y,b+c) ways.
 *
 * ... But also we can use one more a, so one less b,c.
 * So, how?
 * We cannot iterate all possible a,b,c distributions.
 *
 * Let x be the number of ways we can construct the y seeds...
 * We can place min(b+c, a, y) of the a seeds in one of the b and c gardens.
 * We can place min(a+c, b, y) of the b seeds in one of the a and c gardens.
 * We can place min(a+b, c, y) of the c seeds in one of the a and b gardens.
 */
using mint=modint998244353;

void solve() {
	cini(n);
	cini(a);
	cini(b);
	cini(c);
}

signed main() {
    solve();
}
