/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * ???
 * Ok, initial x=a.
 * So, if a is multiple of 10 then there is no sol since we can never shift.
 * Also note that we can never make the number less digits.
 * But note that the seq of moves is fairly short.
 * So dfs back from N to 1.
 */
void solve() {
	cini(a);
	cini(N);

	queue<pii> q;	/* <num,cnt> */
	q.emplace(N, 0);
	map<int,bool> vis;
	vis[N]=true;
	while(q.size()) {
		auto [num,cnt]=q.front();
		q.pop();
		if(num==1) {
			cout<<cnt<<endl;
			return;
		}

		if(num%a==0) {
			if(!vis[num/a]) {
				q.emplace(num/a, cnt+1);
				vis[num/a]=true;
			}
		}
		vi dig;
		int nn=num;
		while(nn) {
			dig.push_back(nn%10);
			nn/=10;
		}
		if(dig.size()>1 && dig[dig.size()-2]>0) {
			int last=dig.back();
			dig.pop_back();
			int val=0;
			while(dig.size()) {
				val*=10;
				val+=dig.back();
				dig.pop_back();
			}
			val*=10;
			val+=last;
			if(!vis[val]) {
				q.emplace(val, cnt+1);
				vis[val]=true;
			}
		}
	}
	cout<<-1<<endl;
}

signed main() {
    solve();
}
