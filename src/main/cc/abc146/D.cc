/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

struct edge {
    int a;
    int b;
    int c;
};

void solve() {
    cini(n);
    vector<set<int>> tree(n);
    vector<edge> edges;
    for(int i=0; i<n-1; i++) {
        cini(a);
        cini(b);
        a--;
        b--;
        edges.push_back({a,b,-1});
        tree[a].insert(i);           
        tree[b].insert(i);
    }

    int maxCol=0;

/* pedge edge to parent, which is colored. */
    function<void(int,int)> dfs=[&](int node, int pedge) {
        int c=1;
        int pc=pedge>=0?edges[pedge].c:-1;
        for(int idx : tree[node]) {
            if(idx==pedge)
                continue;

            int chl;
            if(edges[idx].a==node)
                chl=edges[idx].b;
            else 
                chl=edges[idx].a;
            if(c==pc)
                c++;
            edges[idx].c=c;
            maxCol=max(maxCol,c);
            c++;
            dfs(chl,idx);
        }
    };

    dfs(0,-1);

    cout<<maxCol<<endl;
    for(int i=0; i<n-1; i++)
        cout<<edges[i].c<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int t=1;
    while(t--)
        solve();
}

