/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    cini(m);
    cins(s);

    vi pos(n+1, -1);
    pos[0]=0;
    int maxPos=0;   // max position per move

    while(true) {
        int nMaxPos=maxPos;
        for(int i=1; i<=m; i++) {
            if(maxPos+i>n)
                break;

            if(s[maxPos+i]!='1') {
                pos[maxPos+i]=pos[maxPos]+1;
                nMaxPos=max(nMaxPos, maxPos+i);
            }
        }
        if(nMaxPos>maxPos)
            maxPos=nMaxPos;
        else
            break;
    }

#ifdef DEBUG
    cout<<"dump"<<endl;
    for(int i=0; i<=n; i++)
        cout<<pos[i]<<" ";
    cout<<endl;
#endif


    if(pos[n]<0) {
        cout<<pos[n]<<endl;
        return;
    }

    stack<int> ans;
    int p=n;
    while(p>0) {
#ifdef DEBUG
cout<<"pos="<<p<<endl;
#endif
        if(p<=m) {
            ans.push(p);
            break;
        }

        bool ok=false;
        for(int i=m; i>=1; i--) {
            if(pos[p-i]==pos[p]-1) {
                p-=i;
                ans.push(i);
                ok=true;
                break;
            }
        }
        assert(ok);
    }

    while(ans.size()) {
        int a=ans.top();
        ans.pop();
        cout<<a<<" ";
    }
    cout<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int t=1;
    while(t--)
        solve();
}

