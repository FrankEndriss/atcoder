
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

int sign(int i) {
    if(i<0)
        return -1;
    else if(i==0)
        return 1;
}

/**
 * On first round count number of cells.
 * And offset in the end.
 * Foreach cell find, how much rounds it
 * takes that it is re-entered by a later round. After that
 * round it or a copy of it is recleaned by the later round.
 * So the later round contributes one less.
 *
 * If a cell is re-entered in a later round, then this is because
 * there is another cell that, translated by (n*xoff, n*yoff) equals
 * the current cell.
 * How to find the n in less than O(N)?
 *
 * We can put the cells in aeqvivalenz classes by the offsets.
 * Then check all classes...somehow.
 *
 * Note that the first time the ith cell of a round re-enters an allready
 * seen cell, that cell was first entered in first round.
 *
 * ...but some WA, and might also TLE :/
 */
const int INF=1e9;
void solve() {
    cins(s);
    cini(k);

    int x=0;
    int y=0;
    int minX=0;
    int minY=0;
    int maxX=0;
    int maxY=0;
    set<pii> vis;
    vis.insert(make_pair(0LL,0LL));
    for(int i=0; i<s.size(); i++) {
        if(s[i]=='L')
            x--;
        else if(s[i]=='R')
            x++;
        else if(s[i]=='U')
            y--;
        else if(s[i]=='D')
            y++;
        else
            assert(false);

        vis.insert(make_pair(x,y));

        maxX=max(maxX, x);
        maxY=max(maxY, y);
        minX=min(minX, x);
        minY=min(minY, y);
    }
    const int xoff=x;
    const int yoff=y;

    if(xoff==0 && yoff==0) {
        cout<<vis.size()<<endl;
        return;
    }

    x=0;
    y=0;
    int ans=0;

    set<pii> vis2;
    vis2.insert(make_pair(0LL,0LL));

    for(int i=0; i<s.size(); i++) {
        if(s[i]=='L')
            x--;
        else if(s[i]=='R')
            x++;
        else if(s[i]=='U')
            y--;
        else if(s[i]=='D')
            y++;
        else
            assert(false);

        if(vis2.count({x,y}))
            continue;

        vis2.insert(make_pair(x,y));

        int xx=x;
        int yy=y;
        int cnt=0;
        bool done=false;
        while(cnt<k && xx>=minX && xx<=maxX && yy>=minY && yy<=maxY) {
            cnt++;
            xx+=xoff;
            yy+=yoff;
            if(vis.count({xx,yy})) {
                ans+=cnt;
                done=true;
                break;
            } else 
                vis.insert({xx,yy});
        }
        if(!done)
            ans+=k;
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}
