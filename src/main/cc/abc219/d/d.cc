
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to find foreach [a][b] the min possible boxes.
 * Knapsack like.
 */
const int N=303;
const int INF=1e9;
void solve() {
    cini(n);
    cini(x);    /* we want x times a */
    cini(y);    /* we want y times b */

    /* dp[i][j]=min number of boxes to get i times a and j times b (or more) */
    vvi dp(N, vi(N, INF));
    dp[0][0]=0;

    for(int k=0; k<n; k++) {
        cini(a);
        cini(b);
        vvi dp0=dp;
        for(int i=0; i<N; i++)  {
            const int aa=min(N-1, i+a);
            for(int j=0; j<N; j++) {
                const int bb=min(N-1, j+b);
                dp0[aa][bb]=min(dp0[aa][bb], dp[i][j]+1);
            }
        }
        dp=dp0;
    }

    int ans=INF;
    for(int i=x; i<N; i++)
        for(int j=y; j<N; j++) 
            ans=min(ans, dp[i][j]);

    if(ans==INF)
        ans=-1;

    cout<<ans<<endl;



}

signed main() {
    solve();
}
