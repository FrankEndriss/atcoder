
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
void solve() {
    cins(x);
    assert(x.size()==26);
    cini(n);
    cinas(s,n);

    vi pos(256);
    for(int i=0; i<x.size(); i++) 
        pos[x[i]]=i;

    sort(all(s), [&](string s1, string s2) {
        for(int i=0; i<max(s1.size(), s2.size()); i++) {
            if(i>=s1.size() && i<s2.size())
                return true;
            if(i>=s2.size())
                return false;

            if(s1[i]!=s2[i])
                return pos[s1[i]]<pos[s2[i]];
        }
        return false;   /* s1==s2 */
    });

    for(string ans : s)
        cout<<ans<<endl;
}

signed main() {
    solve();
}
