/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
        tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

const int MOD=1000000007;

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int n, m;
    cin>>n>>m;
    vector<int> s(n);
    vector<int> t(m);

    fori(n)
    cin>>s[i];

    vector<vector<int>> idxT(100001);

    fori(m) {
        cin>>t[i];
        idxT[t[i]].push_back((int)i);
    }

    /* find first of val in t starting search at minIdx */
    function<int(int, int)> firstInTAfter=[&](int minIdx, int val) {
        auto it=lower_bound(idxT[val].begin(), idxT[val].end(), minIdx);
        if(it==idxT[val].end())
            return -1;

        int ret= distance(idxT[val].begin(), it);
        return ret;
    };

    vector<vector<int>> dp(n, vector<int>(m, -1));
    /* count (modulo) the subsequences starting in s at sIdx
     * with match in t after tIdx.
     */
    function<int(int, int)> countAfter=[&](int sIdx, int tIdx) {
        if(sIdx>=n || tIdx>=m)
            return 0;

        int ans=0;
        int ltIdx=firstInTAfter(tIdx, s[sIdx]);
        if(ltIdx>=0) {
            ans+=(m-1-ltIdx);
            ans%=MOD;
            ans+=countAfter(sIdx, ltIdx+1);
            ans%=MOD;
            ans+=countAfter(sIdx+1, ltIdx+1);
            ans%=MOD;
        }

        return ans;
    };

    int ans=0;
    fori(n) {
        ans+=countAfter(i, 0);
        ans%=MOD;
    }
    cout<<ans<<endl;
}

