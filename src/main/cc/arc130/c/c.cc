/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that we need to take care of a carry.
 *
 * Consider WLG a has less digits than b.
 * Then we want to put the 9s of b in position len(a)+1,len(a)+2,...
 * and set the carry at position len(a)-1
 * We can set the carry there, 
 * Before that, we want to make as much possible pairs with sum 9,
 * and set the carry just before the first of these pairs.
 *
 * Then pair all other digits right of that, and since no carry is 
 * possible, oder does not matter.
 *
 * ****
 * ...to tired to implement :/
 */
void solve() {
    vvi ab(2, vi(10));

    cins(a);
    cins(b);
    if(a.size()>b.size())
        a.swap(b);

    for(int c : a)
        ab[0][c-'0']++;
    for(int c : b)
        ab[1][c-'0']++;

    vvi ans(2);
    int c=0;    /* carry */

    cout<<ans<<endl;
}

signed main() {
    solve();
}
