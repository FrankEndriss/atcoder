/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * s(i)==s(j) if ...
 *
 * Consider compressed t<c,cnt>
 * Obiously if cnt>1 we can build pairs of t[i]==t[j],
 * t[i].cnt*(t[i].cnt-1)/2
 *
 * Are there other cases?
 * -> no
 */
void solve() {
    cini(n);
    cins(t);

    vector<pii> tt(1);
    tt[0].first=t[0];
    tt[0].second=1;
    for(int i=1; i<n; i++) {
        if(t[i]==tt.back().first)
            tt.back().second++;
        else
            tt.emplace_back(t[i],1);
    }

    int ans=0;
    for(size_t i=0; i<tt.size(); i++) 
        if(tt[i].second>1) 
            ans+=(tt[i].second*(tt[i].second-1)/2);

    cout<<ans<<endl;

}

signed main() {
    solve();
}
