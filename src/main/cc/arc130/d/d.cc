/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * The rules state that all adj of a vertex must be smaller,
 * or all must be bigger.
 * Looks a bit bipartite... but is not.
 *
 * Consider a leaf (and n>2) p[0], and the parent p[1]
 * 1. If the parent is bigger, it can have n-p[0] values.
 * 2. if the parent is smalle, it can have p[0]-1 values.
 * Then a sibling.
 * In 1. it can have p[0]-1 values,
 * in 2. it can have n-p[0] values.
 * ...but that is dfs, will TLE.
 */
void solve() {
}

signed main() {
    solve();
}
