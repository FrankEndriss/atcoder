/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * How to maintain the data?
 *
 * In begin, all cells have color 0.
 * After painting first row (or col) there are w (or h)
 * cells more painted c and c less painted 0.
 *
 * Each cell/row has a "background" color, which is initially 0,
 * else last color painted.
 * Number of cells of that color in that row/col minus
 * all other col/rows that where painted afterwards.
 *
 * So we need to iterate per color the number of
 * cols of that color, and foreach col subtract the number 
 * of rows that where painted after that col.
 * Same with rows/cols changed.
 *
 * How to find the number of distinct oposite direction painted
 * afterwards?
 * Segment tree somehow?
 * Reverse sort events?
 * -> yes, go from last to first, and count sum in segment.
 */
void solve() {
    cini(h);
    cini(w);
    cini(c);
    cini(q);

    using t3=tuple<int,int,int>;
    vector<t3> evt(q);
    for(int i=0; i<q; i++) {
        cini(t);
        cini(n); n--;
        cini(cc); cc--;
        evt[i]={t,n,cc};
    }

    vi ans(c);

    set<int> visrows;      /* rows painted again later */
    set<int> viscols;      /* cols painted again later */

    for(int i=q-1; i>=0; i--) {
        auto [t,n,cc]=evt[i];
        if(t==1) {  /* paint a row */
            assert(n>=0 && n<h);
            if(visrows.count(n))
                continue;

            ans[cc]+=w-viscols.size();
            visrows.insert(n);
        } else {    /* paint a col */
            assert(n>=0 && n<w);
            if(viscols.count(n))
                continue;

            ans[cc]+=h-visrows.size();
            viscols.insert(n);
        }
    }

    for(int a : ans)
        cout<<a<<" ";
    cout<<endl;
}

signed main() {
    solve();
}
