/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/lazysegtree>
using namespace atcoder;


const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

//#define endl "\n"

using S=int; /* type of values */
using F=int; /* type of upates */

S st_op(S a, S b) {
    return a+b;
}

/* This is the neutral element */
S st_e() {
    return 0;
}

/* This applies an update to some value. */
S st_mapping(F f, S x) {
    return f+x;
}

/* This combines two updates.  */
F st_composition(F f, F g) {
    return f+g;
}

/* This is the neutral update. 
 * It is similar to st_e() in some sense.  */
F st_id() {
    return 0;
}

using stree=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;

/**
 * We want to find
 * let '1'==1, '0'==-1, so we got some score at each prefix.
 * We can invert that score.
 * So we want to find all possible scores of all prefix[j]-prefix[i] with i<j
 * How to collect?
 * Note that the prefix[i] are within a range.
 * So we can use a lazy segtree to update.
 */
const int N=3e5+7;
void solve() {
    cini(n);
    cinai(s,n);

    int mi=0;
    int ma=0;
    int sum=0;

    stree seg(2*N);
    seg.set(N,1);
    for(size_t i=0; i<s.size(); i++) {
        if(s[i]==1)
            sum++;
        else
            sum--;

        mi=min(mi,sum);
        ma=max(ma,sum);

        //cerr<<"N+sum-ma="<<N+sum-ma<<endl;
        //cerr<<"N+sum-mi+1="<<N+sum-mi<<endl;
        seg.apply(N+sum-ma,N+sum-mi+1,1);
    }

    //cerr<<"doing seg.prod"<<endl;
    for(int i=0; i<2*N; i++) {
        if(seg.get(i))
            seg.set(i,1);
    }
    int ans=seg.prod(0,2*N);
    cout<<ans<<endl;
}

signed main() {
    solve();
}
