/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * consider 
 * Let x(i,j,k,...)=a[i]^a[j]^a[k]
 * a[0] -> x(0)        -> x(0)        Allways replayced by a[0], so stays same
 * a[1] -> x(0,1)      -> x(1)    -> x(0,1) -> ...
 * a[2] -> x(0,1,2)    -> x(0,1,2)^x(0,1)^x(0)
 *                       =x(0,2)  -> x(0,2)^x(0)^x(1)
 *                               =x(1,2) -> x(1,2)^x[0]^x(0,1)
 *                                         =x(2)
 * a[3] -> x(0,1,2,3) ->  x(1,3)
 * a[4] -> x(0,1,2,3,4)-> x(0,2,4)
 *
 * So, a[i] after one step
 * 1: x(0,1,2,...,N)
 * 2: if(i&1) x(1,3,5,...,i) else
 *            x(0,2,4,...,i)
 * 3: case i%4:
 *    0: x(0,4,8,...,i)
 *    1: x(0,1,4,5,...
 *    2: x( ,1,2,
 * ...???
 *
 ********************
 * Binary lifting?
 * Consider each number to be a bitset<N> saying if b[pos] is set that a[pos] is in XORsum.
 * 1: 00001,00010,00100,01000,10000
 * 2: 00001,00011,00111,01111,11111
 * 3: 00001,00010,00101,01010,10101
 * 4: 00001,00011,00110,01100,11001
 * 5: 00001,00010,00100,01000,10001
 * 6: 00001,00011,00111,01111,11110
 * 7: 00001,00010,00101,01010,10100
 * 8: 00001,00011,00110,01100,11000
 * 9: 00001,00010,00101,01011,10011
 *
 * ...It repeats after 16 times.
 * So doing it k times means doing it k%16 times.
 */
void test() {
    const int N=18;
    vector<bitset<N>> b(N);
    for(int i=0; i<N; i++) 
        b[i][i]=1;

    for(int i=0; i<200; i++) {
        cerr<<i<<" ";
        for(int j=1; j<N; j++)  {
            cerr<<b[j]<<" ";
            b[j]=b[j]^b[j-1];
        }
        cerr<<endl;
    }
}

const int M=256;
void solve() {
    cini(n);
    cini(m);
    cinai(a,n);

    vvi aa(M, vi(n));
    aa[0]=a;

    for(int j=1; j<M; j++)  {
        //cerr<<"j="<<j<<endl;
        aa[j][0]=aa[j-1][0];
        for(int i=1; i<n; i++)  {
            aa[j][i]=aa[j-1][i]^aa[j][i-1];
            //cerr<<aa[j][i]<<" ";
        }
        //cerr<<endl;
    }

    for(int i=1; i<=m; i++) 
        cout<<aa[i%M][n-1]<<" ";
    cout<<endl;
}

signed main() {
    test();
    //solve();
}
