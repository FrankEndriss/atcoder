/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * let f be number of distinct values in a[], 
 * ans=nCr(f,3)
 * ...no
 *
 * We need to multiply by freq of numbers...
 * But also we need to consider position of elements.
 * How?
 *
 * Consider the symbols at pos==i, we can combime with any j>i a[i]!=a[j], then
 * with any a[k] k>j and a[k]!=a[i]&& a[k]!=a[j]
 *
 * So go from right to left, find number of possible k for given j, then
 * sum up foreach number.
 * Then find sum of j foreach i.
 *
 * But there seems to be some calculation error. :/
 */
void solve() {
    cini(n);
    cinai(a,n);

    vi dpJ(n);  /* dpJ[j]=cnt of possible pairs with j==j */
    map<int,int> f;
    for(int j=0; j<n; j++) {
        dpJ[j]=j-f[a[j]];
        f[a[j]]++;
    }

    int ans=0;
    int sum=0;
    f.clear();
    for(int k=0; k<n; k++) {
        ans+=sum;
        /* subtract number of pairs with a[i]==a[k] || a[j]==a[k], 
         **/
        ans-=(k-f[a[k]])*f[a[k]];

        sum+=dpJ[k];
        f[a[k]]++;
    }

    cout<<ans<<endl;

}

signed main() {
        solve();
}
