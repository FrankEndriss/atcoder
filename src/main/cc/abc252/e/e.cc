/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/dsu>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Problem sound a bit like MST, but is otherwise.
 *
 * Is there any property we can optimize for other than
 * "sum of pathlens should be minimized"?
 * Any other strategy than actually try all possible 
 * permutations of n-1 edges?
 *
 * Ok, one of the edges outgoing from root must be element
 * of the resulting tree.
 * What else?
 * ...Maybe its still simply MST for some reason, give that a try.
 *
 * ...It is not the same.
 * So, what is it?
 */
void solve() {
    cini(n);
    cini(m);
    using t4=tuple<int,int,int,int>;
    vector<t4> edg;
    for(int i=0; i<m; i++) {
        cini(a);
        cini(b);
        cini(c);
        edg.emplace_back(c,a,b,i);
    }
    sort(all(edg));

    dsu d(n);
    int cnt=0;
    for(int i=0; cnt<n-1 && i<m; i++)  {
        auto [c,a,b,idx]=edg[i];
        if(d.same(a-1,b-1))
            continue;

        cout<<idx+1<<" ";
        d.merge(a-1, b-1);
        cnt++;
    }
    cout<<endl;
}

signed main() {
        solve();
}
