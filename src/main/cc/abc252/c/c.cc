/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * So check for each symbol.
 * One of the rolls needs the most steps.
 *
 * ...How do the buttons work?
 * -> We have 10 buttons, but can press only one at some fixed t.
 *
 * So, in example, if there are two rolls with symbol x at position y,
 * then we fix those to rolls at time y and y+10
 */
void solve() {
    cini(n);
    cinas(s,n);

    vi ans(10);
    //for(int i=0; i<10; i++) {   /* symbol '0'+i */
        vvi pos(10, vi(10));
        for(int j=0; j<n; j++) {   /* jth roll */
            for(int k=0; k<10; k++) { /* position */
                ans[s[j][k]-'0']=max(ans[s[j][k]-'0'], k+pos[s[j][k]-'0'][k]*10);
                pos[s[j][k]-'0'][k]++;
            }
        }
    //}

    cout<<*min_element(all(ans))<<endl;

}

signed main() {
        solve();
}
