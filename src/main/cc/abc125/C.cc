/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
ll n;
    cin>>n;
    vector<ll> a(n);
    fori(n)
        cin>>a[i];

    vector<ll> gleft(n);
    gleft[0]=a[0];
    for(int i=1; i<n; i++)
        gleft[i]=__gcd(gleft[i-1], a[i]);

    vector<ll> gright(n);
    gright[n-1]=a[n-1];
    for(ll i=n-2; i>0; i--) 
        gright[i]=__gcd(gright[i+1], a[i]);

    vector<ll> dp(n); // dp[i]= gcd if a[i] is removed
    dp[0]=gright[1];
    dp[n-1]=gleft[n-2];
    for(int i=1; i<n-1; i++)
        dp[i]=__gcd(gleft[i-1], gright[i+1]);

    ll ans=*max_element(dp.begin(), dp.end());
    cout<<ans<<endl;
}

