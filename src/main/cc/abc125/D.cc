/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
    int n;
    cin>>n;
    vector<ll> a(n);
    ll cneg=0;
    ll c0=0;
    ll minabs=1000000001;
    fori(n) {
        cin>>a[i];
        if(a[i]==0)
            c0++;
        else if(a[i]<0)
            cneg++;
        minabs=min(minabs, abs(a[i]));
    }
    ll sub=0;
    if(c0==0 && cneg%2==1)
        sub=minabs;

    ll ans=0;
    fori(n)
        ans+=abs(a[i]);
    ans-=(2*sub);

    cout<<ans<<endl;
}

