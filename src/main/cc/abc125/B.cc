/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
    int n;
    cin>>n;
    vector<int> v(n);
    fori(n)
        cin>>v[i];
    vector<int> c(n);
    fori(n)
        cin>>c[i];
    vector<int>d(n);
    fori(n)
        d[i]=v[i]-c[i];

    sort(d.begin(), d.end(), greater<int>());
    ll ans=0;
    for(int i=0; i<n && d[i]>0; i++)
        ans+=d[i];

    cout<<ans<<endl;
    

}

