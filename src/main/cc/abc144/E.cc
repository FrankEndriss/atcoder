/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cinll(k);
    cinall(a,n);
    cinall(f,n);

    sort(a.begin(), a.end());
    sort(f.begin(), f.end(), greater<ll>());
    vll m(n);
    ll suma=0;
    for(int i=0; i<n; i++) {
        suma+=a[i];
        m[i]=a[i]*f[i];
    }

    vi aid(n);
    iota(aid.begin(), aid.end(), 0);
    sort(aid.begin(), aid.end(), [&](int i1, int i2) {
        m[i1]<m[i2];
    });

    int cnt=1;
    for(int i=0; i<n; i++) {
/* now optimize members aid[0]..aid[i] so that their score
 * is less or eq the score of aid[i+1].
 */
        // ... TODO
    }
    

}

