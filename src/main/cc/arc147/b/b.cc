/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We also want to simulate this.
 * Since N=400 brute force left to right should work.
 *
 * ...
 * Simplest brute force seems to notwork.
 * Note that a swap over two position could be wrong.
 * If the number of A ops for an element eq 1, then
 * how to know at which position a single swap makes
 * most sense?
 * We actually never want to swap an element from 
 * left of its wanted position to right of its wanted position.
 * So, move the element that is 'most to much right" 
 * only on position right.
 * How to implement that?
 * ...seems to notwork as well :/
 * ***
 * Is there some dp?
 *
 */
void solve() {
    cini(n);
    cinai(a,n);

    for(int i=0; i<n; i++)
        a[i]--;

    vector<pair<char,int>> ans;

    for(int i=0; i+1<n; i++) {
        /* fix element i */

        int pos=-1;
        for(int j=i; j<n; j++)  {
            if(a[j]==i) {
                pos=j;
                break;
            }
        }
        assert(pos>=i);

        bool one=(pos-i)%2;

        while(pos-i>=2) {
            if(one && a[pos-2]>pos && a[pos-1]>a[pos-2]) {
                ans.emplace_back('A', pos-1);
                swap(a[pos], a[pos-1]);
                one=false;
                pos-=1;
            } else {
                ans.emplace_back('B', pos-2);
                swap(a[pos], a[pos-2]);
                pos-=2;
            }
        }
        if(pos-i==1) {
            ans.emplace_back('A', pos-1);
            swap(a[pos], a[pos-1]);
            pos-=1;
        }
        assert(pos==i);
    }

    cout<<ans.size()<<endl;
    for(size_t i=0; i<ans.size(); i++) 
        cout<<ans[i].first<<" "<<ans[i].second+1<<endl;

}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
