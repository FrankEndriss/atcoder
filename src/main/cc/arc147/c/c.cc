/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* ternary search, see https://en.wikipedia.org/wiki/Ternary_search */
/* return the point in [l,r] where f(x) is max */
constexpr ld EPS=0.0000001;
ld terns(ld l, ld r, function<ld(ld)> f) {
    while(l+EPS<r) {
        const ld l3=l+(r-l)/3;
        const ld r3=r-(r-l)/3;
        if(f(l3)<f(r3))
            l=l3;
        else
            r=r3;
    }
    return (l+r)/2;
};
/**
 * So we want 
 * the right half of people to be as left as possible, and
 * the left half of people to be as right as possible.
 *
 * Binary search?
 * Lets define a center point somewhere, use it as median.
 *
 * How to define things using median. Consider the median element.
 * How to calculate the sum?
 * Sort intervals by R, then add n/2 to the left side, and the rest to the right.
 * But how to see optimum? Use ternary search?
 *
 * ..to tired :/
 */
const int N=1e7+7;
void solve() {
    cini(n);

    vector<pii> lr(n);
    for(int i=0; i<n; i++) 
        cin>>lr[i].second>>lr[i].first;

    sort(all(lr));

    int l=0; 
    int r=N;
    while(l+1<r) {
    }
}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
