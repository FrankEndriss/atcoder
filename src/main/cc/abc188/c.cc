
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

void solve() {
    cini(n);
    cinai(a,1<<n);

    vi p(1<<n);
    iota(all(p), 0LL);
    while(p.size()>2) {
        vi p0;
        for(size_t i=0; i<p.size(); i+=2) {
            if(a[p[i]]>a[p[i+1]])
                p0.push_back(p[i]);
            else
                p0.push_back(p[i+1]);
        }
        p.swap(p0);
    }
    assert(p.size()==2);
    
    if(a[p[0]]>a[p[1]])
        cout<<p[1]+1<<endl;
    else
        cout<<p[0]+1<<endl;
}

signed main() {
    solve();
}
