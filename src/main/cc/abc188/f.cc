
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Try it backwards:
 * We can reach i from
 * i-1
 * i+1
 * i/2 if even
 *
 * With one step we can reach 
 * y-1; 
 * if y is even then y/2
 * with two steps
 *
 * With two steps we can go from x to
 * x*2-2
 * x*2-1
 * x*2
 * x*2+1
 * x*2+2
 * With n-1 steps
 * x*2^j +=n-j      | for j<n
 *
 * Consider x=1;
 * 1 Step:
 * +2
 * -0
 * *2
 *
 * 2 Step
 * ++  2 3 
 * +-  2 1
 * *+  2 3
 * **  2 4
 * 3 Step
 * *** 2 4 8
 * **+ 2 4 5
 * *++ 2 3 4
 * +++ 2 3 4
 * *+* 2 3 6
 */
void solve() {
    cini(x);
    cini(y);
    if(x>=y) {
        cout<<x-y<<endl;
        return;
    }

    int ans=y-x;
    int cnt=0;
    while(y>x) {
        ans=min(ans, cnt+y-x);
        if(y%2==0) {
            y/=2;
            cnt++;
            ans=min(ans, cnt+abs(x-y));
        } else {

        }
    }


    for(int i=0; i<61; i++) {
        for(int j=0; j<=i; j++) {
            int mi=x*(1<<j)-(i-j);
            int ma=x*(1<<j)+(i-j);

            if(y>=mi && y<=ma) {
                cout<<i<<endl;
                return;
            }
        }
    }

    assert(false);
}

signed main() {
    solve();
}
