
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int INF=1e10+7;
/* lets do bfs from all root towns
 * maintaining the min buy price.
 * Then, foreach relaxation also maintain max ans.
 *
 * WA, read the statement correctly!
 */
void solve() {
    cini(n);
    cini(m);
    cinai(a,n);
    vvi adj(n);

    for(int i=0; i<m; i++) {
        cini(x);
        cini(y);
        adj[x-1].push_back(y-1);
    }

    vi minBuy(n,INF);   /* minBuy[i] minimum price we have bought when in town i */

    int ans=0;

    function<void(int,int)> dfs=[&](int v, int mi) {
        if(minBuy[v]<=mi)
            return;

        minBuy[v]=mi;
        ans=max(ans, a[v]-minBuy[v]);
        for(int chl : adj[v])
            dfs(chl, min(minBuy[v], a[chl]));

    };

    for(int i=0; i<n; i++) 
        dfs(i, a[i]);

    cout<<ans<<endl;
}

signed main() {
    solve();
}
