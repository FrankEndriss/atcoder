/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
void solve() {
	cins(s);
	const size_t n=s.size();
	int front=0;
	int end=0;
	for(size_t i=0; s[i]=='a' && i<s.size(); i++) 
		front++;
	for(size_t i=0; s[n-1-i]=='a' && i<s.size(); i++) 
		end++;

	if(front+end>s.size()) {
		cout<<"Yes"<<endl;
		return;
	}

	/* check if center part is palindrome */
	int l=front;
	int r=n-1-end;
	bool ok=true;
	for(; l<=r; l++, r--) {
		if(s[l]!=s[r])
			ok=false;
	}
	if(!ok) {
		cout<<"No"<<endl;
		return;
	}

	if(front<=end) {
		cout<<"Yes"<<endl;
	} else {
		cout<<"No"<<endl;
	}
	
}

signed main() {
    solve();
}
