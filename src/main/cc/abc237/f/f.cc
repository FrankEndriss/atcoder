/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using mint=modint998244353;
/**
 * Consider the increaseing subseqs of len 3 at
 * idx i0, i1, i2. It divides the list of len N into 4 segments.
 * So s[i0]<s[i1]<s[i2]
 *
 * Iterate the ~10^3 possible values for s[i0], s[i1] and s[i2].
 *
 * Foreach one do a dp on all other positions.
 * Number of increasing subseqs with leftmost value
 *
 * The values in 0th segment must be bg s[i0] and not increaseing.
 *   so a[0..i0] must be not-increasing
 * The values in 1th segment must be lt s[i0]
 *   and not-increasing
 * The values in 2th segment must be lt s[i1] and lt s[i0]
 */
void solve() {
	cini(n);
	cini(m);


}

signed main() {
    solve();
}
