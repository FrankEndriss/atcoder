
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider t consist only of letter s[0], then ans=n
 * Else we need to shift s to the nearest letter !=s[0]
 * and use that one. Let x be that distance.
 * Let sw = number of switches between s[0] and !s[0] in s.
 * Then ans=x+n+sw
 *
 * ...but its simpler to just simulate.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);
    cinai(s,n);
    cinai(t,m);

    int ans=0;
    int pos=0;
    for(int i=0; i<m; i++) {
        bool done=false;
        for(int j=0; !done && j<n; j++) {
            if(s[(pos+j)%n]==t[i] || s[(pos-j+n)%n]==t[i]) {
                ans+=j;
                ans++;
                done=true;
            }
        }
        if(!done) {
            cout<<-1<<endl;
            return;
        }
    }
    cout<<ans<<endl;


}

signed main() {
    solve();
}
