
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * x*x-y is square
 *
 * Dist from one square to the next is the next odd number.
 * So, for a given x is x*2+1
 * and prev is x*2-1
 * and prevprev is x*2-3
 *
 * So foreach j=odd number up to n
 * we can subtract that number from each x where x*x>that number
 * ...no
 * Let q[] be the seq of the quads
 * 1,4,9,16,25,..
 * Consider 25, we can subtract
 * 9, 9+7, 9+7+5, 9+7+5+3, 9+7+5+3+1
 *
 * So, iterate all possible x, and add the number of odd numbers.
 * How to get the number of odd numbers that we can subtract?
 * First subtract x*2-1, then x*2-3 etc up to n
 * Binsearch that number?
 *
 * Fuck...this is so complecated.
 * The sum of the odd numbers that we subtract must be lteq n, so
 * i*x*2-sum[i]<=n
 *
 * What is the biggest number we subtract as y?
 * We can use x up to 2*x-1<=n
 * So we cannot iterate this, TLE :/
 *
 * 2*x-1
 * 2*x-3
 * 2*x-5
 * ...
 * 2*x-(2*i-1)
 *
 * This is some math shit...formulars everywhere. 
 * idk :/
 */
using mint=modint998244353;
const int N=1e6+7;
const int INF=1e15;

void solve() {
    cini(n);

    vi sum(N);  /* sum[j]=sum of first j odd numbers */
    sum[1]=1;
    int num=1;
    for(int i=2; i<N; i++)  {
        num+=2;
        sum[i]=sum[i-1]+num;
    }

    mint ans=0;
    for(int x=0; 2*x-1<=n; x++) {
        /* binsearch the max i, so that i*2*x - sum[i] >=0  */
        int l=0;
        int r=x+1;
        while(l+1<r) {
            const int mid=(l+r)/2;
            const int val=mid*2*x-sum[mid];
            if(val>=0)
                l=mid;
            else
                r=mid;
        }
        ans+=min(x,l);
    }
    cout<<ans.val()<<endl;
}

signed main() {
    solve();
}
