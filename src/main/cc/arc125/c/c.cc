
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * p[0] must be a[0].
 * Because if it would not then a[] would not be a subseq of p[], or there
 * would be alonger LIS in p[].
 * Of same reason p[n-1] must be a[k-1].
 *
 * And the other k-2 elements must be in increasing order in p[].
 * So, nCr(n-2, k-2) possible possitions, and foreach such position
 * there are n-2-(k-2) other position where we can order the elements arbitrarily?
 * so ans*=fac[n-2-(k-2)]
 *
 *
 * ...
 * But we do not want to have the number of perms :/
 * We want the lex smallest one.
 * So then sort all the not-first and not-last numbers in lex order.
 * ...no, it is more complecated.
 * Since in p there must not be any LIS longer than k, and k might
 * be small.
 * idk :/
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,k);


}

signed main() {
    solve();
}
