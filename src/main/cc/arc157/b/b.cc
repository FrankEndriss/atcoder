
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider swapping all X in a subarray, so that K of them
 * are swapped.
 * Iterate rightmost, and foreach find the number. But also 
 * consider that k positions must be swapped.
 * -> But simplify: If there are K or less X, then we can swap
 *  basically all, minus the unused swaps.
 * Else there are more than K x, so just find leftend foreach 
 * rightend.
 *
 * ...It another problem:
 * We need to find max number of _pairs of consecutive Y_, so 
 * not number of consecutive y.
 *
 * So we want to fill gaps, since each fully filled gap gives
 * one extra pair.
 *
 *
 * But note edgecases if number of X < k ans leftmost/rightmost
 * char is 'Y'... no fun to implement. 
 * By.
 */
void solve() {
    cini(n);
    cini(k);
    cins(s);

    int fx=0;   /* freq of x */
    function<int(vi&,char)> freq=[&](vi cnt, char x) {
        int xcnt=0;
        bool first=true;
        for(int i=0; i<n; i++)  {
            if(s[i]==x) {
                xcnt++;
                fx++;
            } else {
                if(!first) {
                    cnt[xcnt]++;
                    first=true;
                    xcnt=0;
                }
            }
        }
        return fx;
    }

    vi cntX(n);
    int fx=freq(cntX, 'X');

    if(fx==k) {
        cout<<n-1<<endl;
        return;
    }

    vi cntY(n);
    int fy=freq(cntY, 'Y');

    int ans=0;   /* count initial pairs */
    for(int i=1; i<n; i++) 
        if(s[i]=='Y' && s[i-1]=='Y')
            yy++;

    if(fx>=k) { /* fill gaps */
        int kk=k;
        for(int i=1; i<=n; i++) {
            while(cntX[i]>0 && i<=kk) {
                ans+=i+1;
                kk-=i;
            }
        }

        cout<<ans<<endl;
    } else {    /* fill all gaps, the reverse Y-gaps */
    }

    assert(false);
}

signed main() {
        solve();
}
