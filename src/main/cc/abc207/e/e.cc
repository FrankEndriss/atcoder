
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Obviously max k eq n
 * dp[i][j]=number of ways if starting at index i with k=j
 */
void solve() {
    cini(n);
    vi a(n);
    for(int k=0; k<n; k++)
        cin>>a[k];

    vvi dp(n, vi(n+1,-1));

    function<int(int,int)> go=[&](int i, int j) {
        //cerr<<"n="<<n<<" i="<<i<<" j="<<j<<endl;
        if(dp[i][j]>=0)
            return dp[i][j];

        dp[i][j]=0;
        int sum=0;

        for(int ii=i; ii<n; ii++) {
            assert(ii<n);

            //cerr<<"n="<<n<<" i="<<i<<" ii="<<ii<<endl;
            sum+=a[ii];
            if(sum%j==0) {
                if(ii==n-1)
                    dp[i][j]++;
                else
                    dp[i][j]+=go(ii+1, j+1);
            }
        }
        return dp[i][j];
    };
    go(0,1);
    cout<<dp[0][1]<<endl;
}

signed main() {
    solve();
}
