
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * For k=0 obviously ans=1
 * k=1 is only possible in N==1
 * k=2 -> ans=leafs
 * k=3 -> ans=non-leafs + number of pairs of sisters
 * k=4...
 *
 * We can do a dfs, this is building all possible trees and
 * count the ways.
 * But obviously that goes TLE.
 *
 * Can we do some coloring?
 *
 * Lets color vertex without taka white, and with taka red.
 * Vertex next to red ones can be blue or green.
 *
 * Then we do dfs, and start the parent once with white, blue and red.
 * If the parent is white, we can choose the current vertex
 * to be white, or blue.
 * if the parent is blue, the current gets red.
 * if the parent is reg, the current gets also red or green.
 * if the parent is green the current gets white.
 *
 * Then count the possible colorings.
 */
using mint=modint1000000007;
void solve() {
    cini(n);
    vvi adj(n);
    for(int i=0; i<n-1; i++)  {
        cini(u);
        cini(v);
        u--;
        v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

#define W 0
#define R 1
#define B 2 
#define G 3

    vi col(n, -1);
    vector<vector<mint>> dp(n, vi(4,-1));
    function<mint(int,int) dfs=[&](int v, int p) {
        if(p>=0 && dp[v][col[p]]>=0)
            return dp[v][col[p]];

        dp[v][col[p]]=0;
        int cnt=0;
        for(int chl : adj[v]) {
            if(chl==p)
                continue;
            cnt++;

            if(col[p]==W) {
                col[v]=W;
                dp[v][col[p]]+=dfs(chl, v);
                col[v]=B;
                dp[v][col[p]]+=dfs(chl, v);
            } else if(col[p]==B) {
                col[v]=R;
                dp[v][col[p]]+=dfs(chl, v);
            } else if(col[p]==R) {
                col[v]=G;
                dp[v][col[p]]+=dfs(chl, v);
            }
        }
        if(col[p]==B && cnt==0)
            dp[v][col[p]]=0;

        return dp[v][col[p]];
    };
    int ans=0;
    col[0]=W;
    ans+=dfs(0,-1);
    col[0]=B;
    ans+=dfs(0,-1);
    col[0]=R;
    ans+=dfs(0,-1);

    cout<<ans<<endl;

}

signed main() {
    solve();
}
