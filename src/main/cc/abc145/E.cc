/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;
typedef vector<vll> vvll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(T);
    vector<pii> ab(n);
    for(int i=0; i<n; i++)
        cin>>ab[i].first>>ab[i].second;

    sort(ab.begin(), ab.end());

/* We want to find max(sum(b[i])) while having sum(a[i])<T .
 * So among all set<i> with sum(a[i])<T
 * we want to find the one with max(sum(b[i]))
 *
 * knapsack max sum(B).
 **/

    vvll memo(n, vll(T+1, -1));
    function<ll(int,int)> knapp=[&](int idx, int t) {
        if(t<=0 || idx>=n)
            return 0LL;

        if(memo[idx][t]>=0)
            return memo[idx][t];

        ll val=max(knapp(idx+1, t), 
            ab[idx].second+knapp(idx+1, t-ab[idx].first));

        memo[idx][t]=val;
        return val;
    };

    ll ans=knapp(0, T);
    cout<<ans<<endl;
}

