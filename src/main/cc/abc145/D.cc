/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MOD=1e9+7;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(x);
    cini(y);

    vi p1;  // p1[i] posibilities one row above at position i
    p1.push_back(1);    // one possiblitie at position 0
    vi p2;  // positions two rows above

    for(int i=1; i<=y; i++) {
        vi p(p1.size()+2);   // current row positions/values
        for(int j=0; j<p.size(); j++) {
            if(j>=2)
                p[j]+=p1[j-2];
            p[j]%=MOD;
            if(j>=1 && p2.size()>j-1)
                p[j]+=p2[j-1];
            p[j]%=MOD;
            if(j+2<p1.size())
                p[j]+=p1[j+2];
            p[j]%=MOD;
            if(j+1<p2.size())
                p[j]+=p2[j+1];
            p[j]%=MOD;
        }
        if(i==y) {
            if(p.size()>x)
                cout<<p[x];
            else
                cout<<0<<endl;
        }

#ifdef DEBUG
cout<<"i="<<i<<" vals: ";
for(int i : p)
    cout<<i<<" ";
cout<<endl;
#endif
        p2.swap(p1);
        p1.swap(p);
    }
}

