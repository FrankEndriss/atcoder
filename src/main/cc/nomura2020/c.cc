/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Every non leaf vertex has one or two childs.
 *
 * On each level there is a number of non-leaf vertex,
 * which determines the max number of vertex of next level.
 *
 * Count bottom up, since in last level there are no 
 * non-leaf nodes it is greedy.
 *
 * The possible number of non leaf vertex on a level is bounded
 * by the number of child vertex (min) and parent vertex (max).
 */
void solve() {
    cini(n);
    cinai(a,n+1);

    if(n==0) {
        if(a[0]==1)
            cout<<1<<endl;
        else
            cout<<-1<<endl;
        return;
    }

    if(a[0]!=0) {
        cout<<-1<<endl;
        return;
    }

    const int INF=2e18;
    vi amax(n+1, INF);      // max number of vertex on level i
    vi amin(n+1, 0);
    amax[0]=1;
    
    for(int i=1; i<=n; i++) {
        amax[i]=min(amax[i], (amax[i-1]-a[i-1])*2);
//cerr<<"p1 i="<<i<<" amax[i]="<<amax[i]<<endl;
        if(amax[i]==INF)
            break;
    }

    bool imp=false;
    int ans=a[n];
    amax[n]=a[n];
//cerr<<"p2 i="<<n<<" amax[i]="<<amax[n]<<endl;
    for(int i=n-1; i>=0; i--) {
        amax[i]=min(amax[i], amax[i+1]+a[i]);
        ans+=amax[i];
        if(amax[i]<a[i])
            imp=true;
//cerr<<"p2 i="<<i<<" amax[i]="<<amax[i]<<endl;
    }

    if(imp)
        cout<<-1<<endl;
    else
        cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
