/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* greedy
 * We create the longest possible (1)* sequence
 * Then insert 0s in optimized way.
 * How?
 * From left to right insert 0s so that even pos are used.
 * Then from right to left so that odd pos are used.
 */
void solve() {
    cini(t);

    /* count the 1s */
    int cnt1=0;
    int ans=0;
    for(int i=0; i<t; i++) {
        if(t[i]=='1') {
            cnt1++;
            ans+=cnt1/2;
        }
    }

/* insert 0s at even positions */
    int cnt0odd=0;  // 0s not inserted so far
    int cnt0eve=0;  // 0s inserted at even positions
    for(int i=0; i<t; i++) {
        if(t[i]=='0') {
            if((i+1)%2==1) {
                cnt0odd++;
            } else {
                cnt0even++;
            }
            ans+=cnt/2;
        }

    // ... todo...
    }

    cout<<42<<endl;

    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
