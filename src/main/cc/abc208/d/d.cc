
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * Solve per city.
 * First put cities s and t and 1 into the pool, and 
 * solve for k=1
 * Then put k=2 into the pool and solve again... and so on.
 * But that O(n^4) :/
 *
 * So how dp[i][j][k] could work?
 */
void solve() {
    cini(n);
    cini(m);
    vector<vector<pii>> adjR(n);
    vector<vector<pii>> adj(n);

    for(int i=0; i<m; i++) {
        cini(a); a--;
        cini(b); b--;
        cini(c);
        adj[a].emplace_back(b,c);
        adjR[b].emplace_back(a,c);
    }

    const int INF=1e9;
    int ans=0;
    for(int s=0; s<n; s++) {
        for(int t=0; t<n; t++) {
            if(t==s)
                continue;

            vb pool(n);
            pool[s]=true;
            pool[t]=true;

            vi d(n,INF);
            d[s]=0;
            priority_queue<pii> q;
            q.emplace(0,s);

            for(int k=0; k<n; k++) {
                pool[k]=true;
                for(pii chl : adjR[k]) {
                    if(d[chl.first]+chl.second<d[k]) {
                        d[k]=d[chl.first]+chl.second;
                        q.emplace(-d[k],k);
                    }
                }

                while(q.size()) {
                    auto [dd,v]=q.top();
                    q.pop();
                    if(-dd!=d[v])
                        continue;
                    for(pii chl : adj[v]) {
                        if(pool[chl.first] && d[v]+chl.second<d[chl.first]) {
                            d[chl.first]=d[v]+chl.second;
                            q.emplace(-d[chl.first], chl.first);
                        }
                    }
                }
                if(d[t]<INF)
                    ans+=d[t];
            }
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}
