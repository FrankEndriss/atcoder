
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * The order of the digits does not matter.
 *
 * First, all numbers with a 0 in it count.
 * Then, answer for number of digits !=1...
 *
 * Can we enumerate all sets of digits with product <=K?
 * Consider starting with a set of 2s.
 * Then subst at most possible by 3s.
 *
 * 22222
 * 22223
 * ...
 * 22229
 * 22233
 * 22234
 * ...
 * 22239
 * ....
 * The core of the problem is:
 * How to find the number of sets of digits with product <=K?
 */
void solve() {
}

signed main() {
    solve();
}
