
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * Sort them, the first n%k ones get one more.
 * Sort them back before output.
 */
void solve() {
    cini(n);
    cini(k);
    vector<pii> a(n);

    for(int i=0; i<n; i++) {
        cin>>a[i].first;
        a[i].second=i;
    }
    sort(all(a));

    int m=k%n;
    for(int i=0; i<m; i++) 
        a[i].first=k/n+1;
    for(int i=m; i<n; i++) 
        a[i].first=k/n;

    for(int i=0; i<n; i++)  {
        int tmp=a[i].second;
        a[i].second=a[i].first;
        a[i].first=tmp;
    }

    sort(all(a));

    for(int i=0; i<n; i++) 
        cout<<a[i].second<<endl;
}

signed main() {
    solve();
}
