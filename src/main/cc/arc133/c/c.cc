/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider some value in all cells.
 * Then, if we decrease some cells value by 1, then one row and one col
 * decrease by 1 or increase by k-1.
 *
 * So, start with all cells k-1 we need to adjust the rows by sum(k-1-a[0..h])
 * If this fits somehow b[], then ans is possible.
 * Construct it by applying the decrements in the fitting cols.
 */
void solve() {
    cini(h);
    cini(w);
    cini(k);

    cinai(a,h);
    cinai(b,w);

    int decA=0;
    for(int i=0; i<h; i++) {
        int rval=(((k-1)*w)-a[i])%k;
        //cerr<<"rval="<<rval<<endl;
        decA+=rval;
    }

    int decB=0;
    for(int i=0; i<w; i++) {
        int cval=(((k-1)*h)-b[i])%k;
        //cerr<<"cval="<<cval<<endl;
        decB+=cval;
    }

    //cerr<<"decA="<<decA<<" decB="<<decB<<endl;
    if(abs(decA-decB)%k!=0) {
        cout<<-1<<endl;
        return;
    }

    int ans=(k-1)*h*w-max(decA,decB);
    cout<<ans<<endl;

    /*
    int dec=max(decA,decB);
    vvi ans(h, vi(w, k-1));

    int c=0;
    int cval=(((k-1)*h)-b[0])%k;
    int r=0;
    int rval=(((k-1)*w)-a[0])%k;

    while(dec>0) {
        int ma=max(rval,cval);
        ans[r][c]-=ma;
	dec-=ma;
        rval=(rval-ma+k)%k;
        cval=(cval-ma+k)%k;

        if(rval==0) {
            r++;
            rval=(((k-1)*w)-a[r])%k;
        }
        if(cval==0) {
            c++;
            cval=(((k-1)*h)-b[c])%k;
	}
        
    }

    int ansV=0;
    for(int i=0; i<h; i++) {
        for(int j=0; j<w; j++) {
            ansV+=ans[i][j];
            cerr<<ans[i][j]<<" ";
        }
        cerr<<endl;
    }

    cout<<ansV<<endl;
    */



}

signed main() {
    solve();
}
