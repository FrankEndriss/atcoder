/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/segtree>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using S=int; /* type of values */

S st_op(S a, S b) {
    return max(a,b);
}

S st_e() {
    return 0;
}

using stree=segtree<S, st_op, st_e>;

const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=100000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

/* Factorization of n using precomputed primes.
 * https://cp-algorithms.com/algebra/factorization.html
 */
void trial_division4(ll n, set<ll> &factorization) {
    for (ll d : pr) {
        if (d * d > n)
            break;
        while (n % d == 0) {
            factorization.insert(d);
            n /= d;
        }
    }
    if (n > 1)
        factorization.insert(n);
}

/**
 * Note that
 * It seems that simple LCS is O(n^2), so how can this one work?
 *
 * Iterate a from left to right.
 * Foreach a[i] iterate all positions j where b[j]=a[i]*x
 *   Update dp[j]=max(dp[0..j-1])+1
 * Since both are permutations the inner loop is quick.
 */
void solve() {
	cini(n);
	cinai(a,n);
	cinai(b,n);

	vvi pos(n+1);	/* pos[i]=positions of multiples of i in b[] */
	for(int i=0; i<n; i++) {
		for(int j=1; j*j<=b[i]; j++) {
			if(b[i]%j==0) {
				pos[j].push_back(i);
				if(j*j!=b[i])
					pos[b[i]/j].push_back(i);
			}
		}
	}

	stree seg(n+1);

	for(int i=0; i<n; i++) {
		sort(all(pos[a[i]]), greater<int>());
		for(int idx : pos[a[i]]) {
			int old=seg.get(idx);
			int ma=seg.prod(0,idx)+1;
			if(ma>old)
				seg.set(idx, ma);
		}
	}

	cout<<seg.prod(0,n+1)<<endl;
}

signed main() {
    solve();
}
