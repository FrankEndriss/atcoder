/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vi a(n+1);
    fori(n+1) {
        cin>>a[i];
    }
    vi b(n);
    fori(n)
        cin>>b[i];

    ll ans=0;
    for(int i=n-1; i>=0; i--) {
        int m=min(b[i], a[i+1]);
        ans+=m;
        a[i+1]-=m;
        b[i]-=m;
        m=min(b[i], a[i]);
        ans+=m;
        a[i]-=m;
        b[i]-=m;
    }
    cout<<ans<<endl;

}

