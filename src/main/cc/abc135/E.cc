/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef pair<ll, ll> pllll;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

//#define DEBUG

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    ll k, x, y;
    cin>>k>>x>>y;

    vector<pllll> ans;
    ll cx=0, cy=0;

    if(((k&1)==0) && ((x+y)&1)!=0) {
        cout<<"-1"<<endl;
        return 0;
    }
    
    while(true) {
        ll dist=abs(x-cx) + abs(y-cy);
#ifdef DEBUG
cout<<"dist="<<dist<<endl;
#endif
        if(dist==k) {
#ifdef DEBUG
cout<<"dist case 1"<<endl;
#endif
            ans.push_back({ x, y });
            break;
        } else if(dist>=2*k) {
#ifdef DEBUG
cout<<"dist case 2"<<endl;
#endif
            ll dx=min(k, abs(x-cx));
            ll dy=k-dx;
            if(cx>x)
                dx=-dx;
            if(cy>y)
                dy=-dy;
            cx+=dx;
            cy+=dy;
            ans.push_back({ cx, cy });
        } else {    // find point of dist k to x,y and cx,cy
#ifdef DEBUG
cout<<"dist case 3"<<endl;
#endif
            int dx[]={ 1, -1, -1, 1 };
            int dy[]={ 1, 1, -1, -1 };

            int sx=cx;
            int sy=cy-k;
            for(int i=0; i<4; i++) {
                for(int j=0; j<k; j++) {
                    int ldist=abs(x-sx) + abs(y-sy);
#ifdef DEBUG
cout<<"check x="<<cx+sx<<" y="<<cy+sy<<endl;
#endif
                    if(ldist==k) {
                        ans.push_back({ sx, sy});
                        ans.push_back({ x, y });
                        i=10;
                        j=k+1;
                        break;
                    }
                    sx+=dx[i];
                    sy+=dy[i];
                }
            }
            break;
        }
    }

    cout<<ans.size()<<endl;
    fori(ans.size()) {
        cout<<ans[i].first<<" "<<ans[i].second<<endl;
    }
}

