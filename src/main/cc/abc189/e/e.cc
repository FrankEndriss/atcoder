
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;


#include <atcoder/lazysegtree>
using namespace atcoder;


using S=signed; /* type of values */
using F=signed; /* type of upates */

S st_op(S a, S b) {
    return min(a,b);
}

const S INF=1e9;
S st_e() {
    return INF;
}

/* This applies an update to some value.
 * ie if st_op is mul, then we would 
 * return f*x.
 */
S st_mapping(F f, S x) {
    return min(f,x);
}

/* This creates a mapping from two given mappings.
 * ie if st_op is mul, then we would
 * return f*g, since f*(g*x)==(f*g)*x
 */
F st_composition(F f, F g) {
    return min(f,g);
}

/* this is the neutral update. ie
 * if st_op is + then st_id would be 0,
 * if st_op is mul then st_id would be 1,
 * if st_op is min then st_id is INF
 * It is similar to st_e() in some sense.
 */
F st_id() {
    return INF;
}

using segtree=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;

// example: segtree my_segtree(N);
#define endl "\n"

/* We can resort the queries by A[i].
 * Then execute A[i] operations on the matrix,
 * ant print the result.
 * However, the matrix is to big, we cannot execute any operation
 * on the whole matrix.
 * So we could execute A[i] operations on element B[i].
 * But that will TLE.
 * :/
 * Can we combine some operations into one operation? Somehow?
 * Ok, two consecutive rotations can be one. But that does
 * not really help.
 * All x-operations of one piece can be combined,
 * and y-operations of one piece can be combined.
 * ****
 * Ha! the number of rotations just tells how the mirror operations
 * are converted to each other.
 * So, we need to maintain the rotation state, then apply the sum
 * of mirroring.
 * ...but still we need to somehow apply the mirror operations
 * by some range update operation.
 * How can this look in a segment tree?
 * We can maintain two segtrees, one for x coordinate, one for y.
 * Then, on rotation, we swap them both, and invert sign on
 * one of them.
 * On mirroring, we mirror the one that is mirrored, by applying
 * range update with like position and offset.
 * In the trees, we store values with position and value.
 */
void solve() {
    cini(n);
    vi x(n);
    vi y(n);
    for(int i=0; i<n; i++) 
        cin>>x[i]>>y[i];

    cini(m);
    vector<pii> op(m);

    for(int i=0; i<m; i++) {
        cin>>op[i].first;
        if(op[i].first==3 || op[i].first==4) 
            cin>>op[i].second;
    }

    using tuple<int,int,int>=tiii;

    cini(cntQ);
    vector<tiii> q(cntQ);
    for(int i=0; i<cntQ; i++) {
        cini(aa);
        cini(bb);
        bb--;
        q[i]={ aa, bb, i };
    }
    sort(all(q));

    vector<pii> ans(cntQ);
    int aop=0;  /* next still not executed operation */
    int rot=0;
    for(int i=0; i<cntQ; i++) {
        auto [a,b,idx]=q[i];
        while(aop+1<a) {
            /* apply operation op[aop] */
        }
    }


}

signed main() {
    solve();
}
