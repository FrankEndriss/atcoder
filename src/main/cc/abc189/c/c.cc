
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* foreach dish, we need to find 
 * the first dish right of it with less oranges,
 * assume dish[N]==0
 */
void solve() {
    cini(n);
    stack<pii> q;
    vi r(n);    /* r[i] index of first element right of i with a[r[i]]<a[i] */
    vi l(n,-1); /* l[i] index of first element left of i with a[l[i]]<a[i] */
    vi a(n);
    for(int i=0; i<n; i++) {
        cini(aux);
        a[i]=aux;
        while(q.size() && q.top().first>aux) {
            pii p=q.top();
            q.pop();
            r[p.second]=i;
        }
        q.push({aux,i});
    }
    while(q.size()) {
            pii p=q.top();
            q.pop();
            r[p.second]=n;
    }

    for(int i=n-1; i>=0; i--) {
        int aux=a[i];
        while(q.size() && q.top().first>aux) {
            pii p=q.top();
            q.pop();
            l[p.second]=i;
        }
        q.push({aux,i});
    }

    while(q.size()) {
            pii p=q.top();
            q.pop();
            l[p.second]=-1;
    }

    int ans=0;
    for(int i=0; i<n; i++)  {
        ans=max(ans, (i-l[i])*a[i] + (r[i]-i)*a[i] - a[i]);
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}
