
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Each OR doubles ans?
 * No.
 * Lets see from back to front, consider x[n-1]
 * If s[i]==AND there is at most 1 possibility,
 * since x[n-1] must be true.
 * Else if s[i]==OR, x[n-1] can be true and the
 * previous statement whatsever, or x[n-1] can be false
 * but the prefious statement true.
 *
 * So, dp[i]= pair<true,false> number of ways to make the statement true or false 
 */
void solve() {
    cini(n);
    vector<pii> dp(n+1);
    dp[0]={1,1};

    for(int i=1; i<=n; i++)  {
        cins(s);
        if(s=="OR") {
            dp[i]={ dp[i-1].first*2+dp[i-1].second, dp[i-1].second };
        } else {
            dp[i]={ dp[i-1].first, dp[i-1].first+dp[i-1].second*2 };
        }
    }

    cout<<dp[n].first<<endl;
}

signed main() {
    solve();
}
