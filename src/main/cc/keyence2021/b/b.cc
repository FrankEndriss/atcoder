
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We get max sum by
 * first creating max possible number in first box,
 * then in second box etc.
 */
const int N=3e5+7;
void solve() {
    cini(n);
    cini(k);
    vi a(N);
    for(int i=0; i<n; i++) {
        cini(aux);
        a[aux]++;
    }

    int ans=0;
    int mi=N;
    for(int i=0; i<N; i++) {
        mi=min(mi, a[i]);
        ans+=min(k,mi);
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}
