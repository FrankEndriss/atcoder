
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* The construction of the max val is unclear.
 * What does it mean, which pairs contribute, 
 * which do not?
 * -> 
 *  c[i]=max(c[i-1], max(a[0..i]*b[i]));
 * 
 */
void solve() {
    cini(n);

    cinai(a,n);
    cinai(b,n);

    vi post(n+1);
    for(int i=n-1; i>=0; i--)
        post[i]=max(post[i+1], b[i]);

    vi preA(n+1);
    vi preB(n+1);
    for(int i=0; i<n; i++) {
        preA[i+1]=max(a[i], preA[i]);
        preB[i+1]=max(b[i], preB[i]);
    }

    int ma=0;
    for(int i=0; i<n; i++)  {
        ma=max(ma, preA[i+1]*b[i]);
        cout<<ma<<endl;
    }
}

signed main() {
    solve();
}
