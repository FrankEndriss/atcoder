
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* works like next_permutation(), but on a permutation like seq in p, with max value i.
 * usually i != p.size(); 
 * Usefull to create all subsets of a given size of a set of values.
 * vi data(n);  // some data...
 * vi idx(m);   // subset of size m; m<=n;
 * iota(idx.begin(), idx.end(), 0);
 * do {
 *      ...something using data readonly...
 * } while(next(idx, n-1));
 *
 * example for m=3, n=4:
 * 0 1 2 
 * 0 1 3 
 * 0 2 3 
 * 1 2 3
 **/
bool next(vi &p, int i) {
    const int k=(int)p.size();

    if(p[k-1]<i) {
        p[k-1]++;
        return true;
    }
    if(k==1)
        return false;

    int incidx=k-2;
    while(incidx>=0 && p[incidx]==p[incidx+1]-1)
        incidx--;

    if(incidx<0)
        return false;

    p[incidx]++;
    for(int j=incidx+1; j<k; j++)
        p[j]=p[j-1]+1;

    return true;
}

/* Consider player 0.
 * There exists n so that player0 was in the same team with
 * every other player n times.
 *
 * What about AAA..AAABBB..BBB ?
 *
 * simplest case, let n=2
 * AABB             | first operation, arbitrary
 * ABAB
 * ABBA
 *
 * So each player is once in the same team with every other
 * player, and two times in a different team.
 *
 * n=3
 * AAAABBBB             | first operation, arbitrary
 * AAABBBBA
 * AABBBBAA
 * ABBBBAAA
 * -> Use this pattern
 *  n=4
 * AAAAAAAABBBBBBBB
 * AAAAAAABBBBBBBBA
 * ...
 * AABBBBBBBBAAAAAA
 * ABBBBBBBBAAAAAAA
 * ******
 * No.
 * The pattern is, first letter is 'A', then
 * all other possible combinations must follow.
 *
 * Consider N=8, ie 256 players.
 * First is allways A.
 * Then there are 255 players, where 128 need to get a B.
 * We need to build all possible combinations of bits
 * covering those string of len 255 with 128 bits set
 * to 0/B.
 * How?
 * perm() function
 */
void solve() {
    cini(n);

    if(n==1) {
        cout<<1<<endl<<"AB"<<endl;
        return;
    }

    vi data(1<<(n-1));
    iota(all(data), 1);

    vs ans;
    do {
        string aa(1<<n, 'B');
        for(int j : data)
            aa[j]='A';
        aa[0]='A';
        ans.push_back(aa);
    }while(next(data, (1<<n)));

    cout<<ans.size()<<endl;
    for(string s : ans)
        cout<<s<<endl;


}

signed main() {
    solve();
}
