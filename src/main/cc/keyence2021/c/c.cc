
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * We need to put in all fields the maximum possible choice,
 * that is both if possible, else one of the dirs.
 * Then do dp on number of paths to each position. left/right, top down.
 * ...
 * Ok, now undetstood.
 * We are not asked for the number of possible paths, but for the sum
 * of possible paths for all possible settings of the fields.
 * ...what is very mathy :/
 */
const int MOD=998244353;
void solve() {
    cini(h);
    cini(w);
    cini(k);
    vvi a(h, vi(w, 'X'));

    for(int i=0; i<k; i++) {
        cini(x); x--;
        cini(y); y--;
        char c; cin>>c;
        a[x][y]=c;
    }

    vvi dp(h, vi(w));
    dp[0][0]=1;
    for(int i=0; i<h; i++) {
        for(int j=0; j<w; j++) {
            if(j>0 && (a[i][j-1]=='R' || a[i][j-1]=='X')) {
                dp[i][j]+=dp[i][j-1];
                if(dp[i][j]>=MOD)
                    dp[i][j]-=MOD;
            }
            if(i>0 && (a[i-1][j]=='D' || a[i-1][j]=='X')) {
                dp[i][j]+=dp[i-1][j];
                if(dp[i][j]>=MOD)
                    dp[i][j]-=MOD;
            }
        }
    }

    cout<<dp[h-1][w-1]<<endl;
}

signed main() {
    solve();
}
