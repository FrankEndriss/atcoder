
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Consider s0 to have all ans right.
 * Then s1 cannot have same number right if he diffs in odd number of positions.
 * So we need to find foreach student the 
 * number of other students haveing odd number of diffs, ie xor has odd number
 * of bits set.
 * How to do that in less than O(n^2)?
 * Since M<=20 there can be 1e6 different answer profiles.
 *
 * Can we iterate per student all masks with odd diffs?
 * -> no, much to much
 *
 * Can we somehow find the number of pairs for one xor?
 */
void solve() {
    cini(n);
    cini(m);

    vi a(n);
    for(int i=0; i<n; i++) {
        cins(s);
        for(int j=0; j<m; j++) 
            if(s[j]=='1')
                a[i]|=(1<<j);
    }

    int ans=0;
    for(int i=0; i<n; i++) {
        for(int j=i+1; j<n; j++) {
            int bcnt=__builtin_popcount(a[i]^a[j]);
            if(bcnt&1)
                ans++;
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}
