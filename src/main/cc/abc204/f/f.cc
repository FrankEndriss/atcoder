
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


const int MOD=998244353;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* vector by matrix multiplication.
 * https://mathinsight.org/matrix_vector_multiplication
 **/
vi mulVM(vvi &m, vi &v, int mod=MOD) {
    assert(m.size()==m[0].size() && v.size()==m.size());
    vi ans(v.size());
    const int sz=v.size();
    for(int i=0; i<sz; i++)
        for(int j=0; j<sz; j++) 
            ans[i]=pl(ans[i], mul(m[i][j],v[i]));
    return ans;
}

/* matrix multiplication */
vvi mulM(vvi &m1, vvi &m2, int mod=MOD) {
    assert(m1.size()==m1[0].size() && m2.size()==m2[0].size() && m1.size()==m2.size());
    const int sz=m1.size();
    vvi ans(sz, vi(sz));
    for(int i=0; i<sz; i++) {
        for(int j=0; j<sz; j++) {
            for(int k=0; k<sz; k++) {
                ans[i][j]=pl(ans[i][j], mul(m1[i][k], m2[k][j]));
            }
        }
    }
    return ans;
}

/* matrix pow */
vvi toPower(vvi a, int p, int mod=MOD) {
    const int sz=a.size();
    vvi res = vvi(sz, vi(sz));
    for(int i=0; i<sz; i++)
        res[i][i]=1;

    while (p != 0) {
        if (p & 1)
            res = mulM(a, res, mod);
        p >>= 1;
        a = mulM(a, a, mod);
    }
    return res;
}

/*
 * How to get rotation and reflection right?
 * -> There is no problem, we can ignore rotations and reflections.
 * So, it seems to be some dp on columns, or some factor per column.
 *
 * Consider h=1:
 * ans=w    (why? -> we can replace 0 to w-1 border between two 1x1 tiles by no border)
 *
 * h=2:
 * Consider the ways to fill the first col, for w>1:
 * 4 ways, resulting in second col to be covered 00, 01, 10, 11
 * ...ok, we can dp this, also for h=3...6.
 * But, the dp is not the sol, since N=1e12
 * We need to matrix pow this.
 *
 * So, how to the matrixes to h=2...6 look like?
 */
void solve() {
    cini(h);
    cini(w);

    vvi m(1<<h, vi(1<<h));
    const int n=1<<h;
    for(int i=0; i<n; i++) {    /* i=possible pattern in current col */
        /* now create all follow pattern to pattern i 
         * This is, each 1 position is a 0 position in follow,
         * each 0 position is a 0 or 1 position in follow.
         * */
        int f=0;
        for(int k=0; k<h; k++) {
            if((i&(1<<k))==0) {
                f|=(1<<k);
            }
        }

        for(int s=f; s>0; s=(s-1)&f)
            m[i][s]=1;

        m[i][0]=1;
    }

    m=toPower(m,w);
    cout<<m[0][0]<<endl;

}

signed main() {
    solve();
}
