
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to bipartite t[] into two sum
 * that should be most possible eq.
 * Knapsack all possible sums.
 *
 * ...some RE
 * ???
 */
const int N=1e3;
void solve() {
    cini(n);
    cinai(t,n);

    const int sum=accumulate(all(t), 0LL);
    vb dp(sum+1);
    dp[0]=true;
    for(int i=0; i<n; i++) {
        for(int j=sum; j>=t[i]; j--) {
            if(dp[j-t[i]])
                dp[j]=true;
        }
    }

    for(int k=(sum+1)/2; k<=sum; k++) {
        if(dp[k]) {
            cout<<k<<endl;
            return;
        }
    }
    assert(false);
}

signed main() {
    solve();
}
