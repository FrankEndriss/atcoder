
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* we need to find how the segment moves between the other 
 * segments...somehow.
 * Note that Y and Q are fairly small.
 *
 * But the trick is to handle the segments somehow.
 * Try simulate this with like two-pointer.
 * ...
 * Simulation goes TLE. We need to somehow handle the movement of 
 * the segments in between the other segments...idk.
 */
void solve() {
    cini(x);
    cini(y);
    cini(p);
    cini(q);

    int offt=x;
    int pert=2*x+2*y;

    int offs=p;
    int pers=p+q;

    int prevt=x;    /* start of segments */
    int prevs=p;

    set<int> d1;
    set<int> d2;
    while(true) {
        cerr<<"prevt="<<prevt<<" prevs="<<prevs<<endl;
        if(prevt==prevs) {
            cout<<prevt<<endl;
            return;
        } else if(prevt>prevs) {
            if(prevs+q>prevt) {
                cout<<prevt<<endl;
                return;
            } 
            /* move prevs until prevs>=prevt */
            int diff=prevt-prevs;
            cerr<<"diff1="<<diff<<endl;
            if(d1.count(diff)) {
                cout<<"infinity"<<endl;
                return;
            }
            d1.insert(diff);

            prevs+=pers*(diff/pers);
            if(prevs<prevt)
                prevs+=pers;

        } else if(prevt<prevs) {
            if(prevt+y>prevs) {
                cout<<prevs<<endl;
                return;
            }
            /* move prevt until prevt>=prevs */
            int diff=prevs-prevt;
            cerr<<"diff2="<<diff<<endl;
            if(d2.count(diff)) {
                cout<<"infinity"<<endl;
                return;
            }
            d2.insert(diff);

            prevt+=pert*(diff/pert);
            if(prevt<prevs)
                prevt+=pert;
        }
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
