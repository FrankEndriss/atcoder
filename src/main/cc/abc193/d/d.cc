/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 2 cases:
 * one has better 4 cards
 * both have same 4 cards
 * How to calc the propability?
 * -> Consider all pairs of remaining cards.
 * Ans=number of pairs where player x has better card
 *   devided by number of all pairs.
 *
 * Ans=prop string s
 */
void solve() {
    cini(k);
    vi cnt(10,k);

    cinas(str,2);

    vvi f(2, vi(10));
    for(int i=0; i<4; i++) {
        f[0][str[0][i]-'0']++;
        f[1][str[1][i]-'0']++;
        cnt[str[0][i]-'0']--;
        cnt[str[1][i]-'0']--;
    }

    vi ten;
    ten.push_back(1);
    for(int i=0; i<7; i++) 
        ten.push_back(ten.back()*10);

    vi scr(2);
    for(int i=1; i<=9; i++) {
        scr[0]+=i*ten[f[0][i]];
        scr[1]+=i*ten[f[1][i]];
    }
    cerr<<"scr[0]="<<scr[0]<<" scr[1]="<<scr[1]<<endl;

    ld ans=0;
    for(int i=1; i<=9; i++) {   /* card of S */
        int nscr=scr[0]+ i*ten[f[0][i]]*9;

        for(int j=1; j<=9; j++) {   /* card of T */
            int tscr=scr[1]+ j*ten[f[1][j]]*9;
            if(nscr>tscr) {
                if(i!=j)
                    ans+=cnt[i]*cnt[j];
                else
                    ans+=cnt[i]*(cnt[i]-1);
            }
        }
    }
    cerr<<"raw ans="<<ans<<endl;

    ans/=(((k*9)-8)*((k*9)-9));
    cout<<ans<<endl;

}

signed main() {
    solve();
}
