
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We can minimize the number of dissatisfied dogs
 * by sorting them by color.
 * Note that max two freqs can be odd.
 * Take the cheapest pair of dogs from those two colors.
 * Find cheapest pair by two-pointer.
 *
 *
 * But, let the odd colors be 0,2, the even one 1.
 * Then we can make a pair 0,1 and one 1,2, and that
 * might be cheaper than the cheapest 0,2.
 */
void solve() {
    cini(n);
    n*=2;
    vi f(3);
    vvi a(3);
    for(int i=0; i<n; i++) {
        cini(la);
        char c;
        cin>>c;
        int lc;
        if(c=='R')
            lc=0;
        else if(c=='G')
            lc=1;
        else if(c=='B')
            lc=2;
        else
            assert(false);

        f[lc]++;
        a[lc].push_back(la);
    }

    for(int i=0; i<3; i++) {
        sort(all(a[i]));
    }

    int i0=-1;
    int i1=-1;
    int i2=-1;  /** even color */

    if(f[0]%2 && f[1]%2) {
        i0=0;
        i1=1;
        i2=2;
    } else if(f[0]%2 && f[2]%2) {
        i0=0;
        i1=2;
        i2=1;
    } else if(f[1]%2 && f[2]%2) {
        i0=1;
        i1=2;
        i2=0;
    } else {
        cout<<0<<endl;
        return;
    }

    function<int(int,int)> cheapest=[&](int i0, int i1) {
        int idx0=0;
        int idx1=0;
        int mi=1e18;
        while(true) {
            mi=min(mi, abs(a[i0][idx0]-a[i1][idx1]));
            if(idx0+1<a[i0].size() && a[i0][idx0]<=a[i1][idx1])
                idx0++;
            else if(idx1+1<a[i1].size() && a[i1][idx1]<=a[i0][idx0])
                idx1++;
            else
                break;
        }
        return mi;
    };

    int ans=cheapest(i0,i1);
    if(a[i2].size()>=2) {
        ans=min(ans, cheapest(i0,i2)+cheapest(i1,i2));
    }

    cout<<ans<<endl;


}

signed main() {
    solve();
}
