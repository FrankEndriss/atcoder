
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * some dp
 *
 * Consider the second value written on the board,
 * we want it to be as close to the first as possible,
 * that is the tastiness of the second group of candies 
 * should be as small as possible.
 *
 * also consider that all candies must be eaten, so the last value written
 * is sum(a[]).
 * We propably want to write the first values as close to that sum as possible,
 * and all other values in between.
 *
 * How to do that?
 * Note that sum can be 0 :/
 *
 * Consider all values are positive.
 * Then we would like to start with the two biggest, then eat all other candies 
 * in any order and groupsize. Same with all negative.
 *
 * consdider 
 * -5, 1, 1, 1, 1, 1
 *  sum=0
 *  Since there is only 1 negative we want to start with that one plus
 *  the biggest positive.
 *
 * consider
 * -5, -5, 1, 1, 1, 1
 *
 * *******************
 * ...All wrong.
 * We do not write the sum of _all_ eaten, only that one or two just eaten :/
 * So it is another problem.
 *
 * We want to find a pairing of all candies, so that max(pair)-min(pair) is minimized
 * Is this not first/last, second/secondlast,...?
 *
 * How it comes to play that we can also choose a single candy?
 * ???
 */
void solve() {
    cini(n);
    cinai(a,n);
    sort(all(a));

    int sum=accumulate(all(a), 0LL);
}

signed main() {
    solve();
}
