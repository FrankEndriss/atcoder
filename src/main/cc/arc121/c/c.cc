
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * Consider 1,3,2
 * We would like to choose pos=2 to swap 3,2, but that not possible in 
 * first operation.
 * So we must choose pos=1, which yields 3,1,2
 * Then we must choose pos=2, 3,2,1
 * 2,3,1
 * 2,1,3
 * 1,2,3
 * So, we swap the smallest inversion if possible, else the next position,
 * else the previous position.
 * How to maintain the "next inversion"?
 * -> Just use a pointer to current element, go left to right.
 *
 * How do we go left to right?
 * Invariant: a[0]..a[i] is sorted
 */
void solve() {
    cini(n);
    cinai(a,n);

    int i=0;
    while(i+1<n && a[i]<a[i+1])
        i++;

    int op=0;
    vi ans;
    while(i+1<n) {

        if(a[i]>a[i+1] && (i%2==op)) {
            swap(a[i], a[i+1]);
            ans.push_back(i+1);
            if(i>0)
                i--;
        } else if(i+2<n) {
            swap(a[i+1], a[i+2]);
            ans.push_back(i+2);
        } else  {
            assert(i>0);
            i--;
            swap(a[i], a[i+1]);
            ans.push_back(i+1);
            if(i>0)
                i--;
        }

        while(i+1<n && a[i]<a[i+1])
            i++;

        op=!op;
    }
    cout<<ans.size()<<endl;
    for(int i : ans)
        cout<<i<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
