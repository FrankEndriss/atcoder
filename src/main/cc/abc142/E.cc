/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(m);
    vi a(m);
    vi b(m);
    vi c(m);

    for(int i=0; i<m; i++) {
        cin>>a[i]>>b[i];
        for(int j=0; j<b[i]; j++)  {
            cini(box);
            c[i]|=(1<<(box-1));
        }
    }

    const ll INF=1e18;
    const int allboxes=(1<<n)-1;

    vi mm(m);
    for(int i=0; i<m; i++) 
        mm[i]=i;
    sort(mm.begin(), mm.end(), [&](int i1, int i2) {
        return b[i1]<b[i2];
    });

/* @return min price to buy all needed boxes starting at key midx. */
    function<ll(int,int,ll)> price=[&](int mmidx, int bought, ll maxprice)  {
        if(mmidx==m-1)   /* nothing more to buy */
            return INF;

        int midx=mm[mmidx];
        /* price if not buy key midx */
        ll p1=price(mmidx+1, bought, maxprice);

        if(a[midx]>=maxprice)
            return p1;

        /* price if buy key midx */
        ll p2=a[midx];
        const int oldbought=bought;
        bought|=c[midx];
        if(bought==oldbought) {
            // this key opens no additional box, dont buy it
            return p1;
        }

        if(bought<allboxes)
            p2+=price(mmidx+1, bought, min(p1, maxprice-a[midx]));

        return min(p1, p2);
    };

    ll ans=price(0, 0, INF);
    if(ans==INF)
        ans=-1;

    cout<<ans<<endl;
}

