/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cinll(a);
    cinll(b);

    /* find common divs */
    vll da;
    for(ll i=1; i*i<=a; i++) {
        if(a%i==0) {
            da.push_back(i);
            ll d1=a/i;
            if(d1!=i)
                da.push_back(d1);
        }
    }
    vll d;
    for(ll x : da) {
        if(b%x==0) {
            d.push_back(x);
//cout<<"common d="<<x<<endl;
        }
    }
    sort(d.begin(), d.end());
    
    vb notok(d.size(), false);
    for(size_t i=0; i<d.size(); i++) {
        for(size_t j=i+1; j<d.size(); j++) {
            if(d[i]!=1 && d[j]%d[i]==0) {
                notok[j]=true;
//cout<<"notok d[j]="<<d[j]<<" d[i]="<<d[i]<<endl;
            }
        }
    }

    int ans=0;
    for(bool bo : notok) {
        if(!bo)
            ans++;
    }

    cout<<ans<<endl;

}

