/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cinai(s, 1<<n);

    sort(s.begin(), s.end(), greater<int>());
for(int i=0; i<s.size(); i++)
    cout<<s[i]<<" ";
cout<<endl;

    int cntParents=1;

    bool ok=true;
    for(int level=0; level<n; level++) {
        for(int i=0; i<cntParents; i++) {
            if(s[i]<=s[cntParents+i]) {
                ok=false;
                break;
            }
        }
        cntParents*=2;
        if(!ok)
            break;
    }

    if(ok)
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;

}

