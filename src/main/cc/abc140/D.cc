/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(k);
    cins(s);

    int cnt=1;
    char curSeg=s[0];
    for(int i=1; i<n; i++) {
        if(curSeg!=s[i])
            cnt++;
        curSeg=s[i];
    }

/* Number of happy person is sum(len_of_seg-1) for all segments.
 * Three kinds of operations:
 * 1. Reverse two consecutive segments of same length in on op. This will in fact
 * remove them, hence increase the number of h-people by 2
 * 2. Reverse the first+second segment, increses number of h-people by 1.
 */

    int ans=n-cnt;
    while(k && cnt>2) {
        k--;
        ans+=2;
        cnt-=2;
    }
    while(k && cnt>1) {
        k--;
        ans++;
        cnt--;
    }

    cout<<ans<<endl;

}

