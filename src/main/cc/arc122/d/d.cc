
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Consider N=1, it does not matter which number to choose.
 *
 * Consider only most sign bit of all numbers, that bit is
 * set an even or an odd number of times.
 * if odd then that bit will be part of max value.
 * But if even, then Bob can allways choose some other
 * num with that bit set, so it will not contribute to ans.
 *
 * So, case odd.
 * Then bob will accept that the first bit is set in ans, so 
 * he wants to minimize the second bit.
 * But he needs to do so only for pairs that contribute the first bit.
 * So bob will optimize that not both bits are set.
 * If A chooses a number there are 4 cases for those two bits, and
 * Bob does not choose a number to set both bits in result. 
 * But which of the other numbers he will choose?
 * ...
 *
 * Case even:
 * The numbers are in two groups, and Bob must allways choose from
 * same group as Alice.
 * Within the groups Bob wants to minimize the second bit, so again
 * that number of numbers is odd or even and so on.
 *
 */
void solve() {
}

signed main() {
    solve();
}
