
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * If first op is '-', second is not
 *
 * Foreach i we can add this number with
 * a '+' for all formulars so far,
 * and we can add it with a '-' for all
 * formulars where prev sign is not '-'.
 *
 * We need to maintain the count and sum of all 
 * formulars having '-' as previous sign and the ones
 * having not '-' as previous sign.
 */
using mint=modint1000000007;
void solve() {
    cini(n);
    cinai(a,n);

    if(n==1) {
        cout<<a[0]<<endl;
        return;
    }
        
    /* dp[i][0]= cnt if prev sign is not '-'
     * dp[i][1]= cnt if prev sign is '-'
     **/
    vector<vector<mint>> dp(n, vector<mint>(2));
    dp[0][0]=1;

    vector<vector<mint>> ans(n, vector<mint>(2));
    ans[0][0]=a[0];
    for(int i=1; i<n; i++) {
        dp[i][0]=dp[i-1][0]+dp[i-1][1];
        dp[i][1]=dp[i-1][0];

        ans[i][0]=ans[i-1][0]+(dp[i-1][0]*a[i]) + ans[i-1][1] + (dp[i-1][1]*a[i]);
        ans[i][1]=ans[i-1][0]-(dp[i-1][0]*a[i]);
    }
    cout<<(ans[n-1][0]+ans[n-1][1]).val()<<endl;
}

signed main() {
    solve();
}
