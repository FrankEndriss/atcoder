
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* ternary search, see https://en.wikipedia.org/wiki/Ternary_search */
/* return the point in [l,r] where f(x) is max */
constexpr ld EPS=0.0000001;
ld terns(ld l, ld r, function<ld(ld)> f) {
    while(l+EPS<r) {
        const ld l3=l+(r-l)/3;
        const ld r3=r-(r-l)/3;
        if(f(l3)<f(r3))
            l=l3;
        else
            r=r3;
    }
    return (l+r)/2;
};

/* 
 * Seems that the minimum is at some a[i].
 * Consider it is not at some a[i]...
 * 
 * The expected roi is sum of all a[i] up to 2*x, 
 * plus 2*x*number of remaining a[].
 * minus a[i]
 *
 * So, if the opt value for x is at some a[i] we can binsearch it.
 * ....
 * No. a[i] is not the cost for the insurance, it is the money 
 * lost in ith scenario.
 * Note that whatever we pay, we never get back more than a[i].
 * So best thing that can happen is that we go out without loss,
 * ie a win of 0.
 *
 * If we pay 0, we will loose avg(a[]).
 * if we pay...more we get some more.
 * However, that looks like a function with only one optimum,
 * so lets ternery search that.
 */
void solve() {
    cini(n);
    cinai(a,n);

    sort(all(a));

    function<ld(ld)> f=[&](ld x) {
        ld ans=0;
        for(int i=0; i<n; i++)
            ans+=min((ld)a[i], 2*x)-x-a[i];

        return (ans/n);
    };

    ld l=0;
    ld r=1e9;
    ld x=terns(l,r,f);
    ld ans=-f(x);
    cout<<ans<<endl;
}

signed main() {
    solve();
}
