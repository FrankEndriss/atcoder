/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/* dijkstra with stupid edge calculations */
void solve() {
    cini(n); /* cities */
    cini(m); /* roads */
    cini(s); /* silver */

    vvi edge(2*n, vi(4));
    vvi adj(n);

    for(int i=0; i<m; i++) {
        for(int j=0; j<4; j++)
            cin>>edge[2*i][j];
        edge[i][0]--;
        edge[i][1]--;
        edge[2*i+1]=edge[2*i];
        edge[2*i+1][0]=edge[2*i][1];
        edge[2*i+1][1]=edge[2*i][0];

        adj[edge[i][0]].push_back(2*i);
        adj[edge[i][1]].push_back(2*i+1);
    }

    const int INF=1e18;
    vi dp(n, INF);
    dp[0]=0;

    struct state {
        int t;  /* time arrived */
        int sm; /* best exchange time, scoins/minute */
        int s;  /* silvercoins avail */
    };

    queue<int> q;
    q.push(0);
    while(q.size()) {
        // ??? to much details
    }
    
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

