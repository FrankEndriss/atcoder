/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

typedef unsigned long long ull;
typedef vector<ull> vull;

void solve() {
    cini(n);
    cinai(s,n);
    cinai(t,n);
    vull u;
    for(int i=0; i<n; i++)
        cin>>u[i];
    vull v;
    for(int i=0; i<n; i++)
        cin>>v[i];


/* ans[i][j]= ith row, jth col */
    vector<vull> ans(n, vi(n));

/* fill all bits that must be set anyway to fullfill the AND constraints */
    for(int i=0; i<n; i++) {
        if(s[i]==0) {
            for(int j=0; j<n; j++)
                ans[i][j]|=u[i];
        }

        if(t[i]==0) {
            for(int j=0; j<n; j++)
                ans[j][i]|=v[i];
        }
    }
/* true if v1 is a subset of the bits of v2 */
    function<bool(ull,ull)> isSubset(ull v1, ull v2) {
        return v1&v2==v1;
    };

/* @return the bits set in v2 but not in v1 */
    function<ull(ull,ull)> missingBits(ull v1, ull v2) {
        return v1^v2;
    }

/* Just brute force the missing ORs 
 * If there is a bit to much for one of the ORs there is no solution.
 * If there is a bit to less for one of the ORs, we need to find
 * an AND row/col where we can add it. This is any cell, as long as
 * there are still ones without that bit.
 **/
    for(int i=0; i<n; i++) {
        if(s[i]==1) {
/* check if ith row OR is a subset of U[i], and if yes 
 * where we could add the missing bits.*/
            int rowor=0;
            for(int j=0; j<n; j++)
                rowor|=ans[i][j];

            if(!isSubset(rowor, u[i])) {
                cout<<-1<<endl;
                return;
            }

/* bits missing in row */
            ull bits=rowor ^ u[i];
/* now find for every bit an AND col where we can add it.
 * Note that if we must not fill a bit in all fields of
 * a col. How to prevent that?
 */
            // todo...
                
        }


        if(t[i]==1) {
            // todo
        }
        
    }
    
}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

