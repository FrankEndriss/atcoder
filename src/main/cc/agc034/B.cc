/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    string s;
    cin>>s;

    int idx=0;
    ll ans=0;
    ll countA=0;
    while(idx<((int)s.size())-2) {
        if(s[idx]=='A' ) {
            if(s[idx+1]=='B' && s[idx+2]=='C') {
                ans++;
                ans+=countA;
                s[idx]='B';
                s[idx+1]='C';
                s[idx+2]='A';
                idx++;
                // dont inc or decrement countA
            } else {
                countA++;
            }
        } else
            countA=0;
        idx++;
    }
    cout<<ans<<endl;
}

