/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
/* We can create a list of how much score on each exam takahashi can gain,
 * whit wich number of study hours.
 * Then we need to add the ones with the best ratio.
 */
    int n, x;
    cin>>n>>x;
    vector<int> b(n), l(n), u(n);
    vector<pair<int, int>> takaGain(n); // takaGain[i].first= score taka can gain if learning 
                                        // takaGain[i].second hours for exam i.
    vector<int> takaGainExtra(n);       // takaGainExtra[i]= score taka gains learning ohne
                                        // hour more. (remainder to X%u[i])
    ll minAokiScore=0
    fori(n) {
        cin>>b[i]>>l[i]>>u[i];
        minAokiScore+=min(l[i]*b[i], x);

        takaGain[i].second=x/u[i];
        takaGain[i].first=takaGain[i].second*u[i];
        takaGainExtra[i]=x-takaGain[i].first;

        takaMore[i]=takaGain[i].first-b[i]*(u[i]-l[i]);
        // idk
    }

}

