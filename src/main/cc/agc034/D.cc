/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vector<pair<int, pair<int, int>>> red(n), blue(n);
    fori(n) {
        int x, y, c;
        cin>>x>>y>>c;
        red.push_back({ c, { x, y }});
    }

    vector<int> redX(n), redY(n), blueX(n), blueY(n);
    fori(n) {
        int x, y, c;
        cin>>x>>y>>c;
        blue.push_back({ c, { x, y }});

        redX[i]=i;
        redY[i]=i;
        blueX[i]=i;
        blueY[i]=i;
    }

    

}

