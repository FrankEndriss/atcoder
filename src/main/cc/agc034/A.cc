/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

bool hasDoubleStone(string &s, int a, int c) {
    for(int i=a; i<c; i++) {
        if(s[i]=='#' && s[i-1]=='#')
            return true;
    }
    return false;
}

bool hasTripleFree(string &s, int a, int c) {
    for(int i=a+1; i<c+1; i++) {
        if(s[i]=='.' && s[i-1]=='.' && s[i-2]=='.')
            return true;
    }
    return false;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, a, b, c, d;
    cin>>n>>a>>b>>c>>d;
    string s;
    cin>>s;

    if(hasDoubleStone(s, a, c) || hasDoubleStone(s, b, d)) {
        cout<<"No"<<endl;
        return 0;
    }

    if(c<d) {
        cout<<"Yes"<<endl;
        return 0;
    }

    /* a must overtake b */
    if(hasTripleFree(s, b-1, d)) {
        cout<<"Yes"<<endl;
        return 0;
    }

    cout<<"No"<<endl;
    return 0;
}

