/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;
typedef vector<vll> vvll;

template<typename T>
void floyd_warshal(vector<vector<T>> &d, int n) {
    for (int k = 0; k < n; ++k) {
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                d[i][j] = min(d[i][j], d[i][k] + d[k][j]);
            }
        }
    }
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(N);    // Towns
    cini(M);    // Roads
    cinll(L);    // Tank size

    const ll INF=1e18;
    vvll fw(N, vll(N, INF));
    for(int i=0; i<N; i++)
        fw[i][i]=0;

    for(int i=0; i<M; i++) {
        cini(a);    
        cini(b);
        cini(c);
        if(c<=L) {
            a--;
            b--;
            fw[a][b]=c;
            fw[b][a]=c;
        }
    }

    floyd_warshal(fw, N);

    for(int i=0; i<N; i++)
        for(int j=0; j<N; j++)
            if(fw[i][j]<=L)
                fw[i][j]=1;
            else
                fw[i][j]=INF;
        
    floyd_warshal(fw, N);

    cini(Q);
    for(int i=0; i<Q; i++) {
        cini(s);
        cini(t);
        s--;
        t--;
        ll ans=fw[s][t];
        if(ans>N)
            cout<<-1<<endl;
        else
            cout<<ans-1<<endl;
    }

}

