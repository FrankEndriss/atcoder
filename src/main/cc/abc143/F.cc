/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);

    vi a(n);
    vi f(n);
    for(int i=0; i<n; i++) {
        cin>>a[i];
        a[i]--;
        f[a[i]]++;
    }

    int nk=-1;
    function<bool(int,int)> fcmp=[&](int i1, int i2) {
        int v1=i1==-1?nk:f[i1];
        int v2=i2==-1?nk:f[i2];
        return v1<v2;
    };

    vi fid(n);
    iota(fid.begin(), fid.end(), 0);
    sort(fid.begin(), fid.end(), fcmp);

    vi fidpre(n);
    fidpre[n-1]=f[fid[n-1]];
    for(int i=n-2; i>=0; i--)
        fidpre[i]=fidpre[i+1]+f[fid[i]];

    cout<<n<<endl;  // k==1
    for(int k=2; k<=n; k++) {
/* Number of time to choose k different cards.
 * Upper bound: n/k
 *
 * If there are cards with f[i]>n/k, some of these cards will be left over,
 * let them be leftOver=f[i]-n/k
 * ans=(n-leftOver)/k
 *
 * How to find sum of leftovers for all k?
 */
        nk=n/k;
        auto it=upper_bound(fid.begin(), fid.end(), -1, fcmp);

        int leftover=0;
        if(it!=fid.end()) {
            int dist=distance(it, fid.end());
            leftover+=fidpre[n-dist]-(dist*nk);
        }
        
        int ans=(n-leftover)/k;
//        cout<<"k="<<k<<" leftover="<<leftover<<" ans="<<ans<<endl;
        cout<<ans<<endl;
    }
    

}

