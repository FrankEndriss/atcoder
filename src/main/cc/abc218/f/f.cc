
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * N times dijkstra?
 *
 * No...m is max N*(N-1).
 *
 * ***
 * We need to consider a shortest path.
 * Its length is max N-1.
 * The removal of all edges not part of that path do not
 * make the path longer.
 * Then do the dijkstra for the graphs resulting of
 * removing them, that makes it O(N*N*M);
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);

    vvi adj(n);
    vi u(m);
    vi v(m);
    for(int i=0;i <m; i++) {
        cin>>u[i]>>v[i];
        u[i]--;
        v[i]--;
        adj[u[i]].push_back(v[i]);
    }

        queue<int> q;
        vi dp(n, INF);

        dp[0]=0;
        q.push(0);
        while(q.size()) {
            int vv=q.front(); 
            q.pop(); 

            for(int chl : adj[vv]) {
                /*
                if(vv==u[i]&&chl==v[i])
                    continue;
                    */

                if(dp[chl]>dp[vv]+1) {
                    dp[chl]=dp[vv]+1;
                    q.push(chl);
                }
            }
        }

    for(int i=0; i<m; i++) {
        if(dp[v[i]]<=dp[u[i]]) 
            cout<<dp[n-1]<<endl;;

        queue<int> q2;
        vi dp2=dp;
        for(int j=0; j<n; j++)
            if(dp2[j]>dp[u[i]])
                dp2[j]=INF;
            else
                q2.push(j);

        while(q2.size()) {
            int vv=q2.front(); 
            q2.pop(); 

            for(int chl : adj[vv]) {
                if(vv==u[i]&&chl==v[i])
                    continue;

                if(dp2[chl]>dp2[vv]+1) {
                    dp2[chl]=dp2[vv]+1;
                    q2.push(chl);
                }
            }
        }



        if(dp2[n-1]==INF)
            cout<<-1<<endl;
        else
            cout<<dp2[n-1]<<endl;
    }

}

signed main() {
    solve();
}
