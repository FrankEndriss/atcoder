
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* rot by 90 degree */
void lrot(vs &data) {
    vs t=data;
    for(size_t i=0; i<data.size(); i++) {
        for(size_t j=0; j<data.size(); j++) {
            int jj=i;
            int ii=data.size()-1-j;
            t[ii][jj]=data[i][j];
        }
    }
    data=t;
}

/**
 */
void solve() {
    cini(n);
    cinas(s,n);
    cinas(t,n);

    for(int cnt=0; cnt<4; cnt++) {
        cerr<<"cnt="<<cnt<<endl;
        for(int i=0; i<n; i++) {
            cerr<<s[i]<<endl;
        }

        set<pii> ps;
        set<pii> pt;
        for(int i=0; i<n; i++) {
            for(int j=0; j<n; j++) {
                if(s[i][j]=='#')
                    ps.insert({i,j});
                if(t[i][j]=='#')
                    pt.insert({i,j});
            }
        }

        if(ps.size()==pt.size()) {
            if(ps.size()==0) {
                cout<<"Yes"<<endl;
                return;
            }

            auto its=ps.begin();
            auto itt=pt.begin();
            int offx=itt->first - its->first;
            int offy=itt->second - its->second;

            bool ok=true;
            while(its!=ps.end()) {
                if(itt->first - its->first != offx) 
                    ok=false;

                if(itt->second - its->second != offy) 
                    ok=false;

                its++;
                itt++;
            }
            if(ok) {
                cout<<"Yes"<<endl;
                return;
            }
        }

        lrot(s);
    }
    cout<<"No"<<endl;
}

signed main() {
    solve();
}
