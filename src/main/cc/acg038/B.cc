
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);

/** 1. if smallest first and biggest last, two sortings end in same P.
 *  2. if subsets are presorted their sorting ends in same result. (n-1 time) */
    cini(n);
    cini(k);
    cinai(p, n);

    vb sorted(n);   // if sorted[i], p[i]...p[i+k-1] is sorted

    int unsort=0;
    int scount=0;
    
    for(int i=1; i<n; i++) {
        if(p[i-1]>p[i])
            unsort=i;

        if(i-unsort+1>=k) {
            sorted[i-(k-1)]=true;
            scount++;
        }
    }

    multiset<int> ms;
    for(int i=0; i<k; i++)
        ms.insert(p[i]);
    

    int ans=n-k+1;
    for(int i=0; i<n-k; i++) {
        ms.insert(p[i+k]);
        int mi=*ms.begin();
        int ma=*ms.rbegin();
        if(p[i]==mi && p[i+k]==ma && !sorted[i])
            ans--;
        ms.erase(ms.find(p[i]));
    }
    if(scount>1)
        ans-=(scount-1);

    cout<<ans<<endl;

}

