/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(rows); // number of rows
    cini(cols);
    cini(rowc); // in every row the min(cnt)==rowc
    cini(colc); // in every row the min(cnt)==colc

    int minOnes1=rows*(min(rowc, cols-rowc));
    int minOnes2=cols*(min(colc, rows-colc));
    int minOnes=min(minOnes1, minOnes2);    // min ones and min zeros

    if(minOnes>(rows*cols)/2) { // not sure if sufficient
        cout<<-1<<endl;
    } else {    // construct result matrix
        vvi m(rows, vi(cols));
        int cnt=0;
        for(int i=0; i<rows; i++) {
            for(int j=0; j<rowc; j++) {
                m[i][cnt%cols]=1;
                cnt++;
            }
        }

        for(int i=0; i<rows; i++) {
            for(int j=0; j<cols; j++) {
                cout<<m[i][j];
            }
            cout<<endl;
        }
    }

}

