/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MAX=1000001;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cinai(a, n);
    vi f(MAX);
    vvi divs(n);
    ll sum=0;

    //sort(a.begin(), a.end());

    for(int i=0; i<n; i++) {
        f[a[i]]++;
        sum+=a[i];

        /* divisors of a[i] */
        for(int j=2; j*j<=a[i]; j++) {
            if(a[i]%j==0) {
                divs[i].push_back(j);
                int d2=a[i]/j;
                if(d2!=j)
                    divs[i].push_back(d2);
            }
        }
        sort(divs[i].begin(), divs[i].end());
    }


    ll ans=0;
    for(int i=0; i<n; i++) {
        ll gsum=0;
        for(int j=a[i]; j<MAX; j+=a[i]) {
            gsum+=f[j]
        }
// idk...
        
        sum-=a[i];
        
        ans+=(sum*a[i]);
        
    }

}

