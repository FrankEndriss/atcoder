/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

class DSU {
public:
    vector<int> p;
/* initializes each (0..size-1) to single set */
    DSU(int size) {
        p.resize(size);
        for(int i=0; i<size; i++)
            p[i]=i;
    }

/* finds set representative for member v */
    int find_set(int v) {
        if (v == p[v])
            return v;
        return p[v] = find_set(p[v]);
    }

/* makes v a single set, removes it from other set if contained there. */
    void make_set(int v) {
        p[v] = v;
    }

/* combines the sets of two members.
 *  * Use the bigger set as param a for optimized performance.  */
    void union_sets(int a, int b) {
        a = find_set(a);
        b = find_set(b);
        if (a != b)
            p[b] = a;
    }
};

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(m);
    cini(q);
    
/* 1. if u,v are togther in a single-group, they must not be together in
 * a multi-group. (and vice versa).
 * 2. Number of N must be big enough.
 */
    DSU sdsu(n);
    set<int> sset;
    DSU mdsu(n);
    set<int> mset;

    bool possible=true;
    for(int i=0; i<q; i++) {
        cini(u);    
        cini(v);
        cini(c);

        if(c==0) {  // single
            sset.insert(u);
            sset.insert(v);
            sdsu.union_sets(u, v);
            if(mdsu.find_set(u)==mdsu.find_set(v))
                possible=false;
        } else {
            mset.insert(u);
            mset.insert(v);
            mdsu.union_sets(u, v);
            if(sdsu.find_set(u)==sdsu.find_set(v))
                possible=false;
        }
    }

// idk... :/
    int minN=sset.size()+mset.size();
    minN-=min(sset.size(), mset.size()/3);
cout<<"pos="<<possible<<" minN="<<minN<<" sset.size()="<<sset.size()<<" mset.size()="<<mset.size()<<endl;
    if(possible && minN <= n && (sset.size()+mset.size())-1<=m) {
        cout<<"Yes"<<endl;
    } else 
        cout<<"No"<<endl;

}

