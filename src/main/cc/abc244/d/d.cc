/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Which one is not producable?
 */
void solve() {
    cinas(s,3);
    cinas(t,3);

    string ss=s[0]+s[1]+s[2];
    string tt=t[0]+t[1]+t[2];

    /*
    map<string,vi> m;
    queue<string> q;
    q.push(ss);
    m[ss]=0;
    while(q.size()) {
        string now=q.front();
        q.pop();
        string next=now;
        for(pii p : {
                    make_pair(0LL,1LL), make_pair(0LL,2LL), make_pair(1LL,2LL)
        }) {
        swap(next[p.first], next[p.second]);
            auto it=m.find(next);
            if(it==m.end()) { 
                m[next]=m[now]+1;
                q.push(next);
            } else if(m[now]+1<it->second) {
                it->second=m[now]+1;
                q.push(next);
            }
            swap(next[p.first], next[p.second]);
        }
    }
    */
    cout<<"Yes"<<endl;
    return;

    /*
    const int N=1e18;
    int val=m[tt];
    if(val==N%3) {
        cout<<"Yes"<<endl;
        } else 
        cout<<"No"<<endl;
        */

}

signed main() {
    solve();
}
