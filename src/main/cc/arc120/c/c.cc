/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/segtree>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


#include <atcoder/lazysegtree>
using namespace atcoder;


using S=int; /* type of values */
using F=int; /* type of upates */

S st_op(S a, S b) {
    return a+b;
}

/* This is the neutral element */
S st_e() {
    return 0;
}

/* This applies an update to some value. */
S st_mapping(F f, S x) {
    return f+x;
}

/* This combines two updates.  */
F st_composition(F f, F g) {
    return f+g;
}

/* This is the neutral update.
 * It is similar to st_e() in some sense.  */
F st_id() {
    return 0;
}

using stree=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;

/* Find min number of ops, -1 if impossible.
 * swap adj elements, inc a[i], dec a[i+1]
 * Note that the operation is reversed if done two time in a row.
 * Otherwise an element goes left and is incremented by on at each position.
 *
 * Consider b[0].
 * Each fitting a[] can go there.
 * So, we need to find a pairing of a[i] to b[j].
 * Note that all a[i] fitting for one b[j] fit all for the same b[j].
 * So, these aquivalenz classes must be same size to work out in the end.
 * Then find foreach class the number of indexes in a[i] and b[j], and
 * subtract this number from the number of elements in the class.
 *
 * But note that we allways move two elements at once, so what we want to
 * sum is the number of position div by 2.
 * ...no. How to count the operations correctly?
 */
void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);

    map<int,vi> fa; /* fa[i]=indexes of aeq class a[i] in a[] */
    map<int,vi> fb; /* fb[i]=indexes of aeq class a[i] in b[] */
    for(int i=0; i<n; i++)  {
        fa[a[i]+i].push_back(i);
        fb[b[i]+i].push_back(i);
    }

    for(auto ent : fa) {
        if(ent.second.size()!=fb[ent.first].size()) {
            cout<<-1<<endl;
            return;
        }
    }

    int ans=0;
    stree seg(n);   /* seg.get(i)== number of moves to right of element i */
    map<int,int> idx;

    for(int i=0; i<n; i++) {
        int val=b[i]+i;
        int off=idx[val];
        idx[val]++;
        int pos1=fa[val][off];
        int pos2=pos1+seg.get(pos1);
        if(i<pos2) {
            ans+=pos2-i;
            seg.apply(0,pos1,1);
        }
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}
