
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Three colors, R,B,.
 *
 * Ok, to make each possible path same freq of colors, all
 * diagonals must be same color cells.
 *
 * So if there is a diagonal with mixed colors, ans=0
 * Else we need to color the whole diagonal in one color,
 * so if there is no colored cell,
 * ans*=2^((len+1)/2)
 *
 * So iterate the diagonals starting at 1..h+w, 1
 *
 ****
 * Still WA, idk :/
 */
using mint=modint998244353;
void solve() {
    cini(h);
    cini(w);
    cinas(s,h);

    bool ok=true;
    int ans=0;
    for(int i=0; i<h+w-1; i++) {
        vi cnt(3);
        for(int j=0; j<w; j++) {
            const int ii=i-j;
            const int jj=j;
            if(ii>=0 && ii<h && jj>=0 && jj<w) {
                if(s[ii][jj]=='R')
                    cnt[0]++;
                else if(s[ii][jj]=='B')
                    cnt[1]++;

                cnt[2]++;
            }
        }

        if(cnt[0]&&cnt[1]) {
            ok=false;
            break;
        } else if(cnt[0]==0 && cnt[1]==0)  {
            ans++;
        }
    }

    mint two=2;
    mint mans=two.pow(ans);
    if(!ok)
        mans=0;
    cout<<mans.val()<<endl;
}

signed main() {
    solve();
}
