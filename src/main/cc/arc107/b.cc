
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* foreach x in 2..n*2
 *    ans+=(number of pairs a+b==x) * (number of pairs c+d+K==x)
 *
 * if x<=n+1
 *    p1=x-1    (since we have a pair foreach a=1..n)
 * else
 *              (we have a pair foreach a=x-n .. n
 *
 */
void solve() {
    cini(n);
    cini(k);

    int ans=0;
    for(int aplusb=2; aplusb<=n+n; aplusb++) {
        /* number of pairs a+b==aplusb */
        int miD=max(1LL, aplusb-n);
        int maD=min(n, aplusb-1);
        int p1=maD-miD+1;

        /* number of pairs c+d+k==aplusb */
        int aplusb2=aplusb-k;
        miD=max(1LL, aplusb2-n);
        maD=min(n, aplusb2-1);
        int p2=max(0LL, maD-miD+1);

        //cerr<<"aplusb="<<aplusb<<" p1="<<p1<<" p2="<<p2<<endl;
        ans+=p1*p2;
    }

    cout<<ans<<endl;


}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
