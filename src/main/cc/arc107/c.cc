
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#include <atcoder/dsu>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


const int MOD=998244353;


int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

const int N=55;
vector<int> fac(N);

const bool initFac=[](const int mod) {
    fac[1]=1;
    for(int i=2; i<N; i++) {
        fac[i]=mul(fac[i-1], i, mod);
    }
    return true;
}(MOD);

/* We need to calc the rows swaps and col swap independent.
 * However, since rows==cols it is the same value.
 * Since the matrix is a permutation there is no overcount.
 *
 * Given we know which col/row can be swapped with which col/rows
 * (brute force find in O(n^3))
 *
 * Then find the components sizes, in each component we can create
 * fac(compsize) permutations.
 */
void solve() {
    cini(n);
    cini(K);
    vvi a(n, vi(n));
    for(int i=0; i<n; i++) 
        for(int j=0; j<n; j++) 
            cin>>a[i][j];

    dsu dsuR(n);
    dsu dsuC(n);

    vvb adjR(n, vb(n, true));
    vvb adjC(n, vb(n, true));
    for(int i=0; i<n; i++) {
        for(int j=i+1; j<n; j++) {
            /* check if i and j can be swapped, for rows and cols */
            for(int k=0; k<n; k++) {
                if(a[i][k]+a[j][k]>K) {
                    adjR[i][j]=false;
                    adjR[j][i]=false;
                }
                if(a[k][i]+a[k][j]>K) {
                    adjC[i][j]=false;
                    adjC[j][i]=false;
                }
            }
        }
    }
    for(int i=0; i<n; i++) {
        for(int j=i+1; j<n; j++) {
            if(adjR[i][j])
                    dsuR.merge(i,j);
            if(adjC[i][j])
                    dsuC.merge(i,j);
        }
    }

    auto grR=dsuR.groups();
    auto grC=dsuC.groups();

    int ans=1;
    for(auto gr : grR)
        ans=mul(ans, fac[gr.size()]);
    for(auto gr : grC)
        ans=mul(ans, fac[gr.size()]);

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
