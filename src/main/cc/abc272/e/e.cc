/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Ok, we add to _all_ a[i], then check.
 * Do this m times.
 *
 * Lets find for 0 in which round it contributes.
 * For all other rounds, find for 1 when it contributes...
 *
 * Also, iterate only in range 0..N
 *
 *
 * Consider prio q, sorted by next round.
 */
void solve() {
    cini(n);
    cini(m);
    cinai(a,n);


    using t3=tuple<int,int,int>; // round,val,inc
    priority_queue<t3> q;

    for(int i=0; i<n; i++) {
        int st=1;   /* first round with a[i] in range 0..n */
        if(a[i]<0)
            st=max(1LL, -a[i]/(i+1));

        if(a[i]+(i+1)*st<0)
            st++;

        q.emplace(-st, a[i]+(i+1)*st, i+1);
    }

    for(int i=1; i<=m; i++) {
        vb ans(n+1);
        while(q.size()) {
            auto [st,val,inc]=q.top();
            if(-st>i)
                break;

            q.pop();
            if(val>=0 && val<=n)
                ans[val]=true;

            if(val+inc<=n)
                q.emplace(st-1, val+inc, inc);
        }
        for(int j=0; j<=n; j++) 
            if(!ans[j]) {
                cout<<j<<endl;
                break;
            }
    }

}

signed main() {
       solve();
}
