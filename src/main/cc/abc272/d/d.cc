/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * First, find all slopes we can go, that is all 
 * sums of pairs of quads eq m.
 * Can we step out of the grid?
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);

    set<pii> sl;
    for(int i=0; i<n; i++) {
        for(int j=0; j<n; j++) {
            if(i*i+j*j==m) {
                sl.emplace(i,j);
                sl.emplace(j,i);
            }
        }
    }

    vvi dp(n, vi(n, INF));
    queue<pii> q;
    q.emplace(0,0);
    dp[0][0]=0;

    vi muli={1,1,-1,-1};
    vi mulj={1,-1,1,-1};
    while(q.size()) {
        auto [i,j]=q.front();
        q.pop();
        for(pii p : sl) {
            for(int k=0; k<4; k++) {
                int ii=i+p.first*muli[k];
                int jj=j+p.second*mulj[k];
                if(ii>=0 && ii<n && jj>=0 && jj<n && dp[ii][jj]==INF) {
                    dp[ii][jj]=dp[i][j]+1;
                    q.emplace(ii,jj);
                }
            }
        }
    }

    for(int i=0; i<n; i++) {
        for(int j=0; j<n; j++) {
            if(dp[i][j]==INF)
                dp[i][j]=-1;

            cout<<dp[i][j]<<" ";
        }
        cout<<endl;
    }
}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
