
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Constraints are small, how works the optimized brute force?
 *
 * There are 2^N sets of remaining intervals.
 * There are 2^N sets of remaining positions.
 *
 * What about intersections?
 * Chooseing lr[i] marks a set of intervals done. All of those intervals
 * also mark interval i done.
 *
 * So, if drawn as a graph, players alternating remove a vertex, and remove
 * all vertex in distance 1 along with it.
 * We need to consider each component separate.
 * And, we can propably split a component into two components :/
 *
 * So, how to count?
 */
void solve() {
    cini(n);
    vector<pii> lr(n);
    for(int i=0; i<n; i++)
        cin>>lr[i].first>>lr[i].second;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
