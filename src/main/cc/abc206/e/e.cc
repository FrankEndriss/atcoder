
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * Brute force would be:
 * check all number in L..R; n
 *   find divisors d[] of n
 *   iterate n+d[0] and count pairs
 *
 * But this clearly TLE.
 *
 * Lets collect all primefactors for the numbers up to N.
 *
 * A pair contributes if 
 * it has at least one differing primefactor (in both numbers)
 * it has at least one common primefactor
 *
 * We can find the numbers with common primefactors quickly, ... 
 * and then?
 * 
 *
 */
void solve() {
    cini(L);
    cini(R);
}

signed main() {
    solve();
}
