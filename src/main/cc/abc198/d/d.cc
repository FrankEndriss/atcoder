
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* dfs?
 * No, simply check all permutations :/
 */
void solve() {
    cinas(s,3);

    reverse(all(s[0]));
    reverse(all(s[1]));
    reverse(all(s[2]));

    if(s[2].size()<s[1].size() || s[2].size()<s[0].size()) {
        cout<<"UNSOLVABLE"<<endl;
        return;
    }

    vector<int> val(256, -1);
    vector<bool> used(10);

    /* row==i, col==j
     * Bad casework, not sure what such problem is good for :/
     * ... do it as last problem.
     * */
    function<bool(int,int,int)> dfs=[&](int i, int j, int carry) {
        cerr<<"dfs, i="<<i<<" j="<<j<<" carry="<<carry<<endl;
        if(j>=s[2].size()) {
            if(carry==0) {
                for(size_t kk=0; kk<3; kk++) {
                    for(size_t k=0; k<s[kk].size(); k++)
                        cout<<val[s[kk][k]];
                    cout<<endl;
                }
                return true;
            }
            return false;
        }

        if(i==0 || i==1) {
            if(j>=s[i].size() || val[s[i][j]]>=0) {
                return dfs(i+1,j,carry);
            } else {
                for(int k=0; k<=9; k++) {
                    cerr<<"k="<<k<<endl;
                    if(!used[k]) {
                        used[k]=true;
                        val[s[i][j]]=k;
                        if(dfs(i+1, j, carry))
                            return true;
                        used[k]=false;
                        val[s[i][j]]=-1;
                    }
                }
            }
        } else if(i==2) {
            int sum=carry;
            if(j<s[0].size()) {
                assert(val[s[0][j]]>=0);
                sum+=val[s[0][j]];
            }
            if(j<s[1].size()) {
                assert(val[s[1][j]]>=0);
                sum+=val[s[1][j]];
            }

            int dsum=sum%10;
            if(val[s[2][j]]>=0) {
                if(val[s[2][j]]!=dsum)
                    return false;
                else {
                    return dfs(0, j+1, sum/10);
                }
            } else {
                if(used[dsum])
                    return false;

                val[s[2][j]]=dsum;
                used[dsum]=true;
                if(dfs(0, j+1, sum/10))
                    return true;
                val[s[2][j]]=-1;
                used[dsum]=false;
            }
        }

    };

    if(!dfs(0,0,0))
        cout<<"UNSOLVABLE"<<endl;
}

signed main() {
    solve();
}
