
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* dfs and maintian visited colors, do not visit colors twice.
 */
const int N=1e5+7;
void solve() {
    cini(n);
    cinai(c,n);
    vvi adj(n);
    for(int i=0; i<n-1; i++) {
        cini(a); a--;
        cini(b); b--;
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    vb good(n); /* visited vertex */
    vi vis(N);  /* visited colors */
    function<void(int,int)> dfs=[&](int v, int p) {
        if(vis[c[v]]==0)
            good[v]=true;

        vis[c[v]]++;

        for(int chl : adj[v]) {
            if(chl!=p)
                dfs(chl, v);
        }

        vis[c[v]]--;
    };

    dfs(0,-1);
    for(int i=0; i<n; i++) {
        if(good[i])
            cout<<i+1<<endl;
    }
}

signed main() {
    solve();
}
