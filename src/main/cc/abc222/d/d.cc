/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * dp[j]=number of combis where c==j after current round
 *
 * So, dp0[k]==
 * Sum of dp[0] ... dp[k]
 * for all k>=a[i] && k<=b[i]
 *
 * Find the sum using prefix sums.
 *
 */
const int N=3007;
using mint=modint998244353;
void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);

    vector<mint> dp(N);
    for(int i=a[0]; i<=b[0]; i++) 
            dp[i]+=1;

    for(int i=1; i<n; i++) {
        vector<mint> pre(N+1);
        for(int j=1; j<N; j++)
            pre[j]=pre[j-1]+dp[j-1];

        vector<mint> dp0(N);
        for(int j=a[i]; j<=b[i]; j++) 
            dp0[j]=pre[j+1];

        dp=dp0;
    }

    mint ans=accumulate(all(dp), mint(0));
    cout<<ans.val()<<endl;
}

signed main() {
    solve();
}
