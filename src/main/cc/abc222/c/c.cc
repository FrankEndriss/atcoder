/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* @return points of c1 
 * Rock==G
 * Sciccoir==C
 * Paper==P
 * */
int play(char c1, char c2) {
    if(c1=='G' && c2=='C')
        return 1;
    else if(c1=='C' && c2=='P')
        return 1;
    else if(c1=='P' && c2=='G')
        return 1;
    else if(c1=='G' && c2=='P')
        return -1;
    else if(c1=='P' && c2=='C')
        return -1;
    else if(c1=='C' && c2=='G')
        return -1;
    else 
        return 0;
}

/**
 * Simulate
 *
 * ...still some implementation bug :/
 * Seems to be the annoying one problem.
 *
 * ...sort the players correctly, most points first.
 */
void solve() {
    cini(n);
    cini(m);

    vector<pii> p(2*n);
    for(int i=0; i<2*n; i++) 
        p[i]={0,i}; /* <points, id> */

    vs a(2*n);
    for(int i=0; i<2*n; i++)
        cin>>a[i];

    for(int j=0; j<m; j++) {
        for(int i=0; i<n; i++) {
            int res=play(a[p[i*2].second][j], a[p[i*2+1].second][j]);
            //cerr<<"play "<<a[p[i*2].second][j]<<" "<<a[p[i*2+1].second][j]<<" res="<<res<<endl;
            if(res==1)
                p[i*2].first--;
            else if(res==-1)
                p[i*2+1].first--;
        }

        sort(all(p));
    }

    for(int i=0; i<2*n; i++) 
        cout<<p[i].second+1<<endl;
}

signed main() {
    solve();
}
