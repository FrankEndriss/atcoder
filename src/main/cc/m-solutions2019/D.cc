/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

//#define DEBUG

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vector<int> c(n);
    vector<vector<int>> tree(n+1);
    fori(n-1) {
        int a, b;
        cin>>a>>b;
        tree[a].push_back(b);
        tree[b].push_back(a);
    }

#ifdef DEBUG
cout<<"didread tree"<<endl;
#endif
    auto vcmp=[&](int i1, int i2) {
            return tree[i2].size()<tree[i1].size();
    };

    fori(n) {
        cin>>c[i];
        /* sort childs by number of edges desc. */
        sort(tree[i+1].begin(), tree[i+1].end(), vcmp);
    }
#ifdef DEBUG
cout<<"didread c"<<endl;
#endif

    sort(c.begin(), c.end(), greater<int>());

    /* find fattest vertex in tree, use as root. */
    int fat=1;
    int fatsz=(int)tree[fat].size();
    for(int i=2; i<=n; i++) 
        if((int)tree[i].size()>fatsz) {  
            fatsz=(int)tree[i].size();
            fat=i;
        }

#ifdef DEBUG
cout<<"didfind fat"<<endl;
#endif
    int cidx=0;
    vector<int> ans(n+1);
    function<ll(int, int)> dfs=[&](int vertex, int parent) {
        ans[vertex]=c[cidx];
#ifdef DEBUG
cout<<"dfs, vertex="<<vertex<<" parent="<<parent<<" cidx="<<cidx<<" ans[vertext]="<<ans[vertex]<<endl;
#endif
        cidx++;
        ll sum=0;
        for(int chld : tree[vertex])
            if(chld!=parent) {
                sum+=dfs(chld, vertex);
                sum+=min(ans[vertex], ans[chld]);
            }
        return sum;
    };

    ll ans2=dfs(fat, -1);
#ifdef DEBUG
cout<<"did dfs"<<endl;
#endif
    cout<<ans2<<endl;
    for(int i=1; i<=n; i++) 
        cout<<ans[i]<<" "<<endl;
    cout<<endl;
}

