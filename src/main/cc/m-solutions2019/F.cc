/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

//#define wins(i, j) (A[max(i, j)][min(i, j)])
#define wins(i, j) (i>j?A[i][j]:!A[j][i])

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);

    int n;
    cin>>n;
    vector<vector<bool>> A(n); //A[i][j]= i defeats j
    for(int i=1; i<n; i++) {
        for(int j=0; j<i; j++) {
            int a;
            cin>>a;
            A[i].push_back(a==1);
        }
    }

/* p1 can be winner 
 * if left of it is a p2 wich is defeatable, and can be winner of
 * group left of p1.
 * And if right of it is a p3 wich is defeatable, and can be winner of group right, of p1.
 *
 * So, we dp the groups from right to left and left to right, finding for the new person
 * at position x if it can be winner in that group.
 */

/* return true if person p can be winner in segment [L, R], where L<=p<=R */
    function<bool(int, int, int)> canBeWinner=[&](int L, int R, int p) {
        bool lWin=false;
        if(p==L)
            lWin=true;
        else {
            // check all defeatables
            for(int j=L; j<p; j++) {
                if(wins(p, j) && canBeWinner(L, p-1, j)) {
                    lWin=true;
                    break;
                }
            }
        }
        if(!lWin)
            return false;

        bool rWin=false;
        if(p==R)
            rWin=true;
        else {
            for(int j=R; j>p; j--) {
                if(wins(p, j) && canBeWinner(p+1, R, j)) {
                    rWin=true;
                    break;
                }
            }
        }

        return rWin;
    };

    int ans=0;
    for(int i=0; i<n; i++)
        if(canBeWinner(0, n-1, i))
            ans++;

    cout<<ans<<endl;
}

