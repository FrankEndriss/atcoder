/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
void solve() {
    cini(q);

    map<int,int> m;

    for(int i=0; i<q; i++) {
        cini(t);
        if(t==1) {
            cini(x);
            m[x]++;
        } else if(t==2) {
            cini(x);
            cini(c);
            auto it=m.find(x);
            if(it!=m.end()) {
                it->second-=c;
                if(it->second<=0) {
                    m.erase(it);
                }
            }
        } else if(t==3) {
            auto it=m.begin();
            int l=it->first;
            auto it2=m.rbegin();
            int r=it2->first;
            cout<<r-l<<endl;
        } else
            assert(false);
    }
}

signed main() {
        solve();
}
