/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/lazysegtree>
using namespace atcoder;


const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using S=int; /* type of values */
using F=int; /* type of upates */

S st_op(S a, S b) {
    return a+b;
}

/* This is the neutral element */
S st_e() {
    return 0;
}

/* This applies an update to some value. */
S st_mapping(F f, S x) {
    return f+x;
}

/* This combines two updates.  */
F st_composition(F f, F g) {
    return f+g;
}

/* This is the neutral update. 
 * It is similar to st_e() in some sense.  */
F st_id() {
    return 0;
}

using stree=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;

/**
 * Let t1=index of query when row i was last set before query qidx.
 * Let x be the value that row was set to.
 * Let sum1=sum of add operations on col j at time t1.
 * Let sum2=sum of add operations on col j at time qidx.
 * So, ans for cell i,j in query qidx is 
 * ans=sum2-sum1+x
 */
void solve() {
    cini(n);
    cini(m);
    cini(q);

    vvi Q;
    vector<pii> row(n);  /* qidx,val of last set-operation in that row */
    stree seg(m);   /* seg.get(i)=sum of updates in col i */

    vector<tuple<int,int,int>> qq;   /* <qidx,x,col */

    for(int qidx=0; qidx<q; qidx++) {
        cini(t);
        if(t==1) {
            cini(l); l--;
            cini(r); r--;
            cini(x);
            const vi dat={1LL,l,r,x,qidx};
            Q.emplace_back(dat);
        } else if(t==2) {
            cini(i); i--;
            cini(x);
            row[i]={qidx,x};
            const vi dat={2LL,i,x,0,qidx};
            Q.emplace_back(dat);
        } else if(t==3) {
            cini(i); i--;
            cini(j); j--;
            qq.emplace_back(row[i].first,row[i].second,j);
            const vi val={3LL,i,j,0,qidx,row[i].first,row[i].second};
            Q.emplace_back(val);
        }
    }

    sort(all(qq));
    map<pii,int> sum1x;  /* <col,qidx>,x-sum1 */

    int ii=0;
    for(int qidx=0; qidx<q; qidx++) {
        vi qv=Q[qidx];
        if(qv[0]==1) {
            seg.apply(qv[1], qv[2]+1, qv[3]);
        } else if(qv[0]==2) {
            ;
        } else if(qv[0]==3) {
            int sum2=seg.get(qv[2]);
            assert(sum1x.find({qv[2],qv[5]})!=sum1x.end());
            int ans=sum2+sum1x[{qv[2],qv[5]}];
            cout<<ans<<endl;
        } else 
            assert(false);

        if(ii<qq.size() && get<0>(qq[ii])==qidx) {
            auto [qqidx,x,col]=qq[ii];
            ii++;
            sum1x[{col,qqidx}]=x-seg.get(col);
        }
    }
}

signed main() {
        solve();
}
