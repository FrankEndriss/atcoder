/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Add all
 * Remove multiple of A
 * Remove multiple of B
 * Add muliple of A*B
 *
 * Edgecase, what if A==B?
 * Ok. 
 * But it seems there is some other one :/
 * Overflow? -> no
 */
void solve() {
    cini(n);
    cini(a);
    cini(b);

    int ans=n*(n+1)/2;
    int cntA=n/a;
    ans-=(cntA*(cntA+1)/2)*a;

    if(a==b) {
        cout<<ans<<endl;
        return;
    }

    int cntB=n/b;
    ans-=(cntB*(cntB+1)/2)*b;

    int ab=a*b/gcd(a,b);
    int cntAB=n/ab;
    ans+=(cntAB*(cntAB+1)/2)*ab;

    cout<<ans<<endl;
}

signed main() {
        solve();
}
