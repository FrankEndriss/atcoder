/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int h, w;
    cin>>h>>w;
    vector<string> s(h);    // one string per row
    vector<int> obsRow[h];  // one list of obstacles per row
    vector<int> obsCol[w];  // one list of obstacles per col

    fori(w)
        obsCol[i].push_back(0);

    fori(h) {
        cin>>s[i];
        obsRow[i].push_back(0);
        for(int j=0; j<w; j++) {
            if(s[i][j]=='#') {
                obsRow[i].push_back(j+1);
                obsCol[j].push_back(i+1);
            }
        }
        obsRow[i].push_back(w+1);
    }

    fori(w)
        obsCol[i].push_back(h+1);

    int ans=0;
    for(int col=1; col<=w; col++) {
        for(int row=1; row<=h; row++) {
            if(s[row-1][col-1]=='#')
                continue;
                
            /* binsearch both directions */
            auto it=upper_bound(obsRow[row-1].begin(), obsRow[row-1].end(), col);
            int horz=*it-*(it-1)-1;
            it=upper_bound(obsCol[col-1].begin(), obsCol[col-1].end(), row);
            int vert=*it-*(it-1)-1;
            ans=max(ans, horz+vert-1);
        }
    }

    cout<<ans<<endl;
    

}

