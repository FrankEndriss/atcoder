
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vector<int> w(n);
    ll s2=0;
    fori(n) {
        cin>>w[i];
        s2+=w[i];
    }
    ll s1=0;

    ll ans=s2;
    for(int i=0; i<n; i++) {
        ans=min(ans, abs(s1-s2));
        s1+=w[i];
        s2-=w[i];
    }
    cout<<ans<<endl;

}

