/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

const int MOD=1000000007;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a);
        p >>= 1;
        a = mul(a, a);
    }
    return res;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    string s;
    cin>>s;

/* For any bit set {
 *  ans+=2^(bit+1) -1
 *  }
 * How to implement?
 */
    int ans=0;
    for(int i=1; i<s.size(); i++) {
        if(s[(int)s.size()-1-i]=='1') {
            int lans=toPower(2, i-1)-1;
            lans=(lans+lans)%MOD;
            ans=(ans+lans)%MOD;
        }
    }
    cout<<ans<<endl;

}

