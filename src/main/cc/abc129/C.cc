/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, m;
    cin>>n>>m;
    vector<bool> a(100001, false);
    fori(m) {
        int x;
        cin>>x;
        a[x]=true;
    }

    static int MOD=1000000007;

    vector<int> dp(100001);
    dp[0]=1;
    if(a[1])
        dp[1]=0;
    else
        dp[1]=1;

    for(int i=2; i<=n; i++) {
        if(a[i])
            dp[i]=0;
        else {
            dp[i]=(dp[i-1]+dp[i-2])%MOD;
        }
    }
    cout<<dp[n]<<endl;

}

