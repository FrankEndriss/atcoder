
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* if a and b are ascending it is always possible, since
 * we could move all completly right, then one after the 
 * other from current positon to desired position.
 *
 * Its greedy, isnt't it?
 * We go from left to right.
 * if current p can be moved to b[i], move it then move
 * stacked pinguins, else stack the position, 
 * and go to next penguin.
 * Repeat.
 * ****
 * Ok, didn't get it.
 * We cannot simply move penguins in place, since they slide until
 * the end  of squares or another penguin.
 *
 * Consider position b[0]. If it is 1 it is trivial.
 * Else we can move the penguin [1] to position b[0]+1,
 * then move [0] to positoin b[0].
 * We can move [1] to position b[0]+1 if
 *   it is already there, a[1]==b[0]+1
 *   a[0]==b[0], but in that case there is no need to move it at all.
 *   a[2]==b[0]+2
 *   Note that a[0] depends on a[1], a[1] on a[2] etc.
 **/
void solve() {
    cini(N);
    cini(L);
    cinai(a,N);
    cinai(b,N);

    int ans=0;
    for(int i=0; i<N; i++) 
        if(a[i]!=b[i])
            ans++;

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
