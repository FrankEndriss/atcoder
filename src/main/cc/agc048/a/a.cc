
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int INF=1e9;

/*
 * foreach position we can move a letter to that
 * position makeing prefix>atcoder, or we can move a 
 * letter to that position makeing prefx==atcoder
 */
void solve() {
    cins(s);
    string atcoder="atcoder";


    if(s[0]>'a') {
        cout<<0<<endl;
        return;
    }

    /* dp[i]==min moves to make prefix 0,i-1 equal */
    vi dp(max(s.size(),atcoder.size())+1, INF);
    dp[0]=0; /* empty prefix is initial same */

    vi ans(dp.size(), INF);

    for(int i=0; i<min(atcoder.size(),s.size()); i++) {
        for(int j=i; j<s.size(); j++) {
            if(s[j]>atcoder[i]) {
                ans[i+1]=min(ans[i+1], dp[i]+j-i);
                //cerr<<"i="<<i<<" j="<<j<<" ans[i+1]="<<ans[i+1]<<endl;
                break;
            }
        }
        for(int j=i; j<s.size(); j++) {
            if(s[j]==atcoder[i]) {
                dp[i+1]=min(dp[i+1], dp[i]+j-i);
                if(j>i)
                    rotate(s.begin()+i, s.begin()+j, s.begin()+j+1);
                break;
            }
        }
    }

    int ans0=*min_element(all(ans));
    if(s.size()>atcoder.size())
        ans0=min(ans0, dp[atcoder.size()]);

    if(ans0==INF)
        ans0=-1;
    cout<<ans0<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
