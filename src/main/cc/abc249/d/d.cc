/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/** good for up to ~100 */
ll nCr(int n, int k) {
    double res = 1;
    for (int i = 1; i <= k; ++i)
        res = res * (n - k + i) / i;
    return (ll)(res + 0.01);
}

/**
 * a[i]*a[j]=a[k]
 *
 * Cases:
 * 1. a[i]=a[j]=a[k]=1
 * 2. a[i]=1, a[j]=a[k]
 * 3. a[i]=a[j]
 * 4. a[i]!=a[j]
 *
 * WA. Some cases missing?
 */
const int N=1e5+3;
void solve() {
    cini(n);
    cinai(a,n);

    /*
    sort(all(a));
    for(int i=0; i<n; i++)
        cerr<<a[i]<<" ";
    cerr<<endl;
    */

    vi f(N);
    for(int i=0; i<n; i++)
        f[a[i]]++;

    int ans=0;
    /* case 1 */
    if(f[1]>=3)
        ans+=nCr(f[1],3)*6;

    /* case 2 */
    if(f[1]>0) {
        for(int j=2; j<N; j++)
            if(f[j]>=2)
                ans+=f[1]*nCr(f[j],2)*2*2;
    }

    /* case 3 */
    for(int j=2; j*j<N; j++)
        if(f[j]>=2 && f[j*j]>=1)
            ans+=nCr(f[j],2)*2*f[j*j];

    /* case 4 */
    for(int i=2; i<N; i++)
        for(int j=i+1; i*j<N; j++)
            ans+=f[i]*f[j]*f[i*j]*2;

    cout<<ans<<endl;
}

signed main() {
    solve();
}
