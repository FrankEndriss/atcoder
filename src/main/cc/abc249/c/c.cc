/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
void solve() {
    cini(n);
    cini(k);
    cinas(s,n);

    vvi f(n, vi(26));
    for(int i=0; i<n; i++) 
        for(size_t j=0; j<s[i].size(); j++) 
            f[i][s[i][j]-'a']=1;

    int ans=0;
    for(int i=0; i<(1<<n); i++) {
        int lans=0;
        for(int c=0; c<26; c++) {
            int a=0;
            for(int j=0; j<n; j++)
                if(i&(1<<j))
                    a+=f[j][c];

            if(a==k)
                lans++;
        }
        ans=max(ans,lans);
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}
