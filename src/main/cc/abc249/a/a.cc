/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
void solve() {
    vvi a(2,vi(3));
    for(int i=0; i<2; i++) 
        for(int j=0; j<3; j++) 
            cin>>a[i][j];
    cini(x);

    vi v(2);
    for(int i=0; i<2; i++) {
        int t=0;
        while(t<x) {
            int cnt=min(a[i][0], x-t);
            //cerr<<"walk cnt="<<cnt<<" times "<<a[i][1]<<endl;
            v[i]+=cnt*a[i][1];
            t+=cnt;
            t+=a[i][2];
        }
    }
    //cerr<<"v[0]="<<v[0]<<" v[1]="<<v[1]<<endl;
    if(v[0]>v[1])
        cout<<"Takahashi"<<endl;
    else if(v[0]<v[1])
        cout<<"Aoki"<<endl;
    else
        cout<<"Draw"<<endl;
}

signed main() {
    solve();
}
