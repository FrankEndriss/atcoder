
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

multiset<int> fact(int n) {
    multiset<int> ans;
    int nn=n;
    for(int i=2; i*i<=nn; i++) {
        while(n%i==0) {
            ans.insert(i);
            n/=i;
        }
    }
    if(n>1)
        ans.insert(n);
    return ans;
}

/* k*(k+1)/2 == n * y
 * find k
 *
 * 1 3 6 10 15 21 28
 * 1 3 2*3 2*5 3*5, 3*7, 
 */
void solve() {
    cini(n);

    int x=0;
    for(int i=1; i<100; i++) {
        x+=i;
        cerr<<x<<" ";
        for(int j : fact(x))
            cerr<<j<<" ";
        cerr<<endl;
    }

    cout<<42<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
