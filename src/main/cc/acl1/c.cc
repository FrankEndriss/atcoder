
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We want to move all into the corner down/right. But
 * since that not possible the second should go to the
 * cell above or left of the corner. Third same.
 * Fourth wants to go to n-1,n-1 and so on.
 * So we consecutivly fill the diagonals from down right.
 * The order in that we do that matters.
 * So basically we have to choose among all possible orders.
 * But if the position of a piece ax is reacheable by another piece
 * bx,it is always optimal to first move the bx, then the ax.
 * Since we can move the bx everywhere the ax can be moved.
 *
 * Note also that it is not possible to 'block' paths in any way.
 * Because if a path would be blocked, we could move the blocking 
 * piece further on the path. But we did not since there is no
 * such path. We always move pieces as far as possible.
 *
 * But still these are to much possibilities. If peaces are on a diagonal
 * they are not reacheable by each other.
 * And if there are some obstacles it gets worse...so we go aprox 
 * numberOfPieces!  different orders.
 */
void solve() {
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
