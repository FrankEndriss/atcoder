
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* see https://cp-algorithms.com/data_structures/disjoint_set_union.html
 * Also known as union find structure. 
 * Note that the above link has some optimizations for faster combining of sets if needed. 
 */

struct Dsu {
    vector<int> p;
    vector<int> s;
/* initializes each (0..size-1) to single set */
    Dsu(int size) {
        p.resize(size);
        s.resize(size, 1);
        iota(p.begin(), p.end(), 0);
    }

/* makes v a single set, removes it from other set if contained there. */
    void make_set(int v) {
        p[v] = v;
    }

/* finds set representative for member v */
    int find_set(int v) {
        if (v == p[v])
            return v;
        return p[v] = find_set(p[v]);
    }

/* combines the sets of two members. 
 * Use the bigger set as param a for optimized performance.  */
    void union_sets(int a, int b) {
        a = find_set(a);
        b = find_set(b);
        if (a != b) {
            p[b] = a;
            s[a]+=s[b];
        }
    }
};

/* we need to find foreach i
 * the number of pairs with both coordinates
 * smaller or both bigger than x[i]/y[i].
 * Utilize 2d fenwick.
 * ......no.
 *
 * We need to somehow use dsu, since it is an arbitrary
 * network.
 * How?
 * This is O(n^2)
 */
const int N=2e5+3;
void solve() {
    cini(n);

    Dsu dsu(N);

    vi x(n);
    vi y(n);
    for(int i=0; i<n; i++)  {
        cin>>x[i]>>y[i];

        for(int j=0; j<i; j++) {
            if((x[j]<x[i] && y[j]<y[i]) || (x[j]>x[i] && y[j]>y[i]))
                dsu.union_sets(i,j);
        }
    }

    for(int i=0; i<n; i++) {
        cout<<dsu.s[dsu.find_set(i)]<<endl;
    }

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
