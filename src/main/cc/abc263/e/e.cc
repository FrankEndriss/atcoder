/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * In each cell i<n the expected value e[i] is
 * Formulars index 1 based:
 * e[n]=0
 * e[i]=1+e[i]/(a[i]+1)+avg(e[i+1..i+a[i]])
 * e[i]-e[i]/(a[i]+1) = 1+avg(e[i+1..i+a[i]])
 * e[i]*(a[i]+1)-e[i] = (a[i]+1)*(1+avg(e[i+1..i+a[i]]))
 * e[i]*a[i]= (a[i]+1)*(1+avg(e[i+1..i+a[i]]))
 * e[i]= (a[i]+1)*(1+avg(e[i+1..i+a[i]])) / a[i]
 * ans=e[1]
 */
using mint=modint998244353;
void solve() {
    cini(n);
    vi a(n);
    for(int i=1; i<n; i++) 
        cin>>a[i];

    vector<mint> dp(n+1);   /* dp[i]=expected number of steps if on cell i */
    dp[n]=0;

    vector<mint> post(n+2); /* postfix sums */

    for(int i=n-1; i>=1; i--) {
        mint avgx=(post[i+1]-post[i+a[i]+1])*mint(a[i]+1).inv();
        dp[i]=(1+a[i])*(1+avgx)*mint(a[i]).inv();
        post[i]=dp[i]+post[i+1];
    }
    cout<<dp[1].val()<<endl;
}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
