/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * dp1[i]=min possible prefix sum 
 * dp2[i]=min possible postfix sum
 */
const int INF=1e18;
void solve() {
    cini(n);
    cini(l);
    cini(r);

    cinai(a,n);

    vi dp1(n+1);
    for(int i=0; i<n; i++) 
        dp1[i+1]=min(l*(i+1), dp1[i]+a[i]);

    vi dp2(n+1);
    for(int i=0; i<n; i++) 
        dp2[n-i-1]=min(r*(i+1), dp2[n-i]+a[n-1-i]);

    int ans=INF;
    for(int i=0; i<=n; i++) 
        ans=min(ans, dp1[i]+dp2[i]);

    cout<<ans<<endl;


}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
