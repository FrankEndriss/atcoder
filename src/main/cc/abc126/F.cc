/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
ll m, k;
    cin>>m>>k;

    int c=(1<<m);
    if(k>=c || (m==1 && k==1)) {
        cout<<-1<<endl;  
        return 0;
    }

    if(m==1) {
        cout<<"0 0 1 1"<<endl;
        return 0;
    }

    fori(c) {
        if(i!=k) 
            cout<<i<<" ";
    }
    cout<<k<<" ";
    for(ll i=c-1; i>=0; i--) {
        if(i!=k) 
            cout<<i<<" ";
    }
    cout<<k<<" ";

    cout<<endl;
}

