/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

bool isMonth(char c1, char c2) {
    int i1=c1-'0';
    int i2=c2-'0';
    int m=i1*10+i2;
    return m>=1 && m<=12;
}

int main() {
    string s;
    cin>>s;
    
    // "YYMM" vs "MMYY"
    // year numbers can be any numbers, but months are limited to 01 - 12
    bool yymm=isMonth(s[2], s[3]);
    bool mmyy=isMonth(s[0], s[1]);
    if(!yymm && !mmyy)
        cout<<"NA";
    else if(yymm && !mmyy)
        cout<<"YYMM";
    else if(!yymm && mmyy)
        cout<<"MMYY";
    else
        cout<<"AMBIGUOUS";
    cout<<endl;
}

