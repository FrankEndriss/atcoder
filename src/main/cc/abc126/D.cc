/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
int n;
    cin>>n;
    vector<vector<pair<int, int>>> tree(n+1);
    vector<int> color(n+1);
    fori(n) {
        int u, v, w;
        cin>>u>>v>>w;   
        tree[u].push_back({ v, w%2 });
        tree[v].push_back({ u, w%2 });
    }

    function<void(int, int, int)> dfs=[&](int node, int parent, int w) { 
        w%=2;
        color[node]=w;
        for(auto c : tree[node]) {
            if(c.first!=parent)
                dfs(c.first, node, w+c.second);
        }
    };

    dfs(1, -1, 0);
    for(int i=1; i<=n; i++)
        cout<<color[i]<<endl;
        

}

