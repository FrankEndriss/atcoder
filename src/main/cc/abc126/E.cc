/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

class DSU {
public:
    vector<int> p;
/* initializes each (0..size-1) to single set */
    DSU(int size) {
        p.resize(size);
        for(int i=0; i<size; i++)
            p[i]=i;
    }

/* finds set representative for member v */
    int find_set(int v) {
        if (v == p[v])
            return v;
        return p[v] = find_set(p[v]);
    }

/* makes v a single set, removes it from other set if contained there. */
    void make_set(int v) {
        p[v] = v;
    }

/* combines the sets of two members.
 *  * Use the bigger set as a for optimized performance.  */
    void union_sets(int a, int b) {
        a = find_set(a);
        b = find_set(b);
        if (a != b)
            p[b] = a;
    }
};

int main() {
    int n, m;
    cin>>n>>m;
    DSU dsu(n+1);
    fori(m) {
        int x, y, z;
        cin>>x>>y>>z;
        dsu.union_sets(x, y);
    }

    set<int> c;
    for(int i=1; i<=n; i++)
        c.insert(dsu.find_set(i));

    cout<<c.size()<<endl;
}

