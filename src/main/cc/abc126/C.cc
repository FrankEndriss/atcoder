/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

int main() {
int n, k;
    cin>>n>>k;
    
    double ans=0.0;
    for(int i=1; i<=n; i++) {
        /* add prob of winning if dice shows i. */
        double p=1.0;
        int x=i;
        while(x<k) {
            p/=2;
            x*=2;
        }
        ans+=p;
    }
    ans/=n;
    cout<<setprecision(12)<<ans<<endl;

}

