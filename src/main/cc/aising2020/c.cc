/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* fix x and y, find numbers z
 */
const int N=10000;
void solve() {
    cini(n);

    for(int nn=1; nn<=n; nn++) {
        int ans=0;
        for(int x=1; x*x+x<nn; x++) {
            for(int y=x; x*x+y*y<nn; y++) {
                int nnn=x*x+y*y+x*y;

                nnn=nn-nnn;
                for(int z=y; z*z<nnn; z++) {
                    if(z*z+z*x+z*y==nnn) {
                        if(x==y && y==z) {
                            ans++;
                        } else if(x==y || y==z || x==z) {
                            ans+=3;
                        } else {
                            ans+=6;
                        }
                    }
                }
            }
        }
        cout<<ans<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
