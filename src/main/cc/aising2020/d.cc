/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

int mul(const int v1, const int v2, const int mod) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

int toPower(int a, int p, int mod) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}


int bcnt(int i) {
    return  __builtin_popcount(i);
}

/* implement binary division?
 * Is there any correlation between popcount and remainder?
 * -> dont think so.
 *
 * Note that after the first divivsion the number is in range of 1e5
 * and everything is fast.
 *
 * With flipping bits we have for first step max two mods,
 * cnt+1 and cnt-1
 * The remainder for both is sum of remainders of single bits.
 * Single bit remainders can be calculated with pow()
 *
 * So, for every bit we take the remainder of all and subtract
 * the remainder of the current bit.
 * And go on with that.
 */
void solve() {
    cini(n);
    cins(s);
    int cnt=0;
    for(int i=0; i<n; i++) 
        if(s[i]=='1')
            cnt++;

    if(cnt==0) {
        for(int i=0; i<n; i++) 
            cout<<1<<endl;
        return;
    }

    int sum1=0;
    int sum0=0;
    for(int i=0; i<n; i++) {
        if(s[i]=='1') {
            sum0=pl(sum0, toPower(2, n-1-i, cnt+1), cnt+1);
            if(cnt>1) {
                sum1=pl(sum1, toPower(2, n-1-i, cnt-1), cnt-1);
            }
        }
    }

    for(int i=0; i<n; i++) {
        if(s[i]=='1') {
            if(cnt==1) {
                cout<<0<<endl;
                continue;
            }

            int lmod=pl(sum1, -toPower(2, n-1-i, cnt-1), cnt-1);
            while(lmod<0)
                lmod+=cnt-1;
            int ans=1;
            while(lmod) {
                ans++;
                lmod=lmod%bcnt(lmod);
            }
            cout<<ans<<endl;
        } else {
            int lmod=pl(sum0, toPower(2, n-1-i, cnt+1), cnt+1);
            while(lmod<0)
                lmod+=cnt+1;
            int ans=1;
            while(lmod) {
                ans++;
                lmod=lmod%bcnt(lmod);
            }
            cout<<ans<<endl;
        }
    }

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
