/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* we need to find the best permutation of camels.
 * Each camel wants to be on one end of the line.
 *
 * We would like to 'bubble up' the camels, that
 * is we move them to the better position if this
 * makes the list of all camels better.
 * Once we cannot make the list better we are done.
 *
 * First we sort them by leftmostness.
 * Then try to find foreach camel which is not placed perfect
 * a counterpart, ans switch them.
 *
 * We try to switch camels with the worst happinessvalue first,
 * if it changes at least one camels value to be better.
 * To do this efficient we need to find among the 
 * camels in the positions the unluckiest one, and switch with 
 * that one.
 * -> Segment tree
 */
void solve() {
    cini(n);
    vi k(n);
    vi l(n);
    vi r(n);
    for(int i=0; i<n; i++) {
        cin>>k[i]>>l[i]>>r[i];
        k[i]--;
    }

    vi id(n);
    iota(all(id), 0);
    sort(all(id), [&](int i1, int i2) {
        return l[i1]-r[i1]<l[i2]-r[i2];
    });

    set<int> ha; /* happiness value of camel. ha[i]= value which would be subtracted from list if moved to other half */

    for(int i=0; i<n; i++) {
        if(k[id[i]]>i)
            ha.insert(r[id[i]]-l[id[i]]);
        else
            ha.insert(l[id[i]]-r[id[i]]);
    }
    
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
