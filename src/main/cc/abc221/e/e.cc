/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
#include <atcoder/lazysegtree>

using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using mint=modint998244353;

using S=mint; /* type of values */
using F=mint; /* type of upates */

S st_op(S a, S b) {
    return a+b;
}

/* This is the neutral element */
S st_e() {
    return 0;
}

/* This applies an update to some value. */
S st_mapping(F f, S x) {
    return x+f;
}

/* This combines two updates.  */
F st_composition(F f, F g) {
    return f+g;
}

/* This is the neutral update. 
 * It is similar to st_e() in some sense.  */
F st_id() {
    return 0;
}

using stree=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;

/**
 * dp[i]=sum(dp[j=0..i-1] with a[j]<=a[i] 
 * Compress a[] and use a segtree to build that sum.
 *
 * NO...everyting otherwise.
 * a[1]<=a[k], not a[i]<=a[j]
 *
 * So we search number of pairs with i<j && a[i]<=a[j]
 * And each pair contributes the number of permutations of 
 * the in-between-elements plus one.
 * ans+= p((j-i-1)!)+1
 *
 * Iterate
 * Consider the elements iterated by non decreasing a[i]
 * ********************************
 * So, first thing: it is _not_ the number of permutations, 
 * but 2^(j-i-1)+1
 * So, going from left to right we need to consider all seen indexes
 * with a[i]<=a[j].
 * Foreach one the contribution is
 * 2^(j-1) / 2^i
 * So store the sum of 2^i in a segment tree, and query at each
 * position.
 */
void solve() {
    cini(n);
    cinai(a,n);

    // TODO

}

signed main() {
    solve();
}
