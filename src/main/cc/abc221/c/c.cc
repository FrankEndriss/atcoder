/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Brute force all combinations.
 * ...not possible, 10^9
 *
 * Given a subset of digits, it is allways optimal to 
 * put the biggest one in more significant positions.
 *
 * Is it allways optimal to use the biggest digit allone, 
 * and all others create the biggest possible number?
 * No, see last example.
 *
 * It is 10 digits, so 2^10 possible combinations, not 10^9 :/
 * fuck, only one minute left :/
 * ...implemented after contest.
 */
void solve() {
    cini(n);
    vi d;
    while(n) {
        d.push_back(n%10);
        n/=10;
    }
    sort(all(d), greater<int>());
    const int N=d.size();

    int ans=0;
    for(int i=0; i<(1<<N); i++) {
        vvi dd(2);  /* two sets of digit indexes */
        for(int j=0; j<N; j++) {
            if(i&(1<<j)) {
                dd[0].push_back(j);
            } else {
                dd[1].push_back(j);
            }
        }

        if(dd[0].size()==0 || d[dd[0][0]]==0 || dd[1].size()==0 || d[dd[1][0]]==0)
            continue;

        function<int(vi&)> conf=[&](vi &v) {
            int lans=0;
            for(size_t j=0; j<v.size(); j++) {
                lans*=10;
                lans+=d[v[j]];
            }
            return lans;
        };

        ans=max(ans, conf(dd[0])*conf(dd[1]));
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}
