/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Event loop.
 * On each day change, add the number of days since last
 * change to the bucket cnt.
 */
const int N=1e5+7;
void solve() {
    cini(n);
    vector<pii> evt;

    for(int i=0; i<n; i++) {
        cini(a);
        cini(b);
        evt.emplace_back(a-1, 1);
        evt.emplace_back(a+b-1, -1);
    }
    sort(all(evt));

    vi ans(n+1);
    int cnt=0;
    int prev=0;
    for(size_t i=0; i<evt.size(); i++) {
        if(evt[i].first!=prev) {
            ans[cnt]+=evt[i].first-prev;
        }
        cnt+=evt[i].second;
        prev=evt[i].first;
    }
    ans[cnt]+= evt.back().first-prev;

    for(int i=1; i<=n; i++) 
        cout<<ans[i]<<" ";
    cout<<endl;
}

signed main() {
    solve();
}
