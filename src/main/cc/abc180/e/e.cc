
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

#define endl "\n"

/*
 * We need to find the cheapest
 * Assume all distances are known (FloydWarshal).
 * ...
 * Is it that we just need to sort by z?
 *
 * What about solving for the first two dimension, how?
 *
 * Two cities have a dist of 
 * max(
 * x1-x2+y1-y2,
 * x1-x2+y2-y1,
 * x2-x1+y1-y2,
 * x2-x1+y2-y1)
 *
 * Can we simply choose the least distant city as next?
 * idk :/
 *
 * We can do a dp on all subsets of the 16 cities.
 */
constexpr int INF=1e9;
void solve() {
    cini(n);

    vvi p(n, vi(3));
    for(int i=0; i<n; i++) 
        cin>>p[i][0]>>p[i][1]>>p[i][2];


    /* @return dist from v1 to v2 */
    const function<int(int,int)> dist=[&](int v1, int v2) {
        return abs(p[v1][0]-p[v2][0])+abs(p[v1][1]-p[v2][1])+max(0LL,p[v2][2]-p[v1][2]);
    };

    vvvi dp(n, vvi(n, vi(1<<(n+1), -1)));

    /* @return min dist to start at first go throu all s and end in last */
    function<int(int,int,int)> go=[&](int first, int last, int s) {
        if(s==0)
            return dist(first,last);

        if(dp[first][last][s]>=0)
            return dp[first][last][s];

        int ans=INF;
        for(int i=1; i<n; i++) {
            if(s&(1<<i)) {
                ans=min(ans, dist(first, i)+go(i, last, s^(1<<i)));
            }
        }

        return dp[first][last][s]=ans;
    };

    int all=0;
    for(int i=1; i<n; i++) 
        all|=(1<<i);
    int ans=go(0, 0, all);

    cout<<ans<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
