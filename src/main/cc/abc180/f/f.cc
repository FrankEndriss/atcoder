
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

constexpr int MOD=1e9+7;




const int MAXN = 301;
ll C[MAXN + 1][MAXN + 1];

const bool init_nCr=[]() {
    C[0][0] = 1;
    for (int n = 1; n <= MAXN; ++n) {
        C[n][0] = C[n][n] = 1;
        for (int k = 1; k < n; ++k)
            C[n][k] = (C[n - 1][k - 1] + C[n - 1][k])%MOD;
    }
    return true;
}();

ll nCr(int n, int k) {
    return C[n][k];
}

/* 
 * First we need exactly one component of size L, we use (at least)
 * L-1 edges for this.
 * Then we can create more components, each of max size L,
 * but smaller is ok, too.
 *
 * So first part is some formular, second part is a dp.
 */
const int N=301;
void solve() {
    cini(n);
    cini(m);
    cini(l);

    vvvi dp(N, vvi(N, vi(N,-1)));
    dp[1][0]=1;
    /* @return posis to build a component from nn different vertex 
     * using exactly use vertex and exactly mm edges. 
     *
     * ... that is complecated :/
     * */
    function<int(int,int,int)> go=[](int nn, int use, int mm) {
        if(mm<use-1)
            return 0;
        if(dp[nn][use][mm]>=0)
            return dp[nn][use][mm];

        int v=nCr(nn, use);
        // ... TODO
    };

    int ans=42;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
