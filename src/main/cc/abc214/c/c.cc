
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Each crea i receives a gem at
 * min(t[i], t[i-1]+s[i-1], t[i-2]+s[i-2]+s[i-1], ...)
 *
 * So start at crea i and calc the min time for all other
 * crea until the first earlier is hit. Then the same for second ...and so on. 
 * Does this work? Looks like O(n^2)
 */
const int INF=1e18;
void solve() {
    cini(n);
    cinai(s,n);
    cinai(dp,n);

    vi id(n);
    iota(all(id), 0LL);
    sort(all(id), [&](int i1, int i2) {
            return dp[i1]<dp[i2];
    });

    for(int ii=0; ii<n; ii++) {
        const int i=id[ii];

        for(int j=i+1; j<i+n; j++) {
            if(dp[j%n]<=dp[(n+j-1)%n]+s[(n+j-1)%n])
                break;
            else
                dp[j%n]=dp[(n+j-1)%n]+s[(n+j-1)%n];
        }
    }

    for(int i=0; i<n; i++) 
        cout<<dp[i]<<endl;

}

signed main() {
    solve();
}
