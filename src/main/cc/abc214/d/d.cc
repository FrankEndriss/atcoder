
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/dsu>
using namespace atcoder;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/** Each edge contributes for all pairs where it is
 * the biggest one.
 * So for all edges we count the number of reacheable vertex
 * on both ends with edges of lower weight.
 * ans+=n1*n2
 * But that seems O(n^2)
 *
 * And what about edges of same weight? 
 * -> Just ignore, traverse smaller weights.
 *
 * ***
 * Can we add edges sorted by weight, and foreach one
 * find the size of the components that edge connects?
 * But here we need to consider edges of same weight :/
 * ...No, we need not!
 */
void solve() {
    cini(n);

    using t3=tuple<int,int,int>;
    vector<t3> edg(n);
    for(int i=0; i<n-1; i++) {
        cini(u);
        u--;
        cini(v);
        v--;
        cini(w);

        edg[i]={w,u,v};
    }
    sort(all(edg));

    dsu d(n);

    int ans=0;
    for(int i=0; i<n; i++) {
        auto [w,u,v]=edg[i];
        int cnt1=d.size(u);
        int cnt2=d.size(v);
        ans+=w*cnt1*cnt2;
        d.merge(u,v);
    }

    cout<<ans<<endl;

}

signed main() {
    solve();
}
