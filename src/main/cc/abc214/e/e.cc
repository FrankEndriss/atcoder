
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/lazysegtree>
using namespace atcoder;


const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"



using S=int; /* type of values */
using F=int; /* type of upates */

S st_op(S a, S b) {
    return max(a,b);
}

/* This is the neutral element */
S st_e() {
    return 1;
}

/* This applies an update to some value. */
S st_mapping(F f, S x) {
    return max(f,x);
}

/* This combines two updates.  */
F st_composition(F f, F g) {
    return max(f,g);
}

/* This is the neutral update. 
 * It is similar to st_e() in some sense.  */
F st_id() {
    return 1;
}

using stree=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;

/** 
 * We can sort ranges by r[i]/l[i]
 * and then put the ball in the smallest box.
 * But we need to maintain a list of free[i]="next box free".
 *
 * How can we handle the 10^9 boxes?
 *
 * Use prev and next pointers, maintained by maps.
 *
 * ***
 * Sort by r[i], then place in first free box starting at l[i].
 * Worst thing can happen is that all l[i] are same,
 * and foreach one we have to linear search the next free box.
 * So, after having placed a ball, maintain the next free
 * if started at that l[i].
 * Maintain next list as lazy seg tree?
 * ...does not work since size 10^9 :/
 *
 * ***
 * What if l[i] increase by one on each interval.
 * Then we need to iterate each box...
 * So after having found the box for the current interval, we need to 
 * update the next pointer foreach box from l[i]..box both including.
 * How to do that without a lazy segtree?
 *
 * Can we maintain the "used interval" somehow?
 */
void solve() {
    cini(n);
    vector<pii> lr(n);
    for(int i=0; i<n; i++)
        cin>>lr[i].first>>lr[i].second;

    sort(all(lr), [&](pii p1, pii p2) {
            if(p1.second!=p2.second)
                return p1.second<p2.second;
            return p1.first<p2.first;
    });

    stree next((int)1e9);
    set<int> vis;
    bool ans=true;

    for(int i=0; ans && i<n; i++) {
        int box=lr[i].first;
        while(vis.count(box)>0) {
            box=max(next[box], box+1);
            if(box>lr[i].second) {
                ans=false;
                break;
            }
        }

        vis.insert(box);
        next[lr[i].first]=box+1;
    }

    if(ans)
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
