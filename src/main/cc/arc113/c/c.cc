
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* from first pair of chars we can change the whole
 * string, but only that positions with are not
 * allready that char.
 *
 * Then note that we can do that for the rightmost char-pair,
 * then for the second rightmost...etc.
 *
 * Note also that we apply operatios from right to 
 * left, so after applying the operation until the 
 * end of the string, the whole string is same char up to the end.
 */
void solve() {
    cins(s);
    vvi cnt(26, vi(s.size()+1));
    const int n=s.size();

    for(size_t i=0; i<s.size(); i++) {
        for(int j=0; j<26; j++)
            cnt[j][i+1]=cnt[j][i];
        cnt[s[i]-'a'][i+1]++;
    }

    char prev=' ';
    stack<pair<int,char>> pos;
    for(int i=1; i+1<s.size(); i++) {
        if(s[i]==s[i-1] && s[i]!=prev) {
            pos.push({i+1,s[i]-'a'});
            prev=s[i];
        }
    }


    int ans=0;
    int pp=s.size();
    while(pos.size()) {
        /* each pair contributes nuber of chars until end of string
         * minus number of same char up to next double char position.
         */
        auto [p,c]=pos.top();
        pos.pop();
        ans+=n-p-( cnt[c][pp]-cnt[c][p]);
        pp=p;
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}
