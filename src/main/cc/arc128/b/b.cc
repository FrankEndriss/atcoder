/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Each operation changes two balls.
 * So, the number of changed balls must be even,
 * so the sum of balls of two different colors must be even,
 * then we maybe can change them to the third color.
 *
 * So, we must make two colors have same number of balls.
 *
 * Consider the diff of number of balls of two colors.
 * If we change into c1, the afterwards
 * c[c1]+=2
 * c[c2]-=1
 * c[c3]-=1
 * So diff between to colors allways changes by 3
 *
 * So, if there are two colors with the freq diff diviseable by 3, 
 * and there are enough of third color, then there is a solution.
 *
 * 0 2 5
 * 2 1 4 
 * 1 3 3 -> no solution
 *
 * 3 2 5
 * 2 4 4 -> solution
 *
 * Some edgecase, countercase?
 *
 * 0 1 1000
 * 2 0 999
 * 1 2 998
 * 3 1 997
 * 2 3 996
 * ...no
 *
 * 1 1 7
 * 0 3 6
 * 2 2 5
 */
const int INF=1e9;
void solve() {
    cinai(c,3);
    sort(all(c));

    int ans=INF;

    do {
        if(c[0]==c[1])
            ans=min(ans, c[0]);

        int d=abs(c[0]-c[1]);
        if(d%3==0)
            ans=min(ans, max(c[0],c[1]));
    }while(next_permutation(all(c)));

    if(ans==INF)
        ans=-1;
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
