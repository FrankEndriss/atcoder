/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * let a[j] be the max value in a[], then let x[j]=M, and all other x[]=0
 * ans=a[j]*m
 * Why so simple?
 * No, missunderstood. s can (and will) be bigger than m.
 * So use x[k]=m on biggest a[j] as long as there is s available.
 * ...
 * We also need to consider x[1]<=x[2]<=x[3]...
 *
 * ****
 * if somewhere left there is an big a[i], we want to make that x[i]
 * as big as possible, so s/(n-i)
 * if somewhere right there is an big a[i], we want to make that x[i]
 * as big as possible, so x[i]=m
 * So we do the first as long as s/(n-i)<m, then do the second.
 *
 * *** 
 *
 * We need to consider putting x[k..n]=m, and put all other values
 * on any prefix x[i..k-1].
 *
 */
const ld EPS=0.00000000000001;
void solve() {
    cini(n);
    ld m;
    cin>>m;
    ld s;
    cin>>s;

    cinai(a,n);
    vi pre(n+1);
    for(int i=0; i<n; i++)
        pre[i+1]=pre[i]+a[i];

    // TODO

    cout<<ans<<endl;

}

signed main() {
    solve();
}
