/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Somewhat mathematical :/
 * So, on small a[i]-days we want to trade to get gold,
 * on big a[i]-days we want to trade to get silver.
 *
 * Three choices per day.
 * Consider starting with gold.
 * Last trade must be get-gold.
 * So we allways have to do two trades on days i,j, where the first a[i]<a[j],
 * else we should not do the trade.
 * So find max number of possible pairs i,j, and foreach one find the optimal
 * pair. Choose optimal pair by choosing the extreme points.
 *
 * Implementation problems...
 * How to simply implement this?
 * -compress repitions
 * -list max and min extremes
 * -iterate the extremes
 */
void solve() {
    cini(n);
    cinai(a,n);

    vector<pii> aa;
    for(int i=0; i<n; i++) {
        if(i==0 || a[i-1]!=a[i])
            aa.emplace_back(a[i], i);
    }

    vi mi;
    vi ma;

    for(int i=0; i<aa.size(); i++) {
        if((i==0 || aa[i].first<aa[i-1].first) && (i==aa.size()-1 || aa[i].first<aa[i+1].first))
            mi.push_back(aa[i].second);
        if((i==0 || aa[i].first>aa[i-1].first) && (i==aa.size()-1 || aa[i].first>aa[i+1].first))
            ma.push_back(aa[i].second);
    }

    assert(mi.size()>0);
    assert(ma.size()>0);

    reverse(all(mi));
    reverse(all(ma));

    while(mi.size() && ma.back()>=mi.back())
        mi.pop_back();

    vi ans(n);
    while(ma.size() && mi.size()) {
        ans[ma.back()]=1;
        ans[mi.back()]=1;
        ma.pop_back();
        mi.pop_back();
    }

    for(int i=0; i<n; i++)
        cout<<ans[i]<<" ";
    cout<<endl;
}

signed main() {
    solve();
}
