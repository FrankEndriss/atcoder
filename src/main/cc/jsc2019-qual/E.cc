/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

/*
#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")
*/

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<pii> vpii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(h);
    cini(w);
    vector<priority_queue<pii>> marow(h+1);
    vector<priority_queue<pii>> macol(w+1);

    for(int i=0; i<n; i++) {
        cini(r);
        cini(c);
        cini(a);

        marow[r].push({ a, c });
        macol[c].push({ a, r });
    }

    ll ans=0;
    for(int i=1; i<=h; i++) {
        if(marow[i].size()==0)
            continue;

        pii p1=marow[i].top();
        marow[i].pop();
        ans+=p1.first;

        pii p2=macol[p1.second].top();
        assert(p2.first>=p1.first);
        if(p2.first==p1.first)
            macol[p1.second].pop();
    }

    for(int i=1; i<w; i++) {
        if(macol[i].size()==0)
            continue;
        pii p2=macol[i].top();
        macol[i].pop();
        ans+=p2.first;
    }

    cout<<ans<<endl;

}

