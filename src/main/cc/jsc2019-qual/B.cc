/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

/*
#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")
*/

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
//#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MOD=1e9+7;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cinll(k);
    cinai(a, n);

    vll dp1(n);  // dp1[i]== cnt of a[j] with a[i]>a[j]
    vll dp2(n);  // dp2[i]== cnt of a[j] with a[i]>a[j] and i<j
    for(int i=0; i<n; i++) {
        for(int j=0; j<n; j++) {
            if(a[i]>a[j]) {
                dp1[i]++;
                if(i<j)
                    dp2[i]++;
            }
        }
    }

    ll ans=0;
    for(int i=0; i<n; i++) {
        ans=ans+dp2[i]+ (dp1[i]*(k-1));
        ans%=MOD;
    }
    cout<<ans<<endl;
}

