/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* works like next_permutation(), but on a permutation like seq in p, with max value i.
 * usually i != p.size(); 
 * Usefull to create all subsets of a given size of a set of values.
 * vi data(n);  // some data...
 * vi idx(m);   // subset of size m; m<=n;
 * iota(idx.begin(), idx.end(), 0);
 * do {
 *      ...something using data readonly...
 * } while(next(idx, n-1));
 *
 * example for m=3, n=4:
 * 0 1 2 
 * 0 1 3 
 * 0 2 3 
 * 1 2 3
 **/
bool next(vi &p, int i) {
    const int k=(int)p.size();

    if(p[k-1]<i) {
        p[k-1]++;
        return true;
    }
    if(k==1)
        return false;

    int incidx=k-2;
    while(incidx>=0 && p[incidx]==p[incidx+1]-1)
        incidx--;

    if(incidx<0)
        return false;

    p[incidx]++;
    for(int j=incidx+1; j<k; j++)
        p[j]=p[j-1]+1;

    return true;
}

/**
 * Consider b[0][0]...
 * How?
 *
 * Iterate rows in A, consider each row to be 
 * the match for first row in B.
 * That defines a subset of possible col-matches.
 * Brute-force try all those col-matches on the 
 * remaining rows greedily.
 */
void solve() {
    cini(h1);
    cini(w1);
    vvi a(h1, vi(w1));
    for(int i=0; i<h1; i++) 
        for(int j=0; j<w1; j++) 
            cin>>a[i][j];

    cini(h2);
    cini(w2);
    vvi b(h2, vi(w2));
    for(int i=0; i<h2; i++) 
        for(int j=0; j<w2; j++) 
            cin>>b[i][j];

    if(h2>h1 || w2>w1) {
        cout<<"No"<<endl;
        return;
    }

    /* return the column matches of the rows r1 in a and r2 in b */
    function<vvi(int,int)> rowMatches=[&](int r1, int r2) {
        vvi ans;
        vi p(w2);
        iota(all(p), 0LL);
        do {
            bool ok=true;
            for(int i=0; ok && i<w2; i++) 
                if(a[r1][p[i]]!=b[r2][i])
                    ok=false;

            if(ok)
                ans.push_back(p);

        } while(next(p, w1-1));
        return ans;
    };

    for(int i=0; i+h2<=h1; i++) {   /* all possible first rows in a[][] */
        vvi cm=rowMatches(i, 0);

        for(size_t j=0; j<cm.size(); j++) {
            int idxB=1;
            for(int k=1; idxB<h2 && k<h1; k++) {
                bool ok=true;
                for(int k2=0; ok && k2<w2; k2++) 
                    if(a[k][cm[j][k2]]!=b[idxB][k2])
                        ok=false;
                if(ok)
                    idxB++;

                if(idxB==h2)
                    break;
            }
            if(idxB==h2) {
                cout<<"Yes"<<endl;
                return;
            }
        }
    }
    cout<<"No"<<endl;

}

signed main() {
       solve();
}
