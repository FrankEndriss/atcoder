/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/dsu>
using namespace atcoder;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Kind of reverse dsu?
 *
 * I.e. we add all edges, but not the removed ones,
 * then check dsu the number of connected cities.
 *
 * Then add one after the other each removed edge, 
 * and check the number of newly electrified cities.
 */
void solve() {
    cini(n);
    cini(m);
    cini(e);

    vector<pii> edg(e);
    for(int i=0; i<e; i++)  {
        cin>>edg[i].first>>edg[i].second;
        edg[i].first--;
        edg[i].second--;
    }
    cini(q);
    vi x(q);
    vb vis(e);
    for(int i=0; i<q; i++) {
        cin>>x[i];
        x[i]--;
        vis[x[i]]=true;
    }

    cerr<<"did parse all "<<endl;

    dsu d(n+m);
    for(int i=0; i<e; i++) 
        if(!vis[i])
            d.merge(edg[i].first,edg[i].second);

    vvi g=d.groups();
    /* check for all groups the number of cities, and if it has power */
    vi cnt(n+m);
    vb hasPower(n+m);
    vi ans(1);
    for(size_t i=0; i<g.size(); i++) {
        for(size_t j=0; j<g[i].size(); j++) {
            if(g[i][j]>=n)
                hasPower[d.leader(g[i][0])]=true;
            else {
                cnt[d.leader(g[i][0])]++;
            }
        }
        if(!hasPower[d.leader(g[i][0])])
            ans[0]+=cnt[d.leader(g[i][0])];
    }

    for(int i=q-1; i>0; i--) {  /* no need to replay the first event */
        int l1=d.leader(edg[x[i]].first);
        int l2=d.leader(edg[x[i]].second);
        if(l1!=l2) {
            d.merge(edg[x[i]].first, edg[x[i]].second);
            int lnew=d.leader(edg[x[i]].first);
            if(lnew==l1) {
                if(hasPower[l1]&&!hasPower[l2]) {
                    ans.push_back(ans.back()-cnt[l2]);
                    hasPower[l2]=true;
                } else if(!hasPower[l1]&&hasPower[l2]) {
                    ans.push_back(ans.back()-cnt[l1]);
                    hasPower[l1]=true;
                } else
                    ans.push_back(ans.back());

                cnt[l1]+=cnt[l2];

            } else if(lnew==l2) {
                if(hasPower[l1]&&!hasPower[l2]) {
                    ans.push_back(ans.back()-cnt[l2]);
                    hasPower[l2]=true;
                } else if(!hasPower[l1]&&hasPower[l2]) {
                    ans.push_back(ans.back()-cnt[l1]);
                    hasPower[l1]=true;
                } else
                    ans.push_back(ans.back());

                cnt[l2]+=cnt[l1];
            }
       } else 
           ans.push_back(ans.back());
    }

    while(ans.size()) {
        cout<<n-ans.back()<<endl;
        ans.pop_back();
    }
}

signed main() {
       solve();
}
