
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * On each ')' we need to find the opening '('
 */
void solve() {
    cins(s);

    stack<int> st;
    set<pair<char,int>> box;
    vi lpos(26, -1);
    for(size_t i=0; i<s.size(); i++) {
        if(s[i]=='(')
            st.push(i);
        else if(s[i]==')') {
            int pos=st.top();
            st.pop();
            for(auto it=box.begin(); it!=box.end(); ) {
                if(it->second>=pos)
                    it=box.erase(it);
                else
                    it++;

                if(it==box.end())
                    break;
            }
        } else {
            assert(s[i]>='a' && s[i]<='z');
            auto it=box.lower_bound({s[i], -1});
            if(it!=box.end() && it->first==s[i]) {
                cout<<"No"<<endl;
                return;
            }
            box.emplace(s[i], i);
        }
    }

    cout<<"Yes"<<endl;
}

signed main() {
        solve();
}
