
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>

/*
#include <ios>
#include <iostream>
#include <iomanip>
#include <functional>
#include <set>
#include <vector>
#include <cmath>
*/
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to create a configuration where
 * no cell is isolated, ie there exists one whit same color next to it.
 *
 * So there are pairs of adj rows that have to have the same parity.
 * Just make/check them same parity.
 *
 *
 * While iterating rows, there are rows that
 * -have no need to be connected
 * -can be flipped
 * -must be flipped
 * -can be connected to the previous
 * -can not be connected to the previous
 *
 * dp[i][flip][ok] = min number of ops to make ith row flipped/notflipped and
 *  ok without isolation or with some isolation that must be resolved
 *  with next row.
 */
void solve() {
    cini(h);
    cini(w);

    vvi a(h+1, vi(w+2, 2));
    for(int i=1; i<=h; i++)
        for(int j=1; j<=w; j++)
            cin>>a[i][j];

    /* return ok if the row can live allone */
    /*
    TODO fix offset
    function<bool(int)> solitair=[&](int i) {
        cerr<<"solitair i="<<i<<endl;
        if(a[i][0]!=a[i][1])
            return false;

        if(a[i][w-1]!=a[i][w-2])
            return false;

        for(int j=1; j+1<w; j++)
            if(a[i][j]!=a[i][j+1] && a[i][j-1]!=a[i][j])
                return false;

        return true;
    };
    */

    /* { true if notflipped i is valid
     *   true if flipped i is valid
     *   true if notflipped k is valid
     *   true if flipped k is valid
     * }
     *
     * */
    function<vb>(int,int)> go=[&](int i, int k) {
        set<int> flipI; /* needed flips to make row i ok */
        set<int> flipK; /* needed flips to make row k ok */

        /* all other pos */
        for(int j=1; flip.size()<2 && j<=w; j++) {
            if(a[k][j-1]!=a[k][j] && a[k][j+1]!=a[k][j]) {
                /* a[k][j] is isolated */
                if(a[i][j]!=a[k][j])
                    flipK.insert(1);
                else
                    flipK.insert(0);
            }

            if(a[i][j-1]!=a[i][j] && a[i][j+1]!=a[i][j]) {
                /* a[i][j] is isolated */
                if(a[i][j]!=a[k][j])
                    flipI.insert(1);
                else
                    flipI.insert(0);
            }
        }

        const vb ans= {
            flipI.count(1)==0, 
            flipI.count(0)==0,
            flipK.count(1)==0, 
            flipK.count(0)==0
        };
        return ans;
    };


    const int INF=1e9;
    vvvi dp(h, vvi(2, vi(2, INF)));
    bool ok0=solitair(0);
    if(ok0)  {
        dp[0][0][1]=0;
        dp[0][1][1]=1;
    } else {
        dp[0][0][0]=0;
        dp[0][1][0]=1;
    }

    for(int i=1; i<h; i++) {
        vb flip=go(i-1,i);

        dp[i][0][0]=min(dp[i-1][1][1], dp[i-1][0][1]);
        dp[i][1][0]=min(dp[i-1][1][1], dp[i-1][0][1])+1;

        if(vb[0]) { /* notflipped i-1 is valid */

        } else {
        }

        if(ok) {    /* can resolve with previous */
            if(flip!=1) {   /* by not flipping */
                dp[i][0][1]=min(dp[i][0][1], dp[i-1][0][0]);
                dp[i][0][1]=min(dp[i][0][1], dp[i-1][1][0]);

            } else if(flip==1) { /* by flipping */
                dp[i][1][1]=min(dp[i][1][1], dp[i-1][0][0]);
                dp[i][1][1]=min(dp[i][1][1], dp[i-1][1][0]);
            }
        } else {
            dp[i][0][0]=min(dp[i-1][0][1], dp[i-1][1][1]);
            dp[i][1][0]=min(dp[i-1][0][1], dp[i-1][1][1])+1;
        }
    }


    int ans=min(dp[h-1][0][1], dp[h-1][1][1]);

    if(ans==INF)
        ans=-1;

    cout<<ans<<endl;
}

signed main() {
    solve();
}
