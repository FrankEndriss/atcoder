
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* The order of the m triples does not make any difference.
 * N is small, consider bitsets
 *
 * At most z numbers <= y means at least n-z numbers > y
 *
 * Order the triples by x, consider the one with the smallest x.
 * So we can choose from nCr(n-y+1,n-z) sets of these numbers,
 * and we can place them in x positions, so mult by nCr(x, n-z)
 *
 * Then we can place all other numbers in the remaining n-x positions,
 * so mult by nCr(n-x, z);
 *
 * But how to combine list of rules?
 * It is somehow inclusion/exclusion.
 * All perms are fac(n);
 *
 * Consider z+1..min(y,x) numbers <=y in x positions.
 * Foreach (zz=z+1,2,3..) it is nCr(y,zz) different sets of numbers,
 * and we put them in nCr(x,zz) positions
 *
 */
void solve() {
    cini(n);
    cini(m);

    for(int i=0; i<m; i++) {
        cini(x);
        cini(y);
        cini(z);
    }
}

signed main() {
    solve();
}
