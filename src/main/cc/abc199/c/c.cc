
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* ...idk :/
 * Op 1 can be executed directly, if possible.
 * Op 2 reverses itself on second execution.
 */
void solve() {
    cini(n);
    cins(s);    /* s.size()=2*n */
    cini(q);

    bool flip=false;
    for(int i=0; i<q; i++) {
        cini(t);
        cini(a);
        cini(b);
        a--;
        b--;


        if(t==1) {
            if(flip) {
                a=(n+a)%(2*n);
                b=(n+b)%(2*n);
            }
            swap(s[a], s[b]);
        } if(t==2) {
            flip=!flip;
        }
    }

    if(flip)
        for(int i=0; i<n; i++) {
            swap(s[i], s[i+n]);
        }

    cout<<s<<endl;

}

signed main() {
    solve();
}
