/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * complecated statement text :/
 * We need to row/col of the 4 corners of the '#'.
 */
void solve() {
    cinas(s,10);
    int a=-1,b=-1,c=-1,d=-1;

    for(int i=0; i<10; i++) {
        for(int j=0; j<10; j++) {
            if(a<0 && s[i][j]=='#')
                a=i;

            if(s[i][j]=='#')
                b=i;

            if(c<0 && s[i][j]=='#')
                c=j;

            if(s[i][j]=='#')
                d=j;
        }
    }

    cout<<a+1<<" "<<b+1<<endl;
    cout<<c+1<<" "<<d+1<<endl;
}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
