/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Knapsack
 * Initially all cards are showing a.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);

    pii pp={INF,INF};
    vector<pii> dp(m+1, pp); /* dp[i].first=smallest index in val[] to reach i,
                            dp[i].second=min num of steps */

    vi val(n);
    int sum=0;

    for(int i=0; i<n; i++) {
        cini(a);
        cini(b);
        val[i]=b-a;
        sum+=a;
    }
    sort(all(val));

    dp[sum].first=0;
    dp[sum].second=0;
    queue<int> q;
    q.emplace(sum);
    while(q.size()) {
        int v=q.front();
        q.pop();

        for(int i=dp[v].first; i<n; i++) {
            if(dp[v+val[i]].second==INF) {
                dp[v+val[i]].second=dp[v].second+1;
                dp[v+val[i]].first=i+1;
                q.push(v+val[i]);
            }
        }
    }

    for(int i=0; i<=m; i++) {
        if(dp[i].second==INF)
            cout<<-1<<endl;
        else
            cout<<dp[i].second<<endl;
    }

}

signed main() {
       solve();
}
