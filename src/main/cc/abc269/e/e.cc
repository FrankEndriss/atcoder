/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/**
 * What that problem asks for?
 * -> somehting with rooks...
 *
 * Ok, one rook is not placed, and we want to find row/col
 * to place it.
 * So somehow binaray search the free row, the same for col.
 * How binsearch?
 * Ask upper half of remaining seg, continue on upper or lower half.
 *
 * Why again such unclear statement? What does the judge answer to
 * the queries, where in the statement is any information about that?
 * sic!
 *
 * Dont get it, shit trivial problem in complected format, annoying!
 */

/*
int tx=17;
int ty=139;

int q(int a, int b, int c, int d) {
    assert(b>=a);
    assert(d>=c);

    int ab=b-a+1;
    if(a<=tx && b>=tx)
        ab--;

    int cd=d-c+1;
    if(c<=ty && d>=ty)
        cd--;

    return min(ab,cd);
}
*/

int q(int a, int b, int c, int d) {
    cout<<"? "<<a+1<<" "<<b+1<<" "<<c+1<<" "<<d+1<<endl;
    cini(ans);
    return ans;
}

void solve() {
    cini(n);

    int l=0, r=n; /* free row is in seg 0,r-1 */
    while(l+1<r) {
        int sz=(r-l)/2;
        int a=q(l, l+sz-1, 0, n-1);
        if(a==sz) {
            l+=sz;
        } else {
            r=l+sz;
        }
    }
    int ansx=l;

    l=0; r=n; /* free col is in seg 0,r-1 */
    while(l+1<r) {
        int sz=(r-l)/2;
        int a=q(0, n-1, l, l+sz-1);
        if(a==sz) {
            l+=sz;
        } else {
            r=l+sz;
        }
    }
    int ansy=l;

    cout<<"! "<<ansx+1<<" "<<ansy+1<<endl;

}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
