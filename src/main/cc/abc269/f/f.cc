/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Some off by one fun.
 * How to implement that right?
 *
 * find first odd row, and number of non-zero cols in that row.
 * add all other odd rows
 * find first even row, and number of non-zero cols in that row.
 * add all other even rows
 *
 * Note a[0][0] is set to 0
 * Note a[2][0] is set to 0
 * Note a[0][2] is set to 0
 * Note a[2][2] is set to 0
 * ....
 *
 * ...no fun to implement.
 */
void solve() {
    cini(n);
    cini(m);
    cini(q);

    for(int i=0; i<q; i++) {
        cini(a);    /* a,b segment of rows */
        cini(b);
        cini(c);
        cini(d);
        a--;
        c--;

        int firstEvenRow=a;
        if(a%2)
            firstEvenRow++;
        if(firstEvenRow<=b) {
            int first
        }
    }

}

signed main() {
       solve();
}
