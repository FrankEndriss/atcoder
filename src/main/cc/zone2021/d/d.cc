
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Do not simulate the whole process.
 * Instead, check while add chars to the deque if
 * there are doubles, and instantly remove them
 * by not adding them at all.
 */
void solve() {
    cins(s);

    deque<char> t;
    bool rev=false;
    for(size_t i=0; i<s.size(); i++) {
        if(s[i]=='R')
            rev=!rev;
        else {
            if(rev) {
                if(t.size() && t[0]==s[i])
                    t.pop_front();
                else
                    t.push_front(s[i]);
            } else {
                if(t.size() && t.back()==s[i])
                    t.pop_back();
                else
                    t.push_back(s[i]);
            }
        }
    }

    if(rev)
        reverse(all(t));

    for(char c : t)
        cout<<c;
    cout<<endl;

}

signed main() {
    solve();
}
