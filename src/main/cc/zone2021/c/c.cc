
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * Maximize the min of the 5 powers.
 * Foreach 5 powers the one of the max person counts.
 * Binary search?
 *
 * Consider check value x:
 * We need to find 3 persons where each of the 5 values are
 * >=x in at least one of them.
 *
 * Bruteforce find a pair where 4 values are >=x
 * find any other person for the 5th value.
 */
void solve() {
    cini(n);

    vvi a(n, vi(5));
    for(int i=0; i<n; i++)  {
        for(int j=0; j<5; j++)
            cin>>a[i][j];
    }

    vvi id(5, vi(n));
    for(int i=0; i<5; i++) {
        iota(all(id[i]), 0);
        sort(all(id[i]), [&](int i1, int i2) {
            return a[i1][i]>a[i2][i];
        });
    }

    int l=0;
    int r=1e9+1;

    while(l+1<r) {
        const int mid=(l+r)/2;
        vb vis(1<<5);
        for(int i=0; i<n; i++) {
            int mask=0;
            for(int k=0; k<5; k++)
                if(a[i][k]>=mid)
                    mask|=(1<<k);
            vis[mask]=true;
        }

        bool ok=false;
        for(int i=0; i<(1<<5); i++) {
            if(!vis[i])
                continue;
            for(int j=i; j<(1<<5); j++) {
                if(!vis[j])
                    continue;
                for(int k=j; k<(1<<5); k++) {
                    if(!vis[k])
                        continue;
                    if((i|j|k)==(1<<5)-1)
                        ok=true;
                }
            }
        }
        if(ok)
            l=mid;
        else
            r=mid;
    }
    cout<<l<<endl;

}

signed main() {
    solve();
}
