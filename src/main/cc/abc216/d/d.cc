
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/** 
 * Order does not matter, we simply have to 
 * remove all possible pairs.
 * So, we need to track the positions of all colors, and remove the next
 * with 2 or more positions.
 * Wile removing, we need to add the new removable color positions.
 */
void solve() {
    cini(n);
    cini(m);
    vi k(m);
    vvi a(m);
    for(int i=0; i<m; i++) {
        cin>>k[i];
        for(int j=0; j<k[i]; j++) {
            cini(aux);
            aux--;
            a[i].push_back(aux);
        }
    }

    vector<set<int>> pos(n);   /* pos[i]=positions of balls of color-i */
    for(int i=0; i<m; i++) 
        pos[a[i].back()].insert(i);

    set<int> pp;  /* set of removable colors */

    int cnt=0;
    while(true) {
        if(pp.size()==0) {
            for(int i=0; i<n; i++) {
                if(pos[i].size()>1)
                    pp.insert(i);
            } 
        }
        if(pp.size()==0)
            break;

        while(pp.size()) {
            int col=*pp.begin();
            pp.erase(pp.begin());

            int i1=*pos[col].begin();
            pos[col].erase(pos[col].begin());
            int i2=*pos[col].begin();
            pos[col].erase(pos[col].begin());

            assert(i1!=i2);
            assert(a[i1].back()==col);
            assert(a[i2].back()==col);
            cnt++;
            a[i1].pop_back();
            a[i2].pop_back();

            if(a[i1].size()) {
                pos[a[i1].back()].insert(i1);
                if(pos[a[i1].back()].size()>1)
                    pp.insert(a[i1].back());
            }

            if(a[i2].size()) {
                pos[a[i2].back()].insert(i2);
                if(pos[a[i2].back()].size()>1)
                    pp.insert(a[i2].back());
            }
        }
    }

    if(cnt==n)
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;

}

signed main() {
    solve();
}
