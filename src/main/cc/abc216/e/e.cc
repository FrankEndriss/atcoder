
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider the attractions sorted.
 * It looks like bars of increasing height.
 * Then we remove a rect after the other from the top right.
 * This is at most n steps.
 */
void solve() {
    cini(n);
    cini(k);
    vi a(n+1);  /* one attraction with zero satisfaction */

    for(int i=1; i<=n; i++)
        cin>>a[i];

    sort(all(a));

    int idx=n;
    int ans=0;

    while(idx>0 && k>0) {
        while(idx>0 && a[idx]==a[idx-1])
            idx--;

        if(idx==0)
            break;

        const int w=n-idx+1;
        const int h=(a[idx]-a[idx-1]);

        if(k>=w*h) {
            /* n-idx attractions, with 
             * (n-idx)*a[idx] satisfaction on first ride,
             * (n-idx)*(a[idx]-1) satisfaction on second ride,
             * (n-idx)*(a[idx]-2) satisfaction on third ride...
             * ...
             * (n-idx)*(a[idx-1]) satisfaction on last ride...
             */
            ans+=w*a[idx]*h
                - w*((h*(h-1))/2);
            k-=w*h;
        } else {
            int sat=a[idx];
            while(k>0) {
                int cnt2=min(k,w);
                if(sat>0)
                    ans+=cnt2*sat;
                k-=cnt2;
                sat--;
            }
        }


        a[idx]=a[idx-1];
    }

    cout<<ans<<endl;

}

signed main() {
    solve();
}
