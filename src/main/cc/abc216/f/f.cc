
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider each a[i] as max a[i], the number of subsets...
 * can be calculated.
 * nCr(i+1, 1) + nCr(i+1, 2) + ... + nCr(i+1, i+1)
 * i.e the sum of the row in the pascal triangle,
 * r[i]=r[i-1]+i*2+1
 *
 * Foreach one we want to find the number of subsets of b[]
 * having sum<=a[i]
 * Consider the max b[j]<=a[i]
 * Let dpB[i]=number of seqs with sum <=i;
 * ...somehow :/
 */
using mint=modint998244353;
const int N=5003;
void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);

    sort(all(a));
    sort(all(b));

    vi dpB(N*(N+1)/2);
    dpB[b[0]]=1;
    int idx=1;
    for(size_t i=b[0]+1; i<dpB.size(); i++) {
        while(idx<n && b[idx]<=i)
            idx++;

        // ... TODO
    }
}

signed main() {
    solve();
}
