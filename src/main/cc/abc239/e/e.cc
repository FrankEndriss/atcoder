/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Problem seems to be heavy/light decomposition :/
 *
 * Can we solve it also with a segment tree?
 * We would provide a multiset per vertex.
 * We can comproess x[]
 *
 * Note that k<=20, so simply create dfs the list of 20 largest
 * numbers in each subtree.
 */
const int M=20;
void solve() {
    cini(n);
    cini(q);
    cinai(x,n);

    vvi adj(n);
    for(int i=0; i<n-1; i++) {
        cini(a); a--;
        cini(b); b--;
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    vector<multiset<int>> data(n);

    function<void(int,int)> dfs=[&](int v, int p) {
        data[v].insert(x[v]);
        for(int chl : adj[v]) {
            if(chl!=p) {
                dfs(chl,v);
                for(int val : data[chl]) 
                    data[v].insert(val);
                while(data[v].size()>M)
                    data[v].erase(data[v].begin());
            }
        }
    };

    dfs(0,-1);

    for(int i=0; i<q; i++) {
        cini(v); v--;
        cini(k); k--;
        //cerr<<"v="<<v<<" k="<<k<<endl;
        auto it=data[v].rbegin();
        while(k) {
            it++;
            k--;
        }
        cout<<*it<<endl;
    }

}

signed main() {
    solve();
}
