/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/dsu>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to check several conditions:
 * -Can each component be connected to some other
 *  component, case number of component >2
 *  to two other components. Maybe there is some component with all vertex 
 *  haveing degree d[i].
 * -The number of new edge plus existing edge match sum(d[])/2 ?
 * -All initial degree of vertex i <= d[i]
 *
 * If all yes, then first connect all components, then arbitrary vertex.
 *
 * **********
 * ...some implementation bugs :/
 * Its unclear if new highways are allowed to connect
 * a city to itself.
 *
 * ******
 * Ok. The iditotic defintion of how many new vertex have to be
 * build is choosen as it was, because the result after
 * the building phase is that there exist n-1 edges, which makes it a tree.
 *
 * This simplyfies the building, since only components have to 
 * be connected.
 */
void solve() {
    cini(n);
    cini(m);

    cinai(d,n);

    dsu ds(n);
    vvi adj(n);

    for(int i=0; i<m; i++) {
        cini(u); u--;
        cini(v); v--;

        ds.merge(u,v);
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    int dsum=0;
    for(int i=0; i<n; i++) {
        if(adj[i].size()>d[i]) {
            //cerr<<"-1 one"<<endl;
            cout<<-1<<endl;
            return;
        }
        dsum+=adj[i].size();
    }

    int acc=accumulate(all(d), 0LL);
    if(dsum+(n-m-1)*2 != acc) {
        //cerr<<"dsum="<<dsum<<" n="<<n<<" m="<<m<<" acc="<<acc<<" -1 two"<<endl;
        cout<<-1<<endl;
        return;
    }

    vvi g=ds.groups();
    vi cnt0;
    vi cnt1;
    vi cntX;

    for(size_t i=0; i<g.size(); i++) {
        int cnt=0;
        for(int v : g[i]) {
            if(adj[v].size()<d[v])
                cnt+=(d[v]-adj[v].size());
        }
        if(cnt==0)
            cnt0.push_back(i);
        else if(cnt==1) 
            cnt1.push_back(i);
        else
            cntX.push_back(i);
    }

    if(g.size()>1 && cnt0.size()>0) {
        //cerr<<"-1 three"<<endl;
        cout<<-1<<endl;
        return;
    }

    if(cnt1.size()>2) {
        //cerr<<"-1 four"<<endl;
        cout<<-1<<endl;
        return;
    }

    vector<pii> ans;
    /* connect cnt1-groups to two different cnt2-groups */
    if(g.size()>1 && cnt1.size()>0) {
        int v1=-1;
        for(int i : g[cnt1[0]]) {
            if(adj[i].size()<d[i]) {
                v1=i;
                cnt1.erase(cnt1.begin());
                break;
            }
        }
        int v2=-1;
        if(cntX.size()>0) {
            for(int i : g[cntX[0]]) {
                if(adj[i].size()<d[i]) {
                    v2=i;
                    break;
                }
            }
        } else {
            for(int i : g[cnt1[0]]) {
                if(adj[i].size()<d[i]) {
                    v2=i;
                    cnt1.erase(cnt1.begin());
                    break;
                }
            }
        }
        adj[v1].push_back(v2);
        adj[v2].push_back(v1);
        ans.emplace_back(v1,v2);
        ds.merge(v1,v2);
    }
    if(cnt1.size()>0) {
        int v1=-1;
        for(int i : g[cnt1[0]]) {
            if(adj[i].size()<d[i]) {
                v1=i;
                break;
            }
        }
        int v2=-1;
        for(int i : g[cntX.back()]) {
            if(adj[i].size()<d[i]) {
                v2=i;
                break;
            }
        }
        adj[v1].push_back(v2);
        adj[v2].push_back(v1);
        ans.emplace_back(v1,v2);
        ds.merge(v1,v2);
    }

    /* connect cntX groups to each other */
    g=ds.groups();
    for(size_t i=0; i+1<g.size(); i++) {
        int v1=-1;
        for(int v : g[i]) {
            if(adj[v].size()<d[v]) {
                v1=v;
                break;
            }
        }
        int v2=-1;
        for(int v : g[i+1]) {
            if(adj[v].size()<d[v]) {
                v2=v;
                break;
            }
        }

        adj[v1].push_back(v2);
        adj[v2].push_back(v1);
        ans.emplace_back(v1,v2);
    }

    /* connect all remaining vertex with to less edges */
    int v1=0;
    int v2=n-1;
    while(v1<n && v2>=0) {
        while(v1<n && adj[v1].size()==d[v1])
            v1++;

        while(v2>=0 && adj[v2].size()==d[v2])
            v2--;

        if(v2>=0 && v1<n && v2!=v1) {
            ans.emplace_back(v1,v2);
            adj[v1].push_back(v2);
            adj[v2].push_back(v1);
        } else 
            break;
    }

    if(ans.size()!=n-m-1) {
        cout<<-1<<endl;
        return;
    }

    for(size_t i=0; i<ans.size(); i++) 
        cout<<ans[i].first+1<<" "<<ans[i].second+1<<endl;

}

signed main() {
    solve();
}
