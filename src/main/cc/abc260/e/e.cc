/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/lazysegtree>
using namespace atcoder;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using S=int; /* type of values */
using F=int; /* type of upates */

S st_op(S a, S b) {
    return a+b;
}

/* This is the neutral element */
S st_e() {
    return 0LL;
}

/* This applies an update to some value. */
S st_mapping(F f, S x) {
    return f+x;
}

/* This combines two updates.  */
F st_composition(F f, F g) {
    return f+g;
}

/* This is the neutral update. 
 * It is similar to st_e() in some sense.  */
F st_id() {
    return 0LL;
}

using stree=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;

/**
 * Note that it is a _contigous_ subseq,
 * so it is no subseq but a _subarray_.
 *
 * Given left end of the s, all numbers must be bgeq s[0],
 * so the min len of that seq is determined by the max value of the ab pairs.
 * So we can update an interval of possible lengths by +1, lazy segtree.
 */
void solve() {
    cini(n);
    cini(m);

    stree ans(m+1);

    set<pii> ab;
    for(int i=0; i<n; i++) {
        cini(a);
        cini(b);
        ab.emplace(min(a,b), max(a,b));
    }

    for(int i=1; i<=m; i++) {   /* s[0]=i */
        bool done=false;
        while(true) {   /* move all smaller i to end */
            auto itL=ab.begin();
            if(itL->first<i) {
                if(itL->second<i) {
                    done=true;
                    break;
                } else {
                    pii pnew={itL->second,itL->first};
                    ab.erase(itL);
                    ab.insert(pnew);
                }
            } else 
                break;
        }

        if(done)
            break;

        auto itR=ab.rbegin();
        int left=itR->first-i+1;    /* min len of seq starting with i */
        int right=m-i+1;              /* max len of seq starting with i */
        if(left<=right) 
            ans.apply(left, right+1, 1);
    }

    for(int i=1; i<=m; i++)
        cout<<ans.get(i)<<" ";
    cout<<endl;
}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
