/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Am I stupid?
 *
 * Create all possible results in every step.
 * Check afterwards for best one.
 *
 * ...idk :/
 *
 */
void solve() {
    cini(n);
    cini(x);
    cini(y);

    /* <number of red on each level,number of blue on each level> */
    vector<pair<vi,vi>> dp(1); 
    for(int i=0; i<=n; i++)  {
        dp[0].first.push_back(0);
        dp[0].second.push_back(0);
    }
    dp[0].first[n]=1;

    int ans=0;
    for(int i=1; i<n; i++) {
        vector<pair<vi,vi>> dp0(); 

        for(int j=0; j<dp.size(); j++)  {
            pair<vi,vi> val={vi(n+1),vi(n+1)};
            for(int a=2; a<=n; a++)
                for(int b=2; b<=n; b++) {
                    val.first[a-1]
                }
        }

        dp.swap(dp0);
        for(int i=0; i<n; i++) 
            for(int j=0; j<dp.size(); j++) 
            ans=max(ans, dp[j][i][1]);
    }

    using t4=tuple<int,int,int,int>;
    queue<pii> q;
    q.emplace(n,n);
    while(q.size()) {
        auto [a,b]=q.front();
        q.pop();
        if(
        int v1=
    }

    for(int i=1; i<n; i++) {
        vector<pii> dp0;
        for(pii p : dp) {
            dp0.emplace_back(p.first
        }
    }
}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
