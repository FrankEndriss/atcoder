/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * something wrong :/
 */
void solve() {
    cini(n);
    cini(x);
    cini(y);
    cini(z);

    cerr<<"n="<<n<<" x="<<x<<" y="<<y<<" z="<<z<<endl;

    vector<pii> a(n);
    vector<pii> b(n);
    vector<pii> ab(n);

    for(int i=0; i<n; i++)  {
        cin>>a[i].first;
        cin>>b[i].first;
        ab[i].first=a[i].first+b[i].first;

        a[i].second=i;
        b[i].second=i;
        ab[i].second=i;

        a[i].first*=-1;
        b[i].first*=-1;
        ab[i].first*=-1;
    }

    sort(all(a));
    sort(all(b));
    sort(all(ab));

    vb vis(n);
    int cnt=0;
    for(int i=0; cnt<x && i<n; i++)
        if(!vis[a[i].second]) {
            cerr<<"x -> "<<a[i].second+1<<endl;
            vis[a[i].second]=true;
            cnt++;
        }

    cnt=0;
    for(int i=0; cnt<y && i<n; i++)
        if(!vis[b[i].second]) {
            cerr<<"y -> "<<b[i].second+1<<endl;
            vis[b[i].second]=true;
            cnt++;
        }

    cnt=0;
    for(int i=0; cnt<z && i<n; i++)
        if(!vis[ab[i].second]) {
            cerr<<"z -> "<<ab[i].second+1<<endl;
            vis[ab[i].second]=true;
            cnt++;
        }


    for(int i=0; i<n; i++)
        if(vis[i])
            cout<<i+1<<endl;
}


signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
    //cout<<"Case #"<<i<<": ";
    solve();
//   }
}
