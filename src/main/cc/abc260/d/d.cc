/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Understand the statement:
 *
 * If no card on the table, put the first (x) on the table.
 *
 * If there are cards on the table select a stack:
 * from all stacks with front card >x choose the first,
 * and put x on it.
 *
 * So, maintain a set of pair<int,vi> // front card, and all cards 
 */
void solve() {
    cini(n);
    cini(k);

    vvi data(n);    /* n stacks */
    int stid=0;

    set<pair<int,int>> dp;  /* front,stackid */
    vi ans(n,-1);
    for(int i=0; i<n; i++) {
        cini(p);

        vi e;
        auto it=dp.upper_bound(make_pair(p,0));
        if(it==dp.end()) {
            data[stid].push_back(p);
            stid++;
            if(k>1) 
                dp.insert(make_pair(p,stid-1));
            else
                ans[p-1]=i+1;
        } else {
            auto pp=make_pair(p, it->second);
            data[pp.second].push_back(p);
            dp.erase(it);
            if(data[pp.second].size()==k) {
                for(int c : data[pp.second]) 
                    ans[c-1]=i+1;
            } else {
                dp.insert(pp);
            }
        }
    }
    for(int i=0; i<n; i++) 
        cout<<ans[i]<<endl;
}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
