/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* x=0
 * Persons do not alternate, but move
 * like s says.
 *
 * So last person can apply the last block numbers to x
 * to make it winning. 
 * Let bsz=size of block. Then a block offers 2^bsz oportunities to act,
 * with up to 2^bsz outcomes.
 *
 * Last player is 0, so that ones win if x is one of its oportunities.
 * Pre-Last player is 1, wins if he can make x any value not in win list...
 * Winning and loosing positions. 
 * But, huge number of positions, how?
 *
 * Can we make some bit-dependencies, say if bit x[i] is set and bit x[j], 
 * then 0 looses/wins?
 *
 * Let win0[i] be the set<int> of values of x after move i which makes 0 win.
 * So, win0[n]={0}
 * win0[n-1]={0,a[n-1]}
 * win0[n-2]={0,a[n-1], a[n-2], a[n-1]^a[n-2]};
 * ...
 * But if s[n-i]=='1' it drastically changes, since all val not in win0 become
 * winning positions.
 * However, with 60-bit enumeration of winning positions does not work.
 */
void solve() {
    cini(n);
    cinai(a,n);
    cins(s);     // len n

    if(s.back()=='1') {
        cout<<1<<endl;
        return;
    }

    set<int> win;   /* winning positions for player 0 */

    int idx=n-1;
    while(idx>=0) {
        if(s[idx]=='0') {
            set<int> win1;
            for(int i : win)  {
                win1.insert(i);
                win1.insert(i^a[idx]);
            }
        }
    }

    
}

signed main() {
    cini(t);
    while(t--);
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
