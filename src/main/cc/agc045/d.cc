/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* There are circles in p[]
 * Snuke can switch on a circle if at least one lamp in the circle is on.
 * So we need to find the number of permutations where every circle has
 * at least one element in [1,a]
 *
 * let ans(x)=ans if a==x
 * let p(x)=number of permutations with exactly x circles
 * let c(x)=number of permutations of x elements forming one circle = x!
 *
 * let lans(x)=ans for a==x and exactly x circles
 * ans(1)==p(1)
 * ans(2)==ans(1)+lans(2)
 * lans(2)==
 * ...etc. Somewhat to complecated :/
 */
void solve() {
    cini(n);
    cini(a);
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
