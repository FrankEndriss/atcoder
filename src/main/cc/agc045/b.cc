/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* We want to make s like "101010..." -> ubal=1/-1
 * Let ubal(s)=pair of overcount 0/1
 *
 * example "??" -> "10" -> ubal=1
 * "11?" -> 110 -> ubal=2
 * 11?? -> 1100 -> ubal=2/-2
 * 11??? -> 11001 -> ubal=2
 *
 * let t=string
 * let x=0/1 1/0
 *
 * ubal(st)=ubal(s)+ubal(t)
 *
 * ubal(s+x)=max(ubal(postfix(s)) + x
 *
 * We need to minimize from both ends at once, somehow...because
 * "11????1111" is clearly "1100001111", but if done from left it would be
 * "110001.."
 *
 * So, simple do it once from left, once from right, and chooce optimal 
 * pair left/right.
 *
 * We need to create the array in a way that maximum subarray sum is minimized.
 * In absolute value.
 *
 * Let '1' count 1, and '0' count -1
 *
 * Can we assume for both dpma and dpmi the '?' as opposite values?
 * Then, setting '?' to '1' will increase dpma, and setting it do '0'
 * will increase dpmi, we would choose min of both... but the later
 * values will change, too. 
 * Let dpmarl[i] be the segment of the sum of dpma[i]. Then the change of '?'
 * to '1' at position j will affect all sums having j in segment.
 * And changing more '?' could connect two segments...
 */
void solve() {
    cins(s);

    vector<pii> dpma(s.size()); /* dpma[i] max sum of subarr ending in i */
    vector<pii> dpmi(s.size()); /* dpma[i] min sum of subarr ending in i */

/* What happens if we set first '?' to 1?
 *
 */

    if(s[0]=='1') {
        dpma=1;
        dpmi=0;
    } else {
        dpma=0;
        dpmi=-1;
    }

    for(int i=1; i<n; i++) {
        int val=0;
        if(s[i]=='0')
            val=-1;
        else if(s[i]=='1')
            val=1;

        dpma[i]=max(0LL, dpma[i-1]+val);
        dpmi[i]=min(0LL, dpmi[i-1]+val;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
