
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/lazysegtree>
using namespace atcoder;


const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


using S=int; /* type of values */
using F=int; /* type of upates */

S st_op(S a, S b) {
    return a;    /* can be ignored */
}

/* This is the neutral element */
S st_e() {
    return 0;
}

/* This applies an update to some value. */
S st_mapping(F f, S x) {
    return 2*f-x;
}

/* This combines two updates.  */
F st_composition(F f, F g) {
    return 2*f-2*g;
}

/* This is the neutral update. 
 * It is similar to st_e() in some sense.  */
F st_id() {
    return 0;
}

using stree=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;

/**
 * Both axes move independent.
 *
 * So, for a query at x, 
 * values i=0..x change to x-i
 * values i=x+1..H change to H-i+x+1
 * How to put this into a segment tree?
 *
 * We cannot apply absolute distance...
 * How to combine two such operations?
 *
 * Consider m to be the mirror point in 1D:
 * x1= m1+m1-x0
 * x2= m2+m2-x1
 * x2= m2+m2-m1-m1+x0
 * x3= m3+m3-x2
 *   = m3+m3-2*m2+2*m1+x0
 *
 * So just add 2*m and multiply by -1 on two segtrees.
 * ...which does not work somehow.. I guess the combination
 * of two operations is simply wrong :/
 *
 * How to combine two mirror operations?
 * I guess it is possible to define some matrix 
 * that expresses the operation, and can be compbine
 * (by dot product) of two such matrix.
 * I seems it is an "affine transformation", and would 
 * most likely be 2x2 matrix since it transforms in 1D space
 *
 * See https://en.wikipedia.org/wiki/Householder_transformation
 */
const int N=5e5+7;
void solve() {

    vi data(N);
    for(int i=0; i<N; i++) 
        data[i]=i*2;

    stree segX(data);
    stree segY(data);

    cini(H);
    cini(W);
    vs a(H);
    for(int i=0; i<H; i++)  {
        cin>>a[i];
        assert(a[i].size()==W);
    }

    cini(q);
    for(int i=0; i<q; i++) {
        cini(x); x--;
        cini(y); y--;
        cerr<<"x="<<x<<" y="<<y<<endl;

        segX.apply(0,x+1, x);
        segX.apply(x+1,H, x+H);
        segY.apply(0,y+1, y);
        segY.apply(y+1,W, y+W);
    }

    vs ans(H, string(W,'#'));
    for(int i=0; i<H; i++)  {
        int i0=segX.get(i);
        for(int j=0; j<W; j++) {
            int j0=segY.get(j);
            cerr<<"i="<<i<<" j="<<j<<" i0="<<i0<<" j0="<<j0<<endl;
        //    ans[i0/2][j0/2]=a[i][j];
        }
    }
    for(int i=0; i<H; i++)  {
            cout<<ans[i]<<endl;
    }
}

signed main() {
        solve();
}
