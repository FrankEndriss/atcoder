
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * + == blue
 * Taka first
 * Print "Takahashi" if first wins.
 * Players get points for + fields, and loose one for -
 *
 * At each position each player has one or two possible moves,
 * the one-moves are clear.
 * Each one of the two moves has an outcome, and the player chooses
 * the better.
 * Theout come is is a pos/neg value.
 *
 * omg...it breaks my brain...
 */
void solve() {
    cini(h);
    cini(w);
    cinas(s,h);

    vvvi dp(h, vvi(w, vi(2)));

    const string first="Takahashi";
    const string second="Aoki";

    bool last=(h-1+w-1)%2;  /* player occ the last fields */

    int score=1;
    if(last)
        score=-score;

    /* Iterate the positions in order and count best possible
     * outcome for current player makeing his both moves.
     **/
    for(int i=h-1; i>=0; i--)
        for(int j=w-1; j>=0; j--) {
            int p=(i+j)%2;  /* player moving into this field */
            if(i+1<h) { /* go S */
                int sc=dp[i+1][j]=='+';
                dp[i][j]=dp[i+1][j]+sc;
            }
            if(j+1<w) { /* go E */
                int sc=dp[i][j+1]=='+';
                dp[i][j]=max(dp[i][j], dp[i][j+1]+sc);
            }
        }

    if(dp[0][0]>0)
        cout<<first<<endl;
    else if(dp[0][0]<0)
        cout<<second<<endl;
    else
        cout<<"Draw"<<endl;

}

signed main() {
    solve();
}
