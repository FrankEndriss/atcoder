
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to find how often a vetex contributes,
 * add it for an odd number of times.
 * let cnt[i][j] be the number of vertex in jth subtree of vertex i (do dfs)
 * The weight adj[i][j].seconds contributes cnt[i][j]*(n-cnt[i][j])
 *
 * Find foreach edge the number of vertex on both ends.
 * If product is odd it contributes, else does not.
 * How to count them?
 * Just dfs and count the childs.
 *
 * ...
 * No, we search the _sum_ of all xors, so we need the count per bit.
 * ...
 * Still no.
 * Consider the edge ending in a leaf. It contributes to n-1 paths.
 * And then?
 * Foreach bit we need to find in how many paths it contributes.
 * It contributes in that paths that have an even number of edges 
 * with that bit set.
 * Consider all weights be 0 or 1. Then each path contributes, or does
 * not, depending on the number of 1-edges in it.
 *
 * So, if we know the number of 1-edges in all incoming paths, we can add the current edge.
 *
 * ***
 * It is somehow about re-rooting.
 * Consider rooting the tree somewhere, find the xor-value for all childs/dfs.
 * Then, the value for the path from/to all other vertex to current vertex
 * is the sum of all those childs xor-values.
 * No...this is a brain-smasher, too :/
 *
 * Consider the current vertex and its subtrees. In each subtree there are a 
 * number of vertex, 
 * and foreach such vertex there is a path,
 * and in such path each bit is there odd times or even times.
 * The number of odd vertex (per bit) is the number that bit contributes.
 * So each subtree has an xor-value, which tells (per bit)...
 * something.
 * ***
 * Start again:
 * Consider the current vertex.
 * We want to know foreach...
 * to complected. I need to quit and do something working for me instead :/
 */
using mint=modint1000000007;
void solve() {
    cini(n);
    vector<vector<pii>> adj(n);
    //vector<pii> edg;
    //vi ww;
    for(int i=0; i<n-1; i++) {
        cini(u); u--;
        cini(v); v--;
        cini(w);
        adj[u].emplace_back(v,w);
        adj[v].emplace_back(u,w);
        //edg.emplace_back(u,v);
        //ww.emplace_back(w);
    }

    /** xor of subtree rooted at v/p */
    function<int(int,int)> dfs=[&](int v, int p) {
        int ans=0;
        for(pii chl : adj[v]) {
            if(chl.first==p)
                continue;
            ans^=dfs(chl.first, v);
            ans^=chl.second;
        }
        return ans;
    };

    mint val=dfs(0, -1);

    int ans=0;
    for(int i=0; i<n-1; i++) {
        int num=cnt[{edg[i].first,edg[i].second}];
        if((num*(n-num))%2)
            ans^=ww[i];
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}
