
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

void solve() {
    cins(s);

    int ans=0;
    for(int a=0; a<10; a++)
        for(int b=0; b<10; b++)
            for(int c=0; c<10;c++)
                for(int d=0; d<10; d++) {
        bool ok=true;
        for(int i=0; i<10; i++) {
            if(s[i]=='o') {
                if(a!=i && b!=i && c!=i && d!=i)
                    ok=false;
            } else if(s[i]=='x') {
                if(a==i || b==i || c==i || d==i)
                    ok=false;
            }
        }
        if(ok)
            ans++;
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}
