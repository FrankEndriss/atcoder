/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Since only N=16 just check all possible pairs.
 *
 * ...which is a bit complected because we actually need to construct
 * all the pairs.
 */
void solve() {
	cini(n);

	vvi a(2*n, vi(2*n));
	for(int i=0; i<2*n; i++) {
		for(int j=i+1; j<2*n; j++) {
			cin>>a[i][j];
		}
	}

	int ans=0;

	function<void(vvi&,int)> go=[&](vvi &p, int idx) {
		if(idx==p.size())  {
			int lans=0;
			for(int i=0; i<n; i++) {
				lans^=a[p[i][0]][p[i][1]];
			}
			ans=max(ans, lans);
			return;
		}

		vb vis(2*n);
		for(int i=0; i<idx; i++) 
			for(int j=0; j<2; j++) 
				vis[p[i][j]]=true;
		vi unused;
		for(int i=0; i<2*n; i++) 
			if(!vis[i])
				unused.push_back(i);

		assert(unused.size()%2==0);

		int first=unused[0];
		for(size_t j=1; j<unused.size(); j++) {
			int second=unused[j];
			p[idx][0]=first;
			p[idx][1]=second;
			go(p, idx+1);
		}
	};

	vvi p(n, vi(2));
	go(p,0);
	cout<<ans<<endl;
}

signed main() {
    solve();
}
