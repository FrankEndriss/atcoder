/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Again some bitmask.
 * Simple brute force?
 * -> no, N=1<<16, ie ~64k
 * Why such definition of N?
 *
 * Consider to find the min cost to get spices of mask=m;
 * if m has only 1 bit set its trivial.
 * Else it is all combinations of set bits.
 * That is iterate all submasks of m, and combine with
 * the xor of it.
 * This might be O(3^16)...not sure if correct and/or it will fit.
 * Also bad that we got 2^30 dp states :/
 *
 * And not that it is NOT or, but xor!!!
 * So, other problem:
 * We need to by odd number of spices with ith bit set, for 
 * all i=0..n-1
 * From all values 1..(1<<n)-1
 *
 * let N=(1<<n)
 * There are N/2 numbers haveing the 0th bit set, and N/2-1 that have not (since spice 0 is missing)
 * There are N/2 numbers haveing the 1th bit set, and N/2-1 that have not (since spice 0 is missing)
 * ...
 *
 * Can we somehow find min cost to create spice of mask exactly m?
 * We can buy that exactly spice. But, maybe we can also buy two
 * other spices making up that one.
 * So this is a set of spices where each one has a disjoint subset
 * of m set.
 * And from all those sets, we can also choose all constructed sets from 
 * that with all other bits same, if the number is even...
 * ...and things. Thats not the solution.
 *
 *...
 * Also, it is still another problem:
 * We need to be able to make _any_ combination of spicy.
 * So, all spices with one bit set would suffice.
 * Can we do better?
 * How can we find the cheapest way to build a single bit?
 * Consider 0th bit.
 * Buy spice 1, or by two spices from xxxx0 and xxxx1, where xxxx are same.
 * What about using 3 spices, can that be even better?
 *   we buy 1 or 3 from xxxx1, and 2 or 0 from xxxx0
 * Then, when haveing the cheapest sets for all bits, how to combine?
 */
void solve() {
	cini(n);
	vi c(1<<n);
	for(int i=1; i<n; i++)
		cin>>c[i];


}

signed main() {
    solve();
}
