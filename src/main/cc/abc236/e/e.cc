/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Solve two problems.
 *
 * avg:
 * 	maintain sum/cnt of choosen cards and go from left to right.
 * 	But that O(n^2) :/
 * 	...
 * Consider first two cards, there are 3 outcomes:
 * choosen first, a[0]/1
 * choosen second, a[1]/1
 * choosen both, (a[0]+a[1])/2
 * So state is sum=boolPrevChoosen,cnt
 * dp[i][j]=max sum if i==true?prevChoosen, and j numbers choosen at all.
 *
 * median:
 * 	same :/
 *
 * Binary search for y?
 * Would work nicely for median, since we can allways choose the number bigger y 
 * if available.
 * But how to binary search/check if y can be constructed as average?
 * -> same, just try to use all numbers bigger y.
 *
 * avg:
 * But if both smaller y we dont know if to choose both or only the bigger.
 * Consider first 3 a[i]<y, we can choose second, or first and third.
 * How to know which is better?
 * -> We cant :/
 ******************************

 *  Observe that we actally do not need to construct the best average.
 *  Instead, subst all a[i] by a[i]-y and find if we can construct
 *  the max sum>=0.
 *  And max sum is very simple dp with two states foreach i in O(n).
 */
const ld EPS=0.00001;
void solve() {
	cini(n);
	cinai(a,n);
	ld ma=(ld)*max_element(all(a));

	ld l=0;
	ld r=ma;
	while(l+EPS<r) {
		ld m=(l+r)/2;
		ld sum=0;
		int cnt=0;
		vb
		for(int i=0; i<n; i++) {

		}
	}
}

signed main() {
    solve();
}
