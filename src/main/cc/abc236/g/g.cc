/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Seems also like bruteforce.
 * Whenever a vertex in the component of 1 is added there might
 * some pathlengths change.
 * Just bfs them, and report fitting ones.
 * ...
 * But, there are loops :/
 * So, whenever a loop is created, then we can add multiples of looplens
 * to all existing paths with one of the loop veritces.
 * ...which makes it horrible complecated :/
 */
void solve() {
	cini(n);
	cini(t);
	cini(L);

	dsu d(n);
	for(int i=0; i<t; i++) {
		cini(u);
		cini(v);
	}
}

signed main() {
    solve();
}
