
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Slide a window of size m
 * find value of kth element
 *
 * on first window, get sum.
 * on consecutive steps, check if the previous number was taken from
 * k elements.
 *
 * Use two multiset to implement.
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);
    cinai(a,n);

    //cerr<<"m="<<m<<" k="<<k<<endl;

    multiset<int> k0;
    multiset<int> m0;

    for(int i=0; i<m; i++)
        m0.insert(a[i]);

    auto it=m0.begin();
    int sum1=0;
    for(int i=0; i<k; i++) {
        sum1+=*it;
        it++;
    }
    vi ans;
    ans.push_back(sum1);
    //cout<<sum1<<" ";

    int sumk=0;
    for(int i=m; i<n; i++) {
        //cerr<<"i="<<i<<endl;
        if(k0.size()>0 && a[i-m]<=*k0.rbegin()) {
            it=k0.find(a[i-m]);
            assert(it!=k0.end());
            sumk-=*it;
            k0.erase(it);
        } else {
            it=m0.find(a[i-m]);
            assert(it!=m0.end());
            m0.erase(it);
        }

        m0.insert(a[i]);
        //cerr<<"m0.size()="<<m0.size()<<endl;

        while(k0.size()<k) {
            //cerr<<"k0.size()="<<k0.size()<<endl;
            it=m0.begin();
            sumk+=*it;
            k0.insert(*it);
            m0.erase(it);
        }

        while(k0.size() && m0.size() && *k0.rbegin() > *m0.begin()) {
            auto it2=k0.rbegin();
            int kk=*it2;
            sumk-=kk;
            it=k0.find(kk);
            k0.erase(it);

            m0.insert(kk);
            it=m0.begin();
            sumk+=*it;
            k0.insert(*it);
            m0.erase(it);
        }

        ans.push_back(sumk);

        //cout<<sumk<<" ";
    }
    for(int i : ans)
        cout<<i<<" ";
    cout<<endl;

}


signed main() {
    solve();
}
