
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * knapsack
 */
const int N=101;
void solve() {
    cini(n);
    cini(k);
    cini(d);
    cinai(a,n);

    /* dp[i][j] = biggest sum of i numbers with sum%d==j */
    vvi dp(N, vi(N, -1));
    dp[0][0]=0;

    for(int i=0; i<n; i++) {
        for(int r=k; r>=1; r--) {
            for(int dd=0; dd<d; dd++) {
                if(dp[r-1][dd]>=0) {
                    int o=(dd+a[i])%d;
                    dp[r][o]=max(dp[r][o], dp[r-1][dd]+a[i]);
                }
            }
        }
    }

    cout<<dp[k][0]<<endl;
}

signed main() {
    solve();
}
