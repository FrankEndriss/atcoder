/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vvvb= vector<vvb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider some kinde of bruteforce...
 *
 * Iterate all possible distances of first dimension.
 * Foreach, iterate the possible distances for second dimension...
 * Also use memoization.
 */
using mint=modint998244353;
void solve() {
    cini(n);
    cini(D);

    cinai(p,n);
    cinai(q,n);

    vector<vector<vector<mint>>> dp(n, vector<vector<mint>>(D+1, vector<mint>(D+1)));
    vvvb vis(n, vvb(D+1, vb(D+1)));

    /* number of points starting at dimension i with max dists dP and dQ */
    function<mint(int,int,int)> go=[&](int i, int dP, int dQ) {
        assert(dP>=0 && dQ>=0);
            
        if(i==n)
            return mint(1);

        if(vis[i][dP][dQ])
            return dp[i][dP][dQ];

        mint ans=0;
        for(int dd=max(p[i]-dP, q[i]-dQ); dd<=min(p[i]+dP, q[i]+dQ); dd++)
            ans+=go(i+1, dP-abs(p[i]-dd), dQ-abs(q[i]-dd));

        vis[i][dP][dQ]=true;
        return dp[i][dP][dQ]=ans;
    };

    mint ans=go(0,D,D);
    cout<<ans.val()<<endl;

}

signed main() {
       solve();
}
