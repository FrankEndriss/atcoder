/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Simulate.
 * ...
 * Use 1 based indexing.
 */
void solve() {
    cini(n);
    cini(m);
    cini(t);

    vi a(n+1);
    for(int i=2; i<=n; i++) 
        cin>>a[i];

    vi y(n+1);

    for(int i=0; i<m; i++)  {
        cini(x); 
        cin>>y[x];
    }

    int tt=0;
    //t+=y[0];
    for(int i=2; i<=n; i++) {
        tt+=a[i];
        //cerr<<"room: "<<i+1<<" tt="<<tt<<" t="<<t<<endl;
        if(tt>=t) {
            cout<<"No"<<endl;
            return;
        }
        t+=y[i];
        if(y[i]>0) {
            //cerr<<"room: "<<i+1<<" bonus: "<<y[i]<<endl;
        }
    }
    cout<<"Yes"<<endl;
}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
