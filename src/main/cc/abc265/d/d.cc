/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Brute force this.
 * With two pointer we get all pairs x,y with sum==P
 * Also the other pairs can be found with twopointer.
 */
void solve() {
    cini(n);
    cini(p);
    cini(q);
    cini(r);
    cinai(a,n);

    vi pre(n+1);
    for(int i=0; i<n; i++) 
        pre[i+1]=pre[i]+a[i];

    int x=0;
    int y=0;
    int z=0;
    int w=0;
    int sumxy=0;
    int sumyz=0;
    int sumzw=0;
    while(y<n) {
        while(y<n && sumxy<p) {
            sumxy+=a[y];
            sumyz-=a[y];
            y++;
        }

        if(sumxy==p) {
            while(z<y) {
                sumyz+=a[z];
                sumzw-=a[z];
                z++;
            }

            while(z<n && sumyz<q) {
                sumyz+=a[z];
                sumzw-=a[z];
                z++;
            }
            if(sumyz==q) {
                while(w<z) {
                    sumzw+=a[w];
                    w++;
                }
                while(w<n && sumzw<r) {
                    sumzw+=a[w];
                    w++;
                }
                if(sumzw==r) {
                    cout<<"Yes"<<endl;
                    return;
                }
            }
        }

        sumxy-=a[x];
        x++;
    }
    cout<<"No"<<endl;

}

signed main() {
       solve();
}
