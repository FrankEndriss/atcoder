/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that each obstacle can be hit by some more or less 
 * fixed number of steps and paths.
 * Subtract them from all paths.
 * But also include/exclude the paths that hit several obstacles :/
 *
 * ...
 * *****
 * Note that foreach step we have 3 choices of what step to take, so there
 * are 3^n possible path we can go.
 * But the number of points we can reach is much smaller, since the order
 * of steps does not matter. We can describe each reacheable point by 
 * the number of step of each kind that we took to get there:
 * dp[x][y][z]=true if that point is reacheable.
 * So we can "simply" find all these endpoints by simulating the steps.
 */
void solve() {
    cini(n);
    cini(m);
    cinai(d,6);

    // TODO
}

signed main() {
       solve();
}
