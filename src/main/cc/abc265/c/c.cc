/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * simulate, check for loop.
 */
void solve() {
    cini(h);
    cini(w);

    cinas(g,h);

    int i=0;
    int j=0;
    vvb vis(h, vb(w));
    vis[0][0]=true;
    while(true) {
        int ii=i;
        int jj=j;

        if(g[i][j]=='U')
            ii--;
        else if(g[i][j]=='D')
            ii++;
        else if(g[i][j]=='L')
            jj--;
        else if(g[i][j]=='R')
            jj++;
        else
            assert(false);


        if(ii<0 || jj<0 || ii==h || jj==w)  {
            cout<<i+1<<" "<<j+1<<endl;
            return;
        } else if(vis[ii][jj]) {
            cout<<-1<<endl;
            return;
        }  else {
            vis[ii][jj]=true;
            i=ii;
            j=jj;
        }
    }
    assert(false);
}

signed main() {
       solve();
}
