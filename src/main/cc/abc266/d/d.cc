/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * dp[i][k]=max outcome at time i if be at position k
 */
const int N=1e5+7;
const int INF=1e18;
void solve() {
    cini(n);

    vector<vector<pii>> data(N);
    for(int i=0; i<n; i++) {
        cini(t);
        cini(x);
        cini(a);
        data[t].emplace_back(x,a);
    }

    vvi dp(N, vi(5, -INF));
    dp[0][0]=0;

    for(int i=1; i<N; i++) {
        dp[i]=dp[i-1];
        for(auto [x,a] : data[i]) {
            for(int k=0; k<5; k++) {
                int dist=max(1LL, abs(k-x));
                if(i-dist>=0)
                    dp[i][x]=max(dp[i][x], dp[i-dist][k]+a);
            }
        }
    }

    int ans=0;
    for(int i=0; i<5; i++) 
        ans=max(ans, dp[N-1][i]);

    cout<<ans<<endl;



}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
