/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We want to continue whenever
 * the expected outcome of the remaining throws is
 * bigger than our current throw.
 * So actually we allways stop after throwing a 6.
 *
 * let ans[i]= E for i throws.
 * ans[0]=0
 * ans[1]=3,5
 * ans[2]=(6 + 5 + 4 + 3,5 + 3,5 + 3,5 ) / 6
 * ...
 */
void solve() {
    cini(n);

    vd ans(n+1);
    ans[0]=0;
    ans[1]=3.5;

    for(int i=2; i<=n; i++) {
        ld sum=0;
        for(int k=1; k<=6; k++) 
            sum+=max((ld)k, ans[i-1]);
        ans[i]=sum/6;
    }
    cout<<ans[n]<<endl;
}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
