/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that M,N<2000, ie O(n^2) will work.
 * So, consider element a[i] to be rightmost element, 
 * so find max m-1 seq left of it.
 *
 * dp[i][j]=max sum of seq of len i with last element at index j
 */
const int INF=1e18;
void solve() {
    cini(n);
    cini(m);
    cinai(a,n);


    vvi dp(m+1, vi(n, -INF));

    for(int j=0; j<n; j++) 
        dp[1][j]=a[j];

    for(int i=2; i<=m; i++) {
        int ma=-INF;
        for(int j=0; j<i-1; j++)
            ma=max(ma, dp[i-1][j]);

        for(int j=i-1; j<n; j++) {
            dp[i][j]=ma+a[j]*i;
            ma=max(ma, dp[i-1][j]);
        }
    }
    int ans=-INF;
    for(int j=0; j<n; j++)
        ans=max(ans, dp[m][j]);

    cout<<ans<<endl;
}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
