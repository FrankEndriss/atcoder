/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We add m*a[i], and remove sum of previous m elements.
 * So each element is subtracted m times.
 *
 * What wrong here?
 */
const int INF=1e12;
void solve() {
    cini(n);
    cini(m);
    cinai(a,n);

    int sum=0;
    int lsum=0;
    for(int i=0; i<m; i++) {
        sum+=a[i]*(i+1);
        lsum+=a[i];
    }
    int ans=sum;

    for(int i=m; i<n; i++) {
        sum+=a[i]*m;
        sum-=lsum;
        ans=max(ans, sum);

        lsum+=a[i];
        lsum-=a[i-m];
    }

    cout<<ans<<endl;
}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
