/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
void solve() {
    cins(s);

    vi c(7);
    vi m={ 3, 2, 4, 1, 3, 5, 0, 2, 4, 6 };
    for(int i=0; i<10; i++) {
        if(s[i]=='1')
            c[m[i]]++;
    }

    int st=0;
    if(s[0]=='0') {     // pin 1
        for(int i=0; i<7; i++) {
            if(st==0 && c[i]>0)
                st=1;
            if(st==1 && c[i]==0)
                st=2;
            if(st==2 && c[i]>0) 
                st=3;
        }
    }
    if(st==3) 
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;
}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
