/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We can sort all vertex by sum of adj vertex.
 * Then upper bound is the most expensive vertex.
 *
 * Clearly, we want to remove (at least one) of its
 * adj vertex before it.
 *
 * Consider the cheapest vertex. Removing that one cannot be wrong.
 * And it makes all other vertex cheaper, too.
 *
 * So just simulate removing the cheapest allways.
 * Use set to maintain the vertex (and their cost).
 */
void solve() {
    cini(n);
    cini(m);

    cinai(a,n);

    vi c(n);    /* c[i]= current cost of vertex i */
    set<pii> s; /* cost,id */
    vvi adj(n);
    for(int i=0; i<m; i++) {
        cini(u); u--;
        cini(v); v--;
        c[u]+=a[v];
        c[v]+=a[u];
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    for(int i=0; i<n; i++) 
        s.emplace(c[i], i);

    int ans=0;
    vb vis(n);
    for(int i=0; i<n; i++) {
        auto it=s.begin();
        auto [cu,u]=*it;
        vis[u]=true;
        s.erase(it);
        ans=max(ans, cu);

        for(int chl : adj[u]) {
            if(vis[chl])
                continue;

            auto it=s.find({c[chl],chl});
            s.erase(it);
            c[chl]-=a[u];
            s.emplace(c[chl],chl);
        }

    }

    cout<<ans<<endl;

}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
