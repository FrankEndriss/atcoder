
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * At each position of s we have 9 possible states, representing
 * the number of possible prefixes of that len.
 *
 * Number of possible prefixes of len==0 are 1 at all positions.
 *
 * So prefix of len j at position i is number of prefixes at position i-1 
 * plus number of 1 shorter prefixes at position i-1.
 */
using mint=modint1000000007;
void solve() {
    cins(s);

    const string choc="chokudai";
    vector<vector<mint>> dp(choc.size()+1, vector<mint>(s.size()+1));
    dp[0]=vector<mint>(s.size()+1, 1);

    for(int i=1; i<=choc.size(); i++) {
        for(int j=1; j<=s.size(); j++) {
            dp[i][j]=dp[i][j-1];
            if(s[j-1]==choc[i-1]) {
                dp[i][j]+=dp[i-1][j-1];
            }
        }
    }

    cout<<dp[choc.size()][s.size()].val()<<endl;;

}

signed main() {
    solve();
}
