
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Find number of shortest paths from city 1 to N.
 * Note that a shortest path never has a loop.
 *
 * So we keep track of a set of vertex where current shortest
 * paths end, and number of these paths.
 *
 * Then, foreach round, put all new seen vertex into the set of vertex
 * of next round. We can ignore allready seen vertex, because a 
 * shortest path never has a loop.
 */
using mint=modint1000000007;

void solve() {
    cini(n);
    cini(m);

    vvi adj(n);
    for(int i=0; i<m; i++) {
        cini(u);
        u--;
        cini(v);
        v--;

        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    vb vis(n);
    vis[0]=true;

    vi cur; /* last vertex of all current shortest paths */
    cur.push_back(0);

    vector<mint> dp(n);   /* dp[i]=number of shortest paths ending in i */
    dp[0]=1;

    vector<mint> ans;
    int cnt=0;
    while(cur.size()) {
        cnt++;
        vi cur0;
        vector<mint> dp0(n);
        for(int v : cur) {
            for(int chl : adj[v]) {
                if(!vis[chl]) {
                    vis[chl]=true;
                    cur0.push_back(chl);
                }
                dp0[chl]+=dp[v];
            }
        }
        cur=cur0;
        ans=dp;
        dp=dp0;
    }
    //cerr<<"path len="<<cnt<<endl;
    cout<<ans[n-1].val()<<endl;

}

signed main() {
    solve();
}
