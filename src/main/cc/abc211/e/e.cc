
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * We need to consider each component separate, then multply them.
 *
 * In each component there must be at least k cells.
 * Then, how to count the number of possible sub-components of size k?
 *
 * Note that the field is small, 8x8, also k<8
 * So, consider the component, order cells by row,col.
 * Consider current cell member of painted set, all prev cells
 * not member.
 * Then count the number of paths of len k.
 *
 * ...to complecated to implement in time :/
 */
void solve() {
    cini(n);
    cini(k);

    cinas(s,n);
    vvb vis(n, vb(n));

    vi dx= { -1, 1, 0, 0};
    vi dy= { 0, 0, -1, 1};
    for(int i=0; i<n; i++) {
        for(int j=0; j<n; j++) {
            if(vis[i][j])
                continue;

            set<pii> c;  /* current component */
            queue<pii> q;
            q.emplace(i,j);
            while(q.size()) {
                auto [ii,jj]=q.front();
                q.pop();
                c.emplace(ii,jj);

                for(int it=0; it=4; it++) {
                    int iii=ii+dx[it];
                    int jjj=jj+dy[it];
                    if(iii>=0 && iii<n && jjj>=0 && jjj<n) {
                        if(!vis[iii][jjj]) {
                            c.emplace(iii,jjj);
                            q.emplace(iii,jjj);
                            vis[iii][jjj]=true;
                        }
                    }
                }

                /* now check the component c */
                for(pii p : c) {
                    for(int kk=0; kk<k; kk++) {
                        vvb vis2(n, vb(n));
                        for(int it=0; it<4; it++) {
                            int iii=ii+dx[it];
                            int jjj=jj+dy[it];
                            if(iii>=0 && iii<n && jjj>=0 && jjj<n && make_pair(iii,jjj)>p) {
                                if(!vis2[iii][jjj]) {
                                    vis2[iii][jjj]=true;
                                }
                            }
                        }
                        // TODO
                    }
                }
            }
        }
    }

}

signed main() {
    solve();
}
