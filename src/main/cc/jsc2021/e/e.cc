
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/dsu>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Consider the size of the string, the deepest level
 * we can have is the number of divisions by 2.
 * Consider the starting string.
 *
 * For level 1 palindrome it is s/2
 * For level 2 palindrome it is s/4
 * For level 3 palindrome it is s/8
 * etc
 *
 * Note concat t,_C_,rev(t)
 * So we need to consider that middle char whenever
 * the lenght of the string is not even.
 *
 * Note that the starting string must be a L-0 palindrome,
 * ie must not be a plindrome at all.
 * ...
 * Why is example3 ans=impossible?
 * Every string of len==1 is a plindrome, so we cannot 
 * start with "a" to get K==3, but we can start with "" or "ac" to get
 * K==4 or K==2.
 *
 */
void solve() {
    cini(k);
    cins(s);

    size_t sz=s.size();
    vi c;
    for(int i=0; i<k; i++) {
        if(sz==0) {
            cout<<"impossible"<<endl;
            return;
        }

        c.push_back(sz%2);
        sz/=2;
    }
    const int iniSz=sz;

    /* no palindromes at start */
    if(sz==1) {
        cout<<"impossible"<<endl;
        return;
    }

    /* Now if we got a plindrome (of len>1) at start, we need to
     * change some chars of it so that it becomes a not-palindrome.
     * It is sz/2 number of char.
     * But since we need to minimize the number of changes in the 
     * end, we need to change these char so to optimze that number.
     * How?
     * Each of the chars in the initial string is a leader in 
     * a component later. So we want to change it to the char
     * with the biggest freq in that component.
     *
     * But since the initial String must not be a palindrome,
     * there must be at least one position (in first half)
     * that differs from the mirrored position in the second half.
     * If all are same we want to change one of them to another char.
     * That for we check the freq of the second most freq char
     * of that component, ant choose that instead of the most
     * freq one. This "instead" gives some additional cost,
     * we need to add them to ans.
     */


    dsu d((int)s.size());
    while(sz<s.size()) {
        sz=sz*2+c.back();
        c.pop_back();
        for(int i=0; i+i<sz; i++)
            d.merge(i, sz-1-i);
    }

    vector<vector<int>> g=d.groups();
    vi p2g(s.size());   /* position to group */

    vi ans(g.size());
    vvi f(g.size(), vi(26));
    for(size_t i=0; i<g.size(); i++) {
        for(int k : g[i])  {
            p2g[k]=i;
            f[i][s[k]-'a']++;
        }

        sort(all(f[i]));
        ans[i]=g[i].size()-f[i].back();
    }

    bool isIniPalin=iniSz>1;
    for(int i=0; i+i<iniSz; i++)
        if(s[i]!=s[iniSz-1-i])
            isIniPalin=false;

    int off=1e9;
    if(isIniPalin) {
        //cerr<<"iniSz="<<iniSz<<" istring="<<s.substr(0,iniSz)<<endl;
        for(int i=0; i+i<iniSz; i++)
            if(i!=iniSz-1-i && s[i]==s[iniSz-1-i]) {
                int l1=d.leader(i);
                int l2=d.leader(iniSz-1-i);
                //cerr<<"i="<<i<<" j="<<iniSz-1-i<<" l1="<<l1<<" l2="<<l2<<endl;
                assert(l1!=l2);
                off=min(off, f[p2g[l1]][25]-f[p2g[l1]][24]);
                off=min(off, f[p2g[l2]][25]-f[p2g[l2]][24]);
            }
    } else 
        off=0;

    cout<<accumulate(all(ans), 0LL)+off<<endl;

}

signed main() {
    solve();
}
