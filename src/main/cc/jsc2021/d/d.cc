
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* all a[i]%=P
 * We go some array of 1 or more, max n elements.
 * Why that are (p-1)^n?
 * -> there are that much _of length N_
 *
 * We got p-1 arrays of length 1
 * (p-1)*(p-1) arrays of length 2
 * etc
 * Problem asks for the number of arrays of length N where
 * there is no prefix of length i>0 that has a sum%p==0
 *
 * Let na be the array of length N.
 * na[0] can be any of p-1 elements
 * na[1] can be any of p-2 elements
 * na[2], again can by any of p-2 elements... etc
 */
const int MOD=1e9+7;
void solve() {
    cini(n);
    cini(p);

    int ans=p-1;
    for(int i=1; i<n; i++) 
        ans=(ans*(p-2))%MOD;

    cout<<ans<<endl;
}

signed main() {
    solve();
}
