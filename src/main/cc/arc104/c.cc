/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We got some intervals (A[i],B[i])
 * C[i]= size of interval (A[i],B[i]) - 2 
 *
 * Note that union(A,B) is a permutation.
 *
 * Note that the set<int> could contain the 
 * smallest available numbers!
 *
 * So, on each result ab[i] we have a min(a) and a min(b).
 *
 * 1,x -> 1,2
 * x,3 -> ?,3
 *
 * 1,x -> 1,2
 * x,4 -> 3,4
 *
 * -1,0 -> -1,0
 * x,x -> 1,2
 * 3,x -> 3,5
 * 4,x -> 4,6
 *
 * 3,x -> 3,4
 * 5,x -> 5,6
 *
 * ???
 * Let overlapping intervals be components.
 * How to construct smallest possible components, or check
 * if we can construct them?
 *
 * At each free position there can an interval start, or end.
 *
 ****************
 * Consider given x0,y0:
 * There must be y0-x0-1 x1, x2... whith x1>x0 && x1<y0
 *
 * Consider given x0,-1 ans x1,-1 with x0<x1
 *
 * Just some casework?
 */
const int INF=1e9;
void solve() {
    cini(n);

    set<int> s;
    for(int i=1; i<=2*n; i++)
        s.insert(i);

    bool yes=true;
    vector<pii> ab;
    vi a;
    vi b;

    vb vis(2*n+1);
    for(int i=0; yes && i<n; i++)  {
        cini(x);
        cini(y);
        if(x==-1 && y==-1)
            continue;

        if(x!=-1) {
            if(vis[x])
                yes=false;
            vis[x]=true;
            s.erase(x);
        }
        if(y!=-1) {
            if(vis[y])
                yes=false;
            vis[y]=true;
            s.erase(y);
        }

        if(x!=-1 && y!=-1) 
            ab.push_back({x,y});
        else if(x!=-1)
            a.push_back(x);
        else 
            b.push_back(y);
    }

    if(!yes) {
        cout<<"No"<<endl;
        return;
    }

    sort(all(ab));
    sort(all(a));
    sort(all(b));
    reverse(all(ab));
    reverse(all(a));
    reverse(all(b));

    int aEnd=-1;    /* last A of current component */
    int bEnd=-1;    /* last B of current component */
    
    // consider next available number
    int compA=-1;   /* smallest A in current component */
    for(int i=0; i<n; i++) {
        if(compStart==-1) {
            ...
        }
    }



    for(int i=1; i+1<=n; i++) {
        if(ab[i].first>ab[i].second)
            yes=false;

        if(ab[i+1].first<ab[i].second && (ab[i].second-ab[i].first !=ab[i+1].second-ab[i+1].first))
            yes=false;
    }

    if(yes)
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;



}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
