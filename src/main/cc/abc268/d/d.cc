/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Sort the statement...
 *
 * We want to build a string ans that is _not_ in t[]
 *
 * ans is a concatenation of strings, all of them
 * prefixes of permutations of s.
 *
 * So, find first partstring of T, then find any permutation
 * prefix of s and use that as the prefix of ans.
 * ...
 * No, that does not make sense.
 * ****
 * S[] is an array of strings. We build ans by appending
 * all these s[i] one after the other, and some number
 * of "_" between them.
 *
 * So we could concat all s[] simply and put "a lot of" _
 * between them.
 * But there is the max len of 16.
 *
 * So try to find a permutation of s[] that does not exist
 * in T.
 * If no such perm exists, use some number of '_' that does not
 * exist in T[].
 *
 * So, classyfy T[] by permutation of strings, and permutation of
 * number of _.
 *
 * Thats no fun to implement... :/
 */
void solve() {
}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
