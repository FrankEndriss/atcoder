/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider each rotation by one position.
 * There are a number of persons that increase frustration, some decrease frustration.
 * Just count and maintain them.
 *
 * Note that on odd circle size, there is on position with inc=0.
 * So try to be very careful with off by one...
 * ...which turns out seems to me to much to do for me :/
 */
const int INF=1e18;
void solve() {
    cini(n);
    cinai(a,n);

    vector<pii> evt;    /* evt[i].second= change in some persons inc/dec if  
                           going from evt[i].first-1 to evt[i].first */

    int inc=0;    /* sum increase of all persons */
    int ans=0;
    for(int i=0; i<n; i++) {
        const int d=(i-a[i]+n)%n;   /* decreasing frust */
        const int dL=n-d;           /* increaseing frust */
        ans+=min(d,dL);

        int p0=(i+n/2-i+n)%n;
        int p1=p0+n%2;

        evt.emplace_back(d, 2);
        evt.emplace_back((p0-i+n)%n, -1);
        evt.emplace_back((p1-i+n)%n, -1);

        if(d<=n/2)
            inc--;
        if(dL<=n/2)
            inc++;
    }
    sort(all(evt));

    int gans=ans;
    size_t i=0;
    while(i<evt.size()) {
        ans+=inc;
        gans=min(gans, ans);

        int e=evt[i].first;
        while(i<evt.size() && e==evt[i].first) {
            inc+=evt[i].second;
            i++;
        }
    }
    cout<<gans<<endl;
}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
