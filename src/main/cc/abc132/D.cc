/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

ll b[2001][2001];
const ll MOD=1e9+7;
int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    ll n, k;
    cin>>n>>k;
    
    for(ll i=0; i<=n; i++)
        for(ll j=0; j<=n; j++)
            b[i][j]=-1;

    function<ll(int, int)> binom=[&](int ln, int lk) {
        if(ln<lk)
            return 0LL;
        if(ln==lk || lk==0)
            return 1LL;

        if(b[ln][lk]>=0)
            return b[ln][lk];

        return b[ln][lk]=(binom(ln-1, lk-1) + binom(ln-1, lk))%MOD;
    };

    const int blue=k;
    const int red=n-k;

    for(int i=1; i<=blue; i++) {
        ll ans=binom(red+1, i); // number of ways to choose i slots
        const int balls=blue-i; // number of balls to distribute to the choosen slots.
        for(int j=0; j<balls; j++) { // every ball can be put into one of i slots
            ans*=i;
            ans%=MOD;
        }

        cout<<ans<<endl;
    }

}

