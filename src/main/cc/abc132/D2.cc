#include "bits/stdc++.h"
using namespace std;
const int N=2e3+20,MOD=1e9+7;
int c[N][N],n,k,red,blue,ans;
int main()
{
    c[0][0]=1;
    for(int n=1;n<N;n++)
    {
        for(int r=0;r<=n;r++)
        {
            if(r==0 or r==n) c[n][r]=1;
            else c[n][r]=c[n-1][r-1]+c[n-1][r];
            c[n][r]%=MOD;
        }
    }
 
    cin>>n>>k;
 
    blue=k,red=n-k;
    for(int k=1;k<=blue;k++)
    {
        ans=(1ll*c[red+1][k]*c[blue-1][k-1])%MOD;
        printf("%d\n",ans);
    }
}
