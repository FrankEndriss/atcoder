/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Would be simple dp for smaller N.
 * Consider lcm(a,b)
 *
 * ????
 * Overflow???
 */
const double INF=1e30+7;
void solve() {
    cini(n);
    cini(a);
    cini(b);
    cini(x);
    cini(y);
    cini(z);

    const int c=a*b/gcd(a,b);
    const int cc=min(c*x, min((c/a)*y, (c/b)*z));   /* cost for increase by c */

    int ans=(n/c)*cc;
    n%=c;

    /* dont do a */
    int ansA=min(ans+n*x, ans+(n/b)*z+(n%b)*x);
    /* dont do b */
    int ansB=min(ans+n*x, ans+(n/a)*y+(n%a)*x);

    if(a<b) {
        swap(a,b);
        swap(y,z);
    }

    ld ans2=INF;
    for(int i=0; i*a<=n; i++) {
        int sum=a*i;
        int lans=y*i;
        int cntB=(n-sum)/b;
        lans+=cntB*z;
        lans+=(n-sum-cntB*b)*x;
        ans2=min(ans2, lans);

        ans2=min(ans2, y*i+(n-sum)*x);
    }
    cout<<min(ansA, min(ansB, ans+ans2))<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
