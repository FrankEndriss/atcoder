/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * There are 1e10 possible pairs.
 *
 * Consider 1,1
 * -> 
 *  1,3; 3,1
 *
 * The other way:
 * Consider 5,5: 20
 * -> 2,6; 6,2; 8,4; 4,8; 
 *
 * From each aequivalence class there can be one pair.
 * Just construct foreach class one.
 * But note that most pairs occ two classes!
 *
 * ...
 * We got two sets of aeq classes, left and right.
 */
void solve() {
    cini(n);
    cini(m);

    int maL=n*3+m;
    int maR=n+m*3;

    vector<pii> ans;
    vb visL(maL+1);  /* nn*3+mm */
    vb visR(maR+1);  /* nn+mm*3 */
    for(int i=4; i<=max(maL,maR); i++) {
        if(i%4==0) {
            ans.emplace_back(i/4, i/4);
            visL[i]=visR[i]=true;
            continue;
        }

        if(i<=maL && !visL[i]) {
            for(int nn=min(n, (i-1)/3); nn>=1; nn--) {
                int mm=i-nn*3;
                if(mm*3+nn<=maR && !visR[mm*3+nn]) {
                    ans.emplace_back(nn,mm);
                    visL[i]=visR[mm*3+nn]=true;
                    continue;
                }
            }
        }

        if(i<=maR && !visR[i]) {
            for(int mm=min(m, (i-1)/3); mm>=1; mm--) {
                int nn=i-mm*3;
                if(nn*3+mm<=maL && !visL[nn*3+mm]) {
                    ans.emplace_back(nn,mm);
                    visR[i]=visL[nn*3+mm]=true;
                    continue;
                }
            }
        }
    }
    //sort(all(ans));
    cout<<ans.size()<<endl;
    for(pii p : ans) 
        cout<<p.first<<" "<<p.second<<endl;
}

signed main() {
    solve();
}
