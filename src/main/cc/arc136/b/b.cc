/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/segtree>
using namespace atcoder;


const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* adjust as needed */
using S=int; /* type of values */

S st_op(S a, S b) {
    return a+b;
}

S st_e() {
    return 0;
}

using stree=segtree<S, st_op, st_e>;

/**
 * Simulate?
 * Should be possible.
 * But simply count inversion would be simpler.
 * One triple rotation changes two inversions, so if number of
 * inversions is even it would work.
 * However, how to count inversions for given b[]?
 *
 * -> so simulate
 * fix positions from left to right, then check if arrays are same.
 * Find some cute "trick" to fix last position.
 *
 * Does not work...somehow :/
 * Why ???
 */
const int N=5007;
void solve() {
    cini(n);
    vvi a(2, vi(n));

    for(int j=0; j<2; j++) 
        for(int i=0; i<n; i++) 
            cin>>a[j][i];

    vector<map<int,int>> f(2);
    vector<stree> seg(2, stree(N));

    vi cnt(2);
    /* count freq and number of inversions in both arrays */
    for(int i=0; i<n; i++) {
        for(int j=0; j<2; j++) {
            cnt[j]+=seg[j].prod(0,a[j][i]);
            f[j][a[j][i]]++;
            seg[j].set(a[j][i], seg[j].get(a[j][i])+1);
        }
    }

    auto it0=f[0].begin();
    auto it1=f[1].begin();
    bool ok=true;
    for(auto ent : f[0]) {
        if(it0!=f[0].end() && it1!=f[1].end() && it0->first==it1->first && it0->second==it1->second) 
            ;
        else
            ok=false;
    }
    if(!ok) {
        cout<<"No"<<endl;
        return;
    }

    if(cnt[0]%2== cnt[1]%2)
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;

}

signed main() {
    solve();
}
