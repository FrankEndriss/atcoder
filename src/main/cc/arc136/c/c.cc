/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that same number of ops apply if we decrease, and want 
 * to find a[i]=0 for all i.
 *
 * So, find max points/plateaus, and decrease until same as next left/right.
 *
 * ...Seems hard implementation :/
 * How can we make it simple?
 *
 * Find a max plateau, lower it to next lower adj cell.
 * Then extends left/right, until we find next adj tow cases
 * smaller:
 *  Then again lower
 * bigger:
 *  Then search next.. to left and right...
 *  Thats annoying complecated, we need to implement search in two directions.
 *
 * Can we maintain list of plateau-segments?
 * Put them into a queue of hights, so we will allways choose the highest,
 * and lower to the max of both adj.
 * ...no, its even more complecated.
 *
 * Compress a[], so that all plateaus are of len==1, then?
 * rotate so that a min_element() is in first position.
 * ...still to comlecated :/
 *
 * What else?
 */
void solve() {
    cini(n);
    cinai(a,n);

    while(a.size()>1 && a.back()==a[0])
        a.pop_back();

    vi b;
    for(int i=0; i<n; i++) {
        if(i==0 || a[i]!=a[i-1])
            b.push_back(a[i]);
    }
    a.swap(b);

    /* then simplyfy by moving a min_element to position a[0] */
    int mi=1e9+7;
    int miidx=-1;
    for(int i=0; i<n; i++) {
        if(a[i]<mi) {
            mi=a[i];
            miidx=i;
        }
    }

    if(miidx!=0)
        rotate(a.begin(), a.begin()+miidx, a.end());

    /* find first local maximum */
    int first=-1;
    for(int k=0; k<n; k++) {
        int le=-1;
        if(k>0)
            le=a[k-1];
        int ri=-1;
        if(k+1<n)
            ri=a[k+1];

        if(a[k]>le && a[k]>ri) {
            first=k;
            break;
        }
    }
    assert(first>=0);

    /* now walk from max to max. Note that we only have to walk to the right,
     * since a[0] is *min_element() */
    int ans=0;
    int l=first;
    int r=l+1;
    while((l+n)%n!=(r+n)%n) {
        int le=a[l];
        if(l>0)
            le=a[l-1];

        int ri=a[l];
        if(r<n)
            ri=a[r];

        if(ri>a[l]) {
            l=r;
            r=l+1;
            continue;
        }

        if(l>0 && le<=ri) {   /* extend to left */
            ans+=a[l]-le;
            l--;
            while(l>0 && a[l]==a[l-1])
                l--;
        }
        if(r<n && ri<=le) {
            if(ri<le)
                ans+=a[l]-ri;

        }
    }





}

signed main() {
    solve();
}
