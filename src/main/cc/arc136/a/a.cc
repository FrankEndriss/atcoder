/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that subst a BB by A allways make a lex smaller string
 * Otherwise never.
 * But, we can repeat.
 * So we first subst all A by BB, 
 * Then all BB by A
 * Thus moving the A in front, and B to the back.
 */
void solve() {
    cini(n);
    cins(s);
    string t;
    for(int i=0; i<n; i++) {
        if(s[i]=='A') {
            t+="BB";
        } else {
            t+=s[i];
        }
    }

    for(size_t i=0; i<t.size(); i++) {
        if(i+1<t.size() && t[i]=='B' && t[i+1]=='B') {
            cout<<'A';
            i++;
        } else 
            cout<<t[i];
    }
    cout<<endl;
}

signed main() {
    solve();
}
