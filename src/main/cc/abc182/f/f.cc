
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* To make y possible we need to pay exactly x, ret=0, works always.
 * Then, foreach coin we need to pay without that coin.
 * Say without a[0], then it must be y < x+a[1], because we will get
 * the return in a[0] coins (note that a[0] might be !=1)
 *
 * Then without a[1], eventually we want to overpay x+n*a[1], where n is 
 * the factor from a[1] to a[2]
 *
 * Its all about these factors...
 * So, possible y are
 * ...
 * We need to fix the coin notused for pay.
 * What about notusing two coins with pay?
 * We can allways use a single coin with a[j]>=x since the return will be some
 * smaller coins.
 *
 * ...
 *
 */
void solve() {
    cini(n);
    cini(x);
    cinai(a,n);
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
