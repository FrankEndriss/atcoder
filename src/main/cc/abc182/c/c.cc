
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* ans=-1, 0,1 or 2
 */
void solve() {
    cini(n);

    vi d(3);
    int sum=0;
    int cnt=0;
    while(n) {
        int dig=n%10;
        d[dig%3]++;
        sum+=dig%3;
        cnt++;
        n/=10;
    }
    sum%=3;

    if(sum==0) {
        cout<<0<<endl;
    } else if(sum==1) {
        if(d[1]>0) {
            if(cnt>1)
                cout<<1<<endl;
            else
                cout<<-1<<endl;
        } else {
            if(d[2]>=2) {
                if(cnt>2)
                    cout<<2<<endl;
                else
                    cout<<-1<<endl;
            } else {
                cout<<-1<<endl;
            }
        }
    } else if(sum==2) {
        if(d[2]>0 && cnt>1)
            cout<<1<<endl;
        else if(d[1]>1 && cnt>2)
            cout<<2<<endl;
        else
            cout<<-1<<endl;
    } else
        assert(false);

    return;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
