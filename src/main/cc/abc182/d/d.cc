
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * We need to maintain the biggest right move relative to the starting
 * position in each cycle.
 * And the ending position after each cycle.
 */
void solve() {
    cini(n);
    cinai(a,n);

    int ans=0;  /* rigthmost position so far */

    int bgr=0;  /* biggest rightshift in cycle so far */
    int pos=0;  /* pos after last cycle */
    int sum=0;  /* sum of cylce */

    for(int i=0; i<n; i++) {
        ans=max(ans, bgr+pos);

        int pos1=pos+sum+a[i];
        bgr=max(bgr, pos1-pos);
        pos=pos1;
        sum+=a[i];
        ans=max(ans, pos1);
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
