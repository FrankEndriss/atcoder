
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to find foreach bulb the leftnearsest and rightnearest blocks.
 * But what about overcount?
 *
 * We count to N and W up to next bulb or block.
 * We count to S and E only if the next block is more near than the next bulb.
 *
 * fu.. This does not work, consider horizontal illuminated fields
 * and vertical ones. They intersect!
 *
 * Can we maintain segment trees and do range updates?
 *
 * -> Consider all four direction separate.
 */
void solve() {
    cini(h);
    cini(w);
    cini(n);
    cini(m);

    vvb bu(h, vb(w));
    vvb bl(h, vb(w));
    for(int i=0; i<n; i++)  {
        cini(r); r--;
        cini(c); c--;
        bu[r][c]=true;
    }
    for(int i=0; i<m; i++)  {
        cini(r); r--;
        cini(c); c--;
        bl[r][c]=true;
    }

    vvb ans(h, vb(w));
        /* dir E */
    for(int i=0; i<h; i++) {
        int on=false;
        for(int j=0; j<w; j++) {
            if(bu[i][j])
                on=true;
            else if(bl[i][j])
                on=false;

            if(on)
                ans[i][j]=true;
        }
    }
        /* dir W */
    for(int i=0; i<h; i++) {
        int on=false;
        for(int j=w-1; j>=0; j--) {
            if(bu[i][j])
                on=true;
            else if(bl[i][j])
                on=false;

            if(on)
                ans[i][j]=true;
        }
    }

        /* dir S */
    for(int j=0; j<w; j++) {
        int on=false;
        for(int i=0; i<h; i++) {
            if(bu[i][j])
                on=true;
            else if(bl[i][j])
                on=false;

            if(on)
                ans[i][j]=true;
        }
    }

        /* dir N */
    for(int j=0; j<w; j++) {
        int on=false;
        for(int i=h-1; i>=0; i--) {
            if(bu[i][j])
                on=true;
            else if(bl[i][j])
                on=false;

            if(on)
                ans[i][j]=true;
        }
    }

    int ansv=0;
    for(int i=0; i<h; i++) 
        for(int j=0; j<w; j++)  {
            if(ans[i][j])
                ansv++;
        }

    cout<<ansv<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
