/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

int GetCeilIndex(vi& arr, vector<int>& T, int l, int r,
                int key)
{
    while (r - l > 1) {
        int m = l + (r - l) / 2;
        if (arr[T[m]] >= key)
            r = m;
        else
            l = m;
    }

    return r;
}

void lis(vi& arr, int n, vi& ret)
{
    vector<int> tailIndices(n, 0); // Initialized with 0
    vector<int> prevIndices(n, -1); // initialized with -1

    int len = 1; // it will always point to empty location
    for (int i = 1; i < n; i++) {
//cout<<"i="<<i<<endl;
        if (arr[i] < arr[tailIndices[0]]) {
//cout<<"c1"<<endl;
            tailIndices[0] = i;
        }
        else if (arr[i] > arr[tailIndices[len - 1]]) {
//cout<<"c2"<<endl;

            prevIndices[i] = tailIndices[len - 1];
            tailIndices[len++] = i;
        }
        else {
            int pos = GetCeilIndex(arr, tailIndices, -1,
                                   len - 1, arr[i]);
//cout<<"c3, pos="<<pos<<endl;
            if(arr[i]==tailIndices[pos])
                continue;

            prevIndices[i] = tailIndices[pos - 1];
            tailIndices[pos] = i;
        }
    }

    //cout << "LIS of given input" << endl;
    for (int i = tailIndices[len - 1]; i >= 0; i = prevIndices[i])
        ret.push_back(i);
        //cout << arr[i] << " ";
    //cout << endl;

    //return len;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vi a(n);
    fori(n) {
        cin>>a[i];
    }

    int ans=0;
    int asz=a.size();
    while(asz>0) {
        vi dp;
        lis(a, asz, dp);
        asz-=dp.size();
        reverse(dp.begin(), dp.end());
        vi b;
        int idx=0;
/*
        for(int i=0; i<a.size(); i++)  {
            if(idx<dp.size() && i==dp[idx]) {
                idx++;
                continue;
            }
            b.push_back(a[i]);
        }
        swap(a, b);
*/
        ans++;
    }
    cout<<ans<<endl;
}

