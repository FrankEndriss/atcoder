/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vi a(n+1);
    vi ans(n+1);
    int ansup=0; // balls in upper half
    for(int i=1; i<=n; i++)  {
        cin>>a[i];
        if(i>n/2 && a[i]==1) {
            ans[i]=1;
            ansup++;
        }
    }

/* boxes >n/2 can contain all a ball or all no ball, does not matter.
 * Because there is exactly that one box with a multiple of i.
 * So one can flex the total number of balls.
 */
    
    for(int i=n/2; i>=1; i--) {
        int cnt=0;
        for(int j=i*2; j<=n; j+=i) 
            if(ans[j]==1)
                cnt++;
        if(cnt&1 && a[i]==1)
        ans[i]=(cnt&1) ^ a[i];
        if(ans[i]==1)
            ansup++;
    }

    cout<<ansup<<endl;
    for(int i=1; i<=n; i++)
        if(ans[i]==1)
            cout<<i<<" ";
    cout<<endl;
}

