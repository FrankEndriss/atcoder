/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/string>
using namespace atcoder;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

char cswap(char dp) {
    if(dp=='d')
        return 'p';
    else if(dp=='p')
        return 'd';
    else
        assert(false);
}
/**
 * We want at most possible 'd' in the beginning.
 * So, find longest prefix of 'ddd...d', let next position be L.
 * Then find some R so that most possible more 'd's are created.
 * That position is at the end of the longest subseq consisting
 * only of 'p'.
 *
 * ...But, what if there is more than one position with same len
 * of consecutive 'p's?
 *
 * So we want the lex max postfix of the reversed right half of
 * the string.
 *
 * Why WA?
 * Some edgecase?
 */
void solve() {
    cini(n);
    cins(s);

    int L=0;    /* first 'p' in s[] */
    while(L<n) {
        if(s[L]!='d')
            break;
        L++;
    }
    //cerr<<"s="<<s<<" L="<<L<<endl;

    if(L==n) {
        cout<<s<<endl;
        return;
    }
    string t=s.substr(L);
    reverse(all(t));

    /* now find the lex biggest postfix of t 
     * should work with suffix array. Didnt do that for ages...
     **/
    vector<signed> sa=suffix_array(t);
    int R=n-sa.back()-1;
    //cerr<<"t="<<t<<" sa.back()="<<sa.back()<<" R="<<R<<endl;

    while(L<=R) {
        s[L]=cswap(s[L]);
        if(R>L) {
            s[R]=cswap(s[R]);
            swap(s[L],s[R]);
        }
        L++;
        R--;
    }

    cout<<s<<endl;

}

signed main() {
    solve();
}
