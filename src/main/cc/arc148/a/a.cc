/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that the elements a[i]<m stay unchanged.
 * ...number theory... thanks a lot.
 *
 * Note that ans is allways eq 1 or 2 since we can make
 * it eq 2 by choosing m=2.
 * ...
 * Its not trivial.
 * Consider all numbers to be 17*y+5 for some y.
 * For m=17 we get all numbers eq 5, so ans=1
 * How to find that?
 *
 * Choose two smallest numbers, and check all possible remainders?
 * But that would TLE...isnt it?
 *
 * ...Does not work, TLE. 
 * idk :/
 */
void solve() {
    cini(n);

    set<int> a;
    for(int i=0; i<n; i++) {
        cini(aux);
        a.insert(aux);
    }

    auto it=a.begin();
    int a0=*it;
    it++;
    if(it==a.end()) {
        cout<<1<<endl;
        return;
    }
    const int a1=*it;
    const int aX=*a.rbegin();
    //const int N=5e7;
    for(int i=1; i*i<=aX; i++) {
        for(int j : {
                    i, a0/i
                }) {
            if(j<2)
                continue;

            const int v=a0%j;
            bool ok=true;
            for(int aa : a) {
                if(aa%j!=v) {
                    ok=false;
                    break;
                }
            }

            if(ok) {
                //cerr<<"j="<<j<<endl;
                cout<<1<<endl;
                return;
            }
        }
    }
    for(int j=aX-a0; j>a0; j-=a0) {
            if(j<2)
                continue;

            const int v=a0%j;
            bool ok=true;
            for(int aa : a) {
                if(aa%j!=v) {
                    ok=false;
                    break;
                }
            }

            if(ok) {
                //cerr<<"j="<<j<<endl;
                cout<<1<<endl;
                return;
            }
    }
    cout<<2<<endl;
}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
    //cout<<"Case #"<<i<<": ";
    solve();
//   }
}
