/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Check each possible ans, that is each value in compressed w[]
 *
 * Why WA?
 * sic, its real :/
 */
const int INF=2e9+7;
void solve() {
    cini(n);
    cins(s);    /* 0=child, 1=adult */
    assert(s.size()==n);
    cinai(w,n);
    for(int i=0; i<n; i++)
        w[i]*=2;

    vi c;
    vi a;
    for(int i=0; i<n; i++)
        if(s[i]=='0')
            c.push_back(w[i]);
        else
            a.push_back(-w[i]);

    sort(all(c));
    sort(all(a));

    w.push_back(-1);
    w.push_back(INF);
    int ans=0;

    for(int xx : w) {
        for(int x : {
                    xx-1, xx, xx+1
                }) {
            if(x<0)
                continue;
            auto it=lower_bound(all(c), x);
            int lcnt=distance(c.begin(), it);
            it=lower_bound(all(a), -x);
            lcnt+=distance(a.begin(), it);
            ans=max(ans, lcnt);
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}
