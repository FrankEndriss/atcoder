/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Greedy
 * We buy as much digits as possible,
 * and within this, the biggest ones possible.
 * Where we buy the bigger ones first.
 */
void solve() {
    cini(n);
    cinai(c,9);

    int mi=*min_element(all(c));
    int miidx=-1;
    //cerr<<"mi="<<mi<<" miidx="<<miidx<<endl;
    for(int i=0; i<9; i++) {
        if(mi==c[i])
            miidx=i;
        c[i]-=mi;
    }
    assert(mi>0);

    if(mi>n) {
        cout<<0<<endl;
        return;
    }

    int cnt=n/mi;
    int r=n-(cnt*mi);   /* remaining money after buying chapest digits */
    for(int i=8; cnt>0 && r>0 && i>miidx; i--) {
        /* try to find a better digits than the cheapest */
        int lcnt=min(cnt, r/c[i]);
        for(int j=0; j<lcnt; j++)
            cout<<i+1;
        cnt-=lcnt;
        r-=c[i]*lcnt;
    }

    for(int i=0; i<cnt; i++)
        cout<<miidx+1;
    cout<<endl;
}

signed main() {
    solve();
}
