/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
//#include <atcoder/dsu>
//using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Find the smallest factor S, so that there exists a 
 * trampoline that reaches all others with that factor.
 * Precals max dist for all trampolines,
 * and for that the min necessary factor.
 * Why so stupid problem?
 * ...
 * Its other problem.
 * We search that smallest distance d, so that all trampoliens
 * for a single component if all edges smeq that dist.
 * So use binary search.
 * ...But we cannot use dsu, but need to use dfs to check for
 * connectivity.
 *
 * What wrong here?
 * Maybe simply to stupid to implement it :/
 * Overflow?
 */
const int INF=8e9+7;
void solve() {
    cini(n);
    vi x(n);
    vi y(n);
    vi p(n);
    for(int i=0; i<n; i++)
        cin>>x[i]>>y[i]>>p[i];

    int l=0;
    int r=INF;
    while(l+1<r) {
        const int S=(l+r)/2;
        //cerr<<"l="<<l<<" r="<<r<<" S="<<S<<endl;

        vvi adj(n);
        for(int i=0; i<n; i++) 
            for(int j=0; j<n; j++)  {
                if(i==j)
                    continue;

                int dist=abs(x[i]-x[j])+abs(y[i]-y[j]);
                if(dist<=S*p[i])
                    adj[i].push_back(j);
            }

        vb vis(n);
        int cnt=0;
        function<void(int)> dfs=[&](int v) {
            if(vis[v])
                return;

            vis[v]=true;
            cnt++;
            for(int chl : adj[v]) 
                dfs(chl);
        };

        bool done=false;
        for(int i=0; !done && i<n; i++) {
            cnt=0;
            for(int j=0; j<n; j++) 
                vis[j]=false;
            dfs(i);
            if(cnt==n) {
                r=S;
                done=true;
            }
        }
        if(!done)
            l=S;
    }
    //cerr<<"ans, l="<<l<<" r="<<r<<endl;
    cout<<r<<endl;
}

signed main() {
        solve();
}
