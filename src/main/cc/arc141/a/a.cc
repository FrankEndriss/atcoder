/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * len of n==d, and divisors of d.
 *
 * each divisor is a possible len of m.
 * So use for m the first len(m) digits
 * of n, and repeat.
 * Also, use the first len(m) digits, and
 * that number minus 1, repeat.
 * One of these numbers is the biggest possible one.
 *
 * But not that the periodic number can also be 1 digit shorter.
 */
void solve() {
    cini(n);
    vi a;
    int nn=n;
    while(nn) {
        a.push_back(nn%10);
        nn/=10;
    }

    vi d;
    for(int i=1; i<=a.size()/2; i++) 
        if(a.size()%i==0)
            d.push_back(i);

    reverse(all(a));
    vi ten(19);
    ten[0]=1;
    for(int i=1; i<19; i++) 
        ten[i]=ten[i-1]*10;

    int ans=0;
    for(int len : d) {
        int num1=0;
        for(int j=0; j<len; j++) {
            num1*=10;
            num1+=a[j];
        }
        int num2=num1-1;

        int ans1=0;
        int ans2=0;
        for(int j=0; j<a.size()/len; j++) {
            ans1*=ten[len];
            ans2*=ten[len];
            ans1+=num1;
            ans2+=num2;
        }
        if(ans1<=n)
            ans=max(ans, ans1);
        assert(ans2<n);
        if(ans2<=n)
            ans=max(ans, ans2);
    }

    int extra=0;
    for(int i=0; i+1<a.size(); i++) {
        extra*=10;
        extra+=9;
    }
    ans=max(ans, extra);
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
