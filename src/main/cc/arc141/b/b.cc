/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Stricly increasing a[], a[i]<=M
 * Also strictly increasing b[]
 *
 * Consider highest bit in b[i].
 * Some a[i] must have set that bit. Actually at most on, since
 * if there would be 3 of them, the sum after 2 would not
 * include that bit, which would make b[j]<b[j-1], not possible.
 *
 * So each a has a new bit set, which amke
 * ans=0 if 1<<N > M
 *
 * Then, second part, how to calc the number of possible seqs?
 *
 * Let MM be number of bits in M.
 * Each of the N a[i] has a distinct highest bit of MM bits set.
 * And the number of possible numbers is 1<<B, or for the highest bit, M - 1<<(MM-1).
 *
 * How to calc the number?
 * Consider N=30, M=aprox 2<<59+123123123
 * So there are nCr(60,30) combinations of bits we can choose, which is a lot.
 *
 * DP:
 * dp[j][k]=number of possibilities at current pos to set highest bit=1<<j while
 *              having set k numbers.
 */
using mint=modint998244353;
void solve() {
    cini(n);
    cini(m);

    int B=0;    /* 1<<B == MSB(m) */
    int one=1;
    while(one*2<=m)  {
        B++;
        one=one*2;
    }

    if(n>B+1) {
        cout<<0<<endl;
        return;
    }


    vector<vector<mint>> dp(B+1, vector<mint>(n+1));

    for(int j=0; j<=B; j++)
        if(j==B)
            dp[j][1]=m-(1LL<<B)+1;
        else
            dp[j][1]=(1LL<<j);

    vector<vector<mint>> dpIni=dp;

    for(int i=1; i<n; i++) {
        vector<vector<mint>> dp0(B+1, vector<mint>(n+1));

        for(int k=1; k<=n; k++) {
            mint sum=0;
            for(int j=0; j<=B; j++)  {
                dp0[j][k]=sum*dpIni[j][1];
                sum+=dp[j][k-1];
            }
        }
        dp=dp0;
    }

    mint ans=0;
    for(size_t j=0; j<dp.size(); j++)
        ans+=dp[j][n];

    cout<<ans.val()<<endl;

}

signed main() {
    solve();
}
