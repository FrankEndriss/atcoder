/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * What about non distinct a[i]==a[j]?
 * Can we simply remove them?
 * No.
 *
 * We have to create something like a LIS on a[]
 * We need to know all positions that belong to same
 * component, that is have a common smallest a[i],
 * and are all multiple of it.
 *
 * Find the/one smallest element of each component, call them roots.
 * Then, on iterating a[], find number of smallest elements 
 * minus divisors of a[i] plus one.
 *
 * ...no :/
 */
void solve() {
    cini(n);
    cini(m);
    vi a(n);

    for(int i=0; i<n; i++)
        cin>>a[i];

    vi b=a;
    sort(all(b));

    vi posd(m*2+1); /* posd[k]=number of roots of k */
    vb isroot(m*2+1);
    vb vis(m*2+1);
    int cnt=0;  /* count of distinct roots in a[] */
    for(int i=0; i<n; i++) {
        if(vis[b[i]])
            continue;

        cnt++;
        isroot[b[i]]=true;

        for(int k=b[i]; k<=2*m; k+=b[i]) {
            vis[k]=true;
            posd[k]++;
        }
    }

    for(int i=0; i<n; i++) {
        if(isroot[a[i]]) {
            if(cnt>=m)
                cout<<"Yes"<<endl;
            else
                cout<<"No"<<endl;
        } else {
            int ans=cnt-posd[a[i]]+1;
            if(ans>=m)
                cout<<"Yes"<<endl;
            else
                cout<<"No"<<endl;
        }
    }

}

signed main() {
        solve();
}
