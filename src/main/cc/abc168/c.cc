/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    std::ios_base::sync_with_stdio(false); std::cin.tie(0);
    std::cout << std::fixed << std::setprecision(12);
    return true;
}();

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;

/* calc positions of hands by angle */
void solve() {
    cini(a); cini(b);
    cini(h);
    cini(m);
    h%=12;
    m%=60;

    double angH=(2*PI*h)/12 + (2*PI*m)/(12*60);
    double angM=(2*PI*m)/60;

    double xH=sin(angH)*a;
    double yH=cos(angH)*a;
    double xM=sin(angM)*b;
    double yM=cos(angM)*b;

    double dist=hypot(xH-xM, yH-yM);
    cout<<dist<<endl;
}

signed main() {
    solve();
}

// DO NOT JUMP BETWEEN PROBLEMS
