/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    std::ios_base::sync_with_stdio(false); std::cin.tie(0);
    std::cout << std::fixed << std::setprecision(12);
    return true;
}();

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;

/* BFS + backtrack.
 * ans[i] indicates
 * one next room on shortest
 * path to first room. */
void solve() {
    cini(n);
    cini(m);
    vvi adj(n);

    for(int i=0; i<m; i++) {
        cini(a);
        cini(b);
        a--;
        b--;
        adj[a].push_back(b);
        adj[b].push_back(a);
    }
    
    vector<pii> dp(n, { -1, 0});
    queue<int> q;
    q.push(0);
    dp[0]={0,0};
    while(q.size()) {
        int node=q.front();
        q.pop();
        for(int chl : adj[node]) {
            if(dp[chl].first<0) {
                dp[chl].first=dp[node].first+1;
                dp[chl].second=node;
                q.push(chl);
            }
        }
    }

    bool posi=true;
    for(int i=0; i<n; i++) {
        if(dp[i].first<0) {
            cout<<"No"<<endl;
            return;
        }
    }
    
    cout<<"Yes"<<endl;
    for(int i=1; i<n; i++)
        cout<<dp[i].second+1<<endl;
}

signed main() {
    solve();
}

// DO NOT JUMP BETWEEN PROBLEMS
