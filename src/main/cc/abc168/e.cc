/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    std::ios_base::sync_with_stdio(false); std::cin.tie(0);
    std::cout << std::fixed << std::setprecision(12);
    return true;
}();

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;

const int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

pii norm(pii p) {
    if(p.first==0 && p.second==0)
        return p;
    else if(p.first==0) {
        p.second=1;
        return p;
    } else if(p.second==0) {
        p.first=1;
        return p;
    } else {
        int g=__gcd(abs(p.first), abs(p.second));
        p.first/=g;
        p.second/=g;
        if(p.first<0) {
            p.first=-p.first;
            p.second=-p.second;
        }
        return p;
    }
    assert(false);
}

int tPow(int i) {
    assert(i>=0);
    if(i==0)
        return 1;
    return toPower(2,i);
}

/* find pairs with
 * p1.a*p2.a==-p1.b*p2.b
 * p1.a=(-p1.b*p2.b)/p2.a;
 * p1.a/-p1.b=p2.b/p2.a;
 *
 * foreach sardine
 * combis=toPower(2, n-opposite);
 */
void solve() {
    cini(n);

    vector<pii> p(n);
    map<pii,int> f;

    for(int i=0; i<n; i++) {
        cin>>p[i].first>>p[i].second;
        p[i]=norm(p[i]);
        f[p[i]]++;
    }

    int ans=0;
    for(pii ab : p) {
        //ans++;
        f[ab]--;
        n--;

        pii r={ -ab.second, ab.first};
        r=norm(r);

        int g=tPow(n-f[r]);
//cerr<<"gaus: "<<g<<" f[r]="<<f[r]<<endl;
        ans+=g;
        ans%=MOD;
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// DO NOT JUMP BETWEEN PROBLEMS
