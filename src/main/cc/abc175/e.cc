
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

#define endl "\n"

/* At most three _per row_
 */
void solve() {
    cini(r);
    cini(c);
    cini(k);

    vvi v(r+1, vi(c+1, 0));
    for(int i=0; i<k; i++) {
        cini(rr);
        cini(cc);
        cin>>v[rr][cc];
    }

    vvvi dp(4, vvi(r+1, vi(c+1)));

    for(int i=1; i<=r; i++) {
        for(int j=1; j<=c; j++) {
            for(int k=0; k<4; k++)
                dp[0][i][j]=max(dp[0][i][j], dp[k][i-1][j]);

            dp[1][i][j]=dp[0][i][j]+v[i][j];

            for(k=1; k<4; k++) {
                dp[k][i][j]=max(dp[k][i][j], dp[k][i][j-1]);
                dp[k][i][j]=max(dp[k][i][j], dp[k-1][i][j-1]+v[i][j]);
            }
        }
    }

    int ans=0;
    for(int k=0; k<4; k++) 
        ans=max(ans, dp[k][r][c]);
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
