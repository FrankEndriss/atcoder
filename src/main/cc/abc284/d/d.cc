
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using ull = unsigned long long;

const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=200000;
vector<ull> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}
();

/**
 * Let N be third root of 9e18, then
 * P or Q must be smaller than N, so
 * Consider p=1...N
 * then q=1....N
 */
const int N=pow(9e18, 1.0/3)+3;
void solve() {
    ull n;
    cin>>n;

    //cerr<<"N="<<N<<" pr[].back()="<<pr.back()<<endl;
    assert(pr.back()>N);

    for(int i=0; pr[i]<=N && pr[i]*pr[i]<n; i++) {
        if(n%(pr[i]*pr[i])==0) {
            cout<<pr[i]<<" "<<n/(pr[i]*pr[i])<<endl;
            return;
        }

        if(n%pr[i]==0) {
            ull ss=n/pr[i];
            ull s=sqrt(ss);
            while(s*s<ss)
                s++;
            cout<<s<<" "<<pr[i]<<endl;
            return;
        }
    }

    assert(false);

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
