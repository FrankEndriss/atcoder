
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Size of n is obviously n.
 * So, consider some i
 *
 * The first i chars of t must repeat at position i+n backwards,
 * and the last n-i chars of t must repeat backwards at 
 * position starting at i+n-i.
 *
 * What if first and last letter of S are same?
 * -> Then first and last letter of T are same.
 *    How to remove?
 *
 * ***
 * Find max i for the prefix,
 * Find max n-i for the postfix.
 * if both...
 * ****
 * Consider first an last char of t.
 * If they differ, they are the first ans last letter of S, else
 * if the same, the can be first and last, but also i can be 0.
 * ...
 * *******
 * Binary search?
 * How to check if i is to big?
 * *****
 * Consider dp from back of T, foreach position find
 * the length of the prefix of T that is reversed from some position.
 */
void solve() {
    cini(n);
    cins(t);    /* t.size()==2*n */
}

signed main() {
        solve();
}
