/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* First wins if xorsum of piles is non-zero.
 * So we would like to make it zero
 * by moving some stones from first to second pile.
 */
void solve() {
    cini(n);
    cinai(a,n);
    int x=0;
    for(int i=2; i<n; i++)
        x^=a[i];

/* now we need to find a number ans in range (0,a[0])
 * with x^(a[0]-ans)^(a[1]+ans)==0
 *
 * if b(x,n) be true if nth bit is set in x
 * So, first bit of ans?
 * if(b(x,1)) that bit must be set in in a01 part, too. 
 * So of both halfes it must be set in exactly one of them.
 * Since it is flipped in both or none, parity of a[0]
 * must not equal parity of a[1].
 * if(!b(x,n)) then oppisite, 
 * parity of a[0] _must_ equal parity of a[1].
 *
 * so, somehow we can combine higher bits with lower bits,
 * since if a bit is set in a[0] and a[1] and ans, it gets
 * removed in a[0], but moved to higher position in a[1].
 * But we need to consider the higher bit in a[1]...
 */
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
