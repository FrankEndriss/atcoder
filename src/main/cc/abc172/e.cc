/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

/* list of distinct numbers of len n with m different numers 
 * So, given a list a[] we can use for
 * b[0] m-1 dist numbers, 
 * b[1] m-1 dist numbers,...
 * b[2] m-2 ...
 * b[n-1] m-n numbers
 * And we can use m different numbers for a[0]
 */
void solve() {
    cini(n);
    cini(m);

    int ans=1; /* number of distinct a[] */
    for(int i=0; i<n; i++) {
        ans=(ans*(m-i))%MOD;
    }

cerr<<"ans1="<<ans<<endl;
/* for every a[] we want to find the number of b[] with
 *   a[0]!=b[0]
 * + a[0]==b[0] but a[1]!=b[1]
 * + a[0]==b[0] and a[1]==b[1] but a[2]!=b[2]
 * ...
 * Other stategy:
 * We calc for i=1..n the number of distinct b[] with i same as a[].
 * ... read a book, man!
 */

    ans=(ans*(m-i))%MOD;    /* number of distinct b[] for every single a[i] */
    for(int i=1; i<n; i++) {
        ans=(ans*toPower(m-i, i))%MOD;
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
