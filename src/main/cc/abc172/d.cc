/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Euler totiem function, number of coprimes of n in (1,n)
 *  * see https://cp-algorithms.com/algebra/phi-function.html
 *   **/
int phi(int n) {
    int result = n;
    for (int i = 2; i * i <= n; i++) {
        if(n % i == 0) {
            while(n % i == 0)
                n /= i;
            result -= result / i;
        }
    }
    if(n > 1)
        result -= result / n;
    return result;
}

/* We need to find the sum of...
 * number of divisors which is eulers totiem
 * multiplied by that number
 * for all numbers up to x.
 */
void solve() {
    cini(n);

    vi tot(n+1, 1);
    tot[0]=0;
/*
    for(int i=1; i<=n; i++) {
        tot[i]=phi(i);
    }
*/
    for(int i=2; i<=n; i++) 
        for(int j=i; j<=n; j+=i)
            tot[j]++;

    vi dp(n+1);
    for(int i=1; i<=n; i++) 
        dp[i]=dp[i-1]+i*tot[i];

    cout<<dp[n]<<endl;
    
    
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
