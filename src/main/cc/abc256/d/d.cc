/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

//const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
//#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
//using pdd= pair<ld, ld>;
//using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using ld=int;

/**
 * Combine overlapping intervals.
 * What means 'real' here in the input description.
 * Is the input decimal numbers, it does not look like it?
 *
 * ...What a sic statement... 'real numbers'. Why?
 */
void solve() {
    cini(n);
    set<pair<ld,ld>> lr;
    //set<pair<ld,ld>> rl;

    for(int i=0; i<n; i++) {
        cini(a);
        cini(b);
        lr.emplace(a,b);
        //rl.emplace(b,a);
    }

    vector<pair<ld,ld>> ans;
    while(lr.size()) {
        auto it=lr.begin();
        ld left=it->first;
        ld rigt=it->second;
        while(true) {
            it=lr.erase(it);
            if(it==lr.end())
                break;
            if(it->first>rigt)
                break;

            rigt=max(rigt,it->second);
        }
        ans.emplace_back(left,rigt);
    } 

    for(auto p : ans)
        cout<<p.first<<" "<<p.second<<endl;

}

signed main() {
        solve();
}
