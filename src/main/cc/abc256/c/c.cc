/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Brute force?
 * Note that the third numbers are determined by the first two numbers.
 * So iterate the first four numbers and simply count.
 *
 *
 */
int N=30;
void solve() {
    cinai(h,3);
    cinai(w,3);

    vvi a(3, vi(3));
    int ans=0;
    for(a[0][0]=1; a[0][0]<N; a[0][0]++)
        for(a[0][1]=1; a[0][1]<N; a[0][1]++)
            for(a[1][0]=1; a[1][0]<N; a[1][0]++)
                for(a[1][1]=1; a[1][1]<N; a[1][1]++)  {
                    a[0][2]=h[0]-(a[0][0]+a[0][1]);
                    a[1][2]=h[1]-(a[1][0]+a[1][1]);
                    a[2][0]=w[0]-(a[0][0]+a[1][0]);
                    a[2][1]=w[1]-(a[0][1]+a[1][1]);
                    a[2][2]=w[2]-(a[0][2]+a[1][2]);


                    bool ok=true;
                    for(int i=0; i<3; i++)  {
                        if(a[i][2]<1 || a[2][i]<1)
                            ok=false;
                        if(a[i][0]+a[i][1]+a[i][2]!=h[i])
                            ok=false;
                        if(a[0][i]+a[1][i]+a[2][i]!=w[i])
                            ok=false;
                    }

                    if(ok)
                        ans++;
                }

    cout<<ans<<endl;

}

signed main() {
        solve();
}
