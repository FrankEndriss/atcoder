/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * In every circle we have to disapoint one person.
 * Choose the cheapest.
 * Note that x[] does not simply creates circles at
 * all positions.
 */
void solve() {
    cini(n);
    cinai(x,n);
    cinai(c,n);

    int ans=0;
    vi vis(n);
    for(int i=0; i<n; i++) {
        if(vis[i])
            continue;

        vi comp;
        int v=i;
        int circ=-1;
        vb vis2(n);
        do {
            vis[v]=true;
            vis2[v]=true;
            comp.push_back(v);
            v=x[v]-1;
            if(vis2[v]) {
                circ=v;
            }
        }while(circ<0 && !vis[v]);

        if(circ>=0) {
            int mi=c[circ];
            int j=x[circ]-1;
            while(j!=circ) {
                mi=min(mi, c[j]);
                j=x[j]-1;
            }
            ans+=mi;
        }

    }
    cout<<ans<<endl;

}

signed main() {
        solve();
}
