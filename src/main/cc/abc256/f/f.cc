/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
#include <atcoder/segtree>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using mint=modint998244353;
using S=mint;

S st_op(S a, S b) {
    return a+b;
}

S st_e() {
    return 0;
}

using stree=segtree<S, st_op, st_e>;

/**
 */
void solve() {
    cini(n);
    cini(q);
    cinai(a,n);

    vector<mint> b(n);
    for(int i=0; i<n; i++) 
        b[i]=1;

    vector<mint> c(n+1);
    for(int i=0; i<n; i++) 
        c[i+1]=c[i]+b[i];

    vector<mint> d(n+1);
    for(int i=0; i<n; i++) 
        d[i+1]=d[i]+c[i+1];

    stree seg(n);
    for(int i=0; i<n; i++) 
        seg.set(i,a[i]*d[i]);

    for(int i=0; i<q; i++) {
        cini(t);
        if(t==1) {
            cini(x);
            cini(v);
            seg.set(x-1, d[x-1]*v);
        } else if(t==2) {
            cini(x);
            mint ans=seg.prod(0,x);
            cout<<ans.val()<<endl;
        } else
            assert(false);
    }

}

signed main() {
        solve();
}
