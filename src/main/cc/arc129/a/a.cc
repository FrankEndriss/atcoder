/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider x, no bit set, so x^N=N
 * So x must have at least one bit set,
 * and no higer bit that is not set in N set.
 *
 * Consider the higest bit of N set in x,
 * so all x^N<N
 * Same for all other bits.
 *
 * Then filter all in range l,r
 */
void solve() {
    cini(n);
    cini(l);
    cini(r);

    int ans=0;
    for(int i=0; i<=60; i++) {
        const int b=n&(1LL<<i);
        if(b==0)
            continue;

        int l0=b;
        int r0=b*2-1;

        int a=min(r,r0)-max(l,l0)+1;
        if(a>0)
            ans+=a;
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}
