/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Median
 * Find a point where half of the L are left of the point.
 *
 * How to calc the sum of distances?
 *
 * Maintain a current posiition, and
 * number of segs left
 * number of segs right
 * number of segs matching
 *
 * Then add a segment, and move position so that
 * left eq right or one nexto that.
 *
 * Maintain the left sorted by right border,
 * the right by left border,
 * and the matching by both...somehow.
 *
 *
 * ...Wow, implementation nightmare :/
 * Compress coordinates and go linear?
 * -> no, does not work since that changes the dists.
 *
 * So we need to consider positions, and move between positions.
 * Each l,r is a position.
 *
 * Note that we never want to move by more than one position
 * since we add only one segment.
 * So simply allways try both directions and choose best ans.
 * ****
 *
 * Fuck it... after like one and a half hours:
 * It is max(dist), NOT sum(dist) :/
 *
 * So we maintain the leftmost R and the right most L,
 * ans=(dist+1)/2
 */
const int INF=1e9+7;
void solve() {
    cini(n);

    int minR=INF;
    int maxL=-1;
    for(int i=0; i<n; i++) {
        cini(l);
        cini(r);
        minR=min(minR, r);
        maxL=max(maxL, l);
        int ans=max(0LL, (maxL-minR+1)/2);
        cout<<ans<<endl;
    }
    return;

    /* below here dead code :/ */
    multiset<pii> L;
    multiset<pii> R;
    multiset<pii> Ml;
    multiset<pii> Mr;

    int pos=-1;  /* current position */
    int ans=0;
    set<int> spos;  /* set of possible positions */

    for(int i=0; i<n; i++)  {
        int l,r;
        cin>>l>>r;
        if(pos<0)
            pos=l;

        spos.insert(l);
        spos.insert(r);
        auto itpos=spos.find(pos);
        assert(itpos!=spos.end());

        if(l>pos) {
            R.emplace(l,r);
            ans+=l-pos;
        } else if(r<pos) {
            L.emplace(r,l);
            ans+=pos-r;
        } else {
            Ml.emplace(l,r);
            Mr.emplace(r,l);
        }

        int ansL=ans;
        if(itpos!=spos.begin()) {
             itpos--;
            /* we might want to move cur one to the left. 
             * That means:
             * all R increase ans by posdiff
             * all L decrease ans by posdiff
             * all M with l==pos increase ans by 1 and move to R
             * all L with r==nextPos move to M
             **/
             int d=pos-*itpos;
             ansL=ans+R.size()*d-L.size()*d;
             auto it=Ml.lower_bound({pos,-1});
             while((it!=Ml.end()) && (ansL<ans) && (it->first==pos)) {
                 ansL+=d;
                 it++;
             }
             itpos++;
        } 

        int ansR=ans;
        itpos++;
        if(itpos!=spos.end()) {
            /* we might want to move cur one to the right. 
             * That means:
             * all L increase ans by 1
             * all M with r==pos increase ans by 1 and move to L
             * all R decrease ans by 1
             * all R with l==nextPos move to M
             **/
             int nextPos=*itpos;
             int d=nextPos-pos;
             ansR=ans-R.size()*d+L.size()*d;
             auto it=Mr.lower_bound({pos,-1});
             while((it!=Mr.end()) && (ansR<ans) && (it->first==pos)) {
                 ansR+=d;
                 it++;
             }
        }
        itpos--;

        if(ansL<ans) {
            itpos--;
            assert(ansL<ansR);
            auto it=Ml.lower_bound({*itpos,-1});
            while(it!=Ml.end() && it->first==*itpos) {
                R.insert(*it);
                Mr.erase({it->second,it->first});
                it=Ml.erase(it);
                if(it==Ml.end())
                    break;
                else
                    it++;
            }
            auto it2=L.lower_bound({*itpos,-1});
            while(it2!=L.end() && it2->first==*itpos) {
                Mr.insert(*it2);
                Ml.insert({it2->second,it2->first});
                it2=L.erase(it2);
                if(L.size()>0)
                    it2--;
            }

            ans=ansL;
            pos=*itpos;

        } else if(ansR<ans) {
            itpos++;
            assert(ansR<ansL);
            auto it=Mr.lower_bound({*itpos,-1});
            while(it!=Mr.end() && it->first==*itpos) {
                L.insert(*it);
                Ml.erase({it->second,it->first});
                it=Mr.erase(it);
                if(it==Mr.end())
                    break;
                else
                    it++;
            }
            auto it2=R.lower_bound({*itpos,-1});
            while(it2!=R.end() && it2->first==*itpos) {
                Mr.insert(*it2);
                Ml.insert({it2->second,it2->first});
                it2=R.erase(it2);
            }

            ans=ansR;
            pos=*itpos;
        }

        cout<<ans<<endl;
    }

}

signed main() {
    solve();
}
