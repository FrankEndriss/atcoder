/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int INF=1e18+7;
/* try dijkstra */
void solve() {
    cini(n);
    cini(a);
    cini(b);
    cini(c);
    cini(d);

    map<int,int> dp;
    function<int(int)> rec=[&](int nn) {
        if(nn==0)
            return 0LL;

        auto it=dp.find(nn);
        if(it!=dp.end())
            return it->second;

//        cerr<<"rec(), nn="<<nn<<endl;

        int mi=INF;
        if(nn<=INF/d)
            mi=nn*d;

        //if(nn%5==0)
            mi=min(mi, c+rec(nn/5)+(nn%5)*d);
            if(nn%5!=0)
                mi=min(mi, c+rec(nn/5+1)+(5-nn%5)*d);
        //if(nn%3==0)
            mi=min(mi, b+rec(nn/3)+(nn%3)*d);
            if(nn%3!=0)
                mi=min(mi, b+rec(nn/3+1)+(3-nn%3)*d);
        //if(nn%2==0)
            mi=min(mi, a+rec(nn/2)+(nn%2)*d);
            if(nn%2!=0)
                mi=min(mi, a+rec(nn/2+1)+(2-nn%2)*d);

        dp[nn]=mi;
        return mi;
    };

    int ans=rec(n);
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
