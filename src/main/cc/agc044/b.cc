/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* How does dp change if one goes out?
 * All with a shortest path over that one have
 * a shortest path one smaller.
 * Which ones are these?
 * Can we do updates?
 * Try.
 */
void solve() {
    cini(n);
    cinai(p,n*n);

    vvi dp(n, vi(n)); /* dp[i][j]==number of haters if u use that path */
    for(int i=0; i<n; i++)
        for(int j=0; j<n; j++)
            dp[i][j]=min(i, min(j, min(n-1-i, n-1-j)));

    vvi notvis(n, vi(n,1));

    const vector<pii> d= { { -1, 0}, {1,0}, {0,-1}, {0,1}};
    int ans=0;
    for(int i=0; i<n*n; i++) {
        int r=(p[i]-1)/n;
        int c=(p[i]-1)%n;

        int mi=1e9;
        for(auto d1 : d) {
            int rr=r+d1.first;
            int cc=c+d1.second;
            if(rr>=0 && rr<n && cc>=0 && cc<n)
                mi=min(mi, dp[rr][cc]+notvis[rr][cc]);
            else
                mi=0;
        }
        ans+=mi;
        dp[r][c]=mi;
        notvis[r][c]=0;
//cerr<<"i="<<i<<" r="<<r<<" c="<<c<<" mi="<<mi<<endl;

        queue<pii> q;
        q.emplace(r,c);
        while(q.size()) {
            auto [r0,c0]=q.front();
            q.pop();
            for(auto d1 : d) {
                int rr=r0+d1.first;
                int cc=c0+d1.second;
                if(rr>=0 && rr<n && cc>=0 && cc<n && dp[rr][cc]>dp[r0][c0]+notvis[r0][c0]) {
                    dp[rr][cc]=dp[r0][c0]+notvis[r0][c0];
                    q.emplace(rr,cc);
                }
            }
        }
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}

