
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to count
 * the components if only walk.
 * then find wich components are connected
 * by magic, then find shortest path
 * of inter components.
 */
const int INF=1e9;
void solve() {
    cini(h);
    cini(w);
    cini(ch);
    cini(cw);
    cini(dh);
    cini(dw);
    cinas(s,h);

    ch--;
    cw--;
    dh--;
    dw--;

    vvi dp(h, vi(w, INF));
    dp[ch][cw]=0;

    const vi dx={ 0, 1, 0, -1 };
    const vi dy={ 1, 0, -1, 0 };
    priority_queue<pair<int,pii>> q;
    q.push({0,{ch,cw}});
    while(q.size()) {
        auto [i,j]=q.top().second;
        int prio=-q.top().first;
        q.pop();
        if(prio!=dp[i][j])
            continue;

        for(int ii=0; ii<4; ii++) {
                if(i+dx[ii]>=0 && i+dx[ii]<h && j+dy[ii]>=0 && j+dy[ii]<w && s[i+dx[ii]][j+dy[ii]]=='.' && dp[i][j]<dp[i+dx[ii]][j+dy[ii]]) {
                    dp[i+dx[ii]][j+dy[ii]]=dp[i][j];
                    q.push({-dp[i+dx[ii]][j+dy[ii]],{i+dx[ii],j+dy[ii]}});
                }
        }

        for(int ii=-2; ii<=2; ii++) {
            for(int jj=-2; jj<=2; jj++) {
                if(i+ii>=0 && i+ii<h && j+jj>=0 && j+jj<w && s[i+ii][j+jj]=='.' && dp[i][j]+1<dp[i+ii][j+jj]) {
                    dp[i+ii][j+jj]=dp[i][j]+1;
                    q.push({-dp[i+ii][j+jj],{i+ii,j+jj}});
                }
            }
        }
    }

    if(dp[dh][dw]==INF)
        cout<<-1;
    else
        cout<<dp[dh][dw]<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
