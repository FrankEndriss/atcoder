
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* we want to find the 
 * row+col with most targets in it
 * Somehow we need to 
 * recognize double count the crossing.
 */
void solve() {
    cini(h);
    cini(w);
    cini(m);

    vector<pii> hcnt(h);
    for(int i=0; i<h; i++) 
        hcnt[i].second=i;
    vector<pii> wcnt(w);
    for(int j=0; j<w; j++)
        wcnt[j].second=j;

    map<pii,int> s;
    for(int i=0; i<m; i++) {
        cini(th);
        cini(tw);
        th--;
        tw--;
        s[{th, tw}]++;
        hcnt[th].first++;
        wcnt[tw].first++;
    }
    sort(all(hcnt), greater<pii>());
    sort(all(wcnt), greater<pii>());

    int ans=0;
    for(int i=0; i<h; i++) {
        if(hcnt[i].first<hcnt[0].first)
            break;

        for(int j=0; j<w; j++) {
            if(wcnt[j].first<wcnt[0].first)
                break;

            int lans=hcnt[i].first+wcnt[j].first;
            int dec=s[{hcnt[i].second,wcnt[j].second}];
            ans=max(ans, lans-dec);
            if(dec==0)
                break;
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
