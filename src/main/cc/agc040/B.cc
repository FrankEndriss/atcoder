/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(n);
    vi l(n);
    vi r(n);
    map<int,int> m;
    for(int i=0; i<n; i++) {
        cin>>l[i]>>r[i];
        m[l[i]]+=1;
        m[r[i]+1]-=1;
    }

    vi iid(n);
    iota(iid.begin(), iid.end(), 0);
    sort(iid.begin(), iid.end(), [&](int i1, int i2) {  // by size of interval desc
        return r[i2]-l[i2]<r[i1]-l[i1];
    });

    int biggest=iid[0];
    /* find the one intersecting the lessest with the biggest */
    int miinter=1e9;
    int miidx=-1;
    for(int i=0; i<n; i++) {
        if(i!=biggest) {
            int inter=min(r[biggest], r[i])-max(l[biggest], l[i]);
            if(inter<miinter) {
                miinter=inter;
                miidx=i;
            }
        }
    }

    /* we have two groups, the one of the biggest, and the other.
     * Now put every other interval into one of both groups.
     * Use the one which makes ans less worse.
     */
    pii g1= { l[biggest], r[biggest] };
    pii g2= { l[miidx], r[miidx] };

    for(int i=0; i<n; i++) {
        if(i==biggest || i==miidx)
            continue;

        pii g1N= { max(g1.first, l[i]), min(g1.second, r[i]) };
        pii g2N= { max(g2.first, l[i]), min(g2.second, r[i]) };
        if(g2.first>g2.second || g1.first>g1.second)
            continue;

        if((g1.second-g1.first)-(g1N.second-g1N.first) < (g2.second-g2.first)-(g2N.second-g2N.first)) {
            g1=g1N;
        } else
            g2=g2N;
    }

    int ans1=max(0, g1.second-g1.first+1);
    int ans2=max(0, g2.second-g2.first+1);
    int ans=ans1+ans2;
    cout<<ans<<endl;

}

