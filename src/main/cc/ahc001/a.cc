
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

vi x;
vi y;
vi r;

vi a, b, c, d;      // ans
int parse() {
    cini(n);
    x.resize(n);
    y.resize(n);
    r.resize(n);
    for(int i=0; i<n; i++)
        cin>>x[i]>>y[i]>>r[i];

    a.resize(n);
    b.resize(n);
    c.resize(n);
    d.resize(n);

    return n;
}
void printAns() {
    for(size_t i=0; i<a.size(); i++)
        cout<<a[i]<<" "<<b[i]<<" "<<c[i]<<" "<<d[i]<<endl;
}

/* We want to define rects around some points,
 * because we get points only for that rects including
 * the points.
 *
 * Lets start with n rects of size 0 with centers at
 * the n points.
 * Then extends the rects in all four directions until
 * we get max possible sum of values.
 *
 * By the quadratic nature of the values, it is most time
 * better to fully create a rect instead of creating two halfe ones.
 *
 * Further more, from a heuristical perspective, rects with a big r value
 * eat more space than smaller ones, but do not give more value (max value==1)
 *
 * Since we must create n rects, we must not create a rect
 * covering more than one point (because then two rects would intersect).
 *
 * Test implementation:
 * -sort points top/down, left/right.
 * -foreach point simle greedy find biggest area possible by extending to
 *  left and up, stop at size r[i].
 */
void solve0() {
    const int n=parse();
    vi id(n);
    iota(all(id), 0);
    sort(all(id), [](int i1, int i2) {
        if(x[i1]==x[i2])
            return y[i1]<y[i2];
        else
            return x[i1]<x[i2];
    });


    /* @true if any of the rects 0..i-1 intersects with rect i */
    function<bool(int)> isec=[&](int ii) {
        const int i=id[ii];
        for(int jj=0; jj<ii; jj++) {
            const int j=id[jj];
            if(max(a[i], a[j])<=min(c[i], c[j])) { // x intersects
                if(max(b[i], b[j])<=min(d[i], d[j])) { // y intersects
                    return true;
                }
            }
        }
        return false;
    };



    for(int ii=0; ii<n; ii++) {
        const int i=id[ii];
        int dx=-1;
        int dy=-1;
        c[i]=x[i]+1;
        d[i]=y[i]+1;
        a[i]=x[i];
        b[i]=y[i];
        while((dx!=0 || dy!=0) && (c[i]-a[i])*(d[i]-b[i])<r[i]) {
            a[i]+=dx;
            if(a[i]<0 || isec(ii)) {
                a[i]-=dx;
                dx=0;
            }
            b[i]+=dy;
            if(b[i]<0 || isec(ii)) {
                b[i]-=dy;
                dy=0;
            }
        }
    }

    printAns();
}

const int N=1e4;
void solve1() {
    const int n=parse();
    const int NN=1e7;

    /* true if any other rect intersects with rect i */
    function<bool(int)> isec=[&](int i) {
        for(int j=0; j<n; j++) {
            if(j==i)
                continue;
            if(max(ans[i][0], ans[j][0])<=min(ans[i][2], ans[j][2])) { // x intersects
                if(max(ans[i][1], ans[j][1])<=min(ans[i][3], ans[j][3])) { // y intersects
                    return true;
                }
            }
        }
        return false;
    };

    vvi ans(n, vi(4));
    for(int i=0; i<n; i++) {
        ans[i][0]=x[i];
        ans[i][1]=y[i];
        ans[i][2]=x[i]+1;
        ans[i][3]=y[i]+1;
    }

    const vi dop= { -1, -1, 1, 1};

    for(int j=0; j<NN; j++) {
        const int idx=rand()%n;
        const int op=rand()%4;
        ans[idx][op]+=dop[op];

        if(a[idx][op]<0 || a[idx][o]>=N || isec(idx)) {
            ans[idx][op]-=dop[op];
        }
    }

    printAns();
}

signed main() {
    solve1();
}
