/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/lazysegtree>
using namespace atcoder;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using S=int; /* type of values */
using F=int; /* type of upates */

S st_op(S a, S b) {
    return min(a,b);
}

/* This is the neutral element */
const S INF=1e9;
S st_e() {
    return INF;
}

/* This applies an update to some value. */
S st_mapping(F f, S x) {
    return f+x;
}

/* This combines two updates.  */
F st_composition(F f, F g) {
    return f+g;
}

/* This is the neutral update.
 * It is similar to st_e() in some sense.  */
F st_id() {
    return 0;
}

using stree=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;

/**
 * consider dp[i] to be the number of notclosed open brackets, if
 * negative notclosed closed brackets.
 * Maintain them in an segtree, query min on that tree.
 * Then do range updates on the tree, ans query the min
 * values on the l,r segements, compare the min value against the
 * prefix value.
 */
void solve() {
    cini(n);
    cini(q);
    cins(s);

    vi ini(n+2);

    for(int i=0; i<n; i++)  {
        if(s[i]=='(')
            ini[i+1]=ini[i]+1;
        else
            ini[i+1]=ini[i]-1;
    }

    stree dp(ini);

    for(int i=0; i<q; i++) {
        cini(t);
        cini(l);
        cini(r);

        if(t==1) {
            /* swap l-1 and r-2, that will increase/decrease all values in that interval */
            if(s[l-1]!=s[r-1]) {
                int inc=2;
                if(s[l-1]=='(') {
                    inc=-2;
                }
                dp.apply(l-1,r,inc);
                swap(s[l-1], s[r-1]);
            }

        } else if(t==2) {
            int mi=dp.prod(l,r)-dp.get(l-1);
            if(mi>=0 && dp.get(l-1)==dp.get(r)) {
                cout<<"Yes"<<endl;
            } else
                cout<<"No"<<endl;
        } else
            assert(false);
    }
}

signed main() {
    solve();
}
