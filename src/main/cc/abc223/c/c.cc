/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Find points in time where elements start to burn
 * from left and from right.
 * Find the rightmost segment where l[i]<=r[i-1]
 *
 * Kind of annoying off-by-one frickling...
 *
 * Try it using kind of two-pointer.
 */
void solve() {
    cini(n);
    vi a(n);
    vi b(n);

    for(int i=0; i<n; i++)
        cin>>a[i]>>b[i];

    ld tL=0;
    ld tR=0;
    int iL=0;
    int iR=n-1;

    ld ans=0;
    while(iL<iR) {
        if(tL+((ld)a[iL])/b[iL] < tR+((ld)a[iR])/b[iR]) {
            tL+=((ld)a[iL])/b[iL];
            iL++;
        } else {
            tR+=((ld)a[iR])/b[iR];
            iR--;
        }
    }

    // TODO
    //... and lost the fun of it... :/

}

signed main() {
    solve();
}
