/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * There are only two orders:
 * All vertical stacked, or two stacked 
 * and one below.
 *
 * Try both x,y swapped, and all 6 permutations,
 * in each case both strategies.
 */
void solve() {
    cini(x);
    cini(y);
    cinai(a,3);


    for(int i=0; i<2; i++) {
        sort(all(a));
        do {
            /* case1: a,b horz, c vert */
            int lines=(a[2]+x-1)/x;
            int yy=y-lines;
            if(yy>0) {
                int cols1=(a[0]+yy-1)/yy;
                int cols2=(a[1]+yy-1)/yy;
                if(cols1+cols2<=x) {
                    cout<<"Yes"<<endl;
                    return;
                }
            }

            /* case2: a,b,c vert */
            int l1=(a[0]+x-1)/x;
            int l2=(a[1]+x-1)/x;
            int l3=(a[2]+x-1)/x;
            if(l1+l2+l3<=y) {
                cout<<"Yes"<<endl;
                return;
            }


        }while(next_permutation(all(a)));

        swap(x,y);
    }

    cout<<"No"<<endl;
    return;
}

signed main() {
    solve();
}
