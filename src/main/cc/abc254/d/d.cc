
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"




const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=1000000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

/* Factorization of n using precomputed primes.
 * https://cp-algorithms.com/algebra/factorization.html
 */
void fac(ll n, map<int,int> &factorization) {
    for (ll d : pr) {
        if (d * d > n)
            break;
        while (n % d == 0) {
            factorization[d]++;
            n /= d;
        }
    }
    if (n > 1)
        factorization[n]++;
}

/**
 * Note, the square can be up to n*n,
 * so there are n squares.
 *
 * So foreach square i*i, check all numbers by
 * iterating them up to i.
 * Its O(n^2).
 *
 * How to do that right?
 *
 * Find factorization of i, then we can use all pairs of factors.
 * ...somehow :/
 *
 * Let f be factors of i.
 * Then we can use use all pairs of factors:
 * i*f2/f1, i*f1/f2
 *
 * How to implement efficient?
 */
void solve() {
    cini(n);

    set<pii> ans;
    for(int i=n; i>=1; i--) {
        vi f;
        for(int j=1; j*j<=i; j++) {
            if(i%j==0) {
                f.push_back(j);
                if(j*j!=i)
                    f.push_back(i/j);
            }
        }
        sort(all(f));

        ans.emplace(i,i);
        //cerr<<i<<" "<<i<<endl;
        for(size_t k=0; k<f.size(); k++) {
            for(size_t j=k+1; j<f.size(); j++) {
                if((i*f[j])/f[k]<=n) {
                    ans.emplace(i*f[j]/f[k], i*f[k]/f[j]);
                    ans.emplace(i*f[k]/f[j], i*f[j]/f[k]);
                } else 
                    break;
            }
        }
    }

    cout<<ans.size()<<endl;
}

signed main() {
    solve();
}
