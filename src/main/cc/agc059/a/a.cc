/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * ans=2
 * ABCCCA
 * AABBBA
 * AAAAAA
 *
 */
void solve() {
    cini(n);
    cini(q);
    cins(s);

    int ans=0;
    int l=0;
    int r=n;

    while(l<r) {

    }

    vvi p(3, vi(n+1));

    for(int i=0; i<n; i++) {
        p[0][i+1]=p[0][i]+(s[i]=='A');
        p[1][i+1]=p[1][i]+(s[i]=='B');
        p[2][i+1]=p[2][i]+(s[i]=='C');
    }

    for(int i=0; i<q; i++) {
        cini(l); l--;
        cini(r); 

        int cnt=((p[0][r]-p[0][l])>0)
            + ((p[1][r]-p[1][l])>0)
            + ((p[2][r]-p[2][l])>0)
            ;
        //cerr<<"pl="<<p[l]<<" p[r]="<<p[r]<<endl;

        cout<<cnt-1<<endl;
    }
}

signed main() {
//    cini(t);
//    while(t--)
        solve();
}
