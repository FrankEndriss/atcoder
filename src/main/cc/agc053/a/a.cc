
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Input data is redundant, since s is determined by a[].
 *
 * However, consider two consecutive numbers diff by x
 * if x==1 there is only one solution... there are exactly
 * x solutions.
 * Consider seq ...,y,y+x,y+x+x,...
 * Again, there are x solutions, since we can do x times y[1]+x
 * So, ans==min(abs(a[i]-a[i-1]));
 */
const int INF=1e9;
void solve() {
    cini(n);
    cins(s);
    cinai(a,n+1);

    int ans=INF;
    for(int i=1; i<=n; i++) 
        ans=min(ans, abs(a[i]-a[i-1]));

    vvi aa(ans, vi(n+1)); 

    for(int i=0; i<=n; i++) {
        int d=a[i]/ans;
        int dd=a[i]%ans;
        for(int j=0; j<ans; j++) {
            aa[j][i]+=d;
            if(dd) {
                aa[ans-1-j][i]++;
                dd--;
            }
        }
    }

    cout<<ans<<endl;
    for(int i=0; i<ans; i++) {
        for(int j=0; j<=n; j++) {
            cout<<aa[i][j]<<" ";
        }
        cout<<endl;
    }
}

signed main() {
    solve();
}
