
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/segtree>
using namespace atcoder;
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;


const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using S=pii; /* <value,index> */

S st_op(S a, S b) {
    return max(a,b);
}

S st_e() {
    return { -1, 0 };
}

using stree=segtree<S, st_op, st_e>;


typedef tree<pii,null_type,less<pii>,rb_tree_tag,
        tree_order_statistics_node_update> indexed_set;

/* Alice starts, consider Bob.
 * In first move, he will get one of two cards,
 * that is, if A did choose some card in lower half
 * a[n/2+1], else a[n/2].
 * Then the same repeats...until no more cards.
 * So simple dp would be to choose all positions,
 * then continue...etc, but obviously TLE.
 * What about greedy choose the optimal number, so that
 * diff of both numbers maximized?
 * Since N is 1e5 that seems to be the solution.
 * Else
 * We would try the best from left half, and best from right
 * half... in O(2^N) which also TLE.
 *
 * ***
 * Simulate that.
 * We need to remove elements from a[], and find
 * elements in a[] by position.
 * That is, in each round we want to read the values
 * at the positions of the two medians, let them be
 * i and i+1.
 * So we have to shift i by the number of removed numbers
 * <=i, then again shift by that amount etc...
 * O((logn)^2) and complecated.
 * Just us an ordered_set?
 * ***
 * Choose greedyly allways biggest possible?
 * ...
 * There seems to be some dp necessary, somehow :/
 * idk, how can we dp if we have two choices for n times?
 *
 * Consider Bob.
 * Bog gets all numbers starting at the original median,
 * then some positions to the left, and some to the right.
 * All in all n positions.
 * So Alice gets all other positions.
 * Just choose the min for Bob, without segtree and things :)
 * No...
 * Alice can make that bob does not get any consecutive segment,
 * because obviously she can choose elements at the both ends
 * of the prev choosen segment.
 * This is, Bob has to start at medL or medR,then
 * chooses the next available on the left end (if allice
 * did choose a number on the right), or the next on the other side...
 * idk
 */
void solve() {
    cini(n);

    const int N=n;

    stree seg(2*n);
    indexed_set s;
    for(int i=0; i<2*n; i++) {
        cini(aux);
        s.insert({i,aux});
        seg.set(i, {aux,i});
    }

    int ans=0;
    int mid=n;
    while(n) {
        const int med=n-1;
        auto it=s.find_by_order(med);
        pii b1=*it;
        it++;
        pii b2=*it;

        pii a1=seg.prod(0,mid);
        pii a2=seg.prod(mid,N*2);

        //if(a1.first-b2.second>a2.first-b1.second) {
        //if(a1.first>a2.first) {
        if(b2.second<b1.second) {
            ans+=a1.first;
            s.erase(b2);
            s.erase({a1.second,a1.first});
            seg.set(b2.first, {-1,0});
            seg.set(a1.second, {-1,0});
        } else {
            ans+=a2.first;
            s.erase(b1);
            s.erase({a2.second,a2.first});
            seg.set(b1.first, {-1,0});
            seg.set(a2.second, {-1,0});
        }

        while(mid<N && seg.get(mid).first<0)
            mid++;

        n--;
    }

    cout<<ans<<endl;

}

signed main() {
    solve();
}
