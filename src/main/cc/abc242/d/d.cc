/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * A -> BC
 * B -> CA
 * C -> AB
 *
 * Note that the number of chars doubles in each step.
 *
 * Consider the first char. After 3 rounds, it is same as before.
 *
 * Foreach query:
 * First find from which starting char the kth char originates.
 * If first then reduce t%=3
 * else simulate.
 *
 *
 * How to simulate?
 * Once the prefix originates from one char, it repeats every 3 steps.
 * ...
 * Step backwards.
 * pos i one step back is
 */
void solve() {
    cins(s);
    cini(q);

    char next[256];
    next['A']='B';
    next['B']='C';
    next['C']='A';
    char prev[256];
    prev['A']='C';
    prev['B']='A';
    prev['C']='B';

    char ans[]= { 'A', 'B', 'C' };

    for(int i=0; i<q; i++) {
        cini(t);
        cini(k);
        k--;    /* zero based index */

        while(t>0 && k>0) {
            if(k&1) {
                for(int j=0; j<3; j++)
                    ans[j]=next[ans[j]];
            } else {
                for(int j=0; j<3; j++)
                    ans[j]=prev[ans[j]];
            }
            t--;
            k/=2;
        }

        t%=3;
        while(t) {
            for(int j=0; j<3; j++)
                ans[j]=prev[ans[j]];
            t--;
        }

        if(s[k]=='A')
            cout<<ans[0]<<endl;
        else if('B'==s[k])
            cout<<ans[1]<<endl;
        else if('C'==s[k])
            cout<<ans[2]<<endl;
        else
            assert(false);

    }
}

signed main() {
    solve();
}
