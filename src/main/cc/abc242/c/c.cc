/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
using mint=modint998244353;

void solve() {
    cini(n);

    vector<mint> dp(10); /* 0='1'; 1='2'-'8'; 2='9' */
    for(int i=1; i<=9; i++)
        dp[i]=1;

    for(int i=1; i<n; i++) {
        vector<mint> dp0(10);
        dp0[1]=dp[1]+dp[2];
        dp0[9]=dp[9]+dp[8];
        for(int j=2; j<=8; j++) 
            dp0[j]=dp[j-1]+dp[j]+dp[j+1];
        dp.swap(dp0);
    }

    mint ans=0;
    for(int i=1; i<10; i++)
        ans+=dp[i];
    cout<<ans.val()<<endl;

}

signed main() {
    solve();
}
