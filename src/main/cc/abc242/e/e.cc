/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Start at center, dp 0=sameOrSmaller and 1=bigger.
 *
 * At each position there are some cases:
 * -use smaller char
 *  -> smaller string
 * -use bigger char
 *  -> bigger string
 * -use same char
 *  -> if second > first then bigger string
 *     else lteq string
 *
 *
 * What is wrong here ?
 */
using mint=modint998244353;
void solve() {
    cini(n);
    cins(s);

    vector<mint> dp(3, mint(0));   /* 0=lt; 1=eq; 2=bg */

    if(n&1) {
        dp[0]=s[n/2]-'A';
        dp[1]=1;
        dp[2]='Z'-s[n/2];
    } else {
        dp[0]=s[n/2-1]-'A';
        dp[2]='Z'-s[n/2-1];
        if(s[n/2-1]<s[n/2])
            dp[0]+=1;
        else if(s[n/2-1]>s[n/2]) 
            dp[2]+=1;
        else
            dp[1]=1;
    }

    int start=n/2-1;
    if((n&1)==0)
        start--;

    for(int i=start; i>=0; i--) {
        vector<mint> dp0(3, mint(0));

        const int v=s[i]-'A';
        dp0[0]=(dp[0]+dp[1]+dp[2])*v; /* use a smaller char */
        const int v2='Z'-s[i];
        dp0[2]=(dp[0]+dp[1]+dp[2])*v2; /* use a bigger char */

        /* use s[i] char */
        if(s[i]<s[n-1-i]) {
            dp0[0]+=(dp[0]+dp[1]);
            dp0[2]+=dp[2];
        } else if(s[i]>s[n-1-i]) {
            dp0[0]+=dp[0];
            dp0[2]+=(dp[1]+dp[2]); 
        } else if(s[i]==s[n-1-i]) {
            dp0[0]+=dp[0];
            dp0[1]+=dp[1];
            dp0[2]+=dp[2];
        } else
            assert(false);

        dp=dp0;
    }

    cout<<(dp[0]+dp[1]).val()<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
