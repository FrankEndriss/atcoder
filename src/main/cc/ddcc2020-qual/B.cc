/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    vll a(n);
    vll s(n);
    ll sum=0;
    for(int i=0; i<n; i++) {
        cin>>a[i];
        sum+=a[i];
        if(i==0)
            s[i]=a[i];
        else
            s[i]=a[i]+s[i-1];
    }

    ll c=sum/2;

    auto it=lower_bound(s.begin(), s.end(), c);
    ll ans2=*it;
    it--;
    ll ans1=*it;

    ll ans=min(abs(c-ans1), abs(c-ans2));

    if(sum&1) {
        c=sum/2+1;
        it=lower_bound(s.begin(), s.end(), c);
        ans2=*it;
        it--;
        ans1=*it;

        ans=min(ans, min(abs(c-ans1), abs(c-ans2)));
    }
    
// ???
    cout<<ans/2<<endl;
}

