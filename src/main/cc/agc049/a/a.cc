
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* On each remove the number of components changes.
 * If it is 0 we are finished.
 *
 * Let a leaf vertex be one with no outgoing edge.
 * removing such one removes only that one.
 * Removing a parent of a leaf vertex removes the parent
 * and the leaf and all other childs of that parent.
 *
 * Let a root vertex be one without incoming edge.
 * Removing a root vertex can be done with all the childs
 * first.
 * Such root works as a tree.
 *
 * Consider circles, a circle is removed completely always, and
 * all childs of the circle.
 * So a circle works as a root.
 *
 * How to find the exp number for some root?
 * And how to work with vertex reacheable by more than one root?
 *
 * Assume result after first operation. bfs the remaining vertex.
 * Each of that results contribute by 1/n.
 */
void solve() {
    cini(n);
    cinas(a,n);
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
