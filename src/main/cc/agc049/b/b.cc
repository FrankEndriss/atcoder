
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* The number of 1s changes by 0 or -2, so the parity of numbers
 * of 1s is invariant.
 * -> if odd number of 1s no sol.
 *
 * If even number of 1s we can "move" a single 1 to the left by one
 * operation.
 * We can remove any pair of 1s.
 * We cannot create any 1s.
 *
 * So we need to greedyly move 1s to the left in s to match t, or
 * remove pairs of two 1s
 */
void solve() {
    cini(n);
    cins(s);
    cins(t);

    int ans=0;
    queue<int> post;
    queue<int> poss;

    for(int i=0; i<n; i++) {
        if(s[i]=='1') {
            if(poss.size()) {
                ans+=i-poss.front();
                poss.pop();
                if(t[i]=='1')
                    post.push(i);
            } else if(post.size()) {
                ans+=i-post.front();
                post.pop();
                if(t[i]=='1')
                    post.push(i);
            } else {
                if(t[i]!='1')
                    poss.push(i);
            }
        } else if(s[i]=='0') {
            if(t[i]=='1')
                post.push(i);
        } else
            assert(false);
    }

    if(poss.size() || post.size())
        cout<<-1<<endl;
    else
        cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
