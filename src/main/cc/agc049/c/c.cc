
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


template <typename E>
class SegmentTree {
  private:
    vector<E> tree;
    E neutral;
    function<E (E, E)> cumul;

    int treeDataOffset;
    const int ROOT = 1;

    inline int leftChild(const int treeIdx) {
        return  treeIdx * 2;
    }
    inline int rightChild(const int treeIdx) {
        return treeIdx * 2 + 1;
    }
    inline int parent(const int treeIdx) {
        return treeIdx / 2;
    }
    inline bool isOdd(const int idx) {
        return (idx & 0x1);
    }

  public:
    /** @param beg, end The initial data.
     * @param pNeutral the Element neutral to cumul, ie 0 for sum, 1 for product etc
     * @param pCumul The cumulative function to create the "sum" of two nodes.
    **/
    template<typename Iterator>
    SegmentTree(Iterator beg, Iterator end, E pNeutral, function<E (E, E)> pCumul) {
        neutral=pNeutral;
        cumul=pCumul;
        treeDataOffset=end-beg;
        tree=vector<E>(treeDataOffset*2, pNeutral);

        int i=treeDataOffset;
        for (auto it=beg; it!=end; it++)
            tree[i++] = *it;

        for (int j=treeDataOffset - 1; j>=1; j--)
            tree[j] = cumul(tree[leftChild(j)], tree[rightChild(j)]);
    }

    /** Updates all elements in interval(idxL, idxR].
    * iE add 42 to all elements from 4 to inclusive 7:
    * update(4, 8, [](int i1) { return i1+42; });
    */
    void updateRange(int pIdxL, int pIdxR, function<E (E)> apply) {
        pIdxL+=treeDataOffset;
        pIdxR+=treeDataOffset;
        for(int i=pIdxL; i<pIdxR; i++)
            tree[i]=apply(tree[i]);

        while (pIdxL != ROOT) {
            pIdxL=parent(pIdxL);
            pIdxR=max(pIdxL, parent(pIdxR-1));
            for(int i=pIdxL; i<=pIdxR; i++)
                tree[i]=cumul(tree[leftChild(i)], tree[rightChild(i)]);
        }
    }

    /** Updates the data at dataIdx to value returned by apply. */
    void updateApply(int dataIdx, function<E(E)> apply) {
        updateSet(dataIdx, apply(get(dataIdx)));
    }

    /** Updates the data at dataIdx by setting it to value. */
    void updateSet(int dataIdx, E value) {
        int treeIdx = treeDataOffset + dataIdx;
        tree[treeIdx] = value;

        while (treeIdx != ROOT) {
            treeIdx = parent(treeIdx);
            tree[treeIdx] = cumul(tree[leftChild(treeIdx)], tree[rightChild(treeIdx)]);
        }
    }

    /** @return value at single position, O(1) */
    E get(int idx) {
        return tree[idx+treeDataOffset];
    }

    /** @return the cumul(idxL, idxR], iE idxL inclusive, idxR exclusive. */
    E get(int pIdxL, int pIdxR) {
        int idxL = pIdxL + treeDataOffset;
        int idxR = pIdxR + treeDataOffset;
        E cum = neutral;
        while (idxL < idxR) {
            if (isOdd(idxL)) { // left is odd
                cum = cumul(cum, tree[idxL]);
                idxL++;
            }
            if (isOdd(idxR)) {
                idxR--;
                cum = cumul(cum, tree[idxR]);
            }
            idxL = parent(idxL);
            idxR = parent(idxR);
        }
        return cum;
    }

};

/* What stupid statement with eating balls and things?
 * What are the numbers a[i] good for? They are replaced
 * in first step of each operation.
 * So we got sum(b[]) equal balls, not more not less.
 * We must make sure to not move a robot at position 1.
 * ...
 * Ok, the steps must be taken only once.
 * So, _once_ we assign numbers to the balls, then
 * we eat all balls one after the other.
 *
 * We want to find the minimum number of balls we need to
 * assign a number so that robot 0 is not killed.
 *
 * Note that the robot numbers never change, so if we got
 * 42 balls with number 3, that means we move robot 3
 * 42 times.
 * And that means we need to destroy it latest after second
 * time we moved it, because else it will kill robot 0.
 *
 * What we want to do is move robot i exactly i-1 times,
 * because then it kills all robots smaller than i, but
 * not kills robot 0.
 * 
 * We need to reassign numbers to the balls in a way
 * that the number of balls with number a[i] is less than a[i],
 * ie b[i]<a[i], or there exists an a[j], j>i where distance
 * j-i<=b[j], ie robot j will kill robot i.
 *
 * Note that we must eat all balls.
 * foreach i we need to dicide if it will be killed by some robot
 * j>i, or if it is a killer.
 *
 * if b[i]<a[i] then it is a killer, and kills all robots k with k<i && a[k]>=a[i]-b[i]
 * note that killers are good.
 * dp[i] in this case is minimum of the killed robots dp value, and the maximum
 * dp of the notkilled ones.
 *
 * if b[i]>=a[i] it must be killed, or b[i]-a[i]+1 balls must be reassigned.
 *
 * let dp[i]=min number of assigns to eat all balls up to a[i] without killing r0.
 * maintain it in a segtree.
 *
 * ***
 * What about assigning k balls from a[i] to a[j], so that a[j] kills a[i]?
 * That could be better choice than anything else :/
 * So at position j, if a[i]<a[j]-b[j] we can resolve all with dp[i]+(a[j]-b[j]-a[i])
 * For all i<j :/
 * ... fuck, thats to complecated :/
 */
const int INF=1e18;
void solve() {
    cini(n);
    vector<pii> a(n);
    for(int i=0; i<n; i++)
        cin>>a[i].first;
    for(int i=0; i<n; i++)
        cin>>a[i].second;
    sort(all(a));

    //vi dp(n+1, INF);
    //dp[0]=max(0, a[0].second-a[0].first+1)

    vi data(n+1, INF);
    data[0]=0;
    SegmentTree<int> seg(all(data), INF, [](int i1, int i2) { return min(i1, i2); }); 
    SegmentTree<int> seg2(all(data), INF, [](int i1, int i2) { return min(i1, i2); }); 

    for(int j=1; j<=n; j++) {
        const int i=j-1;
        if(a[i].second>=a[i].first) {
            seg.updateSet(j, a[i].second-a[i].first+1);
            seg2.updateSet(j, a[i].second-a[i].first+1-a[i].first);
        } else {
            /* dp[j]= minimum of 
             *   maximum in interval of notkilled robots, ie dp[idx],
             *   and...
             * first find index of leftmost killed robot */
            auto it=lower_bound(all(a), make_pair(a[i].first-a[i].second, 0LL));
            int idx=distance(a.begin(), it);
            assert(idx<=i);
            int val=seg.get(idx, n+1);
            val=min(val, seg2.get(0,a[i].first-a[i].second)+a[i].first-a[i].second);
            seg.updateSet(j, val);
        }
    }

    cout<<seg.get(n)<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
