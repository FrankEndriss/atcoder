
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* bfs the dist from all cities to all other cities.
 * Then find foreach city the sum of dist from city to any other city
 * and back.
 * Consider selfloops separate.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);
    vector<vector<pii>> adj(n);
    vi selfloop(n, INF);
    for(int i=0; i<m; i++) {
        cini(a); a--;
        cini(b); b--;
        cini(c);
        if(a==b) 
            selfloop[a]=min(selfloop[a], c);
        else {
            adj[a].emplace_back(b,c);
        }
    }

    vvi dist(n, vi(n, INF));
    for(int i=0; i<n; i++) 
        dist[i][i]=0;

    for(int i=0; i<n; i++) {
        vi dp(n, INF);
        dp[i]=0;
        priority_queue<pii> q;
        q.emplace(0, i);
        while(q.size()) {
            auto [d,v]=q.top();
            q.pop();
            if(d!=-dp[v])
                continue;

            for(int j=0; j<adj[v].size(); j++) {
                if(dp[v]+adj[v][j].second<dp[adj[v][j].first]) {
                    dp[adj[v][j].first]=dp[v]+adj[v][j].second;
                    q.emplace(-dp[adj[v][j].first], adj[v][j].first);
                }
            }
        }
        for(int j=0; j<n; j++)  {
            dist[i][j]=min(dist[i][j], dp[j]);
            //dist[j][i]=min(dist[j][i], dp[j]);
        }
    }

    for(int i=0; i<n; i++) {
        int ans=selfloop[i];
        for(int j=0; j<n; j++) 
            if(i!=j)
                ans=min(ans, dist[i][j]+dist[j][i]);
        if(ans==INF)
            ans=-1;
        cout<<ans<<endl;
    }
}

signed main() {
    solve();
}
