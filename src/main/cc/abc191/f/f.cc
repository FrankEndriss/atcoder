
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* The smallest possible value without using gcd() is min(a[]),
 * and it is the only one possible value.
 * And it is the biggest last value possible.
 *
 * So, we can create new mins by doing gcd on any pairs.
 * We need to find all possible gcds smaller than min(a[])
 *
 * So, then we got those. But we can do better.
 * Use all those new values...somehow :/
 */
void solve() {
    cini(n);
    cinai(a,n);
    int mi=*min_element(all(a));

    set<int> ans;
    ans.insert(mi);

    set<int> next;

    for(size_t i=0; i<a.size(); i++) {
        set<int> nnext;
        for(size_t j=i+1; j<a.size(); j++)  {
            int g=gcd(a[i], a[j]);
            if(g<mi)
                if(ans.insert(g).second)
                    nnext.insert(g);
        }
        while(nnext.size()) {
            set<int> nnext2;
            for(int j : nnext) {
                int g=gcd(a[i], j);
                if(g<mi) {
                    if(ans.insert(g).second)
                        nnext2.insert(g);
                }
            }
            nnext2.swap(nnext);
        }
    }

    cout<<ans.size()<<endl;

}

signed main() {
    solve();
}
