
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

ld x,y,r;
const ld EPS=0.00000000001;

bool inside(int x0, int y0) {
    return hypot(x-x0, y-y0)<=r+EPS;
}

/** Find lowest and highest point
 * in circle, then iterate those lines.
 */
void solve() {
    cin>>x>>y>>r;

    int xmin=x-r-2;
    int xmax=x+r+2;
    int ymin=floor(y);
    int ymax=ceil(y);
    int ans=0;

    while(xmin<=xmax && !inside(xmin, ymin) && !inside(xmin, ymax))
        xmin++;

    while(xmax>=xmin && !inside(xmax, ymin) && !inside(xmax, ymax))
        xmax--;

    for(int i=xmin; i<=xmax; i++) {
        while(!inside(i, ymin))
            ymin++;
        while(inside(i, ymin))
            ymin--;

        while(!inside(i,ymax))
            ymax--;
        while(inside(i, ymax))
            ymax++;
        //cout<<"ymax="<<ymax<<" ymin="<<ymin<<" x="<<i<<endl;
        ans+=max(0LL, ymax-ymin-1);
    }

    cout<<ans<<endl;


}

signed main() {
    solve();
}
