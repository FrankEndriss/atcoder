/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * max N operations :/
 *
 * Consider p[0] and p[1] and p[2]
 * The smallest of them must be in the middle.
 *
 * Consider p[1] and p[2] and p[3]
 * The biggest of them must be in the middle.
 */
void solve() {

    cini(n);
    cinai(a,2*n);

    vi ans;
    for(int i=0; i+2<2*n; i+=2) {
        if(a[i]>a[i+1] && a[i]>a[i+2]) {
            ans.push_back(i+1);
            swap(a[i], a[i+1]);
        } else if(a[i+2]>a[i+1] && a[i+2]>a[i]) {
            ans.push_back(i+2);
            swap(a[i+1], a[i+2]);
        }
    }
    if(a[2*n-2]>a[2*n-1]) 
        ans.push_back(2*n-1);

    cout<<ans.size()<<endl;
    for(size_t i=0; i<ans.size(); i++) 
        cout<<ans[i]<<" ";
    cout<<endl;
}

signed main() {
       solve();
}
