/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vector<ll> a(n), b(n);
    ll ans=0;
    fori(n) {
        ll ai, bi;
        cin>>ai>>bi;
        a[i]=ai;
        b[i]=bi;
        ans+=max(ai, bi);
    }

    cout<<ans<<endl;
}

