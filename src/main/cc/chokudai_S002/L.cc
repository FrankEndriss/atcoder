/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vector<ll> a(n), b(n);
    vector<ll> id(n);
    ll ans=0;
    fori(n) {
        ll ai, bi;
        cin>>ai>>bi;
        a[i]=ai;
        b[i]=bi;
        id[i]=i;
    }

    sort(id.begin(), id.end(), [&](int i1, int i2) {
        return a[i1]*b[i1]<a[i2]*b[i2];
    });

    vector<ll> memo(n, -1);

    /* return max num of squares one can put into square idx. */
    function<ll(int)> dfs=[&](int idx) {
        if(idx==0) /* no more available */
            return 0LL;
    
        if(memo[idx]!=-1)
            return memo[idx];

        ll m=0;
        ll longidx=max(a[id[idx]], b[id[idx]]);
        ll shoridx=min(a[id[idx]], b[id[idx]]);
        for(int i=idx-1; i>=0; i--) {
            ll longi=max(a[id[i]], b[id[i]]);
            ll shori=min(a[id[i]], b[id[i]]);
            if(longi<longidx && shori<shoridx) 
                m=max(m, 1+dfs(i));
        }
        memo[idx]=m;
        return m;
    };

    for(int i=n-1; i>=0; i--) 
        ans=max(ans, 1+dfs(i));

    cout<<ans<<endl;
}

