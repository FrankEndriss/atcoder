
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Segments of 1s stay as long as they are...no.
 * But we can move them left rigth as we need to.
 *
 * So, move them to the left as much positions as needed.
 * Implement:
 * Simulate,
 * Foreach position in t, choose to use the next available char in s
 * of that kind.
 *
 * woah...still WA, is this annoying. How to implement this properly?
 *
 * We iterate t from left to right, t[i] is current t.
 * if t[i]==s[i] is the same then ignore.
 * Else move the next
 *
 * The cost calculation does work somewhat otherwise :/
 *
 * We do not move the next available t[i] in s[i] to position i,
 * but one of the ones available in the next segment, and we
 * try to find the first where t[i] fits.
 *
 * Tf t[i]!=s[i] we search for the next segments of t[i] chars in s
 * If that segment starts at i+1, then we want to swap the next position
 * where s[i] and t[i] fit,
 * else we need to swap the first position.
 *
 * ***
 * Does not work, something basically wrong.
 * Also, it is unclear Sample 3. How we can we solve this in 22 operations?
 * I need more than 22 for the first 26 leading 1s...so I get something wrong.
 */
void solve() {
    cini(n);
    cins(s);
    cins(t);

    vi cnt(2);
    for(int i=0; i<n; i++) {
        cnt[0]+=s[i]-'0';
        cnt[1]+=t[i]-'0';
    }
    if(cnt[0]!=cnt[1]) {
        cout<<-1<<endl;
        return;
    }

    int ans=0;
    vi pos(2);  /* pos[0]=next position to search for a '0' */

    for(int i=0; i<n; i++) {
        if(t[i]==s[i])
            continue;

        pos[0]=max(pos[0], i+1);
        pos[1]=max(pos[1], i+1);

        int c=t[i]-'0';
        while(s[pos[c]]!=t[i])
            pos[c]++;

        ans++;

        if(pos[c]-i>1) { /* we need to swap the first in segment */
            cerr<<"i="<<i<<" swap, pos[c]="<<pos[c]<<endl;
            swap(s[i], s[pos[c]]);
        } else {
            /* we want to swap the first  in segment where t[i]=='1', or
                           if there is none such, then the last in segment. */
            while(t[pos[c]]!=s[i] && pos[c]+1<n && s[pos[c]+1]==t[i])
                pos[c]++;
            cerr<<"i="<<i<<" swap, pos[c]="<<pos[c]<<endl;
            swap(s[i], s[pos[c]]);
        }
        cerr<<"s="<<s<<endl;
        cerr<<"t="<<t<<endl;

    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}
