/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using mint=modint998244353;

const int MOD=998244353;
const int N=1e6;
vector<mint> fac(N);

const bool initFac=[](const int mod) {
    fac[1]=1;
    for(int i=2; i<N; i++) {
        fac[i]=fac[i-1]*i;
    }
    return true;
}(MOD);

/** see https://cp-algorithms.com/combinatorics/binomial-coefficients.html */
mint nCr(int n, int k) {
    if(n<k)
        return mint(0);

    if(k==0 || k==n)
        return mint(1);
    if(k==1 || k==n-1)
        return mint(n);
    if(k>n/2)
        return nCr(n, n-k);

    return fac[n]*fac[k].inv()*fac[n-k].inv();
    //return mul(mul(fac[n], inv(fac[k])), inv(fac[n - k]));
}

/**
 * Consider cell 0,0, we got NN posibilities.
 * In cell 0,1 we got 
 * a[0][0] posibilities to get the row-condition fullfilled, and 
 * NN-a[0][0] posi to get the col-condition fullfilled.
 *
 * ...Note that we search for number of permutations.
 *
 *
 * In each row, we need to consider the smallest number.
 * It must not be the biggest number in its col.
 *
 * Find the number of ways we can place a number so that it is both, 
 * 1. smallest in row and 
 * 2. biggest in col.
 *
 * Consider cell 0,0
 * To make it biggest in col it must be at least N. 
 * Consider a[0][0]=i, then there are 
 * nCr(i-1, N-1)*fac[N-1] ways to fill the row with smaller numbers, and
 * nCr(NN-i, N-1) ways to fill the col with bigger numbers, and
 * fac[(N-1)*(N-1)]  ways to fill all other cells
 * And the same applies to all cells, not only a[0][0]
 *
 * But, how to avoid overcount?
 * -> we cant
 *
 ***********************
 * Try calc positive. Consider i in some cell.
 * i must be:
 * 1. not smallest in row, or
 * 2. smallest in row but not biggest in col
 *
 * But, here same problem, how to avoid overcounting?
 * Can we calc foreach number of only one cell?
 * Would be O(N*N*formular) hence should work.
 *
 * Number of ways to build a row with smallest in row: 
 *  nCr(Number of bigger numbers, N-1 positions in row)
 *  nCr(NN-i, N-1) * fac[n-1]
 *
 * Number of ways to build a row with not the smallest in row: 
 *  All permutations of row minus smallestInRow
 *  nCr(NN-1, N-1)*fac[n-1] - smallestInRow
 *
 *
 * If smallest in row, ways to make it not biggest in col:
 *   Number of bigger numbers times
 *   positions in col
 *   times fac[number of other fields]
 * (NN-i-(N-1))*(N-1)*fac[(N-1)*(N-1)]
 *
 *
 * ...frustrating.
 * Fiddled like two hours on it, with no result :/
 * Basic problem is, that without accepting these kinds of
 * math problems the competitive programming tends to make no 
 * sense at all.
 * Maybe its simply time change to something more interesting.
 */
void solve() {
    cini(n);
    const int nn=n*n;

    if(n==1) {
        cout<<0<<endl;
        return;
    }

    mint ans=0;
    for(int i=1; i<=nn; i++) {
        /* calc number of ways to put i into cell a[0][0] (or any other cell) */
        mint smallestInRow=nCr(nn-i, n-1)*fac[n-1];
        ans+=(nCr(nn-1,n-1)*fac[n-1]-smallestInRow)*fac[n*(n-1)];

        /* add not biggest in col */
        if(nn-i-1>0) 
            ans+=smallestInRow*(nn-i-1)*(n-1)*fac[(n-1)*(n-1)];
    }
    cout<<ans.val()<<endl;
}

signed main() {
        solve();
}
