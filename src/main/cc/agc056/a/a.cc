/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * 6
 ##..#.
 ##..#.
 ..##.#
 ..##.#
 ##...#
 ..###.
 *
 * 7
 .###...
 ....###
 ###....
 #....##
 ...###.
 ##....#
 ..###..
 *
 * 8
 * ...###..
 * ###.....
 * ....###.
 * .###....
 * .....###
 * ..###...
 * ##.....#
 * #.....##
 *
 * 9
 * ###......
 * ....###..
 * .###.....
 * .....###.
 * ..###....
 * ......###
 * ...###...
 * #......##
 * ##......#
 *
 * 10
 * ....###...
 * ###.......
 * .....###..
 * .###......
 * ......###.
 * ..###.....
 * .......###
 * ...###....
 * #.......##
 * ##.......#
 *
 * 11
 * ###........
 * .....###...
 * .###.......
 * ......###..
 * ..###.....
 * .......###.
 * ...###.....
 * ........###
 * ....###....
 * ##........#
 * #........##
 *
 */
void solve() {
    cini(n);

    if(n==6) {
 cout<<"##..#."<<endl;
 cout<<"##..#."<<endl;
 cout<<"..##.#"<<endl;
 cout<<"..##.#"<<endl;
 cout<<"##...#"<<endl;
 cout<<"..###."<<endl;
        return;
    }else if(n==7) {
 cout<<".###..."<<endl;
 cout<<"....###"<<endl;
 cout<<"###...."<<endl;
 cout<<"#....##"<<endl;
 cout<<"...###."<<endl;
 cout<<"##....#"<<endl;
 cout<<"..###.."<<endl;
        return;
    }

    vs ans(n, string(n,'.'));

    cerr<<"n="<<n<<endl;

    if(n%2==0) {
        int idx1=0;
        int n2=n/2;
        for(int i=0; i+1<n/2; i++) {
            for(int j=0; j<3; j++)
                ans[i*2+0][idx1+n2+j-1]='#';
            for(int j=0; j<3; j++)
                ans[i*2+1][idx1+j]='#';

            idx1++;
        }
        ans[n-2][0]='#';
        ans[n-2][n-1]='#';
        ans[n-2][n-2]='#';
        ans[n-1][0]='#';
        ans[n-1][1]='#';
        ans[n-1][n-1]='#';

    } else if(n%2==1) {
        int idx1=0;

        int n2=n/2;
        for(int i=0; i+1<n/2; i++) {
            for(int j=0; j<3; j++)
                ans[i*2][idx1+j]='#';
            for(int j=0; j<3; j++)
                ans[i*2+1][idx1+n2+j]='#';

            idx1++;
        }
        ans[n-3][idx1]='#';
        ans[n-3][idx1+1]='#';
        ans[n-3][idx1+2]='#';
        ans[n-2][0]='#';
        ans[n-2][n-1]='#';
        ans[n-2][n-2]='#';
        ans[n-1][0]='#';
        ans[n-1][1]='#';
        ans[n-1][n-1]='#';
    }

    for(int i=0; i<n; i++) 
        cout<<ans[i]<<endl;

}

signed main() {
    solve();
}
