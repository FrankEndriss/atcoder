/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/segtree>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* adjust as needed */
using S=signed; /* type of values */

S st_op(S a, S b) {
    return a+b;
}

S st_e() {
    return 0;
}

using stree=segtree<S, st_op, st_e>;

/**
 * Greedy?
 *
 * Sort intervals by l, then r.
 * foreach interval find number of 1s.
 * Case to much:
 * move leftmost 1 to next possible 0-position left of seg.
 * Case to less:
 * move leftmost 1 right of the segment into the segment.
 * *****
 * Consider each position from left to right.
 * If it is possible to set a 0, we set a 0.
 * Else we set a 1.
 *
 * So iterate the positions, maintain the intervals.
 * Each interval has a min position, where all rigth of mpos
 * has to be 1s.
 * Whenever we set a 0 to a 1 left of mpos, mpos increments.
 *
 * So, while iteration positions,
 * if we find a position where (at least) one of the segs
 * hase its mpos, then we set the 1 at that position, and
 * update all segments with the mpos>current position.
 *
 * Sort the intervals by l,r, and maintain the active ones.
 * foreach, maintain the mpos. Use a position list to maintain 
 * dist from current pos to mpos.
 *
 * ...How to implement?
 * How to find the mpos foreach segement? 
 */
const int NN=1e6+3;
void solve() {
    cini(n);
    cini(m);

    vector<pii> lr(m);
    int N=-1;
    for(int i=0; i<m; i++)  {
        cin>>lr[i].first>>lr[i].second;
        lr[i].first--;
        lr[i].second--;
        N=max(N, lr[i].second);
    }
    sort(all(lr));

    vi data(n,0);
    stree seg(data);

    /* @return the mpos of a segemnt given the current seg.
     * impl is wrong, and time runs out :/ */
    function<int(int,int)> findmpos=[&](int l, int r) {
        int lval=l;
        int rval=r+1;
        while(lval+1<rval) {
            int mid=(lval+rval)/2;
            int cnt=seg.prod(l, mid+1);
            if(cnt>(r-l+1))
                rval=mid;
            else if(cnt<(r-l+1))
                lval=mid;
            else
                return mid;
        }
        return lval+1;
    };

    int sidx=0;
    vector<set<int>> mpos(n+1);
    int moff=0;
    for(int i=0; i<n; i++) {
        while(sidx<m && lr[sidx].first<=i) {
            int mp=findmpos(lr[sidx].first, lr[sidx].second);
            mpos[mp].insert(sidx);
            sidx++;
        }
        if(mpos[i-moff].size()>0) {
            seg.set(i,1);
            mpos[i].clear();
            moff++;
        } 
    }

    for(int i=0; i<n; i++) 
        cout<<seg.get(i);
    cout<<endl;
}

signed main() {
    solve();
}
