
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* greedy.
 * choose stairs or floors... 
 * bfs.
 * x=corridor
 * y=stairs
 * */
const int INF=1e9;
void solve() {
    cini(a);
    cini(b);
    cini(x);
    cini(y);

    vvi dp(2, vi(101, INF));
    queue<pii> q;
    q.push({0,a});
    dp[0][a]=0;
    while(q.size()) {
        auto [ab, floor]=q.front();
        q.pop();
        if(floor>1 && dp[ab][floor-1]>dp[ab][floor]+y)  {
            dp[ab][floor-1]=dp[ab][floor]+y;
            q.push({ab,floor-1});
        }
        if(floor<100 && dp[ab][floor+1]>dp[ab][floor]+y)  {
            dp[ab][floor+1]=dp[ab][floor]+y;
            q.push({ab,floor+1});
        }

        if(floor>1 && ab==0 && dp[1][floor-1]>dp[0][floor]+x)  { /* dia a to b */
            dp[1][floor-1]=dp[0][floor]+x;
            q.push({1,floor-1});
        }
        if(floor<100 && ab==1 && dp[0][floor+1]>dp[1][floor]+x)  { /* dia b to a */
            dp[0][floor+1]=dp[1][floor]+x;
            q.push({0,floor+1});
        }

        if(dp[!ab][floor]>dp[ab][floor]+x) { /* horizontal corridor */
            dp[!ab][floor]=dp[ab][floor]+x;
            q.push({!ab,floor});
        }
    }
    cout<<dp[1][b]<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
