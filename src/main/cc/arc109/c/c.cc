
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* player k allways uses s[k%n]
 * Find the winner, and print what he always used.
 *
 * We cannot simulate since 2^k iterations.
 * But we know that all starts from left to right.
 * let w(x,y) be the winner of player x and y.
 * w(0,1), w(2,3), w(4,5), ...
 * This repeats for even n after n/2 pairs, for odd n
 * after n pairs. Just use allways n.
 * So after one round we have 2^(k-1) players, and a modified string.
 * repeat.
 */
void solve() {
    cini(n);
    cini(k);
    cins(s);

    function<char(int,int)> winner=[&](char l, char r) {
        if(l==r)
            return l;

        else if(l=='R') {
            if(r=='S')
                return 'R';
            else if(r=='P')
                return 'P';
        } else if(l=='S') {
            if(r=='R')
                return 'R';
            else if(r=='P')
                return 'S';
        } else if(l=='P') {
            if(r=='S')
                return 'S';
            else if(r=='R')
                return 'P';
        }

        assert(false);
        return ' ';
    };

    for(int i=0; i<k; i++) {
        string t=s;
        for(int j=0; j<n; j++) 
            t[j]=winner(s[(j*2)%n], s[(j*2+1)%n]);
        s=t;
    }
    cout<<s[0]<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
