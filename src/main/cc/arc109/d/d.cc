
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need two steps to move the whole quad by on position.
 * With two moves we move the quad by one field.
 * The free position after two moves is in the
 * direktion we moved, and the first move
 * is the correction of the position of the free field.
 *
 * So, if we go into first quad right/up, the free field
 * is in 'wrong' direction, ans=x*2+y*2
 * *****
 * No, we need to somehow utilize the manahattan distance.
 * Note that with most steps we can jump 3 positions.
 * But some moves alow only 2.
 * ...
 * If we move 'diagonal' we can do 3 steps
 */
void solve() {
    vvi p(3,vi(2));
    for(int i=0; i<3; i++) 
        for(int j=0; j<2; j++) 
            cin>>p[i][j];


    function<int(int,int)> findp=[&](int x, int y) {
        for(int i=0; i<3; i++) {
            if(p[i][0]==x & p[i][1]==y)
                return 1;
        }
        return 0;
    };

    sort(all(p));

    /* case dist of 2x2 rect<2 */
    int cnt=findp(0,0)+findp(1,0)+findp(0,1);
    if(cnt>0) {
        cout<<3-cnt<<endl;
        return;
    }

    int minx=min(p[0][0], min(p[1][0], p[2][0]));
    int miny=min(p[0][1], min(p[1][1], p[2][1]));

    int stepx=abs(minx-1);
    int stepy=abs(miny-1);

    int ans=min(stepx, stepy);
    // ???
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
