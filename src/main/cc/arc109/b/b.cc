
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We can buy the n+1 and split it to 1 and n
 * Then there is 1 to n avail and 
 * 2 to n-1 needed.
 * Buy the n and split into 2 and n-2.
 * Buy the n-1
 * Avail 1..n-2
 * Needed 3..n-3
 * Buy the n-2 ans split into 3 and n-5...
 *
 * So we can buy one for two until 
 * i + sum(1..i)>n
 **/
void solve() {
    cini(n);

    int cnt=0;
    int sum=0;
    int ans=n;
    for(int i=1; ; i++) {
        sum+=i;
        if(n+1-sum>i)
            ans--;
        else 
            break;
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
