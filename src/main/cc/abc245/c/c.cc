/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Allways choose max possible.
 *
 * ...No, its abs(x[i]-x[i+1])
 * So, some dp?
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);
    cinai(b,n);

    vi dp={ a[0], b[0] };

    for(int i=1; dp.size() && i<n; i++) {
        set<int> s;
        if(abs(dp[0]-a[i])<=k)
            s.insert(a[i]);
        if(abs(dp[0]-b[i])<=k)
            s.insert(b[i]);
        if(dp.size()>1) {
            if(abs(dp[1]-a[i])<=k)
                s.insert(a[i]);
            if(abs(dp[1]-b[i])<=k)
                s.insert(b[i]);
        }
        dp.clear();
        for(int j : s)
            dp.push_back(j);
    }
    if(dp.size()==0)
        cout<<"No"<<endl;
    else
        cout<<"Yes"<<endl;
}

signed main() {
    solve();
}
