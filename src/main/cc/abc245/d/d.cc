/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Thats so stupid I do not want to implement it.
 * ...
 * 1 indexed arrays
 *
 * c[n+m]=a[n]*b[m]
 * b[m]=c[n+m]/a[n]
 *
 * c[n+m-1]=a[n]*b[m-1]+a[n-1]*b[m]
 * c[n+m-1]-a[n-1]*b[m]=a[n]*b[m-1]
 * b[m-1]=(c[n+m-1]-a[n-1]*b[m]) / a[n]
 * ...???
 *
 * Other way round:
 * 0 indexed arrays
 * c[0]=a[0]*b[0]
 * c[1]=a[0]*b[1] + a[1]*b[0]
 * c[2]=a[0]*b[2] + a[1]*b[1] + a[2]*b[0]
 * ...
 *
 * c[n+m-0]=a[n]*b[m]
 * c[n+m-1]=a[n-1]*b[m] + a[n]*b[m-1]
 * c[n+m-2]=a[n-2]*b[m] + a[n-1]*b[m-1] + a[n]*b[m-2]
 */
void solve() {
    cini(n);
    cini(m);

    vi a(n+1);
    for(int i=0; i<=n; i++) 
        cin>>a[i];
    vi c(n+m+1);
    for(int i=0; i<=n+m; i++) 
        cin>>c[i];
        
    vi b(m+1);
    for(int i=0; i<=m; i++) {
        for(int k=1; k<=i; k++) {
            if(k<=n && i-k<=m)
                c[i]-=a[k]*b[i-k];
        }
        if(a[0]!=0)
            b[i]=c[i]/a[0];
    }
    for(int i=0; i<=m; i++)
        cout<<b[i]<<" ";
    cout<<endl;
}

signed main() {
    solve();
}
