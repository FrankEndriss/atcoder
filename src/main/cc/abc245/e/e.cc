/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider the boxes sorted by one dimension, let it c[].
 *
 * Consider the first box c[0],d[0].
 * It would make sence to put the biggest possible b[j] into that box.
 *
 * So maintain a set of chocolate sorted by b[].
 */
void solve() {
    cini(n);
    cini(m);

    vector<pii> coc(n);
    for(int i=0; i<n; i++) 
        cin>>coc[i].first;
    for(int i=0; i<n; i++)
        cin>>coc[i].second;

    vector<pii> box(m);
    for(int i=0; i<m; i++) 
        cin>>box[i].first;
    for(int i=0; i<m; i++)
        cin>>box[i].second;

    sort(all(coc)); /* sorted by a */
    sort(all(box)); /* sorted by c */

    multiset<int> acoc;

    int idx=0;
    int cnt=0;
    for(int i=0; i<m; i++) {    /* iterate the boxes, and put max choclate into them */
        /* put all coclate that fits a,c into acoc */
        while(idx<n && coc[idx].first<=box[i].first) {
            //cerr<<"put acoc, coc="<<coc[idx].first<<"/"<<coc[idx].second<<endl;
            acoc.emplace(-coc[idx].second);
            idx++;
        }

        if(acoc.size()) {
            auto it=acoc.lower_bound(-box[i].second);
            if(it!=acoc.end()) {
                acoc.erase(it);
                cnt++;
            }
            //cerr<<"coc "<<it->second<<"/"<<it->first<<" into box "<<box[i].first<<"/"<<box[i].second<<endl;
        }
    }

    if(cnt==n)
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;
}

signed main() {
    solve();
}
