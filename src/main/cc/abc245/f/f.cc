/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We want to find all vertex that lead into a circle.
 *
 * Binary lifting?
 *
 * So, vertex with no outgoing edges can be removed.
 * Then we are basically left with only circels and vertex
 * pointing into circles.
 * So just do that.
 */
void solve() {
    cini(n);
    cini(m);

    vector<set<int>> adj(n);
    vector<set<int>> adjR(n);
    for(int i=0; i<m; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].insert(v);
        adjR[v].insert(u);
    }

    queue<int> q;
    for(int i=0; i<n; i++) {
        if(adj[i].size()==0)
            q.push(i);
    }

    int ans=n;
    while(q.size()) {
        ans--;
        int v=q.front();
        q.pop();

        for(int p : adjR[v]) {
            adj[p].erase(v);
            if(adj[p].size()==0)
                q.push(p);
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}
