/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Simple dfs, since switch can be used arbitrary
 * ...
 * No, the switches are on the vertex, not on the edges.
 * So, we need to consider...somehow... the edgestate while dfs.
 * How?
 * No, no need to consider the state.
 * It is simply that each switcheable vertex can use all edges.
 * On the path from 1 to N we never us a vertex twice.
 *
 * ****
 * ...ok, other problem.
 * The switch op switches _all_ edges in the graph.
 * How to do that?
 * -> We need to consider that we reach vertex v in orig state
 *  or in switched state.
 */
void solve() {
    cini(n);
    cini(m);
    cini(k);

    vvi adj(n); /* adj[i]=list of edgeids */
    vvi edg(m);

    for(int i=0; i<m; i++) {
        cini(u);
        cini(v);
        cini(a);
        adj[u-1].push_back(i);
        adj[v-1].push_back(i);
        edg[i]={ u-1, v-1, a };
    }

    vb s(n);
    for(int i=0; i<k; i++) {
        cini(ss); ss--;
        s[ss]=true;
    }

    const int INF=1e9;
    vvi dp(2, vi(n, INF));
    dp[0][0]=0;

    queue<pii> q;
    q.emplace(0,0);

    if(s[0]) {
        dp[1][0]=0;
        q.emplace(1,0);
    }

    while(q.size()) {
        auto [flip,v]=q.front();
        q.pop();
        for(int e : adj[v]) {
            if(flip+edg[e][2]==1) {
                if(edg[e][1]==v && dp[flip][edg[e][0]]>dp[flip][v]+1) {
                    dp[flip][edg[e][0]]=dp[flip][v]+1;
                    q.emplace(flip, edg[e][0]);
                    if(s[edg[e][0]]) {
                        dp[!flip][edg[e][0]]=dp[flip][v]+1;
                        q.emplace(!flip, edg[e][0]);
                    }
                }

                if(edg[e][0]==v && dp[flip][edg[e][1]]>dp[flip][v]+1) {
                    dp[flip][edg[e][1]]=dp[flip][v]+1;
                    q.emplace(flip, edg[e][1]);
                    if(s[edg[e][1]]) {
                        dp[!flip][edg[e][1]]=dp[flip][v]+1;
                        q.emplace(!flip, edg[e][1]);
                    }
                }
            }
        }
    }

    int ans=min(dp[0][n-1], dp[1][n-1]);
    if(ans==INF)
        ans=-1;

    cout<<ans<<endl;
}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
