/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
void solve() {
    cini(n);
    set<string> ss;
    for(int i=0; i<n; i++) {
        cins(s);
        if(s[0]!='H' &&
         s[0]!='D' &&
         s[0]!='C' &&
         s[0]!='S') {
            cout<<"No"<<endl;
            return;
        }

        if(s[1]!='A' &&
         s[1]!='2' &&
         s[1]!='3' &&
         s[1]!='4' &&
         s[1]!='5' &&
         s[1]!='6' &&
         s[1]!='7' &&
         s[1]!='8' &&
         s[1]!='9' &&
         s[1]!='T' &&
         s[1]!='J' &&
         s[1]!='Q' &&
         s[1]!='K') {
            cout<<"No"<<endl;
            return;
        }

        ss.insert(s);
    }

    if(ss.size()!=n) {
            cout<<"No"<<endl;
            return;
    }

    cout<<"Yes"<<endl;
}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
