/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Ok, we can put down all consecutive cards.
 * Go from left to right, find best sum to lay down.
 */
void solve() {
    cini(n);
    cini(m);
    cinai(a,n);
    sort(all(a));
    const int sumall=accumulate(all(a), 0LL);

    for(int i=0; i<n; i++) 
        a.push_back(a[i]);

    int sum=a[0];
    int ans=sumall;
    for(size_t i=1; i<a.size(); i++) {
        if(a[i]==a[i-1] || a[i]==(a[i-1]+1)%m) {
            ;
        } else 
            sum=0;

        sum+=a[i];
        ans=max(0LL, min(ans, sumall-sum));
    }
    cout<<ans<<endl;
}


signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
