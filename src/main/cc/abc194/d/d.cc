
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* State is number of connected vertex, starting with cnt=1
 * At each step either nothing happens cnt/N,
 * or 1 vertex is added (N-cnt)/N
 *
 * We want to find the expected number of times we have to choose
 * a vertex until all vertex are choosen at least once.
 *
 * So let dp[i] be prop that there are i vertex choosen
 * after current step, init dp[1]=1.0;
 *
 * but...it is not a dp, because N does not fit a O(n^x)
 * We somehow have to create a formular.
 * ...
 * blah... see
 * https://blogs.sas.com/content/iml/2011/07/20/how-many-times-must-you-roll-a-die-until-each-side-has-appeared.html
 */
const int N=1e5;
const int NN=5000;
void solve() {
    cini(n);
    ld ans=0;
    ld nn=n;
    for(int i=1; i<n; i++) 
        ans+=nn/i;
    cout<<ans<<endl;
}

signed main() {
    solve();
}
