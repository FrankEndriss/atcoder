
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;
using lll=__int128;

#define endl "\n"

/* 
 * Find the sum of all positive progs starting at 0
 * with sum<=2*N (~2*sqrt(N))
 * Then foreach find if there is a diff of two eq n.
 *
 * Then the negative ones. Note that they are the same as
 * the positive ones. So count that ones twice.
 *
 * How to find the seqs starting with a value>sqrt(n)?
 *
 * We cannot iterat up to N.
 *
 * Sum of such seq is k*(k+1)/2
 * We need to find some k0 and k1 such that the diff eq N.
 * Let k1 be k0+x. Then:
 * (k+x)*(k+x+1)/2 - n = k*(k+1)/2
 * (k^2 + 2kx + k + x - k^2 - k)/2 =n
 * n=2k*x + x
 * n=(2*k+1)*x
 * (2*n)/x = some odd number
 * ??? idk :/
 * ***
 * Consider the sum of a sequence of consecutive ints, let first be l, last be r:
 * N = (l+r)/2 * (r-l+1)
 * The right multiplier is the length  of the sequence. 
 * 1. Note that the length of the seq determines the whole seq, because there are
 *    allways exactly one seq.
 *    (given l is positive, for each such seq there is another one starting with 
 *     0 or a negative number).
 * 2. 2*N is diviseable by the length of the seq
 * 3. We can binary search if there exists a seq for a given length
 */
void solve() {
    cini(n);

    const int nn=n+n;
    //const int nn2=sqrt(n)+7;

    set<int> d;
    for(int i=1; i*i<=nn; i++) { /* simplest factorization */
        if(nn%i==0) {
            d.insert(i);
            if(i>1)
                d.insert(nn/i);
        }
    }

    //cerr<<"n="<<n<<endl;
    int ans=0;
    for(int len : d) {
        //cerr<<"check len="<<len<<endl;
        /* now find the first element of a seq of len len with sum >= n.
         * if that sum==n we found a seq. 
         * Note that the first element cannot be bigger than n/len.
         **/

        lll l=1;
        lll r=n/len+1;
        while(l+1<r) {
            lll mid=(l+r)/2;
            lll ssum=((mid+mid+len-1)*len)/2;
            //cerr<<"mid="<<mid<<" sum="<<ssum<<endl;
            if(ssum<=n)
                l=mid;
            else
                r=mid;
        }
        lll sum=((l+l+len-1)*len)/2;
        //cerr<<"l="<<l<<" sum="<<sum<<endl;
        if(n==sum)
            ans+=2;

    }

    cout<<ans<<endl;

}

signed main() {
    solve();
}
