
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

template<typename T>
void floyd_warshal(vector<vector<T>> &d, int n) {
    for (int k = 0; k < n; ++k) {
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                d[i][j] = min(d[i][j], d[i][k] + d[k][j]);
            }
        }
    }
}

/* We have a graph, and some given vertex (c[])
 * We want to find the shortest path including all vertex
 * given in c[].
 *
 * How to find the shortest path over some given vertex?
 * Note that the number of vetex is small, so its 
 * traveling salesman brute force.
 * Implement recursive each possible permutation of the c[] gems.
 * First find all distances by floyd-warshal.
 *
 * ...
 * Floyd warshal does not work with N=1e5, 
 * and the permutations does not work with K=17
 * So, again, how to find the shortest path 
 * over that max 17 vertex?
 * Ok, we can bfs the shortest path to all other vertex for 
 * those 17 ones.
 * And then?
 * Still we need to check 16! permutations, which is ~10^12
 * ***
 * No, we can do a dp over the remaining vertex as a bitset...
 * but only 7 minutest left... :/
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);

    vvi adj(n);

    for(int i=0; i<m; i++) {
        cini(a); a--;
        cini(b); b--;
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    cini(k);
    cinai(c,k);
    for(int i=0; i<k; i++) 
        c[i]--;

    vvi dp(k, vi(n,INF));    /* dp[i][j]= dist from vertex c[i] to vertex j */
    for(int i=0; i<k; i++) {
        dp[i][c[i]]=0;
        queue<int> q;
        q.push(c[i]);
        while(q.size()) {
            int v=q.front();
            q.pop();
            for(int chl : adj[v]) {
                if(dp[i][chl]==INF) {
                    dp[i][chl]=dp[i][v]+1;
                    q.push(chl);
                }
            }
        }
    }

    vvi memo(k, vi(1<<k, -1));
    function<int(int,int)> go=[&](int cur, int mask) {
        if(mask==0)
            return 0LL;

        if(memo[cur][mask]>=0)
            return memo[cur][mask];

        int mi=INF;
        for(int i=0; i<k; i++) {
            if(mask&(1<<i)) {
                mi=min(mi, dp[cur][c[i]]+go(i, mask^(1<<i)));
            }
        }
        return memo[cur][mask]=mi;
    };

    int mask=(1<<k)-1;

    int ans=INF;
    for(int i=0; i<k; i++) 
        ans=min(ans, 1+go(i, mask^(1<<i)));

    if(ans==INF)
        ans=-1;
    cout<<ans<<endl;
}

signed main() {
    solve();
}
