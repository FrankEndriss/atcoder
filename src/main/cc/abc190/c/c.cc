
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * Try all 2^16 possiblities.
 */
const int N=100;
void solve() {
    cini(n);
    cini(m);

    vi a(m);
    vi b(m);
    for(int i=0; i<m; i++) {
        cin>>a[i]>>b[i];
        a[i]--;
        b[i]--;
    }

    cini(k);
    vi c(k);
    vi d(k);

    for(int i=0; i<k; i++) {
        cin>>c[i]>>d[i];
        c[i]--;
        d[i]--;
    }

    int ans=0;
    /* bit=0 -> dish c[i], else dish d[i] */
    for(int i=0; i<(1<<k); i++) {
        vi cnt(n);      /* cnt[d]==balls on dish d */
        for(int j=0; j<k; j++) {
            if(i&(1<<j))
                cnt[c[j]]++;
            else
                cnt[d[j]]++;
        }
        int lans=0;
        for(int i=0; i<m; i++) {
            if(cnt[a[i]] && cnt[b[i]]) {
                lans++;
            }
        }
        ans=max(ans, lans);
    }

    cout<<ans<<endl;

}

signed main() {
    solve();
}
