/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

/*
#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")
*/

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cins(s);
    cins(t);

    vvi f('z'+1);
    for(size_t i=0; i<s.size(); i++)
        f[s[i]].push_back(i);

    ll ans1=0;
    ll ans2=-1;
    for(size_t i=0; i<t.size(); i++) {
        if(f[t[i]].size()==0) {
            ans1=0;
            ans2=-2;
            break;
        }

        auto it=upper_bound(f[t[i]].begin(), f[t[i]].end(), ans2);
        if(it==f[t[i]].end()) {
            ans1++;
            ans2=f[t[i]][0];
        } else
            ans2=*it;
    }

    ans1*=s.size();
    ll ans=ans1+ans2+1;
    cout<<ans<<endl;
}

