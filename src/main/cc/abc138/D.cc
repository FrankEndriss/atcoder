/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

/*
#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")
*/

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(q);
    vvi tree(n+1);
    for(int i=0; i<n-1; i++) {
        cini(a);
        cini(b);
        tree[a].push_back(b);
        tree[b].push_back(a);
    }

    vll cnt(n+1);
    for(int i=0; i<q; i++) {
        cini(p);
        cini(x);
        cnt[p]+=x;
    }

    vll ans(n+1);
    function<void(int, int)> dfs=[&](int node, int p) {
        ans[node]=ans[p]+cnt[node];
        for(int c : tree[node])
            if(c!=p)
                dfs(c, node);
    };

    dfs(1, 0);

    for(int i=1; i<=n; i++)
        cout<<ans[i]<<" ";
    cout<<endl;
}

