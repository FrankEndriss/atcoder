
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/** faster solution */
const int MAXN = 61;
ll C[MAXN + 1][MAXN + 1];

const bool init_nCr=[]() {
    C[0][0] = 1;
    for (int n = 1; n <= MAXN; ++n) {
        C[n][0] = C[n][n] = 1;
        for (int k = 1; k < n; ++k) {
            C[n][k] = C[n - 1][k - 1] + C[n - 1][k];
        }
    }
    return true;
}();

ll nCr(int n, int k) {
    return C[n][k];
}

/* aaaabbbb
 * aaababbb
 * aaabbabb
 * ...
 * aabaabbb
 * aabababb
 * ...
 * aababbba
 * Number of strings of len x with A 'a' is nCr(x,A)
 * So we check the prefixes until we found the right one.
 */
void solve() {
    cini(a);
    cini(b);
    cini(k);

    int cnt=a+b;

    string ans;
    for(int i=0; i<cnt; i++) {
        int num=nCr(a+b-1, a-1);    /* consider first letter a */
        if(b>0 && (a==0 || num<k)) {
            k-=num;
            ans.push_back('b');
            b--;
        } else {
            ans.push_back('a');
            a--;
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}
