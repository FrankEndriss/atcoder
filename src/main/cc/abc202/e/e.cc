
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* LCA based on euler path with segment tree and tin/tout. */
struct LCA {
    vector<int> height, euler, first, segtree;
    vector<bool> visited;
    int n;
    int time;
    vector<int> tin;
    vector<int> tout;

    LCA(vector<vector<int>> &adj, int root = 0) {
        n = (int)adj.size();
        height.resize(n);
        first.resize(n);
        euler.reserve(n * 2);
        visited.assign(n, false);
        tin.resize(n);
        tout.resize(n);
        time=1;
        dfs(adj, root);
        int m = (int)euler.size();
        segtree.resize(m * 4);
        build(1, 0, m - 1);
    }

    void dfs(vector<vector<int>> &adj, int node, int h = 0) {
        tin[node]=time++;
        visited[node] = true;
        height[node] = h;
        first[node] = (int)euler.size();
        euler.push_back(node);
        for (auto to : adj[node]) {
            if (!visited[to]) {
                dfs(adj, to, h + 1);
                euler.push_back(node);
            }
        }
        tout[node]=time++;
    }

    void build(int node, int b, int e) {
        if (b == e) {
            segtree[node] = euler[b];
        } else {
            int mid = (b + e) / 2;
            build(node << 1, b, mid);
            build(node << 1 | 1, mid + 1, e);
            int l = segtree[node << 1], r = segtree[node << 1 | 1];
            segtree[node] = (height[l] < height[r]) ? l : r;
        }
    }

    int query(int node, int b, int e, int L, int R) {
        if (b > R || e < L)
            return -1;
        if (b >= L && e <= R)
            return segtree[node];
        int mid = (b + e) >> 1;

        int left = query(node << 1, b, mid, L, R);
        int right = query(node << 1 | 1, mid + 1, e, L, R);
        if (left == -1) return right;
        if (right == -1) return left;
        return height[left] < height[right] ? left : right;
    }

    /* @return true if parent is ancestor of node in O(1) */
    bool isAncestor(int node, int parent) {
        return tin[node]>tin[parent] && tin[node]<tout[parent];
    }

    /* @return the lca of u and v */
    int lca(int u, int v) {
        int left = first[u], right = first[v];
        if (left > right)
            swap(left, right);
        return query(1, 0, (int)euler.size() - 1, left, right);
    }
};

/* In the queries, we need to find
 * the number of vertex 
 * in the dfs(U[i], p[U[i]]) with level D[i] from root.
 *
 * This is O(n^2) so we need to memoize somehow.
 * Simply put it in a map?
 *
 * dp[U[i]][x]= sum(dp[childOfUi][x-1])
 * ... but that O(n^2), too :/
 *
 * We can find the number of vertex at depth[D[i]] from 
 * root in one dfs, and also the depth[U[i]]
 * So ans is the number of vertex of depth d[i]
 * having U[i] as ancestor.
 *
 * isAnc(p,v) goes in O(1), but we cannot iterate all vertex.
 * So, what to dp[v]?
 *
 * Consider this to be some heavy/light decomposition problem.
 * somehow... :/
 *
 * Can we somehow sqrt decomposite the vertex on depth[i]?
 * ****
 * Ok, try the brute force:
 * Iterate all vertex on depth d[i] and count the ones
 * having u[i] as ancestor.
 * ***
 * Other dp would be dp[v][d]= number of vertex on level d with v as root.
 * So, dp[v][d]=sum(dp[adj[v]][d-1]) over all childs of v
 * Problem is that dimension of O(n^2)
 * Or put only the logN depths in it?
 */
void solve() {
    cini(n);
    vvi adj(n);
    vi pp(n, -1);
    for(int i=1; i<n; i++) {
        cini(p);
        p--;
        adj[p].push_back(i);
        pp[i]=p;
    }

    vi lvlV(n);
    vvi lvl(1<<20);  /* lvl[i]=list of vertex with distance i from root */

    function<void(int)> dfs=[&](int v, int d) {
        lvlV[v]=d;
        lvl[d].push_back(v);
        for(int c : adj[v])
            dfs(v, d+1);
    }

    dfs(0, 0);

    /* dp[v][i]=childs of vertex v in distance 1<<i from v */
    vvvi dp(n, vvi(20));
    for(int i=0; (1LL<<i)<n; i<<=1) 
        dp[0][i]=lvl[1LL<<i];

    function<void(int)> dfs2[&](int v) {
        for(int c : adj[v]) {
            dfs2(c);
            for(int i=1; i<20; i++) {
                dp[v][i]+=dp[c][i-1];
            }
        }

    };




    cini(q);
    for(int i=0; i<q; i++) {
        cini(u);
        u--;
        cini(d);
        int ans=0;
        for(int v : byH[d])
            if(v==u || lca.isAncestor(v,u))
                ans++;
        cout<<ans<<endl;
    }

}

signed main() {
    solve();
}
