
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * How to find the smallest possible number of operations of taka?
 * binsearch?
 *
 * Consider removing a weed. It seems allways optimal to remove
 * the biggest or the smallest. However, it could also be optimal
 * to remove a middle one. Consider
 * 1,2,6,20,21
 * Here, removing 6 makes 2 operations, any else 3 operations.
 *
 * Pulling a weed never increases the number of operations.
 * So, we need to find the min operations if all k weeds are pulled,
 * then find if we can have the same number operations with less than
 * k pulled weeds.
 *
 *
 */
void solve() {
    cini(n);
    cini(k);

    cinai(a,n);
    sort(all(a));

    if(k>=n) {
        cout<<0<<endl;
        return;
    }


}

signed main() {
    solve();
}
