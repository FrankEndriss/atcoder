
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* add/remove elements, get median() element in O(1) */
struct Median {
    multiset<int> lo;    // smaller elements
    multiset<int> hi;    // bigger elements
    int sumlo=0;
    int sumhi=0;

    void norm() {
        while(lo.size()+1<hi.size()) {
            auto it=hi.begin();
            sumlo+=*it;
            sumhi-=*it;
            lo.insert(*it);
            hi.erase(it);
        }

        while(lo.size()>hi.size()) {
            auto it=lo.end();
            it--;
            sumlo-=*it;
            sumhi+=*it;
            hi.insert(*it);
            lo.erase(it);
        }

        while(lo.size()>0 && *lo.rbegin()>*hi.begin()) {
            auto itlo=lo.end();
            itlo--;
            hi.insert(*itlo);
            sumhi+=*itlo;
            sumlo-=*itlo;
            lo.erase(itlo);
            auto it=hi.begin();
            lo.insert(*it);
            sumlo+=*it;
            sumhi-=*it;
            hi.erase(it);
        }
    }

    void add(int a) {
        if(lo.size()<hi.size()) {
            lo.insert(a);
            sumlo+=a;
        } else {
            hi.insert(a);
            sumhi+=a;
        }
        norm();
    }

    void remove(int a) {
        if(a<*hi.begin()) {
            auto it=lo.find(a);
            assert(it!=lo.end());
            sumlo-=*it;
            lo.erase(it);
        } else {
            auto it=hi.find(a);
            assert(it!=hi.end());
            sumhi-=*it;
            hi.erase(it);
        }
        norm();
    }

    int median() {
        if(hi.size()>lo.size())
            return *(hi.begin());
        else
            return *(lo.rbegin());
    }
};

/* Median of K*K cells in N*N field
 * move a Median of the heights.
 *
 * Actually TLE, idk :/
 * ***********
 * Ok, how does the dp works?
 * Can we somehow utilize kind of prefix sums or the like?
 * let dp[i][j]=median of k*k rect with down/right corner at i,j.
 *
 * Can we binsearch ans?
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(k);

    vvi a(n, vi(n));
    for(int i=0; i<n; i++)
        for(int j=0; j<n; j++)
            cin>>a[i][j];


    Median med;
    for(int i=0; i<k; i++)
        for(int j=0; j<k; j++)
            med.add(a[i][j]);

    int ans=med.median();
    //cerr<<"ans="<<ans<<endl;

    int dir=1;
    for(int i=0; i+k<=n; i++) {
        ans=min(ans, med.median());

        if(dir==1) {
            for(int j=0; j+k<n; j++) {
                for(int ii=i; ii<i+k; ii++) {
                    med.remove(a[ii][j]);
                    med.add(a[ii][j+k]);
                }
                ans=min(ans, med.median());
            }
        } else {
            for(int j=n-k; j>0; j--) {
                for(int ii=i; ii<i+k; ii++) {
                    med.remove(a[ii][j+k-1]);
                    med.add(a[ii][j-1]);
                }
                ans=min(ans, med.median());
            }
        }
        dir=-dir;

        /* move one row down */
        if(i+k<n) {
            if(dir==1) {
                for(int j=0; j<k; j++) {
                    med.remove(a[i][j]);
                    med.add(a[i+k][j]);
                }
            } else {
                for(int j=n-k; j<n; j++) {
                    med.remove(a[i][j]);
                    med.add(a[i+k][j]);
                }
            }
        }
        ans=min(ans, med.median());
    }

    cout<<ans<<endl;

}

signed main() {
    solve();
}
