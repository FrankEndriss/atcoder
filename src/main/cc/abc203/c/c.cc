
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * WA.
 * Some overflow?
 * Got the statement wrong?
 * -> villages are not sorted :/
 */
void solve() {
    cini(n);
    cini(k);

    vector<pii> ab(n);

    for(int i=0; i<n; i++)
        cin>>ab[i].first>>ab[i].second;

    sort(all(ab));

    int pos=0;
    for(int i=0; i<n; i++) {
        pos+=k;
        k=0;
        if(pos>=ab[i].first) {
            k+=ab[i].second;
        } else {
            break;
        }
    }
    pos+=k;
    cout<<pos<<endl;
}

signed main() {
    solve();
}
