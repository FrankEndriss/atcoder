
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* simple bfs, just iterate
 * ...not possible, since N=1e9
 * So, sort black pawns by row, then maintain possible cols
 * of the white pawn while iteration plack pawns.
 * */
void solve() {
    cini(n);
    cini(m);

    vector<pii> xy(m);
    for(int i=0; i<m; i++)
        cin>>xy[i].first>>xy[i].second;

    sort(all(xy));

    set<int> w;
    w.insert(n);

    for(int i=0; i<m;) {
        int now=xy[i].first;
        vi ins;
        vi rem;
        while(i<m && xy[i].first==now) {
            int l=w.count(xy[i].second-1);
            int r=w.count(xy[i].second+1);

            if(l+r==0)
                rem.push_back(xy[i].second);
            else
                ins.push_back(xy[i].second);
            i++;
        }
        for(int j : rem)
            w.erase(j);
        for(int j : ins)
            w.insert(j);
    }
    cout<<w.size()<<endl;

}

signed main() {
    solve();
}
