
/**
 *  * Dont raise your voice, improve your argument.
 *   * --Desmond Tutu
 *    */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 *  * What???
 *   * Where does i come from in the values of k?
 *    *
 *     * Ok, there are M swaps in B, result is B'
 *      * Now we want to answer i in 1..M
 *       * if the ith swap was not done we want to find the position of the '1'.
 *        * Swaps are all of adj elements.
 *         *
 *          * Note that if the not executed swap did not include the 1, its final position
 *           * is not else that doing all swaps.
 *            * Else it is the position of the number that it was swapped with.
 *             *
 *              * So just do all swaps, then iterated 1..M and foreach one ans=position of
 *               * the 1 or the number swapped with the 1 in jth step.
 *                *
 *                 * Since this is only stupid simulation, the whole problem is only
 *                  * text understanding, sic :/
 *                   */
void solve() {
    cini(n);
    cini(m);

    cinai(a,m);
    for(int i=0; i<m; i++)
        a[i]--;

    vi b(n);
    iota(all(b), 0LL);

    vi swl(m);
    vi swr(m);
    for(int i=0; i<m; i++)  {
        swl[i]=b[a[i]];
        swr[i]=b[a[i]+1];
        swap(b[a[i]], b[a[i]+1]);
    }

    vi pos(n);
    for(int i=0; i<n; i++)
        pos[b[i]]=i;

    for(int i=0; i<m; i++) {
        if(swl[i]!=0 && swr[i]!=0)
            cout<<pos[0]+1<<endl;
        else if(swl[i]==0)
            cout<<pos[swr[i]]+1<<endl;
        else
            cout<<pos[swl[i]]+1<<endl;
    }

}

signed main() {
    solve();
}
