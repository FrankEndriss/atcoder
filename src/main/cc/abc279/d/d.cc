/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"



/* ternary search, see https://en.wikipedia.org/wiki/Ternary_search */
/* return the point in [l,r] where f(x) is max */
constexpr ld EPS=0.0000001;
ld terns(ld l, ld r, function<ld(ld)> f) {
    while(l+EPS<r) {
        const ld l3=l+(r-l)/3;
        const ld r3=r-(r-l)/3;
        if(f(l3)<f(r3))
            l=l3;
        else
            r=r3;
    }
    return (l+r)/2;
}


/**
 * We need to find k, so that
 * f(k-1) > f(k) < f(k+1)
 */
void solve() {
    cini(a);
    cini(b);

    function<ld(ld)> f=[&](ld g) {
        return -( (g-1)*b + a/sqrt(g) );
    };

    ld ans=terns(1, 1e18, f);

    int ians0=ans;
    int ians1=ans+1;

    cout<<-max(f(ians0), f(ians1))<<endl;
}

signed main() {
        solve();
}
