/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Names are connected, and we need to two=color the graph.
 * ...No, not at all.
 * How?
 *
 * ...got it wrong.
 * We simply need to find if s[i] or t[i] is distinct.
 * Edgecase if s[i]==t[i]
 */
void solve() {
        cini(n);


        map<string,int> f;
        vs s(n);
        vs t(n);
        for(int i=0; i<n; i++) {
            cin>>s[i]>>t[i];
            f[s[i]]++;
            f[t[i]]++;
        }

        for(int i=0; i<n; i++) {
            int cnt1=f[s[i]];
            int cnt2=f[t[i]];
            if(cnt1>1 && cnt2>1) {
                if(s[i]!=t[i]) {
                    cout<<"No"<<endl;
                    return;
                } else if(cnt1>2) {
                    cout<<"No"<<endl;
                    return;
                }
            }
        }
        cout<<"Yes"<<endl;
}

signed main() {
    solve();
}
