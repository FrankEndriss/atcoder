/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
void solve() {
    cini(n);
    cini(x);

    cins(s);

    string t;
    for(char c : s) {
        if(c=='U') {
            if(t.size()>0 && t.back()!='U')
                t.pop_back();
            else
                x/=2;
        } else if(c=='R') {
            t.push_back('R');
        } else if(c=='L') {
            t.push_back('L');
        }
    }

    for(char c : t) {
        if(c=='R')
            x=x*2+1;
        else if(c=='L')
            x=x*2;
        else
            assert(false);
    }

    cout<<x<<endl;
}

signed main() {
    solve();
}
