/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
void solve() {
    cini(n);
    vi x(n);
    vi y(n);

    for(int i=0; i<n; i++) 
        cin>>x[i]>>y[i];
    cins(s);

    map<int,vi> mL; /* mL[i]=list of people in row i walking Left */
    map<int,vi> mR;

    for(int i=0; i<n; i++) {
        if(s[i]=='L')
            mL[y[i]].push_back(x[i]);
        else if(s[i]=='R')
            mR[y[i]].push_back(x[i]);
        else
            assert(false);
    }

    for(auto [yy,a] : mL) {
        vi b=mR[yy];

        if(b.size()>0 && a.size()>0 && *min_element(all(b)) < *max_element(all(a))) {
            cout<<"Yes"<<endl;
            return;
        }
    }

    cout<<"No"<<endl;

}

signed main() {
    solve();
}
