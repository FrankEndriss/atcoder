/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


template<typename T>
void floyd_warshal(vector<vector<T>> &d, int n) {
    for (int k = 0; k < n; ++k) {
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                d[i][j] = min(d[i][j], d[i][k] + d[k][j]);
            }
        }
    }
}

/**
 * Find a minimum spanning tree, and remove all other vertex.
 *
 * ...really?
 * Consider n=3, edges
 * 1 2 c=1
 * 2 3 c=1
 * 3 1 c=1
 * Here we can remove nothing.
 *
 * What edges can be removed after all?
 * If there is an cheaper connection between two vertex,
 * that edge can be removed.
 * So do Floyd-Warshal.
 *
 * Does not work because:
 * What about equal cheap connections? Here we can actually 
 * remove an edge that was not changed by floyd-warshal.
 * How to find them?
 * idk :/
 * ****************
 * see tutorial:
 * We can remove an edge, if there exists a path with at least
 * two vertex that has same or smaller cost.
 * So we can check each edge in O(n) by iterating the distance matrix.
 * Note that we must not initialize the d[i][i]=0, instead treat them as INF.
 */
const int INF=1e18;
void solve() {
    cini(n);
    cini(m);

    using t3=tuple<int,int,int>;
    vector<t3> edg;

    vvi adj(n, vi(n, INF));

    for(int i=0; i<m; i++) {
        cini(u); u--;
        cini(v); v--;
        cini(c);
        edg.emplace_back(u,v,c);
        adj[u][v]=min(adj[u][v],c);
        adj[v][u]=min(adj[v][u],c);
    }

    floyd_warshal(adj, n);

    int cnt=0;
    for(int i=0; i<m; i++) {
        auto [u,v,c]=edg[i];
        if(adj[u][v]<c) {
            cnt++;
            continue;
        }

        int unused=0;
        for(int j=0; j<n; j++) {
            if(adj[u][j]+adj[j][v]<=c) {
                unused=1;
            }
        }
        cnt+=unused;
    }

    cout<<cnt<<endl;

}

signed main() {
    solve();
}
