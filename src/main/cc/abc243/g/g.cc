/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider big x.
 * In first step we can append y[1]=1..sqrt(x)
 * In second step we can append at each one
 *  y2=1..sqrt(y[1])
 *
 * How man steps to we have at most?
 * -> the 6th number is garanteed to be 0
 *
 * Lets go the other direction.
 * What can be the last number before the first 1?
 * -> 2..x
 * If it is 2..sqrt(x)
 *
 * Consider length of seq before first 1.
 * If it is 6, then smallest possible numbers are:
 * 2^1, 2^2, 2^4, 2^8, 2^16, 2^32, (2^64 overlow)
 *
 * If it is 1, we got 1 sol.
 * If it is 2, then we got sqrt(x)-1 sols.
 * If it is 3, the third number can be one of sqrt(sqrt(x))-1 numbers,
 *    which is not to much (~2e5). Foreach such number j the second one
 *    can be one of sqrt(x)-(j*j), iterate.
 *
 * dp the number of possible postfixes up to sqrt(sqrt(x));
 */
const double INF=9e18;
const int sqsq=sqrt(sqrt(INF));
vi dp(sqsq+1,1);

void init() {
    dp[1]=1;
    dp[2]=1;

    for(int i=2; i<=sqsq; i++) {
        for(int j=i*i; j<=sqsq; j++) {
            dp[j]+=dp[i];      
        }
    }

}

void solve() {
    unsigned long long x;
    cin>>x;

    if(x<=sqsq) {
        cout<<dp[x]<<endl;
    } else {
        int ans=1;
        int xx=sqrt(sqrt(x)); /* third number */
        int sx=sqrt(x);
        for(int i=2; i<=xx; i++) {
            /* how many second numbers are there ? */
            ans+=dp[i]*(sx-(i*i));
        }
        cout<<ans<<endl;
    }
}

signed main() {
    init();
    cini(t);
    while(t--)
        solve();
}
