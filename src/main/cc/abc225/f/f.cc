
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * How to implement without checking all permutations?
 *
 * Consider having some cards in optimized order.
 * Can we simply try where to put the next card?
 *
 * Then sort all of them and choose the first k ones.
 *
 *
 * ...unable to implement :/
 */
void solve() {
    cini(n);
    cini(k);

    pair<string,vs> ans;
    for(int i=0; i<n; i++) {
        cins(s);
        vector<pair<string,vs>> a2(i+1);

        assert(ans.second.size()==i);

        /* pos==first element of right half */
        for(int pos=0; pos<i; pos++) {
            for(int j=0; j<pos; j++)  {
                a2[j].first+=ans.second[j];
                a2[j].second.push_back(ans.second[j]);
            }

            a2[pos].first+=s;
            a2[pos].second.push_back(s);

            for(int j=pos; j<i; j++)  {
                a2[pos].first+=ans.second[j];
                a2[pos].second.push_back(ans.second[j]);
            }
        }
        ans=*min_element(all(a2));
    }
    cout<<ans.first<<endl;
}

signed main() {
    solve();
}
