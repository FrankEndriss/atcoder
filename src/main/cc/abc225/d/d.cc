
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
void solve() {
    cini(nn);
    cini(q);

    vi n(nn, -1); /* next */
    vi p(nn, -1); /* prev */

    for(int i=0; i<q; i++) {
        cini(t);
        if(t==1) {
            cini(x); x--;
            cini(y); y--;
            n[y]=x;
            p[x]=y;
        } else if(t==2) {
            cini(x); x--;
            cini(y); y--;
            n[y]=-1;
            p[x]=-1;
        } else {
            cini(x); x--;
            int v=x;
            while(v<nn && v>=0 && p[v]>=0) {
                v=p[v];
            }

            stack<int> st;
            st.push(v+1);
            //cout<<v+1<< " ";
            while(v>=0 && v<nn && n[v]>=0) {
                v=n[v];
                st.push(v+1);
                //cout<<v+1<< " ";
            }
            cout<<st.size()<<" ";
            while(st.size()) {
                int i=st.top();
                cout<<i<<" ";
                st.pop();
            }
            cout<<endl;
        }
    }
}

signed main() {
    solve();
}
