
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/**
 * We got a number of slope intervals, and need to find
 * the biggest number of non intersecting ones.
 *
 * Fractions and things is actually to complecated to put it into my head :/
 */
const int INF=1e9;
void solve() {

    cini(n);
    vector<pair<pii,pii>> a;
    for(int i=0; i<n; i++) {
        cini(x);
        cini(y);
        a.push_back({{ y, x-1 }, { y-1, x}});
    }
    sort(all(a), [&](pair<pii,pii> p1, pair<pii,pii> p2) {
        if(p1.first.first * p2.first.second < p1.first.second * p2.first.first) {
            return true;
        } else if(p1.first.first * p2.first.second == p1.first.second * p2.first.first) {
            return p1.second.first * p2.second.second < p1.second.second * p2.second.first;
        } else
            return false;
    } );

    int ans=0;
    pii curr= {-INF,1e9};
    for(int i=0; i<n; i++) {
        if(curr.first * a[i].second.second < curr.second * a[i].second.first
                ||
           curr.first * a[i].second.second == curr.second * a[i].second.first ) {
            ans++;
            curr=a[i].first;
        }
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}
