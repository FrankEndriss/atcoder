
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* before each number we are at a position,
 * then can start with the leftmost of the next number,
 * or the rightmost.
 * So we end up at left or rightmost.
 * So outcome after each color is number of steps
 * and one of two positions.
 */
const int INF=1e18;
void solve() {
    cini(n);
    vi cmin(n, INF);
    vi cmax(n, -INF);
    for(int i=0; i<n; i++) {
        cini(x); 
        cini(c);c--;
        cmin[c]=min(x,cmin[c]);
        cmax[c]=max(x,cmax[c]);
    }

    int ansPosL=0;
    int ansCosL=0;

    int ansPosR=0;
    int ansCosR=0;

    for(int i=0; i<n; i++) {
        if(cmin[i]==INF)
            continue;

        /* start left, end right */
        int cosR=ansCosL+abs(ansPosL-cmin[i])+abs(cmin[i]-cmax[i]);
        /* start right, end right */
        cosR=min(cosR, ansCosR+abs(ansPosR-cmin[i])+abs(cmin[i]-cmax[i]));
        /* start left, end left */
        int cosL=ansCosL+abs(ansPosL-cmax[i])+abs(cmin[i]-cmax[i]);
        /* start right, end left */
        cosL=min(cosL, ansCosR+abs(ansPosR-cmax[i])+abs(cmin[i]-cmax[i]));

        ansPosL=cmin[i];
        ansCosL=cosL;
        ansPosR=cmax[i];
        ansCosR=cosR;
    }
    cout<<min(ansCosL+abs(ansPosL), ansCosR+abs(ansPosR))<<endl;
}

signed main() {
    solve();
}
