
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Let a spath be the chars written
 * on the edges of a path.
 * We need to find a vertex v where the spath(1,v) is 
 * same as spath(n,v)
 *
 * So we can maintain a set of identical paths, and do a bfs.
 * But there are more cases.
 * Consider palindromes of odd length.
 */
void solve() {
    cini(n);
    cini(m);
    vector<vector<pair<int,char>>> adj(n);
    for(int i=0; i<m; i++) {
        cini(a); a--;
        cini(b); b--;
        char c;
        cin>>c;

        adj[a].emplace_back(b,c);
        adj[b].emplace_back(a,c);
    }

    /* spath ending in vertex, first=from0, second=fromN */
    map<string,pair<set<int>,set<int>>> s;


    s[""]= { { 0 }, { n-1 }};

    while(true) {
        map<string,pair<set<int>,set<int>>> s0;
        for(auto it=s.begin(); it!=s.end(); it++) {
            for(int v : it->second.first)  {
                for(auto chl : adj[v]) {
                    string spath=it->first+chl.second;
                    s0[spath].first.insert(chl.first);
                }
            }
            for(int v : it->second.second)  {
                for(auto chl : adj[v]) {
                    string spath=it->first+chl.second;
                    s0[spath].second.insert(chl.first);
                }
            }
        }

        auto it=s0.begin(); 
        while(it!=s0.end()) {
            if(it->second.first.size()==0 || it->second.second.size()==0)
                it=s0.erase(it);
            else
                it++;
        }

        // ...TODO
    }
}

signed main() {
    solve();
}
