/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Dijkstra??
 * Consider each of the 900 a[i][j] to be the Kth value,
 * so sum is the sum of all a[k][l]>=a[i][j]
 * Maintain the count of bg used and eq used, and sum.
 * Try to find end.
 */
const int INF=1e18;
void solve() {
    cini(h);
    cini(w);
    cini(k);

    vvi a(h, vi(w));

    using pp=pair<int,priority_queue<int>>;
    using vpp=vector<pp>;
    using vvpp=vector<vpp>;

    vvpp dp(h, vpp(w));
    for(int i=0; i<h; i++) {
        for(int j=0; j<w; j++)  {
            cin>>a[i][j];
        }
    }

    using t3=tuple<int,int,int>;
    function<int(int)> go=[&](int kth) {
        vector<vector<t3>> dp(h, vi(w, { 0, 0, INF}));

        for(int i=0; i<h; i++) 
            for(int j=0; j<w; j++) {
                if(i>0) {
                    auto [bg,eq,sum]=dp[i-1][j];
                    int ssum=sum;
                    if(a[i][j]>kth) {
                        ssum+=a[i][j];
                        bg++;
                    } else if(a[i][j]==kth) {
                        eq++;
                        if(bg+eq<k)
                            ssum+=a[i][j];
                    } else {
                        dp[i][j]=dp[i-1][j];
                    }
                    auto [bg2,eq2,sum2]=dp[i][j];
                    if(ssum<sum2) {
                        dp[i][j]={bg,eq,ssum};
                    }
                }

                if(j>0) {
                    auto [bg,eq,sum]=dp[i][j-1];
                    int ssum=sum;
                    if(a[i][j]>kth) {
                        ssum+=a[i][j];
                        bg++;
                    } else if(a[i][j]==kth) {
                        eq++;
                        if(bg+eq<k)
                            ssum+=a[i][j];
                    }
                    auto [bg2,eq2,sum2]=dp[i][j];
                    if(ssum<sum2) {
                        dp[i][j]={bg,eq,ssum};
                    }
                }
            }

    };




    dp[0][0].first=a[0][0];
    dp[0][0].second.push(-a[0][0]);

    for(int i=0; i<h; i++) 
        for(int j=0; j<w; j++) {
            if(i>0) {
                priority_queue qq=dp[i-1][j].second;
                qq.push(-a[i][j]);
                int val=dp[i-1][j].first+a[i][j];
                if(qq.size()>k) {
                    val+=qq.top();
                    qq.pop();
                }
                if(val<dp[i][j].first || 
                        (val==dp[i][j].first && dp[i][j].second.top()<qq.top())) {
                    dp[i][j].first=val;
                    dp[i][j].second.swap(qq);
                }
            }
            if(j>0) {
                priority_queue qq=dp[i][j-1].second;
                qq.push(-a[i][j]);
                int val=dp[i][j-1].first+a[i][j];
                if(qq.size()>k) {
                    val+=qq.top();
                    qq.pop();
                }
                if(val<dp[i][j].first ||
                        (val==dp[i][j].first && dp[i][j].second.top()<qq.top())) {
                    dp[i][j].first=val;
                    dp[i][j].second.swap(qq);
                }
            }
        }

    cout<<dp[h-1][w-1].first<<endl;
}

signed main() {
    solve();
}
