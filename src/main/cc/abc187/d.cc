
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

void solve() {
    cini(n);
    vector<pii> x(n);

    for(int i=0; i<n; i++) {
        cini(a);
        cini(b);
        x[i]={a+b, a};
    }

    sort(all(x), [](pii p1, pii p2) {
            return p1.first+p1.second>p2.first+p2.second;
            });

    int sum=0;
    for(int i=0; i<n; i++) 
        sum+=x[i].second;

    vi pre(n+1);
    vi pos(n+1);
    pos[0]=sum;
    for(int i=0; i<n; i++) {
        pre[i+1]=pre[i]+x[i].first;
        pos[i+1]=pos[i]-x[i].second;
    }

    int sl=0;
    int sr=0;
    for(int i=0; i<=n; i++) {
        if(pre[i]>pos[i]) {
            cout<<i<<endl;
            return;
        }
    }
    assert(false);

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
