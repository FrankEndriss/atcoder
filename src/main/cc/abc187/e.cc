
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * Solution is more or less simple if done 
 * on a rooted tree. 
 * Then we add the x either to a complete subtree,
 * or to all other nodes. The "all other nodes" 
 * is simply implemented as negating the sign.
 * So we have to do two times dfs.
 * <TODO>
 */
void solve() {
    cini(n);
    vector<pii> e(n-1);
    vvi adj(n);
    vvi dp(n);
    for(int i=0; i<n-1; i++) {
        cin>>e[i].first>>e[i].second;
        e[i].first--;
        e[i].second--;
        adj[e[i].first].push_back(e[i].second);
        dp[e[i].first].push_back(0);
        adj[e[i].second].push_back(e[i].first);
        dp[e[i].second].push_back(0);
    }

    vi ans(n);
    cini(q);
    for(int i=0; i<q; i++) {
        cini(t);
        cini(ei); ei--;
        cini(x);
        //cerr<<"t="<<t<<" ei="<<ei<<" x="<<x<<endl;
        int a,b;
        if(t==1) {
            a=e[ei].first;
            b=e[ei].second;
        } else if(t==2) {
            a=e[ei].second;
            b=e[ei].first;
        } else
            assert(false);

        ans[a]+=x;

        for(size_t j=0; j<adj[a].size(); j++) {
            if(adj[a][j]!=b) {
                dp[a][j]+=x;
            }
        }
    }

    function<void(int,int,int)> dfs=[&](int v, int p, int inc) {
        ans[v]+=inc;
        for(size_t j=0; j<adj[v].size(); j++) {
            if(adj[v][j]!=p) {
                int val=inc+dp[v][j];
                dp[v][j]=0;

                dfs(adj[v][j], v, val);
            }
        }
    };

    for(int i=0; i<n; i++) 
        if(adj[i].size()==1) { /* leafs */
            dfs(i, -1, 0);
        }

    for(int i=0; i<n; i++) 
        cout<<ans[i]<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
