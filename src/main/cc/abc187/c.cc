
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

void solve() {
    cini(n);
    map<string, vb> m;
    for(int i=0; i<n; i++) {
        cins(s);
        int cnt=0;
        while(s[cnt]=='!')
            cnt++;
        if(cnt>0)
            s=s.substr(cnt);
        m[s].resize(3);
        m[s][cnt]=true;
    }

    int ok=false;
    for(auto ent : m) {
        if(ent.second[0] && ent.second[1]) {
            cout<<ent.first<<endl;
            ok=true;
            break;
        }
        if(ent.second[1] && ent.second[2]) {
            cout<<"!"<<ent.first<<endl;
            ok=true;
            break;
        }
    }
    if(!ok)
        cout<<"satisfiable"<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
