
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<long double>;
using vvd= vector<vd>;
using vvvd= vector<vvd>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* its a dp.
 * dp[a][b][c]= prob that that much coins are in the bag after some move.
 */
const int N=100;
void solve() {
    cini(a);
    cini(b);
    cini(c);

    const int sum=a+b+c;

    vvvd dp(N+1, vvd(N+1, vd(N+1)));
    dp[a][b][c]=1.0;

    for(int aa=a; aa<=99; aa++) {
        for(int bb=b; bb<=99; bb++)  {
            for(int cc=c; cc<=99; cc++) {
                dp[aa+1][bb][cc]+=(dp[aa][bb][cc]*aa)/(aa+bb+cc);
                dp[aa][bb+1][cc]+=(dp[aa][bb][cc]*bb)/(aa+bb+cc);
                dp[aa][bb][cc+1]+=(dp[aa][bb][cc]*cc)/(aa+bb+cc);
            }
        }
    }

    /* collect the endstates */
    vd ans(300);
    for(int i=0; i<N; i++)  {
        for(int j=0; j<N; j++) {
            if(i>=a && j>=b)
                ans[i+j+100]+=dp[i][j][100];
            if(i>=a && j>=c)
                ans[i+j+100]+=dp[i][100][j];
            if(i>=b && j>=c)
                ans[i+j+100]+=dp[100][i][j];
        }
    }

    long double aa=0;
    for(size_t i=sum; i<ans.size(); i++)
        aa+=ans[i]*(i-sum);


    cout<<aa<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
