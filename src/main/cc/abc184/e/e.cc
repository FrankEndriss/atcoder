
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int INF=1e9;
void solve() {
    cini(h);
    cini(w);
    cinas(s,h);

    vector<vector<pii>> ports(26);

    pii S={-1,-1};
    pii G={-1,-1};


    for(int i=0; i<h; i++) 
        for(int j=0; j<w; j++)  {
            if(s[i][j]>='a' && s[i][j]<='z')
                ports[s[i][j]-'a'].push_back({i,j});
            else if(s[i][j]=='S')
                S={i,j};
            else if(s[i][j]=='G')
                G={i,j};
        }

    assert(S.first>=0);
    assert(G.first>=0);

    const vi dx={ -1, 1, 0, 0 };
    const vi dy={  0, 0, 1, -1};

    vvi dp(h, vi(w, INF));   /* dp[i][j]=min steps to go to i,j */
    dp[S.first][S.second]=0;
    queue<pii> q;
    q.push(S);
    while(q.size()) {
        auto [x,y]=q.front();
        q.pop();

        for(int i=0; i<4; i++) {
            int xx=x+dx[i];
            int yy=y+dy[i];

            if(xx>=0 && xx<h && yy>=0 && yy<w && dp[xx][yy]>dp[x][y]+1 && s[xx][yy]!='#') {
                dp[xx][yy]=dp[x][y]+1;
                q.push({xx,yy});
            }

            if(s[x][y]>='a' && s[x][y]<='z') {
                for(auto [px,py] : ports[s[x][y]-'a']) {
                    if(dp[px][py]>dp[x][y]+1) {
                        dp[px][py]=dp[x][y]+1;
                        q.push({px,py});
                    }
                }
                ports[s[x][y]-'a'].clear();
            }
        }
    }

    if(dp[G.first][G.second]==INF) 
        cout<<-1<<endl;
    else
        cout<<dp[G.first][G.second]<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
