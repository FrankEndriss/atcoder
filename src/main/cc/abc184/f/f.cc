
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* knapsack with a TLE
 * Somehow meet in the middle?
 * We got 2^20 with first 20 items,
 * and can combine them in log(2^20)
 * */
void solve() {
    cini(n);
    cini(t);
    cinai(a,n);

    set<int> sums1;
    sums1.insert(0);
    for(int i=0; i<min(n, 20LL); i++) {
        set<int> sums0=sums1;
        for(int j : sums1)
            if(j+a[i]<=t)
                sums0.insert(j+a[i]);
        sums1.swap(sums0);
    }

    set<int> sums2;
    sums2.insert(0);
    for(int i=20; i<n; i++) {
        set<int> sums0=sums2;
        for(int j : sums2)
            if(j+a[i]<=t)
                sums0.insert(j+a[i]);
        sums2.swap(sums0);
    }

    int ans=*sums1.rbegin();
    for(int j : sums1) {
        auto it=sums2.upper_bound(t-j);
        if(it!=sums2.begin())
            it--;

        if(it!=sums2.end())
            ans=max(ans, j+*it);
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
