
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We can go everywhere with three moves,
 * and need to check if we can go there
 * with one or two moves.
 *
 * two moves is possible 
 * if the field color is same or 
 * if one of the dists<=5
 * if one is 0 and other <=6
 * or if one of the diagonals is not "far" 
 *   away from (c,d).
 */
void solve() {
    cini(a);
    cini(b);
    cini(c);
    cini(d);

    if(a==c && b==d)
        cout<<0<<endl;
    else if(a+b==c+d || a-b==c-d || abs(a-c)+abs(b-d)<=3)
        cout<<1<<endl;
    else { 
        if((a+b)%2 == (c+d)%2 || abs(a-c)+abs(b-d)<=6) {
            cout<<2<<endl;
            return;
        }

        for(int i=-3; i<=3; i++) 
            for(int j=-3; j<=3; j++) {
                if(abs(i)+abs(j)>3)
                    continue;

                int pi=c+i;
                int pj=d+j;

                if(a-b==pi-pj || a+b==pi+pj) {
                    cout<<2<<endl;
                    return;
                }
            }

        cout<<3<<endl;
    }
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
