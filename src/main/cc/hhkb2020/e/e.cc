
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


const int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

/* each position is is lighted
 * if at least on of the lightning positions is turned on.
 * So these are 2^(lightning positions)-1 contributions.
 * Simply count all.
 * So, how to find number of lightning positions?
 * Find foreach position the next untidy in all four dirs.
 */
void solve() {
    cini(h);
    cini(w);
    cinas(a,h);

    int k=0;    /* number of tidy positions */
    vvi up(h, vi(w));   /* up[i]=number of light fields in up direction */
    vvi down(h, vi(w));   /* down[i]=number of light fields in down direction */
    for(int j=0; j<w; j++) {
        if(a[0][j]=='.')
            k++;

        if(a[0][j]=='.')
            up[0][j]=1;

        if(a[h-1][j]=='.') 
            down[h-1][j]=1;

        for(int i=1; i<h; i++) {
            if(a[i][j]=='.')
                k++;

            up[i][j]=up[i-1][j];
            if(a[i][j]=='.')
                up[i][j]++;
            else
                up[i][j]=0;

            down[h-i-1][j]=down[h-i][j];
            if(a[h-i-1][j]=='.')
                down[h-i-1][j]++;
            else
                down[h-i-1][j]=0;
        }
    }

    vvi le(h, vi(w));   /* le[i]=number of light fields in le direction */
    vvi ri(h, vi(w));   /* ri[i]=number of light fields in ri direction */
    for(int i=0; i<h; i++) {
        if(a[i][0]=='.')
            le[i][0]=1;

        if(a[i][w-1]=='.') 
            ri[i][w-1]=1;

        for(int j=1; j<w; j++) {
            le[i][j]=le[i][j-1];
            if(a[i][j]=='.')
                le[i][j]++;
            else
                le[i][j]=0;

            ri[i][w-j-1]=ri[i][w-j];
            if(a[i][w-j-1]=='.')
                ri[i][w-j-1]++;
            else
                ri[i][w-j-1]=0;
        }
    }

    int ans=0;
    int twoPk=toPower(2,k);

    for(int i=0; i<h; i++) 
        for(int j=0; j<w; j++) {
            if(a[i][j]=='.') {
                int cnt=up[i][j] + down[i][j] + le[i][j] + ri[i][j] - 3;
                assert(cnt>0);
                //cerr<<"i="<<i<<" j="<<j<<" cnt="<<cnt<<endl;
                int con=pl(twoPk, -toPower(2, k-cnt));
                ans=pl(ans, con);
            }
        }

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
