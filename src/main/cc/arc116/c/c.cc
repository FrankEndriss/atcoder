
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"



const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=100000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

/* Factorization of n using precomputed primes.
 * https://cp-algorithms.com/algebra/factorization.html
 */
void trial_division4(ll n, vector<ll> &factorization) {
    for (ll d : pr) {
        if (d * d > n)
            break;
        while (n % d == 0) {
            factorization.push_back(d);
            n /= d;
        }
    }
    if (n > 1)
        factorization.push_back(n);
}

const int MOD=998244353;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* int pow */
int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a, mod);
    }
    return res;
}

/*
 * Consider a[0]=1;
 * a[1]=a[0] || a[0]*2 || a[0]*3 || ...
 * So, there are M/a[0] possible values for a[1]
 * Same for a[1] and a[2]...
 *
 * Consider a[n-1]; foreach a[n-1] in 1..M there is 
 * 1 possibility, dp[1..M]=1;
 * Consider a[n-2]; foreach a[n-2] in 1..M there is 
 * dp[a[n-2]]+dp[a[n-2]*2]+dp[a[n-3]]+... posibilities
 *
 * So we can solve it in O(N*logN*M) :/
 *
 * Can we somehow construct a segtree to get the sum
 * of all elements beeing a multiple of q[i]?
 * Would that help?
 *
 * ...
 * Consider the sorted list of seqs.
 * the first is "1 1 1...1 1 1"
 * then "1 1 1...1 1 2"
 * "1 1 1...1 1 3"
 * ...
 *
 * "1 1 1...1 1 M"  | +M
 * "1 1 1...1 2 M"  | +M/2
 * "1 1 1...1 3 M"  | +M/3
 * ...
 * "1 1 1...2 2 M"  | +M/2
 * "1 1 1...2 3 M"  | +M/3
 * ...
 * "1 1 1...3 3 M"  | +M/3
 * "1 1 1...3 4 M"  | +M/4
 * ...
 *
 * So we still need to multiply N*M times.
 *
 * What about incrementing M?
 * for M=1 there is 1 solution
 * for M=2 there are N+1 solutions (0..N 2s)
 * for M=3 there are (N+1)*2 solutions   (each primefactor 0..N times)
 * for M=4 there are (N+1)*N solutions
 * ...
 * for M=6 ...???
 *
 * Foreach number there is a certain number of multiples available.
 */
void solve() {
    cini(n);
    cini(m);

    // TODO
}

signed main() {
    solve();
}
