
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Consider knowing the prime divisors...
 * The only one even prime divisor is 2, all others are odd.
 * So, if 2 is not a divisor there are only odd ones.
 * else there is foreach odd one propably several even ones...
 *
 * If there is more than one odd primefactor, the number of odd
 * divisors is more.
 *
 * How to find quick if there are more than some given number 
 * odd prime factors?
 */
void solve() {
    cini(n);

    int two=0;
    while(n%2==0) {
        two++;
        n/=2;
    }

    if(two==1)
        cout<<"Same"<<endl;
    else if(two==0)
        cout<<"Odd"<<endl;
    else
        cout<<"Even"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
