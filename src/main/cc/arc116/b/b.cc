
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


const int MOD=998244353;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/*
 * Consider how often each a[i] contributes, let a[] be sorted asc.
 * a[i] is smallest in 2^(n-i-1) subseqs. (i zero based)
 *   In 2^(n-i-2) of them a[n-1] is biggest
 *   In 2^(n-i-3) of them a[n-2] is biggest
 *   In 2^(n-i-4) of them a[n-3] is biggest
 *   ...
 *   In 2^(n-i-(n-i) of them a[n-(n-i)==i] is biggest
 */
void solve() {
    cini(n);
    cinai(a,n);
    sort(all(a), greater<int>());

    int ans=0;
    int sum=0;
    int prev=0;
    for(int i=0; i<n; i++) {
        sum=mul(sum,2);
        sum=pl(sum,prev);
        ans=pl(ans, mul(a[i], pl(a[i],sum)));
        prev=a[i];
        //cerr<<"i="<<i<<" a[i]="<<a[i]<<" sum="<<sum<<" ans="<<ans<<endl;
    }

    cout<<ans<<endl;

}

signed main() {
    solve();
}
