
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#include <atcoder/maxflow>
using namespace atcoder;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* some bug... :/
 */
void solve() {
    cini(n);
    cini(m);
    cinas(s,n);

#define idx(i,j) ((i)*m+(j))
#define white(i,j) ((i+j)%2)

    int S=n*m;
    int T=S+1;
    mf_graph<int> g(T+1);

    for(int i=0; i<n; i++) {
        for(int j=0; j<m; j++) {
            if(s[i][j]=='.') {
                if(i+1<n && s[i+1][j]=='.') {
                    if(white(i,j))
                        g.add_edge(idx(i,j), idx(i+1,j), 1);
                    else
                        g.add_edge(idx(i+1,j), idx(i,j), 1);
                }
                if(j+1<m && s[i][j+1]=='.') {
                    if(white(i,j))
                        g.add_edge(idx(i,j), idx(i,j+1), 1);
                    else
                        g.add_edge(idx(i,j+1), idx(i,j), 1);
                }
            }
        }
    }
    for(int i=0; i<n; i++) {
        for(int j=0; j<m; j++) {
            if(s[i][j]!='.')
                continue;

            if(white(i,j))
                g.add_edge(S, idx(i,j), 1);
            else
                g.add_edge(idx(i,j), T, 1);
        }
    }

    int ans=g.flow(S,T);

    for(auto e : g.edges()) {
        if(e.from<S && e.to<S && e.flow>0) {
            int i1=e.from/m;
            int j1=e.from%m;
            int i2=e.to/m;
            int j2=e.to%m;
            //cerr<<"from ="<<i1<<" "<<j1<<" to="<<i2<<" "<<j2<<endl;
            if(i1==i2) {
                if(j1<j2) {
                    s[i1][j1]='>';
                    s[i2][j2]='<';
                } else {
                    s[i1][j1]='<';
                    s[i2][j2]='>';
                }
            } else {
                if(i1<i2) {
                    s[i1][j1]='v';
                    s[i2][j2]='^';
                } else {
                    s[i1][j1]='^';
                    s[i2][j2]='v';
                }
            }
        }
    }
    cout<<ans<<endl;
    for(int i=0; i<n; i++)
        cout<<s[i]<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
