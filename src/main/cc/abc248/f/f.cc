/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We must not remove two top of each other edges,
 * and must not remove two next to top of each other and the vertical between them.
 *
 * There are so much cases... :/
 * Consider the first removed edge:
 * -can be one of he verticals
 * -one of the horizontals
 *
 * Consider the second removed edge.
 * -if first was horizontal, then second must not be same col horizontal
 * -if first was vertical, second can be anything
 * result is ...
 * ???
 */
void solve() {
    cini(n);
    cini(p);
    vi ans(n-1);
    ans[1]=n*3-2;   /* each edge can be removed */
}

signed main() {
    solve();
}
