/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* see https://cp-algorithms.com/data_structures/sqrt_decomposition.html#toc-tgt-4
 * Queries are inclusive l and inclusive r.
 **/

//const int BLOCK_SIZE=(int)sqrt(5e5+5)+1;
const int BLOCK_SIZE=512;

bool mo_cmp(pair<int, int> p, pair<int, int> q) {
    if (p.first / BLOCK_SIZE != q.first / BLOCK_SIZE)
        return p < q;
    return (p.first / BLOCK_SIZE & 1) ? (p.second < q.second) : (p.second > q.second);
}

/* example impl of the three functions
 ** for finding number of numbers in
 ** interval where freq==number */
const int N=2e5+7;
vi f(N);

vi a(N);

inline void moremove(int idx) {
    f[a[idx]]--;
}

inline void moadd(int idx) {
    f[a[idx]]++;
}

inline int moget_answer(int x) {
    return f[x];
}

/* (l,r) both inclusive, 0 based */
struct Query {
    int l, r, idx;
    int x;

    bool operator<(Query other) const {
        return mo_cmp(make_pair(l, r), make_pair(other.l, other.r));
    }
};

vector<int> mo(vector<Query>& queries) {
    vector<int> answers(queries.size());
    sort(queries.begin(), queries.end());
    int cur_l = 0;
    int cur_r = -1;

    /* invariant: data structure will always reflect the range [cur_l, cur_r] */
    for (Query q : queries) {
        while (cur_l > q.l) {
            cur_l--;
            moadd(cur_l);
        }
        while (cur_r < q.r) {
            cur_r++;
            moadd(cur_r);
        }
        while (cur_l < q.l) {
            moremove(cur_l);
            cur_l++;
        }
        while (cur_r > q.r) {
            moremove(cur_r);
            cur_r--;
        }
        answers[q.idx] = moget_answer(q.x);
    }
    return answers;
}
/**
 */
void solve() {
    cini(n);
    for(int i=0; i<n; i++)
        cin>>a[i];

    cini(q);
    vector<Query> v(q);
    for(int i=0; i<q; i++) {
        cin>>v[i].l>>v[i].r>>v[i].x;
        v[i].l--;
        v[i].r--;
        v[i].idx=i;
    }

    vi ans=mo(v);
    for(int i=0; i<q; i++)
        cout<<ans[i]<<endl;

}

signed main() {
    solve();
}
