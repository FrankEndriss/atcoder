/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Check all pairs, and foreach pair the number of
 * points on that line.
 * But we need to consider all the pairs done.
 *
 * Fuck this shit geometry :/
 */
void solve() {
    cini(n);
    cini(k);

    if(k==1) {
        cout<<"Infinity"<<endl;
        return;
    }

    vi x(n);
    vi y(n);
    for(int i=0; i<n; i++)
        cin>>x[i]>>y[i];

    function<pii(int,int)> slope=[&](int i, int j) {
        pii sl= { x[i]-x[j], y[i]-y[j] };
        if(sl.first==0)
            sl.second=1;
        else if (sl.second==0)
            sl.first=1;

        if(sl.first<0) {
            sl.first=-sl.first;
            sl.second=-sl.second;
        }

        int g=gcd(abs(sl.first), abs(sl.second));
        sl.first/=g;
        sl.second/=g;
        return sl;
    };

    vvb vis(n, vb(n));
    int ans=0;
    for(int i=0; i<n; i++) {
        for(int j=i+1; j<n; j++) {
            if(vis[i][j])
                continue;
            vis[i][j]=vis[j][i]=true;

            pii sl1=slope(i,j);
            vi oth={i,j};
            for(int kk=j+1; kk<n; kk++) {
                if(kk!=i && kk!=j) {
                    pii sl2=slope(i,kk);
                    if(sl1==sl2)
                        oth.push_back(kk);
                }
            }
            for(int i1 : oth)
                for(int i2 : oth) 
                    vis[i1][i2]=vis[i2][i1]=true;

            if(oth.size()>=k)
                ans++;
        }
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}
