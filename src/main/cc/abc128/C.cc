/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

const int MAXBITS=11;

int bitcount(int val) {
    int ans=0;
    int bit=1;
    fori(MAXBITS)  {
        if(val&bit)
            ans++;
        bit<<=1;
    }
    return ans;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int n, m;
    cin>>n>>m;
    vector<vector<int>> k(m);
    fori(m) {
        int s;
        cin>>s;
        for(int j=0; j<s; j++) {
            int x;
            cin>>x;
            k[i].push_back(x);
        }
    }
    vector<int> p(m);
    fori(m) {
        cin>>p[i];
        p[i]%=2;
    }

    /* For each bulb we can create set of bits (M bits) where the bulb is "on".
     * Intersection of all sets is ans;
     */

    const int N=1<<(n);   // max 1024
    vector<bool> gmask(N, true);
    fori(m) {
        /* create mask for bulb i */
        int mask=0;
        for(auto b : k[i])
            mask|= 1<<(b-1);

        for(int j=0; j<N; j++)
            gmask[j]= gmask[j] && bitcount(mask&j)%2==p[i];
    }

    int ans=0;
    fori(N)
    if(gmask[i])
        ans++;
    cout<<ans<<endl;
}

