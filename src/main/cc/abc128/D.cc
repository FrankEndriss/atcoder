/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
int n, k;
    cin>>n>>k;
    vector<int> v(n);
    fori(n)
        cin>>v[i];

/* On any move, we can decide 
 * 1. to stop 
 * 2. take one more
 * 3. pushback all lvg
 *
 * .. does not work, since we need to lookahead valueable gems, hidden behind
 * some negative gems...
 */


    int lidx=0;
    int ridx=n-1;
    ll ans=0;
    multiset<int> hand;
    for(int i=0; i<k; i++) {
        // case pushback
        ll ans3=ans;
        int j=i;
        for(int h : hand) {
            if(j==k)
                break;
            j++;
            if(h<0)
                ans3-=h;
            else
                break;
        }
        

        //case take one more
        ll ans2=ans;
        if(lidx<=ridx) {
            int nextgem=max(v[lidx], v[ridx]);
            if(nextgem==v[lidx])
                lidx++;
            ans2+=nextgem;
        }


        ans=max(ans, max(ans2, ans3));
    }
    cout<<ans<<endl;

}

