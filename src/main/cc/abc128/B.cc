
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n;
    cin>>n;
    vector<string> s(n);
    vector<int> p(n), id(n);
    
    fori(n) {
        cin>>s[i]>>p[i];
        id[i]=i;
    }

    sort(id.begin(), id.end(), [&](int i1, int i2) {
        if(s[i1]!=s[i2])
            return s[i1]<s[i2];
        return p[i2]<p[i1];
    });

    fori(n)
        cout<<id[i]+1<<endl;
    

}

