/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/dsu>
using namespace atcoder;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Reverse the order, add vertices in reverse order
 * and count the components after each...somehow.
 */
void solve() {
    cini(n);
    cini(m);

    dsu d(n);

    vvi adj(n);
    for(int i=0; i<m; i++) {
        cini(u);
        u--;
        cini(v);
        v--;
        if(v>u)
            adj[u].push_back(v);
        if(u>v)
            adj[v].push_back(u);
    }

    vi ans(n);
    int cnt1=0;
    int cnt2=0;
    for(int i=n-1; i>=0; i--) {
        ans[i]=cnt1-cnt2;
        cnt1++;
        for(int j : adj[i]) {
            if(!d.same(i,j))
                cnt2++;

            d.merge(i,j);
        }
    }

    for(int i=0; i<n; i++)
        cout<<ans[i]<<endl;

}

signed main() {
    solve();
}
