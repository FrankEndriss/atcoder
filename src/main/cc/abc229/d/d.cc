/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * dp
 * Foreach position maintain 
 * dp1[i]=position of next '.' left of s[i]
 *
 * Also, foreach '.' maintain position of kth '.' left of it
 *
 * Then, find max dp2[i]+dp1[i-k]
 *
 * *****
 * Why so difficult to implement?
 */
void solve() {
    cins(s);
    cini(k);

    const int n=(int)s.size();

    vi dp1(n);
    int pos=-1;

    for(int i=0; i<n; i++) {
        dp1[i]=pos;
        if(s[i]=='.')
            pos=i;
    }

    vi ppos(k);
    vi dp2(n, -1);
    int ans=0;
    for(int i=0; i<n; i++) {
        if(k==0) {
            if(s[i]!='.') {
                ans=max(ans, i-dp1[i]);
            }
            continue;
        }

        if(s[i]=='.')
            ppos.push_back(i);

        dp2[i]=ppos[ppos.size()-k];
        int lans=i-dp2[i]+1;
        lans+=dp2[i]-dp1[dp2[i]]-1;
        ans=max(ans, lans);
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}
