/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider N is even, or is odd.
 * And weights...
 *
 * We need to remove one edge from each odd sized circle.
 * We got circles of size==3 at
 * 0,i,i+1
 * and, if n is odd,
 * 1,2,3,...,1
 *
 * First do a dp on the size==3 circles, foreach circle we can remove
 * one of the three edges, and the third edge is the first of the next circle.
 * Also maintain if there was a 2 edge removed.
 *
 * Then, if N is odd, use only the solution with an removed edge 2.
 *
 * ***
 * On the last triangle, we need to consider that it maybe was
 * broken up by the edge removal of the first triangle.
 * So we need another state, that means "edge0 used in first triangle".
 * Lets run the dp twice, foreach one of that cases.
 *
 * ****
 * ...but for some reason it does not work :/
 */
const int INF=1e18;
void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);

    int ans=INF;
    for(int k=0; k<2; k++) {
        /**  dp[i][j][k]= i=edge2 removed?; j=edge012; k=up to circle.
         */
        vvvi dp(2, vvi(3, vi(n,INF)));

        if(k==0) {
            dp[0][0][0]=a[0];
        } else {
            dp[0][1][0]=a[1];
            dp[1][2][0]=b[0];
        }


        for(int i=1; i<n; i++) {
            /* removing edg 0 of that circle */
            dp[0][0][i]=min(dp[0][0][i-1]+a[i], dp[0][1][i-1]);
            dp[1][0][i]=min(dp[1][0][i-1]+a[i],
                            min(dp[1][1][i-1], dp[1][2][i-1]+a[i]));

            /* removing edg 1 of that cirlce, consider both k-cases */
            if(k==0 && i+1==n) {
                dp[0][1][i]=min(dp[0][1][i-1], dp[0][0][i-1]);
                dp[1][1][i]=min(min(dp[1][1][i-1], dp[1][0][i-1]), dp[1][2][i-1]);
            } else {
                dp[0][1][i]=min(dp[0][0][i-1]+a[(i+1)%n], dp[0][1][i-1]+a[(i+1)%n]);
                dp[1][1][i]=min(dp[1][0][i-1]+a[(i+1)%n],
                                min(dp[1][1][i-1]+a[(i+1)%n], dp[1][2][i-1]+a[(i+1)%n]));
            }

            /* removing edg 2 of that cirlce */
            dp[1][2][i]=min(dp[0][0][i-1]+b[i],
                            min(dp[0][1][i-1]+b[i],
                                min(dp[1][0][i-1]+b[i],
                                    min(dp[1][1][i-1]+b[i],
                                        dp[1][2][i-1]+b[i]))));
        }

        if(n%2==0) {
            for(int i=0; i<2; i++)
                for(int j=0; j<3; j++)
                    ans=min(ans, dp[i][j][n-1]);
        } else {
            int mi=*min_element(all(b));
            for(int j=0; j<3; j++)  {
                ans=min(ans, dp[1][j][n-1]);
                ans=min(ans, dp[0][j][n-1]+mi);
            }
        }
    }

    cout<<ans<<endl;

}

signed main() {
    solve();
}
