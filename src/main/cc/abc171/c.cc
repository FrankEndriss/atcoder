
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/*  There are 26 of len 1
 *  26 * cnt[1]+cnt[1] on len=2
 *  26 * cnt[2]+cnt[2] of len 3
 */
const int N=1e16+1;
void solve() {
    cini(n);
    vi cnt(14);
    vi c26(14);
    cnt[0]=0;
    cnt[1]=26;
    c26[0]=1;
    c26[1]=26;
    int x26=26;
    for(int i=2; i<14; i++) {
        x26*=26;
        cnt[i]=cnt[i-1]+x26;
        c26[i]=x26;
    }
    //cerr<<"cnt.size()="<<cnt.size()<<" cnt.back()="<<cnt.back()<<endl;

    if(n<=26) {
        string ans=" ";
        ans[0]=(char)('a'+n-1);
        cout<<ans<<endl;
        return;
    }

    string ans;
    for(int i=cnt.size()-1; i>=0; i--) {
        if(ans=="") {
            if(cnt[i]+1<=n) {
                ans=string(i+1, 'a');
                n-=(cnt[i]+1);
            }
        } 

        if(ans!="") {
            while(c26[i]<=n) {
                ans[i]++;
                n-=c26[i];
//cerr<<"cnt[i]="<<cnt[i]<<" ans="<<ans<<" n="<<n<<endl;
            }
        }
    }

    reverse(all(ans));
    cout<<ans<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
