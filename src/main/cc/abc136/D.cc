/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

/*
#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")
*/

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cins(s);
    vector<int> Lidx;   // from left, next L after some R
    vector<int> Ridx;  // from left, next R after some L
    int ri=s.size()-1;
    for(int i=1; i<s.size(); i++, ri--) {
        if(s[i]!=s[i-1]) {
            if(s[i]=='L')
                Lidx.push_back(i);
        }
        if(s[ri]!=s[ri-1]) {
            if(s[ri-1]=='R')
                Ridx.push_back(ri-1);
        }
    }

    vi ans(s.size());

    for(int i=0; i<s.size(); i++) {
        if(s[i]=='L') { // find next R
            auto it=upper_bound(Ridx.begin(), Ridx.end(), i, greater<int>());
            int j=*it;
            int cnteven=(i-j)&1;
            assert(s[j]=='R');
            if(!cnteven) {
                ans[j]++;
            } else {
                ans[j+1]++;
            }
        } else {
            auto it=upper_bound(Lidx.begin(), Lidx.end(), i);
            int j=*it;
            int cnteven=(j-i)&1;
            assert(s[j]=='L');
            if(!cnteven) {
                ans[j]++;
            } else {
                ans[j-1]++;
            }
        }
    }
    for(int i=0; i<ans.size(); i++)
        cout<<ans[i]<<" ";
    cout<<endl;

}

