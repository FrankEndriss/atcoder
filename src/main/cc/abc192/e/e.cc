
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using t3=tuple<int,int,int>;

/* dijkstra */
const int INF=1e18;
void solve() {
    cini(n);
    cini(m);
    cini(x); x--;
    cini(y); y--;

    vector<vector<t3>> adj(n);
    for(int i=0; i<m; i++) {
        cini(a); a--;
        cini(b); b--;
        cini(t);
        cini(k);
        adj[a].emplace_back(b,t,k);
        adj[b].emplace_back(a,t,k);
    }

    vi dp(n, INF);
    dp[x]=0;
    priority_queue<pii> q;  /* <-time, vertex> */
    q.emplace(0, x);
    while(q.size()) {
        auto [tt,v]=q.top();
        q.pop();
        if(dp[v]!=-tt)
            continue;

        for(auto [chl,t,k] : adj[v]) {
            int wait=dp[v]%k;
            if(wait==0)
                wait=k;
            wait=k-wait;
            int next=dp[v]+wait+t;
            if(dp[chl]>next) {
                dp[chl]=next;
                q.emplace(-dp[chl], chl);
            }
        }
    }

    if(dp[y]==INF)
        cout<<-1<<endl;
    else
        cout<<dp[y]<<endl;

}

signed main() {
    solve();
}
