
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using vll=vector<int>;

/* Maintains arbitrary large integer as
 * single "digits" of base 1e9 in a
 * long long array.
 * The base 1e9 allows us to quickly convert to decimal.
 * Smallest significant digit at position 0.
 */
constexpr ll BASE=1e9;
struct bigint {
    vector<ll> data;
    bigint(ll i=0) {
        while(i) {
            data.push_back(i%BASE);
            i/=BASE;
        }
    }

    bigint& operator+=(const bigint& rhs) {
        for(int i=0; i<rhs.data.size(); i++) {
            if(data.size()<i+1)
                data.push_back(0);
            data[i]+=rhs.data[i];
            int j=i;
            while(data[j]>BASE) {
                if(data.size()==j)
                    data.push_back(0);
                data[j+1]+=data[j]/BASE;
                data[j]%=BASE;
            }
        }
        _norm(data);
        return *this;
    }

    friend bigint operator+(bigint lhs, const bigint &rhs) {
        lhs+=rhs;
        return lhs;
    }

    bigint& operator*=(const bigint& rhs) {
        data=_mul(data, rhs.data);
        _norm(data);
        return *this;
    }

    friend bigint operator*(bigint lhs, const bigint &rhs) {
        lhs*=rhs;
        return lhs;
    }

    vll _mul(const vll &a, const vll &b) {
        vll ans(a.size()+b.size()+1);
        for(size_t i=0; i<b.size(); i++) {
            for(size_t j=0; j<a.size(); j++) {
                ans[i+j]+=b[i]*a[j];
                int k=i+j;
                while(ans[k]>BASE) {
                    ans[k+1]+=ans[k]/BASE;
                    ans[k]%=BASE;
                    k++;
                }
            }
        }
        _norm(ans);
        return ans;
    }

    void _norm(vll &a) {
        while(a.size() && a.back()==0)
            a.pop_back();
    }

    bool operator<(const bigint &rhs) {
        if(data.size()!=rhs.data.size())
            return data.size()<rhs.data.size();

        for(int i=data.size()-1; i>=0; i--)
            if(data[i]!=rhs.data[i])
                return data[i]<rhs.data[i];

        return false;
    }
};

/*
 * Choose base n:
 * - s base n <= m
 * - n>max(d);
 *
 * How this TLE?
 * -> if s=="1" ans can be up to m
 * So it is a binary search.
 * -> No, if s==1 then value of s==1
 *
 *
 */
void solve() {
    cins(s);
    cini(mm);
    bigint m(mm);

    reverse(all(s));
    int d=*max_element(all(s))-'0';

    int l=d;
    int r=1e18+7;
    while(l+1<r) {
        int mid=(l+r)/2;
        //cerr<<"mid="<<mid<<endl;

        bigint bmid(mid);

        bigint base(1);
        bigint sum(0);

        for(char c : s) {

            sum+=bigint((int)(c-'0'))*base;
            if(m<sum || m<base) {
                sum=m+1;
                break;
            }
            base*=mid;
        }
        //cerr<<"sum="<<(int)sum<<" m="<<(int)m<<endl;

        if(!(m<sum))
            l=mid;
        else
            r=mid;
    }
    int ans=l-d;
    cout<<ans<<endl;
}

signed main() {
    solve();
}
