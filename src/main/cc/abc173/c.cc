
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

void solve() {
    cini(h);
    cini(w);
    cini(k);

    vector<vector<char>> c(h, vector<char>(w));
    for(int i=0; i<h; i++) 
        for(int j=0; j<w; j++) {
            cin>>c[i][j];
        }

    int ans=0;
    for(int i=0; i<(1<<h); i++)
        for(int j=0; j<(1<<w); j++) {
            int lans=0;
            for(int ii=0; ii<h; ii++)  {
                for(int jj=0; jj<w; jj++) {
                    if((i&(1<<ii)) && (j&(1<<jj)) && c[ii][jj]=='#')
                        lans++;
                }
            }
            if(lans==k)
                ans++;
        }

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
