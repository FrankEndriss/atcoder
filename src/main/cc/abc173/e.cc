/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    while(res < 0)
        res += mod;

    while(res>=mod)
        res-=mod;

    return res;
}

/* We need to choose abs biggest elements
 * with an even number of negative ones.
 *
 * Cases:
 * neg even, ok
 * neg odd, a[k].first==a[k-1].first
 *
 * 2. all neg, k=odd
 *
 * Greedy:
 * choose positive numbers
 * whenever mult of last two pos numbers<mult of next two neg numbers
 * substitute them.
 *
 * edgecase: result negative
 * -> we need to choose the smallest numbers possible
 *
 * edgecase: 0
 */

void solve() {
    cini(n);
    cini(k);

    int cntzero=0;
    vi pos;
    vi neg;

    for(int i=0; i<n; i++) {
        cini(aux);
        if(aux==0)
            cntzero++;
        else if(aux>0)
            pos.push_back(aux);
        else
            neg.push_back(aux);

    }
    sort(all(pos), greater<int>());
    sort(all(neg));

    /* zero result */
    if(pos.size()+neg.size()<k) {
        cout<<0<<endl;
        return;
    }

    /* check for negative result */
    if((pos.size()+neg.size()==k && neg.size()%2==1) || (pos.size()==0 && k%2==1)) {
        if(cntzero) {
            cout<<0<<endl;
            return;
        }
        for(int i : neg)
            pos.push_back(abs(i));
        sort(all(pos));

        int ans=1;
        for(int i=0; i<k; i++) {
            ans=(ans*pos[i])%MOD;
        }
        cout<<pl(-ans, 0)<<endl;
        return;
    }

    int pidx=0;
    int nidx=0;
    vi st;
    if(k%2==1)
        st.push_back(pos[pidx++]);

    while(st.size()<k) {
//cerr<<"st.size()="<<st.size()<<" pos.size()="<<pos.size()<<" neg.size()="<<neg.size()<<endl;
        /* put the next two elements onto the stack */
        if(pidx+2>pos.size()) {
//cerr<<"case pidx+2<pos.size(), pidx="<<pidx<<endl;
            st.push_back(neg[nidx++]);
            st.push_back(neg[nidx++]);
        } else if(nidx+2>neg.size()) {
            st.push_back(pos[pidx++]);
            st.push_back(pos[pidx++]);
        } else {
            int vp=pos[pidx]*pos[pidx+1];
            int vn=neg[nidx]*neg[nidx+1];
            if(vp>vn) {
                st.push_back(pos[pidx++]);
                st.push_back(pos[pidx++]);
            } else {
                st.push_back(neg[nidx++]);
                st.push_back(neg[nidx++]);
            }
        }
    }

    int ans=1;
    for(int i : st) {
        ans=(ans*abs(i))%MOD;
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
