/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* 
 * First 0.
 * new play c=min(left,right)
 * 2: f(0)
 * 3: min(f(0), f(1))
 * 4: ...
 * Greedy, send them in biggest first
 * and always choose best possible position.
 * Maintain list of all adj-pairs in circle
 * to find best.
 */
void solve() {
    cini(n);
    cinai(a,n);

    sort(all(a), greater<int>());

    if(n==2) {
        cout<<a[0]<<endl;
        return;
    }

    multiset<pair<int,pii>> p;

    p.insert({-a[1], { a[0], a[1] }});
    p.insert({-a[2], { a[1], a[2] }});
    p.insert({-a[2], { a[2], a[0] }});
    
    int ans=a[0];
    ans+=a[1];

    for(int i=3; i<n; i++) {
        auto it=p.begin();
        ans-=it->first;
        int l=it->second.first;
        int r=it->second.second;
        p.erase(it);
        p.insert({-min(l,a[i]), {l,a[i]}});
        p.insert({-min(a[i],r), {a[i],r}});
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
