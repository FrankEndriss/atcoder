/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=120000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

/**
 * What this, is this funny?
 */
void solve() {
    cini(n);
    int ans=0;

    cerr<<"pr[0]="<<pr[0]<<endl;
    int prev=0;
    for(int i=0; ;i++) {
        int iii=pr[i]*pr[i]*pr[i];
        if(iii*2>n || iii<prev)
            break;

        /* find how many primes are there pr[j]<i && pr[j]*d<=n */
        auto it=lower_bound(all(pr), min(pr[i], n/iii+1));
        ans+=distance(pr.begin(), it);

        prev=iii;
    }

    cout<<ans<<endl;

}

signed main() {
    solve();
}
