/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
void solve() {
    cini(n);
    cini(q);
    
    vi a(n);
    iota(all(a), 0LL);
    vi pos(n);
    iota(all(pos), 0LL);

    for(int i=0; i<q; i++) {
        cini(x);
        x--;

        int p1=pos[x];
        int p2=(p1==n-1)?n-2:p1+1;
        int x2=a[p2];
        swap(a[p1],a[p2]);
        pos[a[p1]]=p1;
        pos[a[p2]]=p2;
    }
    for(int i=0; i<n; i++) 
        cout<<a[i]+1<<" ";
    cout<<endl;
}

signed main() {
    solve();
}
