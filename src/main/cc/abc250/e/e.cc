/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Hashing? Try simple xor
 */
const int MOD=1e9+7;
void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);
    cini(q);

    set<int> sa;
    set<int> sb;

    vi xa(n+1);
    vi cnta(n+1);
    vi suma(n+1);
    vvi elema(n+1);

    vi xb(n+1);
    vi cntb(n+1);
    vi sumb(n+1);
    vvi elemb(n+1);

    const int M=20;

    for(int i=0; i<n; i++) {
        if(sa.count(a[i])==0) {
            xa[i+1]=xa[i]^a[i];
            suma[i+1]=(suma[i]+a[i])%MOD;
            cnta[i+1]=cnta[i]+1;

            sa.insert(a[i]);
            int j=0;
            for(auto it=sa.begin(); j<M && it!=sa.end(); it++,j++)
                elema[i+1].push_back(*it);
        } else {
            xa[i+1]=xa[i];
            suma[i+1]=suma[i];
            cnta[i+1]=cnta[i];
            elema[i+1]=elema[i];
        }

        if(sb.count(b[i])==0) {
            xb[i+1]=xb[i]^b[i];
            sumb[i+1]=(sumb[i]+b[i])%MOD;
            cntb[i+1]=cntb[i]+1;
            sb.insert(b[i]);
            int j=0;
            for(auto it=sb.begin(); j<M && it!=sb.end(); it++,j++)
                elemb[i+1].push_back(*it);
        } else {
            xb[i+1]=xb[i];
            sumb[i+1]=sumb[i];
            cntb[i+1]=cntb[i];
            elemb[i+1]=elemb[i];
        }
    }

    for(int i=0; i<q; i++) {
        cini(x);
        cini(y);
        if(xa[x]==xb[y] && cnta[x]==cntb[y] && suma[x]==sumb[y] && elema[x]==elemb[y])
            cout<<"Yes"<<endl;
        else
            cout<<"No"<<endl;
    }

}

signed main() {
    solve();
}
