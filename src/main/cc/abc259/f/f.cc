/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Sort edges by weight.
 *
 * 1 - 2 - 3 - 4
 * d[2]=1
 * d[3]=1
 * We can choose edg[1]+edg[3]
 * or edg[2]
 */
void solve() {
    cini(n);
    cinai(d,n); /* d[i]=max number of edges of vertex i */

    using t3=tuple<int,int,int>;
    vector<t3> edg;
    for(int i=0; i<n-1; i++) {
        cini(u);
        cini(v);
        cini(w);
        edg.emplace_back(w,u-1,v-1);
    }
    sort(all(edg), greater<t3>());

    vi cnt(n);
    int ans=0;
    for(int i=0; i<n-1; i++) {
        auto [w,u,v]=edg[i];
        if(w>0 && cnt[u]<d[u] && cnt[v]<d[v]) {
            ans+=w;
            cnt[u]++;
            cnt[v]++;
        }
    }
    cout<<ans<<endl;

}

signed main() {
//    cini(t);
//   while(t--)
        solve();
}
