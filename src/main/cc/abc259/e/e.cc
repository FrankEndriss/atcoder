/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Each number removed that has a max primefactor
 * alters the LCM in a distinct way.
 * So we need to find foreach p the max(e),
 * and foreach number a, if it has one of those max(e), 
 * and is the only one.
 *
 * So we count the freq of the max(e)
 */
void solve() {
    cini(n);

    if(n==1) {
        cout<<1<<endl;
        return;
    }

    map<int,int> maf;   /* maf[p]=max(e) of p in all numbers */

    /* colllect input */
    vector<map<int,int>> a(n);
    for(int i=0; i<n; i++) {
        cini(m);
        map<int,int> pe;
        for(int j=0; j<m; j++) {
            cini(p);
            cini(e);
            a[i][p]=e;
            maf[p]=max(maf[p], e);
        }
    }

    map<int,vi> cnt;   /* cnt[p]=number of numbers that have factor p maf[p] times */
    for(int i=0; i<n; i++) 
        for(auto ent : a[i]) {
            if(ent.second==maf[ent.first])
                cnt[ent.first].push_back(i);
        }

    vb ans(n);  /* flag each number if altering */
    for(auto ent : cnt) 
        if(ent.second.size()==1)
            ans[ent.second[0]]=true;

    int c=0;
    for(int i=0; i<n; i++) 
        if(ans[i])
            c++;
    if(c==n)
        c--;
    cout<<c+1<<endl;
}

signed main() {
//    cini(t);
//   while(t--)
        solve();
}
