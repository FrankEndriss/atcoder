/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/dsu>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * ...
 * Most likely it is simpler to do all calculations as integer.
 */
const ld EPS=0.00000000001;
void solve() {
    cini(n);

    cind(sx);
    cind(sy);
    cind(tx);
    cind(ty);

    vd x(n);
    vd y(n);
    vd r(n);
    for(int i=0; i<n; i++)
        cin>>x[i]>>y[i]>>r[i];

    dsu d(n);

    for(int i=0; i<n; i++) {
        for(int j=i+1; j<n; j++) {
            ld dd=hypot(x[i]-x[j], y[i]-y[j]);
            if(dd<=r[i]+r[j]+EPS && dd+EPS+min(r[i],r[j])>=max(r[i],r[j])) {
                d.merge(i,j);
            }
        }
    }

    int ii=-1;
    int jj=-1;
    for(int i=0; i<n; i++) {
        if(abs(hypot(x[i]-sx, y[i]-sy)-r[i])<EPS) 
            ii=i;
        if(abs(hypot(x[i]-tx, y[i]-ty)-r[i])<EPS) 
            jj=i;
    }
    assert(ii>=0);
    assert(jj>=0);

    if(d.same(ii,jj)) 
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;
}

signed main() {
//    cini(t);
//   while(t--)
        solve();
}
