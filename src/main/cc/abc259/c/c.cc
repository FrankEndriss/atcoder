/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

vector<pair<char,int>> c(string s) {
    vector<pair<char,int>> ans;

    int cnt=1;
    int p=s[0];
    for(size_t i=1; i<s.size(); i++) {
        if(s[i]==p) 
            cnt++;
        else {
            ans.emplace_back(p,cnt);
            p=s[i];
            cnt=1;
        }
    }
    ans.emplace_back(p, cnt);
    return ans;
}

/**
 */
void solve() {
    cins(s);
    cins(t);

    auto v1=c(s);
    auto v2=c(t);

    if(v1.size()!=v2.size()) {
        cout<<"No"<<endl;
        return;
    }

    for(size_t i=0; i<v1.size(); i++) {
        if(     v1[i].first!=v2[i].first || 
                v1[i].second>v2[i].second ||
                (v1[i].second==1 && v2[i].second>1)) {
            cout<<"No"<<endl;
            return;
        }
    }

    cout<<"Yes"<<endl;


}

signed main() {
//    cini(t);
//   while(t--)
        solve();
}
