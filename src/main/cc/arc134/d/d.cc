/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * First thing to optimize is left half of constructed seq.
 * so all the selected indexes from the left half have to
 * be lex smallest possible.
 *
 * But, the result could be better, we use the second
 * element of this seq only if the right halve value of
 * the first element is not lex smaller than the left
 * half one of the second.
 * What about if they are same?
 * -> We append only if smaller
 *
 * see:
 * 1 2 3 2 3 1 7 9
 * -> we can construct
 * 1 3
 * 1 2  3 1
 * 1 2 2  3 1 9
 *
 * ****
 * Something wrong
 * Consider all pairs a[i],a[i+n] with same a[i].
 * We choose to use the lex smallest of them as first.
 * Then we append all others where a[i+n]<a[i].
 * What about a[i+n]==a[i]?
 * -> Dont append
 *  ...
 *  The rule seems to be more complected, or some edgecase :/
 */
void solve() {
	cini(n);
	cinai(a,2*n);

	using t3=tuple<int,int,int>;
	vector<t3> b(n);
	for(int i=0; i<n; i++) {
		b[i]={a[i], a[i+n], i};
	}
	sort(all(b));

	vi ans;
	ans.push_back(0);
	auto [a0,pa0,i0]=b[0];

	for(int i=1; i<n; i++) {
		auto [pa, pan, pi]=b[i];
		if(pa<pa0 && i0<pi) {
			ans.push_back(i);
			i0=pi;
		}
	}
	for(size_t i=0; i<ans.size(); i++) {
		auto [la,lan,li]=b[ans[i]];
		cout<<la<<" ";
	}
	for(size_t i=0; i<ans.size(); i++) {
		auto [la,lan,li]=b[ans[i]];
		cout<<lan<<" ";
	}
	cout<<endl;
}

signed main() {
    solve();
}
