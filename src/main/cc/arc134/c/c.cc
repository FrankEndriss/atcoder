/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * So obviously it is some dp, most likely O(n^3)
 *
 * Let b be the number of balls 1.
 * Consider dp[i][j]=number of ways to
 * use i balls1 and j boxes.
 *
 * We can put 0..b b1 into first box.
 * That determines the number of other balls possible in that box.
 * Then go on and put the remaining balls into the other boxes.
 * But that state is big, because it depends on which balls we
 * did put into the first box.
 * ???
 *
 * ****
 * Think about O(N*K)
 * Note that _all_ balls must go to boxes.
 *
 * Consider the distributions of the b1 into j boxes.
 * if j==1 we can use up to b1-1 balls in one box. (which is 0 or 1 possible distribution)
 * if j==2 we can use up to b1-2 balls in two boxes...and so on.
 * But we need to remove the cases where some same balls are in same box.
 * How?
 */
void solve() {
	cini(n);
	cini(k);
	cinai(a,n);
}

signed main() {
    solve();
}
