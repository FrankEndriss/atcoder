/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Reverse even length subseq
 * It is not written, but asume we can do that exactly once.
 *
 * let 'a' denote the smallest symbol, 'b' the next...
 * Choose (from right to left) all 'a', then after first 'a' all 'b'
 * and so on.
 * While the prefix is at least same size as the number of choosen positions.
 * Then choose the same number of positions from the prefix, so that
 * the remaining ones are lex smallest possible.
 *
 * ...but we need to take care that we do not swap smaller syms from left to right :/
 * So do a more greedy aproach.
 *
 * Iterate all symbols and swap/move them to positions in the left half.
 * Untill left becomes bigger than right.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cins(s);

    vvi pos('z'+1, {-1});	/* positions of sym */
    for(int i=0; i<n; i++)
        pos[s[i]].push_back(i);

    int l=0;
    int r=n;
    for(char sym='a'; l<r && sym<='z'; ) {
        while(l<n && s[l]<=sym)
            l++;

        while(pos[sym].back()>=r)
            pos[sym].pop_back();

        if(pos[sym].back()>l) {
            swap(s[pos[sym].back()], s[l]);
            r=pos[sym].back();
            pos[sym].pop_back();
        } else
            sym++;
    }

    cout<<s<<endl;
}

signed main() {
    solve();
}
