/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * It is a dp.
 * We go from left to right, find uncovered segments.
 * Then we cover those with a tarp, which makes the segment up to
 * pos+w covered.
 * Repeat.
 *
 * What are L and R for?
 * -> There is no R.
 *  It is length of bridge, and With of one tarp.
 *
 * Since all input is integer, we do not need any float.
 */
void solve() {
    cini(n);
    cini(l);
    cini(w);

    cinai(a,n);
    sort(all(a));
    
    int c=0;	/* covered up to */
    int ans=0;
    for(int i=0; i<n; i++) {
        if(a[i]<=c) {
            c=max(c, a[i]+w);
            continue;
        }
        int d=a[i]-c;
        int cnt=(d+w-1)/w;
        ans+=cnt;
	c=a[i]+w;
    }
    int d=l-c;
    if(d>0) {
        int cnt=(d+w-1)/w;
        ans+=cnt;
    }

    cout<<ans<<endl;

}

signed main() {
    solve();
}
