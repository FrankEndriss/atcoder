
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider each position the begin of the K-seq.
 * We can move the 1 from left or from right,
 * then the 2 and so on.
 *
 * There are 
 * 200-n positions
 * times 200 steps for search the next occ of kk to the left and right
 * times K.
 *
 * Recursive sol should work.
 * ...it seems to be not optimal to construct the K-seq in order
 * of the elements.
 * If we need to move some right element from far left, we need
 * to do that moves first...then the ones in between.
 * This is, because moving an element "over" others moves
 * the others by one position. After they where moved to 
 * the correct position :/
 * So we need to consider all possible left/right patterns,
 * which are 2^K ones.
 * Then foreach pattern and starting position, 
 * take alle left elements from left, then the right ones from right.
 * And the other way.
 *
 * But only two minutes left in contest... :/
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(k);

    cinai(a,n);

    /* @return number of moves needed to construce the sublist on a[]
     * starting at position pos with value kk.
     */
    function<int(vi&,int,int)> go=[&](vi &aa, int pos, int kk) {
        if(kk>k)
            return 0LL;

        assert(pos<n);

        int p=pos;
        for(; p>=0; p--)
            if(aa[p]==kk)
                break;
        int ansL=INF;

        if(p>=0) {
            vi aaa=aa;
            if(p!=pos)
                rotate(aaa.begin()+p, aaa.begin()+p+1, aaa.begin()+pos+1);
            ansL=pos-p+go(aaa, pos+1, kk+1);
        }

        p=pos;
        for(; p<n; p++)
            if(aa[p]==kk)
                break;
        int ansR=INF;
        if(p<n) {
            vi aaa=aa;
            if(p!=pos)
                rotate(aaa.begin()+pos, aaa.begin()+p, aaa.begin()+p+1);
            ansR=p-pos+go(aaa,pos+1, kk+1);
        }

        return min(ansL, ansR);
    };

    int ans=INF;
    for(int i=0; i+k<=n; i++) {
        ans=min(ans, go(a, 0, 1));
    }
    assert(ans<INF);
    cout<<ans<<endl;

}

signed main() {
    solve();
}
