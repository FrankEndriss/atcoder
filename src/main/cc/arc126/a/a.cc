
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * 3,3,4
 * 4,4,2
 * 3,3,2,2
 * 4,2,2,2
 * 2,2,2,2,2
 */
void solve() {
    cini(n2);
    cini(n3);
    cini(n4);

    int ans=min(n3/2, n4);
    n3-=2*ans;
    n4-=ans;

    int a2=min(n4/2, n2);
    ans+=a2;
    n4-=2*a2;
    n2-=a2;

    int a3=min(n3/2, n2/2);
    ans+=a3;
    n3-=2*a3;
    n2-=2*a3;

    int a4=min(n4, n2/3);
    ans+=a4;
    n4-=a4;
    n2-=3*a4;

    ans+=n2/5;
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
