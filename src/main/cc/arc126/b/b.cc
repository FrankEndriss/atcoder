
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/lazysegtree>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using S=int; /* type of values */
using F=int; /* type of upates */

S st_op(S a, S b) {
    return max(a,b);
}

/* This is the neutral element */
const S INF=1e9;
S st_e() {
    return 0;
}

/* This applies an update to some value. */
S st_mapping(F f, S x) {
    return max(f,x);
}

/* This combines two updates.  */
F st_composition(F f, F g) {
    return max(f,g);
}

/* This is the neutral update. 
 * It is similar to st_e() in some sense.  */
F st_id() {
    return 0;
}

using stree=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;

/**
 * Choose max number of points from a[] or b[],
 * to connect to the other end.
 * maxFlow
 *
 *
 * ...no, it is _cross free_.
 *
 * So, its kind of a dp.
 * Works like a LIS.
 *
 * How to do the inserts/updates correctly?
 * Try a segtree...
 */
void solve() {
    cini(n);
    cini(m);

    vvi adj(n);
    for(int i=0; i<m; i++) {
        cini(a); a--;
        cini(b);
        adj[a].push_back(b);
    }

    /* dp.prod(0,i)=max len at pos=i-1 */
    stree dp(n+1);

    for(int i=0; i<n; i++) {
        sort(all(adj[i]));
        reverse(all(adj[i]));
        for(int b : adj[i]) {
            /* find max len at pos<b */
            int len=dp.prod(0,b)+1;
            dp.apply(b,n+1,len);
        }
    }

    for(int i=0; i<n+1; i++) 
        cerr<<dp.get(i)<<" ";
    cerr<<endl;

    int ans=dp.get(n);
    cout<<ans<<endl;
}


signed main() {
    solve();
}
