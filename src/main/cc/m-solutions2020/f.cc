/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Check planes on opposite direction first, if there
 * are any trivially calc time of first crash.
 *
 * Then foreach horizontal plane find the crashes with vertical ones.
 * This is, for plane i find the j ones where
 * y[j]+-t=y[i]
 * x[j]=x[i]+t
 * For t>0 and sorted by t. Then the first one is ans.
 * This means we are searching for planes where y diff to current 
 * plane equals x diff to current plane.
 *
 * So, lets group the vertical planes by y coordinate of a plane
 * which starts at x=0 and crashes with it.
 * Sort them by x or y.
 *
 * Then we can find the group in O(1), and in the group the first
 * one in O(logn) by binary search.
 *
 * Do this whit signs set up for all 4 pairs of directions.
 * Or do it ones and rotate 3 times?
 */
void solve() {
    // TODO
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
