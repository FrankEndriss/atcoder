/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

void solve() {
    cini(n);

    vi a;
    for(int i=1; i<=n; i++) {
        cini(aux);
        if(a.size()==0 || aux!=a.back())
            a.push_back(aux);
    }

    vi mi;
    vi ma;

    for(int i=1; i+1<a.size(); i++) {
        if(a[i-1]>a[i] && a[i+1]>a[i])
            mi.push_back(i);
        else if(a[i-1]<a[i] && a[i+1]<a[i])
            ma.push_back(i);
    }

    int mon=1000;
    int amt=0;
    if(a[0]<a[1]) {
        amt=mon/a[0];
        mon-=amt*a[0];
    }

    int idxmi=0;
    int idxma=0;
    while(idxmi<mi.size() || idxma<ma.size()) {
        if(amt>0) {
            mon+=amt*a[ma[idxma]];
            amt=0;
            idxma++;
        }  else {
            amt=mon/a[mi[idxmi]];
            mon-=amt*a[mi[idxmi]];
            idxmi++;
        }
    }
    if(amt>0)
        mon+=amt*a.back();

    cout<<mon;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
