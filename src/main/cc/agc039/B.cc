/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI=3.1415926535897932384626433;
typedef long long ll;
//#define DEBUG

#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int INF=1e9+7;

template<typename T>
void floyd_warshal(vector<vector<T>> &d, int n) {
    for (int k = 0; k < n; ++k) {
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                d[i][j] = min(d[i][j], d[i][k] + d[k][j]);
            }
        }
    }
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(n);
    cinas(s, n);

    vvi tree(n, vi(n));
    for(int i=0; i<n; i++)
        for(int j=i+1; j<n; j++)
            if(s[i][j]=='1') {
                tree[i][j]=tree[j][i]=1;
            } else {
                tree[i][j]=tree[j][i]=INF;
            }

#ifdef DEBUG
    cout<<"parsed tree"<<endl;
#endif

    vi level(n);

    function<bool(int,int)> dfs=[&](int node, int parent) {
#ifdef DEBUG
    cout<<"bfs node="<<node<<" parent="<<parent<<" level[node]="<<level[node];
    if(parent>=0)
        cout<<" level[parent]="<<level[parent];
    cout<<endl;
#endif
        if(level[node]>0) {
            int loopsz=abs(level[node]-level[parent])+1;
#ifdef DEBUG
    cout<<"loopsz="<<loopsz<<endl;
#endif
            if(loopsz&1)
                return false;    // loop of odd size are not ok
            else
                return true;
        }

        if(parent>=0)
            level[node]=level[parent]+1;
        else
            level[node]=1;
#ifdef DEBUG
    cout<<"level[node]="<<level[node]<<endl;
#endif

        for(int i=0; i<n; i++) {
            if(tree[node][i]==1 && (i!=parent))
                if(!dfs(i, node))
                    return false;
        }

        return true;
    };
    bool ok=dfs(0, -1);

    if(!ok) {
        cout<<-1<<endl;
        return 0;
    }

    floyd_warshal(tree, n);
    int ans=0;
    for(int i=0; i<n; i++) {
        for(int j=0; j<n; j++) {
            ans=max(ans, tree[i][j]);
#ifdef DEBUG
            cout<<tree[i][j]<<" ";
#endif
        }
#ifdef DEBUG
        cout<<endl;
#endif
    }

    cout<<ans+1<<endl;
}

