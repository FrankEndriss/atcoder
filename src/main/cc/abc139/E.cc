/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
//#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<pii> vpii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);

    typedef vector<set<pii>> vspii;

    const int cnt=n*(n-1)/2;
    vector<vspii> adj(n+1, vspii(n+1));; // adj[i][j]== set of matches happen after match i,j
    vector<vspii> adjR(n+1, vspii(n+1)); // adjR[i][j]== set of matches happen before match i,j

/* create a graph
 * Edges are dependencies, vertices are matches.
 * Remove on every day all vertices without incoming dependencies.
 * Repeat.
 */

    for(int i=1; i<=n; i++) {
        vpii row;
        pii prev;
        for(int j=0; j<n-1; j++) {
            int op;
            cin>>op;
            int p1=min(i, op);
            int p2=max(i, op);
            pii curr={ p1, p2 };
            if(j>0) {
                adj[prev.first][prev.second].insert(curr);
                adjR[curr.first][curr.second].insert(prev);
            }
            prev=curr;
        }
    }

    vpii rem;   // list of matches on current day
    for(int i=1; i<=n; i++) {
        for(int j=i+1; j<=n; j++) {
            if(adjR[i][j].size()==0)
                rem.push_back({i,j});
        }
    }

    int day=0;
    int c=0;
    while(true) {
        vpii rem2;
        if(rem.size()==0)
            break;
        for(pii p : rem) {
            c++;
            for(pii p2 : adj[p.first][p.second]) {
                adjR[p2.first][p2.second].erase(p);
                if(adjR[p2.first][p2.second].size()==0)
                    rem2.push_back(p2);
            }
        }
        rem.swap(rem2);
        day++;
    }

    if(c!=cnt) 
        cout<<-1<<endl;
    else
        cout<<day<<endl;
}

