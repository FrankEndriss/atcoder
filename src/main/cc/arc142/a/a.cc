/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Construct all numbers and compare.
 * ...try to not get confused by sic problem statement.
 *
 * We need to kind of reverse the operations, starting with k.
 * So, the smallest number possibly outcome for f(x) is 
 * min(k, reverse(k))
 *
 * And all numbers with {k,reverse(k)} and zeros appended will end in that number.
 *
 * WA?
 * Not able to tell how much I hate that kind of problem.
 */
void solve() {
    cini(n);
    cins(k);

    istringstream iss(k);
    int x1;
    iss>>x1;

    reverse(all(k));
    istringstream iss2(k);
    int x2;
    iss2>>x2;

    if(x2<x1) {
        cout<<0<<endl;
        return;
    }


    set<int> ans;
    while(x1<=n) {
        ans.insert(x1);
        x1*=10;
    }
    while(x2<=n) {
        ans.insert(x2);
        x2*=10;
    }

    cout<<ans.size()<<endl;


}

signed main() {
        solve();
}
