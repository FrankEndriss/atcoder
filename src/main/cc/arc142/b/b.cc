/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * 0..n*n-1
 *  There must not be the same number of adj cells 
 *  bg and sm a[i][j].
 *
 *  Corners have 3 adj
 *  sides have 5 adj, so only those with 8 adj count
 *
 *  1 2 3 4
 *  5 6 7 8
 *  9 ...
 *
 *  Then swap each two cols.
 *  2 1 4 3
 *  ...
 */
void solve() {
    cini(n);

    vvi a(n, vi(n));
    for(int i=0; i<n; i++) 
        for(int j=0; j<n; j++) 
            a[i][j]=i*n+1 + j;

    for(int i=0; i<n; i++) 
        for(int j=1; j<n; j+=2) 
            swap(a[i][j], a[i][j-1]);

    for(int i=0; i<n; i++)  {
        for(int j=0;  j<n; j++) 
            cout<<a[i][j]<<" ";
        cout<<endl;
    }

}

signed main() {
        solve();
}
