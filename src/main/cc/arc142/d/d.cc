/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that operations are symetric, we can undo every operation.
 * so we want to find the number of possible settings that allow 
 * any operation, because then we can do K of them.
 *
 * Consider putting a token on v, then there must be at least
 * one adj[v][i]=u with no token.
 * Also, that vertex u must not be paired with any other vertex.
 *
 * Its a tree so there are no circles.
 * A configuration is valid if we can decompose all vertex with a token
 * into paths of len>=2 ending in a non token vertex.
 *
 * How to count?
 * Let pl[] be the set of parents of all leafs.
 * Note that from each pl[i] at most 1 child can have a token.
 *
 * Root the tree somewhere.
 * dp1[i]=possible configuration with unused vertex i in step 1
 * dp2[i]=possible configuration with used vertex i in step 1 from a child
 * dp3[i]=possible configuration with need to move to parent in step 1.
 *
 *
 * ...How can answer be 0?
 * We can allways put no token anywhere, which satisfies all conditions.
 * But its written "put a piece on one or more..."
 * However, something wrong with the definition of "Sk is unique".
 * WTF?
 */
using mint=modint998244353;
void solve() {
    cini(n);
    vvi adj(n);
    for(int i=0; i<n-1; i++) {
        cini(a); a--;
        cini(b); b--;
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    vi dp1(n);
    vi dp2(n);
    vi dp3(n);

    function<void(int,int)> dfs=[&](int v, int p) {
        dp1[v]=0;
        dp3[v]=0;
        for(int chl : adj[v]) {
            if(chl==p)
                continue;

            /* no token on v  or token moved to chl */
            dp1[v]+=(dp1[chl]+dp1[chl]);
            /* no token on v */
            dp1[v]+=dp2[chl];

            /* no token on v */
            dp2[v]+=dp3[chl];
            /* token on v */
            dp3[v]+=dp1[chl]+dp2[chl];
        }
    };

    dfs(0,-1);
    mint ans=dp1[0]+dp2[0]+dp3[0];
    cout<<ans.val()<<endl;
}

signed main() {
        solve();
}
