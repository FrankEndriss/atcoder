/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

int ask(int u, int v) {
    cout<<"? "<<u+1<<" "<<v+1<<endl;
    cini(resp);
    if(resp<0)
        exit(0);
    return resp;
}

/**
 * We may ask for all, but not for 1,2
 * But have to find 1,2
 *
 * consider for i in 3..n
 * ask(1,i)
 * ask(2,i)
 *
 * Is that enough?
 * consider smallest sum of both distances.
 * If it is >3 then it is the dist.
 * if it is 2 then there is 1 v between them, hence ans=2
 * 1 - 3 - 2
 * if it is 1 ... not possible
 * Else it is 3, then there can be several of dist==3
 * Cases:
 * 1 - 3 - 4 - 2
 * 3 - 1 - 2 - 4
 *
 * 3
 *  \
 *  / 1 - 2 - 5 
 * 4
 *
 * If all of them are on the same 
 * check the dist of them both.
 * if 1 then d(1,2)==3, else d(1,2)==1
 * If there is only 1 of sum==3, then there is none on the other side,
 * and d(1,2)==1
 *
 * And after 5 WA this is burned... dont like it either :/
 */
const int INF=1e9;
void solve() {
    cini(n);

    vi s0(n);
    vi s1(n);
    vi s(n,INF);

    vi v3;
    for(int i=2; i<n; i++)  {
        s0[i]=ask(0,i);
        s1[i]=ask(1,i);
        s[i]=s0[i]+s1[i];
    }

    const int mi=*min_element(all(s));

    cout<<"! ";
    if(mi!=3) {
        cout<<mi<<endl;
    } else {
        for(int i=2; i<n; i++)
            if(s[i]==3)
                v3.push_back(i);

        /* now there can be 0 or 2 vertex on path from 0 to 1 */

        if(v3.size()==1) {
            cout<<1<<endl;
        } else {
            int d=ask(v3[0],v3[1]);
            if(d==2 || d==3) {
                cout<<1<<endl;
            } else {
                cout<<3<<endl;
            }
        }
    }
}

signed main() {
    solve();
}
