
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * Find all distinct primefactors,
 * ans=product.
 * No, we need to find the smallest multiple of a set of primefactors
 * that cover all numbers.
 * How to do that?
 * It can be like up to 50 different primefactors.
 * It seems to be some kind of knapsack.
 *
 * Foreach factor, we can include it in the set, or not include it.
 * Based on that we get a min product.
 * No.
 *
 * Foreach number we can include any of its factors, for the cost of
 * multipliying ans by this factor.
 * So we must try all, but can stop once ans is not optimal.
 */
const int N=51;
void solve() {
    cini(n);
    cinai(a,n);

    vector<set<int>> p(n);
    for(int i=0; i<n; i++) {
        for(int j=2; j*j<=a[i]; j++) {
            while(a[i]%j==0)  {
                p[i].insert(j);
                a[i]/=j;
            }
        }
        if(a[i]>1)
            p[i].insert(a[i]);
    }

    function<int(int,vb)> go=[&](int idx, vb factors) {
        if(idx==n)
            return 1LL;

        for(int pf : p[idx]) {
            if(factors[pf])
                return go(idx+1, factors);
        }

        int ans=1e18;
        for(int pf : p[idx]) {
            factors[pf]=true;
            ans=min(ans, pf*go(idx+1, factors));
            factors[pf]=false;
        }
        return ans;
    };

    vb factors(N);
    int ans=go(0, factors);
    cout<<ans<<endl;
}

signed main() {
    solve();
}
