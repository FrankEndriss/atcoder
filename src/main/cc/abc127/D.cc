/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
ll n, m;
    cin>>n>>m;
    vector<ll> a(n);
    vector<int> id(n);
    vector<pair<ll, ll>> cb;
    ll sum=0;
    fori(n) {
        cin>>a[i];
        sum+=a[i];
        id[i]=i;
    }

    fori(m) {
        ll b, c;
        cin>>b>>c;
        cb.push_back({ c, b });
    }

    sort(cb.begin(), cb.end());
    sort(id.begin(), id.end(), [&](int i1, int i2) {
        return a[i1]<a[i2];
    });

    int nextCb=m-1;
    int nextA=0;
    while(nextCb>=0 && nextA<n) {
        int d= cb[nextCb].first - a[id[nextA]];
        if(d>0) {
            sum+=d;
            nextA++;
            cb[nextCb].second--;
            if(cb[nextCb].second==0)
                nextCb--;
        } else 
            break;
    }
    
    cout<<sum<<endl;

}

