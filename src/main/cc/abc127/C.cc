/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, m;
    cin>>n>>m;
    vector<ll> l(m), r(m);
    ll maxL=0;
    ll minR=1000000000;
    fori(m) {
        cin>>l[i]>>r[i];
        maxL=max(maxL, l[i]);
        minR=min(minR, r[i]);
    }

    ll ans=max(0LL, minR-maxL+1);
    cout<<ans<<endl;

}

