/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * How to combine the ops?
 * Does the order matter? Yes.
 * Can we consider bit by bit?
 * Find foreach bit the last position where it was set
 * to 1 or to 0.
 * After that op it can only be flipped (xor 1), count parity how often.
 *
 * ...
 * Again, why this WA?
 * ***
 * Think about composition function.
 * At each bit position we got two possible input values,
 * and we can apply all N operations to both values, thus
 * getting that bit after the ith round.
 */
const int N=40;
void solve() {
    cini(n);
    cini(c);

    vvi fun(N, {0,1});

    for(int i=0; i<n; i++) {
        cini(t);
        cini(a);

        int c0=0;
        for(int j=0; j<N; j++) { 
            const int b=(1LL<<j);
            for(int k=0; k<2; k++)  {
                if(t==1) {
                    fun[j][k]=((a&b)!=0)&fun[j][k];
                } else if(t==2) {
                    fun[j][k]=((a&b)!=0)|fun[j][k];
                } else if(t==3) {
                    fun[j][k]=((a&b)!=0)^fun[j][k];
                } else
                    assert(false);

            }
            c0=c0|(fun[j][(c&b)!=0]<<j);
        }
        cout<<c0<<endl;
        c=c0;
    }
}

signed main() {
    solve();
}
