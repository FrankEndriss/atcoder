/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * W=win for player 1
 * D=no winner
 * L=los for player 1
 */
void solve() {
    cini(n);
    cinas(s,n);

    bool correct=true;
    for(int i=0; i<n; i++) {
        for(int j=0; j<n; j++) {
            if(s[i][j]=='W') {
                if(s[j][i]!='L')
                    correct=false;
            } else if(s[i][j]=='L') {
                if(s[j][i]!='W')
                    correct=false;
            } else if(s[i][j]=='D') {
                if(s[j][i]!='D')
                    correct=false;
            }
        }
    }

    if(correct)
        cout<<"correct"<<endl;
    else
        cout<<"incorrect"<<endl;
}

signed main() {
//   cini(t);
//   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
//   }
}
