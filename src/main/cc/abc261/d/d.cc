/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * dp
 * dp[i]=max possible money after current strike if counter is i
 *
 * Understand the rules:
 * we can choose head or tail.
 *
 * WA??? :/
 * ...
 * idk, there is something wrong, maybe with the statement, sic.
 */
const int N=5e3+7;
void solve() {
    cini(n);
    cini(m);
    cinai(x,n);

    vi cy(N);
    for(int i=0; i<m; i++) {
        cini(c);
        cini(y);
        cy[c]=y;
    }

    vi dp(N);
    for(int i=0; i<n; i++) {    /* ith toss */
        vi dp0(N);
        for(int j=0; j<=i; j++) { /* counter is j bofore current toss, and j+1 after */
            dp0[0]=max(dp0[0], dp[j]);      /* tails */
            dp0[j+1]=dp[j]+x[i]+cy[j+1];       /* heads */
        }

        dp.swap(dp0);
    }
    cout<<*max_element(all(dp))<<endl;
}

signed main() {
    solve();
}
