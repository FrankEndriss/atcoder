/**
 *  * Dont raise your voice, improve your argument.
 *   * --Desmond Tutu
 *    */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

void solve() {
    cini(h);
    cini(w);
    cini(a);
    cini(b);

    if(a*2+b !=h*w) {
        cout<<0<<endl;
        return;
    }

    vvb vis(h, vb(w));
    /* number of possibilities putting next tile at i,j */
    function<int(int,int)> dfs=[&](int i, int j) {
        if(i>=h)
            return 1LL;

        int ii=i;
        int jj=j;
        jj++;
        if(jj>=w) {
            ii++;
            jj=0;
        }

        int ans=0;
        if(vis[i][j]) {
            return dfs(ii,jj);
        } else {
            vis[i][j]=true;
            if(b) {
                b--;
                ans+=dfs(ii,jj);
                b++;
            }
            if(a && i+1<h && !vis[i+1][j]) {
                a--;
                vis[i+1][j]=true;
                ans+=dfs(ii,jj);
                vis[i+1][j]=false;
                a++;
            }
            if(a && j+1<w && !vis[i][j+1]) {
                a--;
                vis[i][j+1]=true;
                ans+=dfs(ii,jj);
                vis[i][j+1]=false;
                a++;
            }
            vis[i][j]=false;
        }

        return ans;
    };

    int ans=dfs(0,0);
    cout<<ans<<endl;
}

signed main() {
    solve();
}
