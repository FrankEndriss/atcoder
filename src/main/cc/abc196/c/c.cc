
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to split the number in two halfs
 * second halve on digit longer.
 *
 * Then two cases, len even or len odd.
 *
 * Some edgecase :/
 * Leading zeros in input?
 */
void solve() {
    cins(s);
    while(s.size() && s[0]=='0')
        s=s.substr(1,s.size()-1);

    if(s.size()<2) {
        cout<<0<<endl;
        return;
    }

    int len=s.size();
    int len1=len/2; /* left half smaller */
    int len2=len-len1;

    string s1=s.substr(0,len1);
    string s2=s.substr(len1, len2);

    istringstream iss1(s1);
    int n1;
    iss1>>n1;

    istringstream iss2(s2);
    int n2;
    iss2>>n2;

    int ans=0;
    if(len1==len2) {
        ans=n1;
        if(n2<n1)
            ans--;
    } else {
        ans=1;
        while(ans<=n1)
            ans*=10;
        ans--;
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}
