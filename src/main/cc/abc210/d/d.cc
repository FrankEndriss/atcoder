
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/lazysegtree>
using namespace atcoder;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"



using S=ll; /* type of values */
using F=ll; /* type of upates */

S st_op(S a, S b) {
    return min(a,b);
}

/* This is the neutral element */
const S INF=1e18;
S st_e() {
    return INF;
}

/* This applies an update to some value. */
S st_mapping(F f, S x) {
    return f+x;
}

/* This combines two updates.  */
F st_composition(F f, F g) {
    return f+g;
}

/* This is the neutral update.
 * It is similar to st_e() in some sense.  */
F st_id() {
    return 0;
}

using stree=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;

/*
 * How in less than O(n^4)?
 *
 * Cost of railway is a[i0][j0]+a[i1][j1]+ c*dist(i0,j0, i1,j1)
 *
 * How to find the cheapest second station for a fixed first one?
 * Iterate all :/
 *
 * Use some segment tree.
 * Initial all a[i,j] are in there, plus the dist from 0,0
 * Then choose the min of all.
 * Then update the tree to reflect the dist from second cell.
 * Again choose min of all...
 * etc
 *
 * ... to complecated to implement :/
 */
void solve() {
    cini(h);
    cini(w);
    cini(c);
    vector<ll> a(h*w);

    for(ll i=0; i<h; i++)
        for(ll j=0; j<w; j++)
            cin>>a[i*w+j];

    stree seg(a);
    ll ans=INF;
    /* got top down, then same in next col... */
    for(ll j=0; j<w; j++) {
        if(j>0) { /* next col */
            for(ll ii=0; ii<h; ii++) {
                seg.apply(ii*w, ii*w+j, 1);
                seg.apply(ii*w+j, ii*w+w, -1);
            }
        }

        for(ll i=0; i<h; i++) {
            /* next row */
            if(i>0) {
                seg.apply(0, i*w, 1);
                seg.apply(i*w,h*w, -1);
            }

            ll lans=a[i*w+j]+seg.prod(0, h*w);
            ans=min(ans, lans);
        }
            /* last to first row */
            for(ll i=0; i<h; i++) {
                seg.apply(i*w, i*(w+1), -(h-1-i));
                seg.apply(h-1-i*w, h-1-i*(w+1), (h-1-i));
            }

    }
    cout<<ans<<endl;

}

signed main() {
    solve();
}
