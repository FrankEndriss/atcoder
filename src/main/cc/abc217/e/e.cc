
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
void solve() {
    cini(n);

    deque<int> next;
    multiset<int> cur;

    for(int i=0; i<n; i++) {
        cini(q);
        if(q==1) {
            cini(x);
            next.push_back(x);
        } else if(q==2) {
            if(cur.size()>0) {
                cout<<*cur.begin()<<endl;
                cur.erase(cur.begin());
            } else {
                cout<<next[0]<<endl;
                next.erase(next.begin());
            }
        } else if(q==3) {
            while(next.size()) {
                int nn=next.back();
                next.pop_back();
                cur.insert(nn);
            }
        }
    }
}

signed main() {
    solve();
}
