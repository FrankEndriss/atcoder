
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Its a DP somehow.
 *
 * To clear a segment l,r we have some choices:
 * l,l0; l0+1,r0; r0+1,r in any of 6 orders
 * ...no.
 *
 * Consider student 1.
 * 1 can be pair with 2, 
 * or 2,3 then 1,4
 * or 2,...,5 then 1,6
 * or 2....,7 then 1,8
 * and so on.
 * 
 *
 * So consider all prefixes starting at 1 with even lengths
 *
 * A seq with l1*2 elements need l1 steps to be cleared.
 * So, two seqs with l1*2 and l2*2 elements can be merged in
 * how many ways?
 * ...stars'n'bars or something, idk :/
 */
using mint=modint998244353;


const int N=1e4;
vector<mint> fac(N);

const bool initFac=[]() {
    fac[1]=1;
    for(int i=2; i<N; i++) {
        fac[i]=fac[i-1]*i;
    }
    return true;
}();

/** see https://cp-algorithms.com/combinatorics/binomial-coefficients.html */
mint nCr(int n, int k) {
    if(k==0 || k==n)
        return 1;
    if(k==1 || k==n-1)
        return n;
    if(k>n/2)
        return nCr(n, n-k);

    return fac[n]*fac[k].inv()*fac[n-k].inv();
}

/* number of ways to merge two seqs of len l1 and l2
 * */
mint comb(int l1, int l2) {
    //cerr<<"comb, l1="<<l1<<" l2="<<l2<<" ans="<<nCr(l1+l2, l1).val()<<endl;
    return nCr(l1+l2, l2);
}

void solve() {
    cini(n);
    cini(m);

    vector<set<int>> a(2*n);
    for(int i=0; i<m; i++) {
        cini(aidx);
        cini(b);
        a[aidx-1].insert(b-1);
    }

    vector<vector<mint>> memo(2*n+1, vector<mint>(2*n+1));
    vector<vector<bool>>  vis(2*n+1, vector<bool>(2*n+1));

    function<mint(int,int)> go=[&](int l, int r) {
        assert(l<r);

        if(vis[l][r])
            return memo[l][r];
        vis[l][r]=true;

        if(l+1==r) {
            if(a[l].count(r))
                return memo[l][r]=mint(1);
            else
                return memo[l][r]=mint(0);
        }

        mint ans=0;
        if(a[l].count(r)>0)
            ans=go(l+1,r-1)*(((r-1-(l+1)+1)/2)+1);

        for(int i=l+1; i<r; i+=2)
            ans+=go(l,i)*go(i+1,r)*comb((i-l+1)/2, (r-i)/2);
        return memo[l][r]=ans;
    };

    mint ans=go(0,2*n-1);

    cout<<ans.val()<<endl;
}


signed main() {
    solve();
}

