package main

import (
	"bufio"
	. "fmt"
	"os"
	"strconv"
	//"sort"
	//"container/heap"
)

const _0 int64 = 0
const _1 int64 = 1
const INF int64 = 1e18 + 7

func btoi(b bool) int64 {
	if b {
		return 1
	} else {
		return 0
	}
}

func max(i1 int64, i2 int64) int64 { return []int64{i1, i2}[btoi(i1 < i2)] }
func min(i1 int64, i2 int64) int64 { return []int64{i2, i1}[btoi(i1 < i2)] }
func abs(i1 int64) int64           { return max(i1, -i1) }

func makeii(n int64, m int64) [][]int64 {
	a := make([][]int64, n)
	for i := _0; i < n; i++ {
		a[i] = make([]int64, m)
	}
	return a
}

/*
*/
var r *bufio.Reader
var wr *bufio.Writer
var er *bufio.Writer
func cini() int64 {
	s:=cins()
	ans, err := strconv.ParseInt(string(s), 10, 64);
	if err!=nil {
		Fprintln(os.Stderr, "cini failed, s=", s)
		panic("cini failed")
	}
	return ans
}

func cins() []byte {
	s:=make([]byte,0)
	for {
		b,err:=r.ReadByte()
		if err!=nil {
			return s
		}
		if b==10 || b==13 || b=='\t' || b==' ' {
			continue
		} else {
			s=append(s, b)
			break;
		}
	}
	for {
		b,err:=r.ReadByte()
		if err!=nil || b==10 || b==13 || b=='\t' || b==' ' {
			break
		}
		s=append(s,b)
	}

	return s
}

func solve() {
	l := cini()-1
	r := cini()-1
	a := cins()

	for l<r {
		a[l],a[r]=a[r],a[l]
		l++
		r--
	}
	Fprintln(wr, string(a))
}

func main() {
	r = bufio.NewReader(os.Stdin)
	wr=bufio.NewWriter(os.Stdout)
	er=bufio.NewWriter(os.Stderr)
	defer wr.Flush()
	defer er.Flush()
	//t:=cini()
	//for ; t>0; t-- {
	solve()
	//}
}
