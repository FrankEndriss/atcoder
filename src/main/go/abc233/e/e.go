package main

import (
	"bufio"
	. "fmt"
	"os"
	"strconv"
	//"sort"
	//"container/heap"
)

const _0 int64 = 0
const _1 int64 = 1
const INF int64 = 1e18 + 7

func btoi(b bool) int64 {
	if b {
		return 1
	} else {
		return 0
	}
}

func max(i1 int64, i2 int64) int64 { return []int64{i1, i2}[btoi(i1 < i2)] }
func min(i1 int64, i2 int64) int64 { return []int64{i2, i1}[btoi(i1 < i2)] }
func abs(i1 int64) int64           { return max(i1, -i1) }

func makeii(n int64, m int64) [][]int64 {
	a := make([][]int64, n)
	for i := _0; i < n; i++ {
		a[i] = make([]int64, m)
	}
	return a
}

/*
 */
var sc *bufio.Scanner
var wr *bufio.Writer
var er *bufio.Writer

func cini() int64 {
	sc.Scan()
	ans, err := strconv.ParseInt(sc.Text(), 10, 64)
	if err != nil {
		panic("cini failed")
	}
	return ans
}

/*
 1234
+ 123
+  12
+   1

*/
func solve() {
	r:=bufio.NewReader(os.Stdin)
	s:=make([]byte,1)
	s[0]='0'
	for {
		b,err:=r.ReadByte()
		if err!=nil || b<'0' || b>'9' {
			break
		}
		s=append(s,b)
	}
	//Fprintln(os.Stderr, "input:", s)

	pre := make([]int64, len(s)+1)
	for i := 0; i < len(s); i++ {
		val := s[i] - byte('0')
		if val > 9 || val < 0 {
			panic("something wrong")
		}

		pre[i+1] = pre[i] + int64(val)
	}

	ans := make([]byte, len(s))
	for i := range ans {
		ans[i] = '0'
	}

	carry := _0
	for i := 0; i < len(s); i++ {
		val := pre[len(s)-i] + carry
		ans[len(s)-1-i] = '0' + byte(val%10)
		carry = val / 10
		//Fprintln(er,string(ans)," carry=", carry, " ch=",ch, " val=", val, " pre[len(s)-i]=", pre[len(s)-i])
	}

	idx:=0
	for ans[idx]=='0' {
		idx++
	}
	Fprintln(wr, string(ans[idx:]))
}

func main() {
	//sc.Split(bufio.ScanLines)
	wr = bufio.NewWriter(os.Stdout)
	er = bufio.NewWriter(os.Stderr)
	defer wr.Flush()
	defer er.Flush()
	//t:=cini()
	//for ; t>0; t-- {
	solve()
	//}
}
