package main

import (
	"bufio"
	. "fmt"
	"os"
	"strconv"
	//"sort"
	//"container/heap"
)

const _0 int64 = 0
const _1 int64 = 1
const INF int64 = 1e18 + 7

func btoi(b bool) int64 {
	if b {
		return 1
	} else {
		return 0
	}
}

func max(i1 int64, i2 int64) int64 { return []int64{i1, i2}[btoi(i1 < i2)] }
func min(i1 int64, i2 int64) int64 { return []int64{i2, i1}[btoi(i1 < i2)] }
func abs(i1 int64) int64           { return max(i1, -i1) }

func makeii(n int64, m int64) [][]int64 {
	a := make([][]int64, n)
	for i := _0; i < n; i++ {
		a[i] = make([]int64, m)
	}
	return a
}

var sc *bufio.Scanner
var wr *bufio.Writer
var er *bufio.Writer
func cini() int64 {
	sc.Scan();
	ans, err := strconv.ParseInt(sc.Text(), 10, 64);
	if err!=nil {
		panic("cini failed")
	}
	return ans
}
func cinai(n int64) []int64 {
	ans:=make([]int64, n)
	for i:=range ans {
		ans[i]=cini()
	}
	return ans
}

/*
We need to paint the n,n row/col cells '#'
Then the both 1,x and x,1 cells '.'
Then the both n-1,x and x,n-1 "#'
and so on.
But actually we do not create the grid, since MLE.
On query, 
if r[rr] or c[cc] == n then cell='#'
else if r[rr] or c[cc] == 1 then cell='.'
else if r or c == n-1 then cell='#'
else if r or c == 2 then cell='.'
and so on...

On query, how to find the first match?
*/
func solve() {
	n:=cini()
	r:=cinai(n);
	c:=cinai(n);

	//posR:=make([]int64, n)
	//posC:=make([]int64, n)
	for i:=range r {
		r[i]--
		c[i]--
		//posR[r[i]-1]=int64(i)
		//posC[c[i]-1]=int64(i)
	}

	for q:=cini();q>0; q-- {
		rr:=cini()
		rr--
		cc:=cini()
		cc--
		//Fprintln(os.Stderr, "rr=", rr, "cc=",cc)

		v1:=max(r[rr],c[cc]);
		v2:=min(r[rr],c[cc]);
		if(n-1-v1<=v2) {
			Fprintf(wr,"#")
		}else {
			Fprintf(wr,".")
		}
	}
	Fprintln(wr)
}

func main() {
	sc = bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)
	wr=bufio.NewWriter(os.Stdout)
	er=bufio.NewWriter(os.Stderr)
	defer wr.Flush()
	defer er.Flush()
	//t:=cini()
	//for ; t>0; t-- {
	solve()
	//}
}
