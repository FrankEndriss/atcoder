package main

import (
	"bufio"
	. "fmt"
	"os"
	"strconv"
	//"sort"
	//"container/heap"
)

const _0 int64 = 0
const _1 int64 = 1
const INF int64 = 1e18 + 7

func btoi(b bool) int64 {
	if b {
		return 1
	} else {
		return 0
	}
}

func max(i1 int64, i2 int64) int64 { return []int64{i1, i2}[btoi(i1 < i2)] }
func min(i1 int64, i2 int64) int64 { return []int64{i2, i1}[btoi(i1 < i2)] }
func abs(i1 int64) int64           { return max(i1, -i1) }

func makeii(n int64, m int64) [][]int64 {
	a := make([][]int64, n)
	for i := _0; i < n; i++ {
		a[i] = make([]int64, m)
	}
	return a
}

var sc *bufio.Scanner
var wr *bufio.Writer
var er *bufio.Writer
func cini() int64 {
	sc.Scan();
	ans, err := strconv.ParseInt(sc.Text(), 10, 64);
	if err!=nil {
		panic("cini failed")
	}
	return ans
}
func cinai(n int64) []int64 {
	ans:=make([]int64, n)
	for i:=range ans {
		ans[i]=cini()
	}
	return ans
}

/*
Cause of rule d
in position i we can use one of 11 numbers, that is i-5..i+5
Lets denote that with a bitset<11>
In next position we must use one of the unset bits.
but that can go TLE, since O(2^11 * 500)

...Thats hard to implement :/
*/
const MOD=998244353
func solve() {
	n:=cini()
	d:=cini()

	a:=cinai(n)

	N:=1<<((d*2)+1)
	M:=^N

	dp:=make([]int, N)
	dp[((1<<(d+1))-1)<<(d)]=1
	Fprintln(os.Stderr, ((1<<(d+1))-1)<<d)
	for i:=_0; i<n; i++ {
		a[i]--
		dp0:=make([]int, N)

		if a[i]>=0 {
			dd:=i-a[i]
			if abs(dd)>d || a[i]>=n {
				Println(0)
				return;
			}
			b:=(1<<(d+dd))
			for j:=N/2; j<N; j++ {
				j1:=(j<<1)&M
				if((j1&b)==0) {
					dp0[j1|b]+=dp[j]
					dp0[j1|b]%=MOD
				}
			}
		} else {
			for j:=N/2; j<N; j++ {
				j1:=(j<<1)&M
				for k:=_0; k<d*2+1 && i+k-(d+1)<n; k++ {
					if j1&(1<<k)==0 {
						dp0[j1|(1<<k)]+=dp[j]
						dp0[j1|(1<<k)]%=MOD
					}
				}
			}
		}

		Fprintln(os.Stderr, "dp0=", dp0)
		dp=dp0

	}
	Println(dp[((1<<(d+1))-1)<<(d)])
}

func main() {
	sc = bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)
	wr=bufio.NewWriter(os.Stdout)
	er=bufio.NewWriter(os.Stderr)
	defer wr.Flush()
	defer er.Flush()
	//t:=cini()
	//for ; t>0; t-- {
	solve()
	//}
}
