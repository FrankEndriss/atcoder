package main

import (
	"bufio"
	. "fmt"
	"os"
	//"container/heap"
)

var _ = Scan // reference fmt

type ll int64

const _0 ll = 0
const _1 ll = 1
const INF int = 1e9 + 7

func btoi(b bool) ll {
	if b {
		return 1
	} else {
		return 0
	}
}

func max(i1 ll, i2 ll) ll { return [2]ll{i1, i2}[btoi(i1 < i2)] }
func min(i1 ll, i2 ll) ll { return [2]ll{i2, i1}[btoi(i1 < i2)] }
func abs(i1 ll) ll        { return max(i1, -i1) }

var in = bufio.NewReader(os.Stdin)
var out = bufio.NewWriter(os.Stdout)
var err = bufio.NewWriter(os.Stderr)

// read a WS delimited word, works also on atcoder with long strings
func cinb() []byte {
	s := make([]byte, 0)
	for {
		b, err := in.ReadByte()
		if err != nil {
			return s
		}
		if b == 10 || b == 13 || b == '\t' || b == ' ' {
			continue
		} else {
			s = append(s, b)
			break
		}
	}
	for {
		b, err := in.ReadByte()
		if err != nil || b == 10 || b == 13 || b == '\t' || b == ' ' {
			break
		}
		s = append(s, b)
	}

	return s
}

func cins() string {
	return string(cinb())
}

func bgeq(b1, b2 []byte) bool {
	if len(b1) != len(b2) {
		panic(":/")
	}
	for i := range b1 {
		if b1[i] > b2[i] {
			return true
		} else if b1[i] < b2[i] {
			return false
		}
	}
	return true
}

// consider 33333 be a anum
// But consider x=54993
// ...
// Allways use same first digit, if possible same second digit...
// if not then first digit one less, and second digit biggest posible.
// Casework :/
// ***
// Other problem, we need to find the smallest one _bigger_ than x
// So do basically the same, it is allways possible to use same
// len string as 999999
func solve() {
	s := cinb()
	if len(s) <= 2 {
		Println(string(s))
		return
	}

	ans := make([]byte, len(s))

	for off := 0; off < 9; off++ {
		ans[0] = s[0] + byte(off)
		if ans[0] > '9' {
			break
		}
		for dd := -8; dd < 9; dd++ {
			ok := true
			for i := 1; ok && i < len(s); i++ {
				if ans[i-1]+byte(dd) < '0' || ans[i-1]+byte(dd) > '9' {
					ok = false
				} else {
					ans[i] = ans[i-1] + byte(dd)
				}
			}

			if ok && bgeq(ans, s) {
				Println(string(ans))
				return
			}
		}
	}
	Println(99)
}

func main() {
	defer out.Flush()
	defer err.Flush()
	//for t:=cini(); t>0; t-- {
	solve()
	//}
}
