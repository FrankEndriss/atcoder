package main

import (
	"bufio"
	. "fmt"
	"os"
	//"container/heap"
)

var _ = Scan // reference fmt

type ll int64

const _0 ll = 0
const _1 ll = 1
const INF int = 1e9+7

func btoi(b bool) ll {
	if b {
		return 1
	} else {
		return 0
	}
}

func max(i1 ll, i2 ll) ll { return [2]ll{i1, i2}[btoi(i1 < i2)] }
func min(i1 ll, i2 ll) ll { return [2]ll{i2, i1}[btoi(i1 < i2)] }
func abs(i1 ll) ll        { return max(i1, -i1) }

var in = bufio.NewReader(os.Stdin)
var out = bufio.NewWriter(os.Stdout)
var err = bufio.NewWriter(os.Stderr)

// read a WS delimited word, works also on atcoder with long strings
func cinb() []byte {
	s := make([]byte, 0)
	for {
		b, err := in.ReadByte()
		if err != nil {
			return s
		}
		if b == 10 || b == 13 || b == '\t' || b == ' ' {
			continue
		} else {
			s = append(s, b)
			break
		}
	}
	for {
		b, err := in.ReadByte()
		if err != nil || b == 10 || b == 13 || b == '\t' || b == ' ' {
			break
		}
		s = append(s, b)
	}

	return s
}

func cins() string {
	return string(cinb())
}

func solve() {
}

func main() {
	defer out.Flush()
	defer err.Flush()
	//for t:=cini(); t>0; t-- {
	solve()
	//}
}
