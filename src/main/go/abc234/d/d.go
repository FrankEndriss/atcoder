package main

import (
	"bufio"
	"container/heap"
	. "fmt"
	"os"
	//"container/heap"
)

var _ = Scan // reference fmt

type ll int64

const _0 ll = 0
const _1 ll = 1
const INF int = 1e9 + 7

func btoi(b bool) ll {
	if b {
		return 1
	} else {
		return 0
	}
}

func max(i1 ll, i2 ll) ll { return [2]ll{i1, i2}[btoi(i1 < i2)] }
func min(i1 ll, i2 ll) ll { return [2]ll{i2, i1}[btoi(i1 < i2)] }
func abs(i1 ll) ll        { return max(i1, -i1) }

var in = bufio.NewReader(os.Stdin)
var out = bufio.NewWriter(os.Stdout)
var err = bufio.NewWriter(os.Stderr)

// read a WS delimited word, works also on atcoder with long strings
func cinb() []byte {
	s := make([]byte, 0)
	for {
		b, err := in.ReadByte()
		if err != nil {
			return s
		}
		if b == 10 || b == 13 || b == '\t' || b == ' ' {
			continue
		} else {
			s = append(s, b)
			break
		}
	}
	for {
		b, err := in.ReadByte()
		if err != nil || b == 10 || b == 13 || b == '\t' || b == ' ' {
			break
		}
		s = append(s, b)
	}

	return s
}

func cins() string {
	return string(cinb())
}

// PriorityQueue<int>, see https://pkg.go.dev/container/heap
// The constructor supports a configuration by a Less function.
// We are supposed to store the actual data in some slice, and put indexes
// of that slice into the PriorityQueue
// use like:
// heap.Push(&q, 42)
// val:=heap.Pop(&q)
// Pop returns the smallest value first according to Less.
type PriorityQueueInt struct {
	data []int64
	less func(i, j int64) bool
}

func NewPriorityQueueInt(less func(i, j int64) bool) PriorityQueueInt {
	return PriorityQueueInt{
		make([]int64, 0),
		less}
}

func (q PriorityQueueInt) Len() int           { return len(q.data) }
func (q PriorityQueueInt) Less(i, j int) bool { return q.less(q.data[i], q.data[j]) }
func (q *PriorityQueueInt) Swap(i, j int)     { q.data[i], q.data[j] = q.data[j], q.data[i] }

func (q *PriorityQueueInt) Push(i interface{}) { q.data = append(q.data, i.(int64)) }
func (q *PriorityQueueInt) Pop() interface{} {
	ans := q.data[len(q.data)-1]
	q.data = q.data[0 : len(q.data)-1]
	return ans
}
func solve() {
	var n, k int
	Fscan(in, &n, &k)
	a := make([]int, n)
	for i := range a {
		Fscan(in, &a[i])
	}

	q := NewPriorityQueueInt(func(i, j int64) bool { return i < j })
	heap.Init(&q)
	for i := 0; i < k; i++ {
		heap.Push(&q, int64(a[i]))
	}
	for i := k; i < n; i++ {
		val := heap.Pop(&q).(int64)
		heap.Push(&q, val)
		heap.Push(&q, int64(a[i]))
		heap.Pop(&q)
		Fprintln(out, val)
	}
	val := heap.Pop(&q).(int64)
	Fprintln(out, val)
}

func main() {
	defer out.Flush()
	defer err.Flush()
	//for t:=cini(); t>0; t-- {
	solve()
	//}
}
