package main

import (
	"bufio"
	. "fmt"
	"os"
	//"container/heap"
)

var _ = Scan // reference fmt

type ll int64

const _0 ll = 0
const _1 ll = 1
const INF int = 1e9 + 7

func btoi(b bool) ll {
	if b {
		return 1
	} else {
		return 0
	}
}

func max(i1 ll, i2 ll) ll { return [2]ll{i1, i2}[btoi(i1 < i2)] }
func min(i1 ll, i2 ll) ll { return [2]ll{i2, i1}[btoi(i1 < i2)] }
func abs(i1 ll) ll        { return max(i1, -i1) }

var in = bufio.NewReader(os.Stdin)
var out = bufio.NewWriter(os.Stdout)
var err = bufio.NewWriter(os.Stderr)

// read a WS delimited word, works also on atcoder with long strings
func cinb() []byte {
	s := make([]byte, 0)
	for {
		b, err := in.ReadByte()
		if err != nil {
			return s
		}
		if b == 10 || b == 13 || b == '\t' || b == ' ' {
			continue
		} else {
			s = append(s, b)
			break
		}
	}
	for {
		b, err := in.ReadByte()
		if err != nil || b == 10 || b == 13 || b == '\t' || b == ' ' {
			break
		}
		s = append(s, b)
	}

	return s
}

func cins() string {
	return string(cinb())
}

/*
Lets call a multisubset the set of symbols in a subarray.
Find all subsets
Foreach subset find number of permutations.

How to find the subsets?
Surely subsets of different size are different.
let ss[x] be the subsets of size x
let n=number of distinct symbols
ss[1]=number of distinct symbols
let f[s]=freq of symbol s
ff[i]=number of symbols with freq[s]==i
ss[2]=ff[1]*(n-1)*(n-ff[1])
ss[3]=...
This is somehow stars'n'bars or something.

What about simle dp?
dp[i][j]=number of strings buildable by substring[i,j]
...
Consider adding a char to existing sets.
Maintain
dp[i][j][sym]=number of of subsets of size i with j times sym in it
...but that does not help, since we need to consider the freq of
all symbols in the sets.
****
The order of the symbols does not matter. So input is actually [26]int of freqs.
How to calc number of possible subsets from that?
And then foreach set number of perms?
*/
func solve() {
}

func main() {
	defer out.Flush()
	defer err.Flush()
	//for t:=cini(); t>0; t-- {
	solve()
	//}
}
